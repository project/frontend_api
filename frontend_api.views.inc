<?php

/**
 * @file
 * Contains Views-related hooks.
 */

/**
 * Adds base tables to the "base" list on the Views plugin definition.
 *
 * This adds specified base tables to the list of tables where the plugin is
 * only available.
 *
 * @param array $plugins
 *   The list of plugin definitions.
 * @param string $plugin_id
 *   The plugin ID to update.
 * @param array $additional_bases
 *   Base table names to add.
 */
function _frontend_api_views_add_plugin_definition_bases(
  array &$plugins,
  string $plugin_id,
  array $additional_bases
): void {
  $plugin_bases = $plugins[$plugin_id]['base'] ?? [];
  $plugin_bases = array_merge($plugin_bases, $additional_bases);
  $plugins[$plugin_id]['base'] = $plugin_bases;
}

/**
 * Implements hook_views_plugins_row_alter().
 */
function frontend_api_views_plugins_row_alter(array &$plugins) {
  /** @var \Drupal\frontend_api\Views\BaseTableEntityMapperInterface $search_api_mapper */
  $search_api_mapper = \Drupal::service(
    'frontend_api.views.search_api.base_table_entity_mapper'
  );
  $search_api_bases = $search_api_mapper->getBaseTables();

  // Limit Search API data entity plugin to the Search API.
  _frontend_api_views_add_plugin_definition_bases(
    $plugins,
    'frontend_api_search_api_data_entity',
    $search_api_bases
  );

  // @TODO We have to wipe these meta-info on adding new Search API indexes, as
  //   it causes the plugin to not appear in the list one new indexes.
}

/**
 * Implements hook_views_plugins_style_alter().
 */
function frontend_api_views_plugins_style_alter(array &$plugins) {
  /** @var \Drupal\frontend_api\Views\BaseTableEntityMapperInterface $db_mapper */
  $db_mapper = \Drupal::service(
    'frontend_api.views.database.base_table_entity_mapper'
  );
  $db_bases = $db_mapper->getBaseTables();
  _frontend_api_views_add_plugin_definition_bases(
    $plugins,
    'ViewsExposedHandlerNormalizerserializer',
    $db_bases
  );

  /** @var \Drupal\frontend_api\Views\BaseTableEntityMapperInterface $search_api_mapper */
  $search_api_mapper = \Drupal::service(
    'frontend_api.views.search_api.base_table_entity_mapper'
  );
  $search_api_bases = $search_api_mapper->getBaseTables();

  // Limit DB and Search API serializer styles to corresponding base tables.
  _frontend_api_views_add_plugin_definition_bases(
    $plugins,
    'frontend_api_search_api_front_serializer',
    $search_api_bases
  );

  // @TODO We have to wipe these meta-info on adding new Search API indexes, as
  //   it causes the plugin to not appear in the list one new indexes.
}

/**
 * Implements hook_views_data_alter().
 */
function frontend_api_views_data_alter(array &$data) {
  /** @var \Drupal\frontend_api\Views\BaseTableEntityMapperInterface $search_api_mapper */
  $search_api_mapper = \Drupal::service(
    'frontend_api.views.search_api.base_table_entity_mapper'
  );
  $map = $search_api_mapper->getMap();
  foreach ($map as $table_name => $entity_type_id) {
    if (!isset($data[$table_name])) {
      continue;
    }

    // Add entity type information to the table definition, so that we don't
    // have to extract it at runtime.
    $data[$table_name]['table']['frontend api entity type'] = $entity_type_id;
  }
}
