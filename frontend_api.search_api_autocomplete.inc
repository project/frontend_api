<?php

/**
 * @file
 * Contains Search API hooks.
 */

use Drupal\frontend_api\Rest\SearchApi\Autocomplete\ViewsSearch;

/**
 * Implements hook_search_api_autocomplete_search_info_alter().
 */
function frontend_api_search_api_autocomplete_search_info_alter(
  array &$searches
): void {
  // Replace views searches with a customized class.
  foreach ($searches as &$search) {
    $provider = $search['provider'] ?? NULL;
    if ($provider !== 'views') {
      continue;
    }

    $search['class'] = ViewsSearch::class;
  }
}
