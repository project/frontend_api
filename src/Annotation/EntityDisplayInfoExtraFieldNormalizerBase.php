<?php

namespace Drupal\frontend_api\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Provides base annotation for entity display extra field normalizer plugins.
 *
 * phpcs:disable Drupal.NamingConventions.ValidVariableName
 *
 * @TODO NOW Shorter name.
 */
abstract class EntityDisplayInfoExtraFieldNormalizerBase extends Plugin {

  /**
   * The extra field ids that are supported by this normalizer.
   *
   * Extra Field plugins get "extra_field_" prefix to their
   * plugin ID and should be specified with that prefix here.
   *
   * @var string[]
   */
  public $extra_field_ids = [];

}
