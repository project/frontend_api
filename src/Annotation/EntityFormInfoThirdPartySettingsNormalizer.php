<?php

namespace Drupal\frontend_api\Annotation;

/**
 * Provides an annotation for the entity form extra field normalizer plugins.
 *
 * @Annotation
 *
 * @TODO NOW Shorter name.
 */
class EntityFormInfoThirdPartySettingsNormalizer extends EntityDisplayInfoThirdPartySettingsNormalizerBase {

}
