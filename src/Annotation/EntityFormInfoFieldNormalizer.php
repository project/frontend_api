<?php

namespace Drupal\frontend_api\Annotation;

/**
 * Provides an annotation for the entity form field normalizer plugins.
 *
 * phpcs:disable Drupal.NamingConventions.ValidVariableName
 *
 * @Annotation
 *
 * @TODO NOW Shorter name.
 */
class EntityFormInfoFieldNormalizer extends EntityDisplayInfoFieldNormalizerBase {

  /**
   * The widget types supported by this normalizer.
   *
   * An asterisk "*" means all types are supported.
   *
   * @var string[]
   *
   * @TODO NOW Wipe support for * here and in view.
   */
  public $widget_types = [];

}
