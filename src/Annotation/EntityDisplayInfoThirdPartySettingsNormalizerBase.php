<?php

namespace Drupal\frontend_api\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Provides base annotation for third-party settings normalizer plugins.
 *
 * phpcs:disable Drupal.NamingConventions.ValidVariableName
 *
 * @TODO NOW Shorter name.
 */
abstract class EntityDisplayInfoThirdPartySettingsNormalizerBase extends Plugin {

  /**
   * The setting module name.
   *
   * For display info, settings stored in 'third_party_settings' property by
   * module name. In order to distinguish it, we need to specify module name in
   * annotations.
   *
   * @var string
   */
  public $setting_module;

}
