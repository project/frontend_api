<?php

namespace Drupal\frontend_api\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Provides plugin annotation to the field constraint normalizers.
 *
 * phpcs:disable Drupal.NamingConventions.ValidVariableName
 *
 * @Annotation
 *
 * @TODO NOW Shorter name.
 */
class FieldConstraintNormalizer extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The list of supported field constraint plugin IDs.
   *
   * @var string[]
   */
  public $field_constraints = [];

  /**
   * The list of supported field types.
   *
   * @var string[]
   */
  public $field_types = [];

  /**
   * The plugin priority among others supporting same constraint + field type.
   *
   * @var int
   */
  public $priority = 0;

}
