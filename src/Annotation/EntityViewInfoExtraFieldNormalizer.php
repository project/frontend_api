<?php

namespace Drupal\frontend_api\Annotation;

/**
 * Provides an annotation for the entity view extra field normalizer plugins.
 *
 * @Annotation
 *
 * @TODO NOW Shorter name.
 */
class EntityViewInfoExtraFieldNormalizer extends EntityDisplayInfoExtraFieldNormalizerBase {

}
