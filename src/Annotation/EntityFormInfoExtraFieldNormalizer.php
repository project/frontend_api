<?php

namespace Drupal\frontend_api\Annotation;

/**
 * Provides an annotation for the entity form extra field normalizer plugins.
 *
 * phpcs:disable Drupal.NamingConventions.ValidVariableName
 *
 * @Annotation
 *
 * @TODO NOW Shorter name.
 */
class EntityFormInfoExtraFieldNormalizer extends EntityDisplayInfoExtraFieldNormalizerBase {

  /**
   * Fields that are included in extra field plugin.
   *
   * @var string[]
   */
  public $field_names = [];

}
