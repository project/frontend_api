<?php

namespace Drupal\frontend_api\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Provides annotation for the normalizer plugins of the Views exposed handler.
 *
 * phpcs:disable Drupal.NamingConventions.ValidVariableName
 *
 * @Annotation
 *
 * @TODO NOW Shorter name.
 */
class ViewsExposedHandlerNormalizer extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin priority among others with the same handler type & class.
   *
   * @var int
   */
  public $priority = 0;

  /**
   * The exposed form plugin IDs this handler supports.
   *
   * An asterisk "*" means all exposed forms are supported.
   *
   * @var string[]
   */
  public $exposed_forms = [];

  /**
   * The Views handler type the plugin is able to normalize.
   *
   * @var string
   */
  public $handler_type = 'filter';

  /**
   * The plugin IDs of the Views handler the plugin is able to normalize.
   *
   * @var string[]
   */
  public $handlers = [];

}
