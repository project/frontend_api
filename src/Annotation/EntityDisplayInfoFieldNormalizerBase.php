<?php

namespace Drupal\frontend_api\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Provides base annotation for the entity display field normalizer plugins.
 *
 * phpcs:disable Drupal.NamingConventions.ValidVariableName
 *
 * @TODO NOW Shorter name.
 */
abstract class EntityDisplayInfoFieldNormalizerBase extends Plugin {

  /**
   * The plugin priority among others with the same field type supported.
   *
   * @var int
   */
  public $priority = 0;

  /**
   * Field types supported.
   *
   * An asterisk "*" means all types are supported.
   *
   * @var array
   */
  public $field_types = [];

}
