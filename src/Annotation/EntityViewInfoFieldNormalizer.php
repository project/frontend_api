<?php

namespace Drupal\frontend_api\Annotation;

/**
 * Provides an annotation for the entity view field normalizer plugins.
 *
 * phpcs:disable Drupal.NamingConventions.ValidVariableName
 *
 * @Annotation
 *
 * @TODO NOW Shorter name.
 */
class EntityViewInfoFieldNormalizer extends EntityDisplayInfoFieldNormalizerBase {

  /**
   * The formatter types supported by this normalizer.
   *
   * An asterisk "*" means all types are supported.
   *
   * @var string[]
   */
  public $formatter_types = [];

}
