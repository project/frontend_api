<?php

namespace Drupal\frontend_api\Field\Formatter;

/**
 * Settings reader of the multi-style image formatter.
 *
 * @see \Drupal\frontend_api\Plugin\Field\FieldFormatter\FrontMediaMultiStyleFormatterBase
 */
interface MultiStyleImageSettingsReaderInterface {

  /**
   * Returns style names.
   *
   * @param array $formatter_settings
   *   The formatter settings.
   *
   * @return string[]|null[]
   *   The list of image style names. Keys of the array should be used when
   *   exposing images. Value of the item could be NULL, which means original
   *   image should be exposed.
   */
  public function getStyleNames(array $formatter_settings): array;

}
