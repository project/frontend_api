<?php

namespace Drupal\frontend_api\Field\Formatter;

/**
 * Settings reader of the multi-style image formatter.
 *
 * @see \Drupal\frontend_api\Plugin\Field\FieldFormatter\FrontMediaMultiStyleFormatterBase
 */
class MultiStyleImageSettingsReader implements MultiStyleImageSettingsReaderInterface {

  /**
   * The image styles setting key.
   */
  public const STYLES_SETTING = 'styles';

  /**
   * {@inheritdoc}
   */
  public function getStyleNames(array $formatter_settings): array {
    return $formatter_settings[static::STYLES_SETTING] ?? [];
  }

}
