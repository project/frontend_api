<?php

namespace Drupal\frontend_api\Field;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Creates slice of the field item list.
 *
 * @see \Drupal\frontend_api\Field\FieldItemListSlicerInterface
 */
class FieldItemListSlicer implements FieldItemListSlicerInterface {

  /**
   * {@inheritdoc}
   */
  public function sliceItemList(
    FieldItemListInterface $item_list,
    int $offset,
    int $length = NULL
  ): FieldItemListInterface {
    $limited_value = array_slice($item_list->getValue(), $offset, $length);

    $result = clone $item_list;
    $result->setValue($limited_value, FALSE);
    return $result;
  }

}
