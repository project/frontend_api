<?php

namespace Drupal\frontend_api\Field;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Creates slice of the field item list.
 *
 * It's purpose is to safely slice a subset of items from the field item list.
 */
interface FieldItemListSlicerInterface {

  /**
   * Returns clone of the field item list with a subset of items.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $item_list
   *   The original field item list.
   * @param int $offset
   *   The zero-based offset to start from.
   * @param int|null $length
   *   The optional number of items to slice. NULL to get all items to the end.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   Clone of the field item list with a subset of items.
   */
  public function sliceItemList(
    FieldItemListInterface $item_list,
    int $offset,
    int $length = NULL
  ): FieldItemListInterface;

}
