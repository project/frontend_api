<?php

namespace Drupal\frontend_api\Field;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Trait that limits the formatter/widget to a specific host entity type.
 *
 * The host entity type is the one the field is attached to.
 */
trait HostTypeLimitedComponentTrait {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(
    FieldDefinitionInterface $field_definition
  ): bool {
    $host_type_id = $field_definition->getTargetEntityTypeId();

    // This widget/formatter is only available for fields attached to specific
    // entity type.
    return ($host_type_id == static::HOST_ENTITY_TYPE);
  }

}
