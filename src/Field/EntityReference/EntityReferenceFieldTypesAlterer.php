<?php

namespace Drupal\frontend_api\Field\EntityReference;

use Drupal\frontend_api\Field\FieldTypesAltererInterface;

/**
 * Alters entity reference field types.
 */
class EntityReferenceFieldTypesAlterer implements FieldTypesAltererInterface {

  /**
   * The map of field types to the corresponding field item list class.
   *
   * The field item list specified as a value replaces the default one set in
   * the field type annotation.
   */
  public const LIST_CLASS_MAP = [
    'entity_reference' => EntityReferenceFieldItemList::class,
  ];

  /**
   * The list class key in the field type definition.
   */
  public const LIST_CLASS_KEY = 'list_class';

  /**
   * {@inheritdoc}
   */
  public function alterFieldInfo(array &$field_types): void {
    foreach (static::LIST_CLASS_MAP as $field_type => $replacement_class) {
      if (!isset($field_types[$field_type])) {
        continue;
      }

      $field_types[$field_type][static::LIST_CLASS_KEY] = $replacement_class;
    }
  }

}
