<?php

namespace Drupal\frontend_api\Field\EntityReference;

use Drupal\Core\Field\EntityReferenceFieldItemList as CoreFieldItemList;

/**
 * Customized class for the item list of the entity reference field.
 */
class EntityReferenceFieldItemList extends CoreFieldItemList implements InlineEditingAwareFieldItemListInterface {

  use InlineEditingAwareFieldItemListTrait;

}
