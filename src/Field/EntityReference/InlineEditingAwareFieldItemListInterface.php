<?php

namespace Drupal\frontend_api\Field\EntityReference;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;

/**
 * Interface of a field item list that support inline editing of entities.
 *
 * The field item list stores the list of entities that were updated/deleted
 * through inline editing and must be saved/deleted inside the host entity
 * saving transaction.
 *
 * @see \Drupal\frontend_api\Field\EntityReference\InlineEditingAwareFieldItemListTrait
 * @see \Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer\FrontInlineEntityNormalizerBase
 */
interface InlineEditingAwareFieldItemListInterface extends EntityReferenceFieldItemListInterface {

  /**
   * Checks if the field has updated entities queued for saving.
   *
   * @return bool
   *   TRUE if there were updated entities while using inline entity form.
   */
  public function hasInlineUpdatedEntities(): bool;

  /**
   * Queues the updated entity for saving during host entity saving.
   *
   * You still have to add it to the field as usual. New entities are not added,
   * as they get saved by the base field anyway.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to queue for saving.
   */
  public function addInlineUpdatedEntity(
    ContentEntityInterface $entity
  ): void;

  /**
   * Returns the list of updated entities that are queued for saving.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   The list of updated entities.
   */
  public function getInlineUpdatedEntities(): array;

  /**
   * Clears the queue of the entities queued for saving.
   */
  public function resetInlineUpdatedEntities(): void;

  /**
   * Checks if the field has entities queued for deletion.
   *
   * @return bool
   *   TRUE if there were deleted entities while using inline entity form.
   */
  public function hasInlineDeletedEntities(): bool;

  /**
   * Queues the entity for deletion during host entity saving.
   *
   * You still have to remove it from the field as usual. New entities are not
   * added, as they can't be deleted.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to queue for deletion.
   */
  public function addInlineDeletedEntity(
    ContentEntityInterface $entity
  ): void;

  /**
   * Returns the list of entities that are queued for deletion.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   The list of entities.
   */
  public function getInlineDeletedEntities(): array;

  /**
   * Clears the queue of the entities queued for deletion.
   */
  public function resetInlineDeletedEntities(): void;

}
