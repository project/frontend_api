<?php

namespace Drupal\frontend_api\Field\EntityReference;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides trait for field item list that support inline editing of entities.
 *
 * @see \Drupal\frontend_api\Field\EntityReference\InlineEditingAwareFieldItemListInterface
 */
trait InlineEditingAwareFieldItemListTrait {

  /**
   * The list of updated entities that were edited inline.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface[]
   */
  protected $inlineUpdatedEntities = [];

  /**
   * The list of deleted entities that were edited inline.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface[]
   */
  protected $inlineDeletedEntities = [];

  /**
   * {@inheritdoc}
   */
  public function hasInlineUpdatedEntities(): bool {
    return !empty($this->inlineUpdatedEntities);
  }

  /**
   * {@inheritdoc}
   */
  public function getInlineUpdatedEntities(): array {
    return $this->inlineUpdatedEntities;
  }

  /**
   * {@inheritdoc}
   */
  public function resetInlineUpdatedEntities(): void {
    $this->inlineUpdatedEntities = [];
  }

  /**
   * {@inheritdoc}
   */
  public function addInlineUpdatedEntity(
    ContentEntityInterface $entity
  ): void {
    if ($entity->isNew()) {
      return;
    }

    $entity_id = $entity->id();
    $this->inlineUpdatedEntities[$entity_id] = $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function hasInlineDeletedEntities(): bool {
    return !empty($this->inlineDeletedEntities);
  }

  /**
   * {@inheritdoc}
   */
  public function getInlineDeletedEntities(): array {
    return $this->inlineDeletedEntities;
  }

  /**
   * {@inheritdoc}
   */
  public function resetInlineDeletedEntities(): void {
    $this->inlineDeletedEntities = [];
  }

  /**
   * {@inheritdoc}
   */
  public function addInlineDeletedEntity(
    ContentEntityInterface $entity
  ): void {
    if ($entity->isNew()) {
      return;
    }

    $entity_id = $entity->id();
    $this->inlineDeletedEntities[$entity_id] = $entity;
  }

  /**
   * Updates queued inline entities.
   */
  protected function updateInlineEntities(): void {
    if (!$this->hasInlineUpdatedEntities()) {
      return;
    }

    $entities = $this->getInlineUpdatedEntities();
    foreach ($entities as $entity) {
      $entity->save();
    }

    // Don't let them be saved twice.
    $this->resetInlineUpdatedEntities();
  }

  /**
   * Deleted queued inline entities.
   */
  protected function deleteInlineEntities(): void {
    if (!$this->hasInlineDeletedEntities()) {
      return;
    }

    $entities = $this->getInlineDeletedEntities();
    foreach ($entities as $entity) {
      $entity->delete();
    }

    // Don't let them be deleted twice.
    $this->resetInlineDeletedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();

    $this->updateInlineEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    $result = parent::postSave($update);

    if ($update) {
      $this->deleteInlineEntities();
    }

    return $result;
  }

}
