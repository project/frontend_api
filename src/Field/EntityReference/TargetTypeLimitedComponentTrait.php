<?php

namespace Drupal\frontend_api\Field\EntityReference;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Trait for the widget/formatter that is only suitable for given entity type.
 */
trait TargetTypeLimitedComponentTrait {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(
    FieldDefinitionInterface $field_definition
  ): bool {
    $target_entity_type = $field_definition->getFieldStorageDefinition()
      ->getSetting('target_type');

    // This widget/formatter is only available for fields that reference
    // given entity.
    return ($target_entity_type == static::TARGET_ENTITY_TYPE);
  }

}
