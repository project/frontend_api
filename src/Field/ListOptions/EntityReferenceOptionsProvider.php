<?php

namespace Drupal\frontend_api\Field\ListOptions;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\frontend_api\Dictionary\CacheContexts;
use Drupal\handy_cache_tags\HandyCacheTagsManager;
use Drupal\frontend_api\Utility\SampleEntityProviderInterface;
use Drupal\frontend_api\Field\EntityReference\EntityReferenceSettingsReaderInterface;

/**
 * Provides options for the entity reference fields.
 *
 * @see \Drupal\frontend_api\Field\ListOptions\OptionsProviderInterface
 *
 * @todo This should probably become a plugin system matching plugins by
 *   entity reference selection handler and some other criteria. But for now a
 *   single class should work fine.
 *
 * @TODO NOW Make dependency on the Handy Cache Tags optional.
 */
class EntityReferenceOptionsProvider implements OptionsProviderInterface {

  /**
   * The language cache context prefix.
   */
  protected const LANGUAGE_CONTEXT_PREFIX = CacheContexts::LANGUAGE_PREFIX;

  /**
   * The content language type.
   */
  protected const LANGUAGE_TYPE_CONTENT = LanguageInterface::TYPE_CONTENT;

  /**
   * The handy cache tags manager.
   *
   * @var \Drupal\handy_cache_tags\HandyCacheTagsManager
   */
  protected $handyTagsManager;

  /**
   * The entity reference settings reader.
   *
   * @var \Drupal\frontend_api\Field\EntityReference\EntityReferenceSettingsReaderInterface
   */
  protected $settingsReader;

  /**
   * The entity reference selection manager.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected $selectionManager;

  /**
   * The sample entity provider.
   *
   * @var \Drupal\frontend_api\Utility\SampleEntityProviderInterface
   */
  protected $sampleEntityProvider;

  /**
   * A constructor.
   *
   * @param \Drupal\handy_cache_tags\HandyCacheTagsManager $handy_tags_manager
   *   The handy cache tags manager.
   * @param \Drupal\frontend_api\Field\EntityReference\EntityReferenceSettingsReaderInterface $settings_reader
   *   The entity reference settings reader.
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selection_manager
   *   The entity reference selection manager.
   * @param \Drupal\frontend_api\Utility\SampleEntityProviderInterface $sample_entity_provider
   *   The sample entity provider.
   */
  public function __construct(
    HandyCacheTagsManager $handy_tags_manager,
    EntityReferenceSettingsReaderInterface $settings_reader,
    SelectionPluginManagerInterface $selection_manager,
    SampleEntityProviderInterface $sample_entity_provider
  ) {
    $this->handyTagsManager = $handy_tags_manager;
    $this->settingsReader = $settings_reader;
    $this->selectionManager = $selection_manager;
    $this->sampleEntityProvider = $sample_entity_provider;
  }

  /**
   * Adds cacheable metadata for the field options.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $definition
   *   The field definition.
   * @param \Drupal\Core\Cache\CacheableMetadata $metadata
   *   The cacheable metadata to add to.
   */
  protected function addOptionsCacheMetadata(
    FieldDefinitionInterface $definition,
    CacheableMetadata $metadata
  ): void {
    // Cache entry must vary by content language, as we show translated labels.
    // Translation happens in a selection handler.
    $metadata->addCacheContexts([
      static::LANGUAGE_CONTEXT_PREFIX . static::LANGUAGE_TYPE_CONTENT,
    ]);

    $entity_type_id = $this->settingsReader->getTargetEntityTypeId($definition);
    $bundle_ids = $this->settingsReader->getTargetBundleIds($definition);
    if (!empty($bundle_ids)) {
      // Use more specific bundle tags if we were able to find them.
      $bundle_tags = [];
      foreach ($bundle_ids as $bundle_id) {
        $bundle_tags[] = $this->handyTagsManager
          ->getBundleTag($entity_type_id, $bundle_id);
      }
      $metadata->addCacheTags($bundle_tags);
    }
    else {
      // Fallback to the entity type tag.
      $metadata->addCacheTags([
        $this->handyTagsManager->getTag($entity_type_id),
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions(
    FieldDefinitionInterface $definition,
    FieldableEntityInterface $entity = NULL,
    CacheableMetadata $metadata = NULL
  ): array {
    $this->addOptionsCacheMetadata($definition, $metadata);
    return $this->loadOptions($definition, $entity);
  }

  /**
   * Returns either specified or sample entity for the field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $definition
   *   The field definition.
   * @param \Drupal\Core\Entity\FieldableEntityInterface|null $entity
   *   The specified entity, if any.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface
   *   The specified or sample entity.
   */
  protected function getEntity(
    FieldDefinitionInterface $definition,
    FieldableEntityInterface $entity = NULL
  ): FieldableEntityInterface {
    if (isset($entity)) {
      return $entity;
    }

    $entity_type_id = $definition->getTargetEntityTypeId();
    $bundle_id = $definition->getTargetBundle();

    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    $entity = $this->sampleEntityProvider
      ->getEntity($entity_type_id, $bundle_id);
    return $entity;
  }

  /**
   * Returns entity reference selection handler.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $definition
   *   The field definition.
   * @param \Drupal\Core\Entity\FieldableEntityInterface|null $entity
   *   The entity, if any.
   *
   * @return \Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface
   *   The selection handler.
   */
  protected function getSelectionHandler(
    FieldDefinitionInterface $definition,
    FieldableEntityInterface $entity = NULL
  ): SelectionInterface {
    $entity = $this->getEntity($definition, $entity);
    return $this->selectionManager
      ->getSelectionHandler($definition, $entity);
  }

  /**
   * Flattens options provided by selection handler.
   *
   * @param array $entity_options
   *   The options provided by selection handler.
   *
   * @return array
   *   The flattened and decoded list of options.
   */
  protected function flattenOptions(array $entity_options): array {
    $options = [];

    foreach ($entity_options as $bundle_options) {
      foreach ($bundle_options as $key => $label) {
        // Decode HTML entities, as we need raw data.
        $options[$key] = Html::decodeEntities($label);
      }
    }

    return $options;
  }

  /**
   * Loads the list of options using entity reference selection handler.
   *
   * All the cacheable metadata should be handled outside of this method.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $definition
   *   The field definition.
   * @param \Drupal\Core\Entity\FieldableEntityInterface|null $entity
   *   The entity, if any.
   *
   * @return array
   *   The list of options.
   */
  protected function loadOptions(
    FieldDefinitionInterface $definition,
    FieldableEntityInterface $entity = NULL
  ): array {
    $handler = $this->getSelectionHandler($definition, $entity);
    $grouped_options = $handler->getReferenceableEntities();

    return $this->flattenOptions($grouped_options);
  }

}
