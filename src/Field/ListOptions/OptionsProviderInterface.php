<?php

namespace Drupal\frontend_api\Field\ListOptions;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Provides options to the list fields.
 *
 * It is different from the core options provider as it tracks and exposes the
 * cacheable metadata from the options list, so it's not lost and we're able to
 * add it to the form info requests, for example.
 */
interface OptionsProviderInterface {

  /**
   * Returns options.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $definition
   *   The field definition.
   * @param \Drupal\Core\Entity\FieldableEntityInterface|null $entity
   *   The entity the field is attached to. If omitted, sample entity is used
   *   when necessary.
   * @param \Drupal\Core\Cache\CacheableMetadata|null $metadata
   *   The cacheable metadata to populate. It should never contain anything
   *   initially, as it may mess up the internal caching.
   *
   * @return array
   *   The list of available options in arbitrary format.
   */
  public function getOptions(
    FieldDefinitionInterface $definition,
    FieldableEntityInterface $entity = NULL,
    CacheableMetadata $metadata = NULL
  ): array;

}
