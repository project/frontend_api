<?php

namespace Drupal\frontend_api\Field\ListOptions;

/**
 * An interface of a class that is aware of the custom options provider.
 *
 * It is usually a field item that is also used as a system options provider,
 * but has to expose that it actually uses custom options provider.
 */
interface OptionsProviderAwareInterface {

  /**
   * Returns the custom options provider that actually provides the options.
   *
   * @return \Drupal\frontend_api\Field\ListOptions\OptionsProviderInterface
   *   The custom options provider.
   */
  public function getOptionsProvider(): OptionsProviderInterface;

}
