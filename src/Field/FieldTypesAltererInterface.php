<?php

namespace Drupal\frontend_api\Field;

/**
 * Provides interface for a field type alterer.
 */
interface FieldTypesAltererInterface {

  /**
   * Alters the information about all the registered field types.
   *
   * @param array $field_types
   *   The list of field type definitions keyed by the field type to alter.
   *
   * @see hook_field_info_alter()
   */
  public function alterFieldInfo(array &$field_types): void;

}
