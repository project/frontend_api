<?php

namespace Drupal\frontend_api\Plugin\views\row;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\search_api\LoggerTrait;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\row\RowPluginBase;
use Drupal\views\Plugin\views\ViewsPluginInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\ViewsData;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin which "renders" Search API results as entities.
 *
 * That's a Search API version of the DataEntityRow plugin in core.
 *
 * @ViewsRow(
 *   id = "frontend_api_search_api_data_entity",
 *   title = @Translation("Search API result entity"),
 *   help = @Translation(
 *     "Uses entities of Search API results as row data.",
 *   ),
 *   display_types = {"data"},
 * )
 *
 * @see frontend_api_views_plugins_row_alter()
 * @see \Drupal\rest\Plugin\views\row\DataEntityRow
 */
class SearchApiDataEntityRow extends RowPluginBase {

  use LoggerTrait;

  /**
   * {@inheritdoc}
   */
  protected $usesOptions = FALSE;

  /**
   * The search index.
   *
   * @var \Drupal\search_api\IndexInterface
   */
  protected $index;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The views data.
   *
   * @var \Drupal\views\ViewsData
   */
  protected $viewsData;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    $instance->setViewsData($container->get('views.views_data'));
    $instance->setLogger($container->get('logger.channel.frontend_api'));

    return $instance;
  }

  /**
   * Sets the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The new entity type manager.
   *
   * @return static
   */
  protected function setEntityTypeManager(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;

    return $this;
  }

  /**
   * Sets the views table data.
   *
   * @param \Drupal\views\ViewsData $viewsData
   *   The views data.
   *
   * @return static
   */
  protected function setViewsData(ViewsData $viewsData): ViewsPluginInterface {
    $this->viewsData = $viewsData;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function init(
    ViewExecutable $view,
    DisplayPluginBase $display,
    array &$options = NULL
  ) {
    parent::init($view, $display, $options);

    $base_table = $view->storage->get('base_table');
    $table_data = $this->viewsData->get($base_table);
    $index_id = $table_data['table']['base']['index'] ?? NULL;
    if (!isset($index_id)) {
      throw new \InvalidArgumentException(sprintf(
        'Search API index ID is missing on the %s table of the %s view.',
        $base_table,
        $view->storage->id()
      ));
    }

    $this->index = $this->entityTypeManager
      ->getStorage('search_api_index')
      ->load($index_id);
    if (!$this->index) {
      throw new \InvalidArgumentException(sprintf(
        "Search API index %s can't be loaded by the %s view.",
        $index_id,
        $view->storage->id()
      ));
    }
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\search_api\Plugin\views\row\SearchApiRow::preRender()
   */
  public function preRender($result) {
    // Load all result objects at once, before rendering.
    $items_to_load = [];
    foreach ($result as $i => $row) {
      if (empty($row->_object)) {
        $items_to_load[$i] = $row->search_api_id;
      }
    }

    $items = $this->index->loadItemsMultiple($items_to_load);
    foreach ($items_to_load as $i => $item_id) {
      if (isset($items[$item_id])) {
        $result[$i]->_object = $items[$item_id];
        $result[$i]->_item->setOriginalObject($items[$item_id]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render($row) {
    $datasource_id = $row->search_api_datasource;

    if (!($row->_object instanceof ComplexDataInterface)) {
      $this->getLogger()
        ->warning(
          'Failed to load item %item_id in view %view.',
          [
            '%item_id' => $row->search_api_id,
            '%view' => $this->view->storage->label(),
          ]
        );
      return NULL;
    }

    if (!$this->index->isValidDatasource($datasource_id)) {
      $this->getLogger()
        ->warning(
          'Item of unknown datasource %datasource returned in view %view.',
          [
            '%datasource' => $datasource_id,
            '%view' => $this->view->storage->label(),
          ]
        );
      return NULL;
    }

    // @todo Take care about the entity translation.
    return $row->_object->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function query() {}

}
