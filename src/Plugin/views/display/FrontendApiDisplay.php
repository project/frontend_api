<?php

namespace Drupal\frontend_api\Plugin\views\display;

use Drupal\rest\Plugin\views\display\RestExport;

/**
 * Overrides displaysExposed() to output exposed form data.
 *
 * @ingroup views_display_plugins
 *
 * @ViewsDisplay(
 *   id = "frontend_api",
 *   title = @Translation("Front-end API"),
 *   help = @Translation("Create a front-end API REST export resource."),
 *   uses_route = TRUE,
 *   admin = @Translation("Front-end API"),
 *   returns_response = TRUE,
 * )
 */
class FrontendApiDisplay extends RestExport {

  /**
   * {@inheritdoc}
   */
  public function displaysExposed() {
    return TRUE;
  }

}
