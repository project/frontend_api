<?php

namespace Drupal\frontend_api\Plugin\views\style;

use Drupal\search_api\Plugin\views\query\SearchApiQuery;
use Drupal\frontend_api\Rest\Views\QueryDependentHandlerInterface;
use Drupal\views\ViewsData;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Style plugin that serializes the results for the REST Search API Views.
 *
 * @codingStandardsIgnoreStart
 * @ViewsStyle(
 *   id = "frontend_api_search_api_front_serializer",
 *   title = @Translation("Search API front serializer"),
 *   help = @Translation("Wraps results in rows container and provides paging info, view mode can be applied"),
 *   display_types = { "data" }
 * )
 * @codingStandardsIgnoreEnd
 */
class SearchApiFrontSerializer extends FrontSerializer {

  /**
   * Views handler types to check for dependency on query execution.
   */
  public const QUERY_DEPENDENT_HANDLER_TYPES = [
    'filter',
  ];

  /**
   * The views table data.
   *
   * @var \Drupal\views\ViewsData
   */
  protected $viewsData;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->setViewsData($container->get('views.views_data'));

    return $instance;
  }

  /**
   * Sets the views table data.
   *
   * @param \Drupal\views\ViewsData $views_data
   *   The views data.
   *
   * @return static
   */
  protected function setViewsData(
    ViewsData $views_data
  ): SearchApiFrontSerializer {
    $this->viewsData = $views_data;

    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * @see frontend_api_views_data_alter()
   */
  protected function getEntityTypeId() {
    $view_base_table = $this->view->storage->get('base_table');
    $views_data = $this->viewsData->get($view_base_table);
    $entity_type_id = $views_data['table']['frontend api entity type'] ?? NULL;
    if (!isset($entity_type_id)) {
      throw new \Exception(
        'The style plugin supports indexes of a single entity type only.'
      );
    }

    return $entity_type_id;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    parent::query();

    if (!$this->isQueryEnabled()) {
      $query = $this->view->getQuery();
      if (!$query instanceof SearchApiQuery) {
        throw new \Exception('Only Search API views are supported.');
      }

      // @todo Find a way to abort the SQL query too.
      $query->abort();
    }
  }

  /**
   * Checks if the query should be performed.
   *
   * The query could be avoided if both of the following are TRUE:
   * - The REST parameters don't ask for results.
   * - None of the query-dependent handlers ask to run the query.
   *
   * @return bool
   *   TRUE if the query is required, FALSE if it could be aborted/skipped.
   */
  protected function isQueryEnabled(): bool {
    $is_query_enabled = $this->getRestParameters()
      ->isQueryEnabled();
    if ($is_query_enabled) {
      return TRUE;
    }

    $handler_types = static::QUERY_DEPENDENT_HANDLER_TYPES;
    foreach ($handler_types as $handler_type) {
      $handlers = $this->view->{$handler_type};
      foreach ($handlers as $handler) {
        if (!$handler instanceof QueryDependentHandlerInterface) {
          continue;
        }

        if ($handler->needsQuery()) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

}
