<?php

namespace Drupal\frontend_api\Plugin\views\style;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\rest\Plugin\views\style\Serializer;
use Drupal\serialization\Normalizer\CacheableNormalizerInterface;
use Drupal\frontend_api\Rest\NormalizerContextKeys;
use Drupal\frontend_api\Rest\Views\Parameters\FrontSerializerParameters;
use Drupal\frontend_api\Rest\Views\Parameters\FrontSerializerParametersFactoryInterface;
use Drupal\frontend_api\Rest\Views\Parameters\FrontSerializerParametersInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\frontend_api\Rest\Views\ExposedFormWrapper;

/**
 * Style plugin that serializes the results for the REST database Views.
 *
 * @codingStandardsIgnoreStart
 * @ViewsStyle(
 *   id = "ViewsExposedHandlerNormalizerserializer",
 *   title = @Translation("Front-end API serializer"),
 *   help = @Translation("Wraps results in rows container and provides paging info, view mode can be applied"),
 *   display_types = { "data" }
 * )
 * @codingStandardsIgnoreEnd
 */
class FrontSerializer extends Serializer {

  /**
   * The view mode key on the serialization context.
   */
  protected const VIEW_MODE_CONTEXT = NormalizerContextKeys::VIEW_MODE_CONTEXT;

  /**
   * The option key that enables the returned component REST parameter.
   */
  protected const RETURN_PARAMETER = FrontSerializerParameters::RETURN_KEY;

  /**
   * The context key that holds the cacheable metadata.
   */
  protected const CACHEABILITY_CONTEXT_KEY = CacheableNormalizerInterface::SERIALIZATION_CONTEXT_CACHEABILITY;

  /**
   * Entity display repository service.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The REST parameters wrapper.
   *
   * @var \Drupal\frontend_api\Rest\Views\Parameters\FrontSerializerParametersInterface
   */
  protected $restParameters;

  /**
   * The factory of the REST parameter wrappers.
   *
   * @var \Drupal\frontend_api\Rest\Views\Parameters\FrontSerializerParametersFactoryInterface
   */
  protected $restParametersFactory;

  /**
   * FrontSerializer constructor.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    SerializerInterface $serializer,
    array $serializerFormats,
    array $serializerFormatProviders,
    EntityDisplayRepositoryInterface $entityDisplayRepository,
    FrontSerializerParametersFactoryInterface $restParametersFactory
  ) {
    parent::__construct(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $serializer,
      $serializerFormats,
      $serializerFormatProviders
    );

    $this->entityDisplayRepository = $entityDisplayRepository;
    $this->restParametersFactory = $restParametersFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('serializer'),
      $container->getParameter('serializer.formats'),
      $container->getParameter('serializer.format_providers'),
      $container->get('entity_display.repository'),
      $container->get(
        'frontend_api.rest.front_serializer_parameters_factory'
      )
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['view_mode'] = ['default' => NULL];
    $options['rest_parameters'] = ['default' => []];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $formState) {
    parent::buildOptionsForm($form, $formState);

    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#description' => $this->t('View mode to render entity in specific format. If none are selected default view mode will be used.'),
      '#options' => $this->getViewModes(),
      '#default_value' => $this->options['view_mode'],
    ];

    $form['rest_parameters'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('REST parameters accepted'),
      '#description' => $this->t('Parameters allowed to specify by the client.'),
      '#options' => [
        static::RETURN_PARAMETER => $this->t('Returned components (exposed form, rows & pager)'),
      ],
      '#default_value' => $this->options['rest_parameters'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $formState) {
    parent::validateOptionsForm($form, $formState);

    // @todo Is there a built-in way to filter-out unchecked checkboxes? With
    //   some #element_validate would be the best.
    $checkboxOptionPaths = [
      ['style_options', 'rest_parameters'],
    ];
    foreach ($checkboxOptionPaths as $path) {
      $values = $formState->getValue($path);
      $values = array_filter($values);
      $formState->setValue($path, $values);
    }
  }

  /**
   * Renders an array with the view result and the pager.
   *
   * @return array
   *   The result array with rows and pager.
   */
  protected function renderResult(): array {
    $isReturned = $this->getRestParameters()
      ->isResultReturned();
    if (!$isReturned) {
      return [];
    }

    $result = [];

    $result['rows'] = $this->getRows();

    $pager = $this->view->pager;
    if ($pager) {
      $result['pager'] = $pager;
    }

    return $result;
  }

  /**
   * Renders an array with the exposed form.
   *
   * @return array
   *   The result array with the exposed form, if any.
   */
  protected function renderExposedForm(): array {
    $isReturned = $this->getRestParameters()
      ->isExposedFormReturned();
    if (!$isReturned) {
      return [];
    }

    $result = [];

    if ($this->view->display_handler->usesExposed()) {
      $result['exposed_form'] = $this->getExposedForm();
    }

    return $result;
  }

  /**
   * Returns the serialization format.
   *
   * @return string
   *   The format.
   */
  protected function getSerializationFormat(): string {
    // Get the content type configured in the display or fallback to the
    // default.
    if (empty($this->view->live_preview)) {
      return $this->displayHandler->getContentType();
    }
    else {
      return !empty($this->options['formats'])
        ? reset($this->options['formats'])
        : 'json';
    }
  }

  /**
   * Wraps results in rows container and provides pagination info.
   */
  public function render() {
    $result = [];
    $result += $this->renderResult();
    $result += $this->renderExposedForm();

    $contentType = $this->getSerializationFormat();

    $context = [
      'views_style_plugin' => $this,
      static::VIEW_MODE_CONTEXT => $this->options['view_mode'],
      static::CACHEABILITY_CONTEXT_KEY => new CacheableMetadata(),
    ];

    $result = $this->serializer->serialize($result, $contentType, $context);

    $response = $this->view->element['#response'] ?? NULL;
    if ($response instanceof CacheableResponseInterface) {
      $response->addCacheableDependency(
        $context[static::CACHEABILITY_CONTEXT_KEY]
      );
    }

    return $result;
  }

  /**
   * Returns REST parameters wrapper.
   *
   * @return \Drupal\frontend_api\Rest\Views\Parameters\FrontSerializerParametersInterface
   *   The REST parameters.
   */
  protected function getRestParameters(): FrontSerializerParametersInterface {
    if (!isset($this->restParameters)) {
      $this->restParameters = $this->restParametersFactory
        ->create($this->view, $this->options['rest_parameters']);
    }

    return $this->restParameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();

    // Make sure the cache entries vary by all the handled query parameters.
    $parameterNames = $this->getRestParameters()
      ->getHandledQueryParameters();
    foreach ($parameterNames as $parameterName) {
      $contexts[] = 'url.query_args:' . $parameterName;
    }

    return $contexts;
  }

  /**
   * Exposed form data.
   */
  protected function getExposedForm() {
    $exposedForm = $this->view->display_handler->getPlugin('exposed_form');
    return new ExposedFormWrapper($this->view->exposed_widgets, $exposedForm);
  }

  /**
   * Get result rows from view.
   *
   * @return array
   *   Result rows.
   */
  protected function getRows() {
    $rows = [];
    foreach ($this->view->result as $rowIndex => $row) {
      $this->view->row_index = $rowIndex;
      $rows[] = $this->view->rowPlugin->render($row);
    }
    unset($this->view->row_index);

    return $rows;
  }

  /**
   * Returns the entity type ID of the base view table.
   *
   * @return string
   *   The entity type ID.
   */
  protected function getEntityTypeId() {
    $entity_type = $this->view->getBaseEntityType();
    if (!$entity_type) {
      throw new \Exception('The style plugin supports entity tables only.');
    }

    return $entity_type->id();
  }

  /**
   * Gets view modes for current entity.
   *
   * @return array
   *   Array with view modes available.
   */
  protected function getViewModes() {
    $entityTypeId = $this->getEntityTypeId();
    $viewModes = $this->entityDisplayRepository->getViewModeOptions($entityTypeId);

    return $viewModes;
  }

}
