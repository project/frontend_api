<?php

namespace Drupal\frontend_api\Plugin\ExtraField\Display;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\frontend_api\Exception\FrontOnlyComponentException;

/**
 * Provides trait for a extra field that is only usable on the front app.
 *
 * Extra field plugins that use this trait can't be displayed by Drupal.
 */
trait FrontOnlyExtraFieldTrait {

  /**
   * Builds a renderable array for the field.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The field's host entity.
   */
  public function view(ContentEntityInterface $entity) {
    throw new FrontOnlyComponentException();
  }

}
