<?php

namespace Drupal\frontend_api\Plugin\ExtraField\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\frontend_api\Exception\FrontOnlyComponentException;

/**
 * Provides trait for a extra field that is only usable on the front app.
 *
 * Extra field plugins that use this traits can't be displayed on a Drupal form.
 */
trait FrontOnlyExtraFieldTrait {

  /**
   * Builds a renderable array for the field.
   *
   * @param array $form
   *   The entity form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function formView(array $form, FormStateInterface $form_state) {
    throw new FrontOnlyComponentException();
  }

}
