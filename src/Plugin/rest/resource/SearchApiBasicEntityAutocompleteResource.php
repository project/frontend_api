<?php

namespace Drupal\frontend_api\Plugin\rest\resource;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides REST resource with basic entity Search API Autocomplete suggestions.
 *
 * It is necessary to normalize the suggestion list using a different format
 * expected by the entity autocomplete widget.
 *
 * @RestResource(
 *   id = "frontend_api_search_api_entity_basic_autocomplete",
 *   label = @Translation("Search API basic entity autocomplete"),
 *   uri_paths = {
 *     "canonical" = "/frontend-api/search-api-autocomplete/entity/basic/{search_api_autocomplete_search}",
 *   },
 * )
 *
 * @see \Drupal\frontend_api\Rest\SearchApi\Autocomplete\BasicEntitySuggestionList
 */
class SearchApiBasicEntityAutocompleteResource extends SearchApiAutocompleteResourceBase {

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->suggestionListBuilder = $container->get(
      'frontend_api.search_api.autocomplete.basic_entity_suggestion_list_builder'
    );

    return $instance;
  }

}
