<?php

namespace Drupal\frontend_api\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase as CoreResourceBase;
use Drupal\rest\RestResourceConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides base REST resource with code shared across all the resources.
 *
 * @todo Switch all the resources to this base class.
 */
abstract class ResourceBase extends CoreResourceBase {

  /**
   * The GET method.
   */
  protected const METHOD_GET = Request::METHOD_GET;

  /**
   * The POST method.
   */
  protected const METHOD_POST = Request::METHOD_POST;

  /**
   * The PATCH method.
   */
  protected const METHOD_PATCH = Request::METHOD_PATCH;

  /**
   * The DELETE method.
   */
  protected const METHOD_DELETE = Request::METHOD_DELETE;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   *
   * @todo Drop this variable overrides from all the child classes.
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function permissions() {
    // Stop the resource from introducing new permissions.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRoute($canonical_path, $method) {
    $route = parent::getBaseRoute($canonical_path, $method);

    // Add format requirements to the DELETE method. It's here and not in the
    // getBaseRouteRequirements(), because it's not a business rule, but a
    // workaround for the core issue.
    if ($method === static::METHOD_DELETE) {
      $route->addRequirements($this->getDeleteFormatRequirements());
    }

    return $route;
  }

  /**
   * Returns the route requirements for the DELETE route to return responses.
   *
   * The REST module doesn't add the format requirements to the DELETE routes,
   * but it is allowed for DELETE routes to return responses.
   *
   * @return array
   *   The requirements to add to the DELETE route.
   *
   * @see \Drupal\rest\Routing\ResourceRoutes::getRoutesForResourceConfig
   *
   * @link https://www.drupal.org/docs/8/modules/commerce-cart-api/delete-cartorderitemsitem
   */
  protected function getDeleteFormatRequirements(): array {
    $plugin_id = $this->getPluginId();
    $resource_config = $this->entityTypeManager
      ->getStorage('rest_resource_config')
      ->load($plugin_id);
    if (!$resource_config instanceof RestResourceConfigInterface) {
      throw new \LogicException(sprintf(
        'Unable to load the %s REST resource config.',
        $plugin_id
      ));
    }

    $formats = $resource_config->getFormats(static::METHOD_DELETE);
    return [
      '_format' => implode('|', $formats),
    ];
  }

}
