<?php

namespace Drupal\frontend_api\Plugin\rest\resource;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base resource to get information about the entity form.
 */
abstract class EntityFormInfoResourceBase extends EntityDisplayInfoResourceBase {

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('frontend_api'),
      $container->get('frontend_api.rest.entity_form_info_builder'),
      $container->get('entity_type.manager')
    );
  }

}
