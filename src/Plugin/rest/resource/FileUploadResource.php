<?php

namespace Drupal\frontend_api\Plugin\rest\resource;

use Drupal\frontend_api\Rest\ModifiedResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * File upload resource.
 *
 * @RestResource(
 *   id = \Drupal\frontend_api\Plugin\rest\resource\FileUploadResource::PLUGIN_ID,
 *   label = @Translation("File Upload"),
 *   uri_paths = {
 *     "create" = "/frontend-api/file/upload/{entity_type_id}/{bundle}/{field_name}",
 *   }
 * )
 */
class FileUploadResource extends FileUploadResourceBase {

  /**
   * The resource plugin ID.
   */
  public const PLUGIN_ID = 'frontend_api_file_upload';

  /**
   * Permission for creating files.
   */
  public const UPLOAD_FILE_PERMISSION = 'create files through frontend api';

  /**
   * The service that normalizes the file entity for the front app.
   *
   * @var \Drupal\frontend_api\Rest\File\FileNormalizerInterface
   */
  protected $fileNormalizer;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->fileNormalizer = $container->get(
      'frontend_api.rest.file_normalizer'
    );

    return $instance;
  }

  /**
   * Represents file as resources.
   *
   * @param string $entity_type_id
   *   Entity type ID.
   * @param string $bundle
   *   Entity bundle.
   * @param string $field_name
   *   The field name.
   * @param array $body
   *   The unserialized request body.
   *
   * @return \Drupal\frontend_api\Rest\ModifiedResourceResponse
   *   Resource response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @todo Move access checks from the file resource helper.
   */
  public function post(
    $entity_type_id,
    $bundle,
    $field_name,
    array $body
  ) {
    $field_definition = $this->fileResourceHelper
      ->getFieldDefinition($entity_type_id, $bundle, $field_name);
    $destination = $this->fileResourceHelper
      ->getUploadLocation($field_definition->getSettings());

    try {
      $file_name = $this->getFileName($body);
      $file_data = $this->getFileData($body);
    }
    catch (HttpException $e) {
      return new ModifiedResourceResponse(
        ['message' => $e->getMessage()],
        $e->getStatusCode()
      );
    }
    $file_uri = $this->getTargetFileUri($destination, $file_name);
    $file = $this->createFile(
      $file_uri,
      $file_name,
      $file_data
    );

    $file_validators = $this->fileResourceHelper
      ->getFileValidatorsFromField($field_definition);
    $errors = $this->fileResourceHelper
      ->validate($file, $file_validators);
    if (empty($errors)) {
      $file->save();
      $response = new ModifiedResourceResponse(
        $this->fileNormalizer->normalizeFile($file),
        Response::HTTP_CREATED
      );
    }
    else {
      $response = ['message' => $this->t('Validation error')];
      $response += ['errors' => $errors];
      $response = new ModifiedResourceResponse(
        $response,
        Response::HTTP_CONFLICT
      );
    }

    return $response;
  }

}
