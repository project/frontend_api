<?php

namespace Drupal\frontend_api\Plugin\rest\resource;

use Drupal\search_api_autocomplete\SearchInterface;
use Drupal\frontend_api\Rest\ModifiedResourceResponse;
use Drupal\frontend_api\Dictionary\EntityTypes;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides base class for the Search API autocomplete REST resources.
 */
abstract class SearchApiAutocompleteResourceBase extends ResourceBase {

  /**
   * Entity type ID of the Search API Autocomplete search.
   */
  protected const SEARCH_ENTITY_TYPE_ID = EntityTypes::SEARCH_API_AUTOCOMPLETE;

  /**
   * The Search API autocomplete suggestion list builder.
   *
   * @var \Drupal\frontend_api\Rest\SearchApi\Autocomplete\SuggestionListBuilderInterface
   */
  protected $suggestionListBuilder;

  /**
   * {@inheritdoc}
   */
  protected function getBaseRouteRequirements($method) {
    $requirements = parent::getBaseRouteRequirements($method);

    // Use access check of the Search API Autocomplete module.
    $requirements['_search_api_autocomplete'] = 'TRUE';

    return $requirements;
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRoute($canonical_path, $method) {
    $route = parent::getBaseRoute($canonical_path, $method);

    // Make sure the search entity is loaded.
    $parameters = $route->getOption('parameters') ?? [];
    $parameters[static::SEARCH_ENTITY_TYPE_ID] = [
      'type' => 'entity:' . static::SEARCH_ENTITY_TYPE_ID,
    ];
    $route->setOption('parameters', $parameters);

    return $route;
  }

  /**
   * Handles GET method.
   *
   * @param \Drupal\search_api_autocomplete\SearchInterface $search_api_autocomplete_search
   *   The Search API Autocomplete Search entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\frontend_api\Rest\ModifiedResourceResponse
   *   The response.
   */
  public function get(
    SearchInterface $search_api_autocomplete_search,
    Request $request
  ) {
    $list = $this->suggestionListBuilder
      ->buildSuggestionList($search_api_autocomplete_search, $request);

    // @todo Try to return a cacheable response with all the metadata.
    return new ModifiedResourceResponse($list);
  }

}
