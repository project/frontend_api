<?php

namespace Drupal\frontend_api\Plugin\rest\resource;

use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\Site\Settings;
use Drupal\frontend_api\Dictionary\Field\EntityReference\SelectionHandlerSettings;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\frontend_api\Rest\ModifiedResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to get autocomplete suggestions.
 *
 * @RestResource(
 *   id = "frontend_api_entity_autocomplete",
 *   label = @Translation("Front-end API: Entity autocomplete"),
 *   uri_paths = {
 *     "canonical" = "/frontend-api/entity-autocomplete/{selection_key}"
 *   }
 * )
 */
class EntityAutocompleteResource extends ResourceBase {

  /**
   * Default match operator.
   */
  public const MATCH_OPERATOR = 'CONTAINS';

  /**
   * Amount of results displayed.
   */
  public const LIMIT = 10;

  /**
   * The query parameters map setting of the entity reference selection handler.
   */
  protected const HANDLER_QUERY_MAP_SETTING = SelectionHandlerSettings::QUERY_PARAMETERS_MAP;

  /**
   * The query parameters key of the selection handler settings.
   */
  protected const HANDLER_QUERY_PARAMETERS_SETTING = SelectionHandlerSettings::QUERY_PARAMETERS;

  /**
   * The entity reference selection handler plugin manager.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected $selectionManager;

  /**
   * Current request stack.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValue;

  /**
   * Read only settings that are initialized with the class.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin_id for the plugin instance.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   * @param array $serializerFormats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selectionManager
   *   The entity reference selection handler plugin manager.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current request stack.
   * @param \Drupal\Core\KeyValueStore\KeyValueStoreInterface $keyValue
   *   The key value factory.
   * @param \Drupal\Core\Site\Settings $settings
   *   The available serialization formats.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    array $serializerFormats,
    LoggerInterface $logger,
    SelectionPluginManagerInterface $selectionManager,
    Request $request,
    KeyValueStoreInterface $keyValue,
    Settings $settings
  ) {
    parent::__construct(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $serializerFormats,
      $logger
    );

    $this->selectionManager = $selectionManager;
    $this->request = $request;
    $this->keyValue = $keyValue;
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('plugin.manager.entity_reference_selection'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('keyvalue')->get('frontend_api_entity_autocomplete'),
      $container->get('settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function permissions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRouteRequirements($method) {
    parent::getBaseRouteRequirements($method);
    $requirements['_permission'] = 'query entities through frontend api';

    return $requirements;
  }

  /**
   * Returns autocomplete suggestions.
   *
   * @param string $selectionKey
   *   Selection handler.
   *
   * @return \Drupal\frontend_api\Rest\ResourceResponse
   *   Response with results if any.
   */
  public function get(string $selectionKey) {
    $matches = [];

    if ($input = $this->request->query->get('q')) {
      $typedString = Tags::explode($input);
      $typedString = mb_strtolower(array_pop($typedString));

      // Get the array of query options from key value storage.
      $options = $this->keyValue->get($selectionKey, FALSE);
      // Check selection key.
      $this->checkSelectionKey($options, $selectionKey);
      if ($typedString !== '') {
        $this->addHandlerQueryParameters($options);
        $handler = $this->selectionManager->getInstance($options);

        // Get an array of matching entities.
        $entityTypes = $handler->getReferenceableEntities(
          $typedString,
          $options['match_operator'] ?? static::MATCH_OPERATOR,
          static::LIMIT
        );

        // Loop through the entities and convert them into autocomplete output.
        foreach ($entityTypes as $values) {
          foreach ($values as $entityId => $label) {
            // Some selection handlers render HTML and all of them escape HTML
            // entities, while front app expects plain text.
            $label = strip_tags($label);
            $label = Html::decodeEntities($label);

            $matches[] = ['id' => $entityId, 'value' => $label];
          }
        }
      }
    }

    return new ModifiedResourceResponse($matches);
  }

  /**
   * Maps query parameters declared by handler and adds them to its settings.
   *
   * @param array $handler_settings
   *   The entity reference selection handler.
   */
  protected function addHandlerQueryParameters(array &$handler_settings): void {
    $parameters = [];
    $map = $handler_settings[static::HANDLER_QUERY_MAP_SETTING] ?? [];
    foreach ($map as $parameter_name => $key) {
      $value = $this->request->query->get($parameter_name);
      if ($value === NULL) {
        continue;
      }

      $parameters[$key] = $value;
    }
    if (empty($parameters)) {
      return;
    }

    $handler_settings[static::HANDLER_QUERY_PARAMETERS_SETTING] = $parameters;
  }

  /**
   * Checks if selection hash exists and valid.
   *
   * @param mixed $options
   *   Selection options.
   * @param string $selectionKey
   *   Key for getting options.
   */
  protected function checkSelectionKey($options, string $selectionKey) {
    if ($options !== FALSE) {
      $selectionKeyHash = Crypt::hmacBase64(
        serialize($options),
        $this->settings->getHashSalt()
      );
      if ($selectionKeyHash !== $selectionKey) {
        throw new AccessDeniedHttpException('Invalid selection settings key.');
      }
    }
    else {
      throw new AccessDeniedHttpException('Selection settings key was not found.');
    }
  }

}
