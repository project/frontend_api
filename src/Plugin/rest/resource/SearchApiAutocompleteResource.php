<?php

namespace Drupal\frontend_api\Plugin\rest\resource;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides REST resource with Search API Autocomplete suggestions.
 *
 * @RestResource(
 *   id = "frontend_api_search_api_autocomplete",
 *   label = @Translation("Search API autocomplete"),
 *   uri_paths = {
 *     "canonical" = "/frontend-api/search-autocomplete/{search_api_autocomplete_search}",
 *   }
 * )
 *
 * @see \Drupal\frontend_api\Rest\SearchApi\Autocomplete\DefaultSuggestionList
 */
class SearchApiAutocompleteResource extends SearchApiAutocompleteResourceBase {

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->suggestionListBuilder = $container->get(
      'frontend_api.search_api.autocomplete_suggestion_list_builder'
    );

    return $instance;
  }

}
