<?php

namespace Drupal\frontend_api\Plugin\rest\resource;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\frontend_api\Rest\Denormalizer\DenormalizerResultInterface;
use Drupal\frontend_api\Rest\EntityDenormalizer\EntityDenormalizerFactoryInterface;
use Drupal\frontend_api\Rest\EntityDenormalizer\EntityDenormalizerInterface;
use Drupal\frontend_api\Rest\ModifiedResourceResponse;
use Drupal\frontend_api\Rest\ResourceResponseInterface;
use Drupal\frontend_api\Rest\ResourceValidator\ResourceValidatorInterface;
use Drupal\frontend_api\Routing\EntityParamConverter;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Base class for content entity resources.
 */
abstract class EntityResourceBase extends ResourceBase {

  /**
   * The entity type id.
   *
   * Need to implements it on children classes.
   */
  protected const ENTITY_TYPE_ID = NULL;

  /**
   * The prefix of the entity route parameter type.
   */
  protected const ROUTE_PARAM_TYPE_PREFIX = EntityParamConverter::TYPE_PREFIX;

  /**
   * The language option name of the entity route parameter.
   */
  protected const ROUTE_PARAM_LANGUAGE_OPTION = EntityParamConverter::LANGUAGE_OPTION;

  /**
   * The special language code that specifies default entity translation.
   */
  protected const DEFAULT_LANGCODE = LanguageInterface::LANGCODE_DEFAULT;

  /**
   * ResourceValidator service.
   *
   * @var \Drupal\frontend_api\Rest\ResourceValidator\ResourceValidatorInterface
   */
  protected $resourceValidator;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity denormalizer factory.
   *
   * @var \Drupal\frontend_api\Rest\EntityDenormalizer\EntityDenormalizerFactoryInterface
   */
  protected $denormalizerFactory;

  /**
   * ProductResource constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger,
    ResourceValidatorInterface $resource_validator,
    EntityTypeManagerInterface $entity_type_manager,
    EntityDenormalizerFactoryInterface $entity_denormalizer_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition,
      $serializer_formats, $logger);

    $this->resourceValidator = $resource_validator;
    $this->entityTypeManager = $entity_type_manager;
    $this->denormalizerFactory = $entity_denormalizer_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('frontend_api.rest.resource_validator'),
      $container->get('entity_type.manager'),
      $container->get('frontend_api.rest.entity_denormalizer_factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRoute($canonical_path, $method) {
    $route = parent::getBaseRoute(
      $canonical_path,
      $method
    );

    $entity_parameter = [
      'type' => static::ROUTE_PARAM_TYPE_PREFIX . static::ENTITY_TYPE_ID,
    ];
    if ($method === static::METHOD_PATCH) {
      // Make sure the entity is loaded in default language on update.
      $entity_parameter[static::ROUTE_PARAM_LANGUAGE_OPTION] =
        static::DEFAULT_LANGCODE;
    }

    $parameters = $route->getOption('parameters') ?? [];
    $parameters[static::ENTITY_TYPE_ID] = $entity_parameter;
    $route->setOption('parameters', $parameters);

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRouteRequirements($method): array {
    $requirements = [];

    // Check access via entity access checker.
    switch ($method) {
      case static::METHOD_GET:
        $requirements['_entity_access'] = static::ENTITY_TYPE_ID . '.view';
        break;

      case static::METHOD_POST:
        $requirements['_entity_create_access'] = static::ENTITY_TYPE_ID;
        break;

      case static::METHOD_PATCH:
        $requirements['_entity_access'] = static::ENTITY_TYPE_ID . '.update';
        break;

      case static::METHOD_DELETE:
        $requirements['_entity_access'] = static::ENTITY_TYPE_ID . '.delete';
    }

    return $requirements;
  }

  /**
   * Returns entity de-normalizer for a specified entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $entity
   *   Entity for de-normalization or NULL to let de-normalizer create new one.
   *
   * @return \Drupal\frontend_api\Rest\EntityDenormalizer\EntityDenormalizerInterface
   *   The created de-normalizer.
   */
  protected function getEntityDenormalizer(
    ContentEntityInterface $entity = NULL
  ): EntityDenormalizerInterface {
    return $this->denormalizerFactory
      ->create(static::ENTITY_TYPE_ID, $entity);
  }

  /**
   * Builds response for a failed validation.
   *
   * @param \Drupal\frontend_api\Rest\Denormalizer\DenormalizerResultInterface $denormalizer_result
   *   The denormalizer result with errors to return.
   *
   * @return \Drupal\frontend_api\Rest\ResourceResponseInterface
   *   The response.
   */
  protected function buildValidationErrorResponse(
    DenormalizerResultInterface $denormalizer_result
  ): ResourceResponseInterface {
    $data = [
      'message' => $this->t('Validation error'),
      'errors' => $denormalizer_result->getErrors(),
    ];
    $response = new ModifiedResourceResponse(
      $data,
      Response::HTTP_CONFLICT
    );
    return $response;
  }

}
