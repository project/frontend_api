<?php

namespace Drupal\frontend_api\Plugin\rest\resource;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Error;
use Drupal\frontend_api\Rest\ModifiedResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Handles batch processing requests from the front application.
 *
 * @RestResource(
 *   id = "frontend_api_batch",
 *   label = @Translation("Front-end API: Batch processing"),
 *   uri_paths = {
 *     "canonical" = "/frontend-api/batch"
 *   }
 * )
 */
class BatchProcessingResource extends ResourceBase {

  use StringTranslationTrait;

  /**
   * The batch ID key in the request query.
   */
  public const BATCH_ID_KEY = 'id';

  /**
   * The processing status of the batch.
   *
   * It means the batch is still in progress and requires more requests to
   * complete.
   */
  public const STATUS_PROCESSING = 'processing';

  /**
   * The finished status of the batch.
   *
   * It means the batch processing is complete and no more requests needed.
   */
  public const STATUS_FINISHED = 'finished';

  /**
   * The error status of the batch.
   *
   * It means the batch processing has failed with an error.
   */
  public const STATUS_ERROR = 'error';

  /**
   * The error level of the log.
   */
  protected const ERROR_LOGGER_LEVEL = RfcLogLevel::ERROR;

  /**
   * The app root.
   *
   * @var string
   */
  protected $root;

  /**
   * The batch storage.
   *
   * @var \Drupal\Core\Batch\BatchStorageInterface
   */
  protected $batchStorage;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->root = $container->get('app.root');
    $instance->batchStorage = $container->get('batch.storage');
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->logger = $container->get('logger.channel.frontend_api');

    return $instance;
  }

  /**
   * Handles PATCH request.
   *
   * The front application should make requests to this endpoint until
   * processing is complete or an error returned.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function patch(Request $request): Response {
    $request_id = $request->query->get(static::BATCH_ID_KEY);
    if (!$request_id) {
      throw new NotFoundHttpException('The batch ID is missing.');
    }

    require_once $this->root . '/core/includes/batch.inc';
    $batch = &batch_get();

    // Retrieve the current state of the batch.
    if (!$batch) {
      // The batch is connected with the session and can't be loaded by other
      // users.
      $batch = $this->batchStorage->load($request_id);
      if (!$batch) {
        throw new NotFoundHttpException("The batch doesn't exist.");
      }
    }

    // @see _batch_page()
    drupal_register_shutdown_function('_batch_shutdown');
    _batch_needs_update(TRUE);

    $percentage = 0;
    $message = NULL;
    $results = [];
    $status = static::STATUS_PROCESSING;

    try {
      [$percentage, $message] = _batch_process();
    }
    catch (\Throwable $e) {
      $this->finishBatch();
      $this->logThrowable($e);
      $status = static::STATUS_ERROR;
      $message = $this->t('Unable to complete the process. See log for details.');
    }

    if ($percentage === '100') {
      $results = $this->extractBatchResults($batch);
      $this->finishBatch();
      $status = static::STATUS_FINISHED;
    }

    if ($batch) {
      $this->batchStorage->update($batch);
    }
    _batch_needs_update(FALSE);

    return new ModifiedResourceResponse([
      'status' => $status,
      'progress' => $percentage,
      'message' => $message,
      'results' => $results,
    ]);
  }

  /**
   * Extracts results from the batch.
   *
   * It merges result arrays from all the batch sets. In case the key exists in
   * multiple arrays, value of the latest set is taken.
   *
   * @param array|null $batch
   *   The batch, as returned by batch_get().
   *
   * @return array
   *   The results array merged from all the batch sets.
   */
  protected function extractBatchResults(array $batch = NULL): array {
    if (!$batch) {
      return [];
    }

    $result = [];
    $batch_sets = $batch['sets'] ?? [];
    foreach ($batch_sets as $batch_set) {
      if (empty($batch_set['success'])) {
        continue;
      }

      if (!empty($batch_set['results']) && is_array($batch_set['results'])) {
        $set_results = end($batch_set['results']);
      }
      else {
        $set_results = $batch_set['results'] ?? [];
      }
      if (!is_array($set_results)) {
        continue;
      }

      // Merge results of all sets together. Last one wins.
      $result = $set_results + $result;
    }
    return $result;
  }

  /**
   * Finishes the batch.
   *
   * It should be called for both successful and failed batch processes.
   *
   * It's a limited version of the core function that does not deal with the
   * form state, as our batches aren't triggered from forms.
   *
   * @see _batch_finished()
   */
  protected function finishBatch() {
    $batch = &batch_get();

    // Execute the 'finished' callbacks for each batch set, if defined.
    foreach ($batch['sets'] as $batch_set) {
      if (isset($batch_set['finished'])) {
        // Check if the set requires an additional file for function
        // definitions.
        if (isset($batch_set['file']) && is_file($batch_set['file'])) {
          include_once $this->root . '/' . $batch_set['file'];
        }
        if (is_callable($batch_set['finished'])) {
          $queue = _batch_queue($batch_set);
          $operations = $queue->getAllItems();
          $elapsed = $this->dateFormatter
            ->formatInterval($batch_set['elapsed'] / 1000);
          call_user_func_array(
            $batch_set['finished'],
            [
              $batch_set['success'],
              $batch_set['results'],
              $operations,
              $elapsed,
            ]
          );
        }
      }
    }

    // Clean up the batch table.
    if ($batch['progressive']) {
      $this->batchStorage->delete($batch['id']);

      foreach ($batch['sets'] as $batch_set) {
        if ($queue = _batch_queue($batch_set)) {
          $queue->deleteQueue();
        }
      }

      // Clean-up the session. Not needed for CLI updates.
      if (isset($_SESSION['batches'])) {
        unset($_SESSION['batches'][$batch['id']]);
        if (empty($_SESSION['batches'])) {
          unset($_SESSION['batches']);
        }
      }
    }

    // Unset the static $batch variable.
    $batch = NULL;
  }

  /**
   * Logs the throwable.
   *
   * @param \Throwable $e
   *   The throwable to log.
   *
   * @see watchdog_exception()
   *
   * @todo Use watchdog_exception() or its replacement when it supports any
   *   throwable and not just exceptions.
   */
  protected function logThrowable(\Throwable $e): void {
    $message = '%type: @message in %function (line %line of %file).';
    $variables = Error::decodeException($e);

    $this->logger->log(static::ERROR_LOGGER_LEVEL, $message, $variables);
  }

}
