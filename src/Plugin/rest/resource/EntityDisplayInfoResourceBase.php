<?php

namespace Drupal\frontend_api\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoBuilderInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a base resource to get information about the entity form or view.
 */
abstract class EntityDisplayInfoResourceBase extends ResourceBase {

  use StringTranslationTrait;

  /**
   * The entity type the info is built for.
   *
   * Should be overridden in sub-classes.
   */
  protected const ENTITY_TYPE_ID = NULL;

  /**
   * The form or view mode map.
   *
   * Keys are API names, values are corresponding Drupal display mode names.
   *
   * Should be overridden in sub-classes.
   */
  protected const DISPLAY_MODE_MAP = [];

  /**
   * The permission(s) required to access the form info.
   *
   * Should be overridden in sub-classes.
   */
  protected const ACCESS_PERMISSION = NULL;

  /**
   * The form info builder.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoBuilderInterface
   */
  protected $displayInfoBuilder;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoBuilderInterface $display_info_builder
   *   The form/view info builder.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    DisplayInfoBuilderInterface $display_info_builder,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $serializer_formats,
      $logger
    );

    $this->displayInfoBuilder = $display_info_builder;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRouteRequirements($method): array {
    $requirements = [];

    $requirements['_permission'] = static::ACCESS_PERMISSION;

    return $requirements;
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRoute($canonical_path, $method) {
    $route = parent::getBaseRoute(
      $canonical_path,
      $method
    );

    $bundle_entity_type_id = $this->entityTypeManager
      ->getDefinition(static::ENTITY_TYPE_ID)
      ->getBundleEntityType();

    if (isset($bundle_entity_type_id)) {
      // Make sure the product bundle entity is loaded.
      $parameters = $route->getOption('parameters') ?? [];
      $parameters['bundle'] = [
        'type' => 'entity:' . $bundle_entity_type_id,
      ];
      $route->setOption('parameters', $parameters);
    }

    return $route;
  }

  /**
   * Maps display mode from React to Drupal.
   *
   * @param string $source_more
   *   The React view mode name.
   *
   * @return string
   *   The Drupal view mode name.
   */
  protected function mapDisplayMode(string $source_more): string {
    $result_mode = static::DISPLAY_MODE_MAP[$source_more] ?? NULL;
    if (!isset($result_mode)) {
      throw new NotFoundHttpException(sprintf(
        'The %s mode is not defined.',
        $source_more
      ));
    }

    return $result_mode;
  }

}
