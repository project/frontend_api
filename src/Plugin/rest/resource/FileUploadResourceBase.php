<?php

namespace Drupal\frontend_api\Plugin\rest\resource;

use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileInterface;
use Drupal\frontend_api\Dictionary\EntityTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Base class for uploading files from front app.
 */
abstract class FileUploadResourceBase extends ResourceBase {

  /**
   * File entity type id.
   */
  protected const FILE_ENTITY_TYPE_ID = EntityTypes::FILE;

  /**
   * Permission for uploading files.
   *
   * Must be overridden in child classes.
   */
  protected const UPLOAD_FILE_PERMISSION = NULL;

  /**
   * Flag to create directory if not exists when preparing it.
   */
  protected const CREATE_DIRECTORY_OPTION = FileSystemInterface::CREATE_DIRECTORY;

  /**
   * Flag to rename the destination file until it doesn't exist.
   */
  protected const FILE_EXISTS_RENAME_OPTION = FileSystemInterface::EXISTS_RENAME;

  /**
   * Flag to return an error if the destination file exists.
   */
  protected const FILE_EXISTS_ERROR_OPTION = FileSystemInterface::EXISTS_ERROR;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The currently authenticated user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The MIME type guesser.
   *
   * @var \Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface
   */
  protected $mimeTypeGuesser;

  /**
   * The lock service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The file resource helper.
   *
   * @var \Drupal\frontend_api\Rest\File\FileResourceHelperInterface
   */
  protected $fileResourceHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->fileSystem = $container->get('file_system');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->currentUser = $container->get('current_user');
    $instance->mimeTypeGuesser = $container->get('file.mime_type.guesser');
    $instance->lock = $container->get('lock');
    $instance->fileResourceHelper = $container->get(
      'frontend_api.rest.file_resource_helper'
    );

    return $instance;
  }

  /**
   * Returns the uploaded file name.
   *
   * @param array $data
   *   The request data.
   *
   * @return string
   *   The file name.
   */
  protected function getFileName(array $data): string {
    $fileName = $data['file']['file_name'];
    if (empty($fileName)) {
      throw new HttpException(
        Response::HTTP_CONFLICT,
        'File name can not be empty.'
      );
    }

    return $fileName;
  }

  /**
   * Returns base64 file data string.
   *
   * @param array $data
   *   The request data.
   *
   * @return string
   *   The base64 file data string.
   */
  protected function getFileData(array $data): string {
    $fileData = $data['file']['value'];
    if (empty($fileData)) {
      throw new HttpException(
        Response::HTTP_CONFLICT,
        'File data can not be empty.'
      );
    }

    return $fileData;
  }

  /**
   * Returns target file URI.
   *
   * @param string $destinationFolder
   *   The destination folder.
   * @param string $fileName
   *   The uploaded file name.
   *
   * @return bool|string
   *   The destination filepath, or FALSE if the file already exists.
   */
  protected function getTargetFileUri(
    string $destinationFolder,
    string $fileName
  ) {
    $isPrepared = $this->fileSystem->prepareDirectory(
      $destinationFolder,
      static::CREATE_DIRECTORY_OPTION
    );
    if (!$isPrepared) {
      throw new HttpException(
        Response::HTTP_INTERNAL_SERVER_ERROR,
        'Destination file path is not writable'
      );
    }
    return $this->fileSystem->getDestinationFilename(
      "{$destinationFolder}/{$fileName}",
      static::FILE_EXISTS_RENAME_OPTION
    );
  }

  /**
   * Acquires a lock.
   *
   * @param string $fileUri
   *   The file URI.
   * @param string $fileName
   *   The file name.
   *
   * @return string
   *   The lock ID.
   */
  protected function acquireLock(string $fileUri, string $fileName): string {
    $lockId = $this->fileResourceHelper
      ->generateLockIdFromFileUri($fileUri);

    if (!$this->lock->acquire($lockId)) {
      throw new HttpException(
        Response::HTTP_SERVICE_UNAVAILABLE,
        sprintf('File "%s" is already locked for writing', $fileName),
        NULL,
        ['Retry-After' => 1]
      );
    }

    return $lockId;
  }

  /**
   * Creates a file.
   *
   * @param string $fileUri
   *   The destination file URI.
   * @param string $fileName
   *   The file name.
   * @param string $fileData
   *   The base64 file data string.
   *
   * @return \Drupal\file\FileInterface
   *   The created file entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function createFile(
    string $fileUri,
    string $fileName,
    string $fileData
  ): FileInterface {
    $tempFilePath = $this->streamUploadData($fileData);
    $file = $this->entityTypeManager
      ->getStorage(static::FILE_ENTITY_TYPE_ID)
      ->create([]);
    $file->setOwnerId($this->currentUser->id());
    $file->setFilename($fileName);
    $file->setMimeType($this->mimeTypeGuesser->guess($fileName));
    $file->setFileUri($fileUri);
    // Set the size. This is done in File::preSave() but we validate the file
    // before it is saved.
    $file->setSize(@filesize($tempFilePath));

    // Move the file to the correct location after validation.
    try {
      $this->fileSystem
        ->move($tempFilePath, $fileUri, static::FILE_EXISTS_RENAME_OPTION);
    }
    catch (FileException $e) {
      watchdog_exception('frontend_api', $e);
      throw new HttpException(
        Response::HTTP_INTERNAL_SERVER_ERROR,
        'Temporary file could not be moved to file location'
      );
    }

    return $file;
  }

  /**
   * Streams file upload data to temporary file.
   *
   * @return string
   *   The temp file path.
   */
  protected function streamUploadData($data) {
    $data = explode(',', $data, 2);
    if (empty($data[1])) {
      throw new HttpException(
        Response::HTTP_CONFLICT,
        'File data can not be empty.'
      );
    }
    $decodedData = base64_decode($data[1]);

    $tempFilePath = $this->fileSystem
      ->tempnam('temporary://', 'file');
    $tempFile = fopen($tempFilePath, 'wb');

    if (!$tempFile) {
      throw new HttpException(
        Response::HTTP_INTERNAL_SERVER_ERROR,
        'Temporary file could not be opened'
      );
    }
    if (fwrite($tempFile, $decodedData) === FALSE) {
      fclose($tempFile);
      $this->logger->error(
        'Temporary file data for "%path" could not be written',
        ['%path' => $tempFilePath]
      );
      throw new HttpException(
        Response::HTTP_INTERNAL_SERVER_ERROR,
        'Temporary file data could not be written'
      );
    }
    fclose($tempFile);

    return $tempFilePath;
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRouteRequirements($method) {
    $requirements = [];

    // Add permission check.
    switch ($method) {
      case 'POST':
        $requirements['_permission'] = static::UPLOAD_FILE_PERMISSION;
        break;
    }

    return $requirements;
  }

}
