<?php

namespace Drupal\frontend_api\Plugin\rest\resource;

use Drupal\Core\Language\LanguageInterface;
use Drupal\frontend_api\Routing\EntityParamConverter;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataBuilderInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base resource that exposes entity data for the edit form.
 */
abstract class EntityFormDataResourceBase extends ResourceBase {

  /**
   * The entity type the form data is built for.
   *
   * Should be overridden by child classes.
   */
  protected const ENTITY_TYPE_ID = NULL;

  /**
   * The form mode to retrieve available fields from.
   *
   * Should be overridden by child classes.
   */
  protected const FORM_MODE = NULL;

  /**
   * The prefix of the entity route parameter type.
   */
  protected const ROUTE_PARAM_TYPE_PREFIX = EntityParamConverter::TYPE_PREFIX;

  /**
   * The language option name of the entity route parameter.
   */
  protected const ROUTE_PARAM_LANGUAGE_OPTION = EntityParamConverter::LANGUAGE_OPTION;

  /**
   * The special language code that specifies default entity translation.
   */
  protected const DEFAULT_LANGCODE = LanguageInterface::LANGCODE_DEFAULT;

  /**
   * The form data builder.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataBuilderInterface
   */
  protected $formDataBuilder;

  /**
   * A constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger,
    FormDataBuilderInterface $form_data_builder
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $serializer_formats,
      $logger
    );

    $this->formDataBuilder = $form_data_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('frontend_api.rest.entity_form_data_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRoute($canonical_path, $method) {
    $route = parent::getBaseRoute(
      $canonical_path,
      $method
    );

    // Make sure the entity is loaded in default language.
    $parameters = $route->getOption('parameters') ?? [];
    $parameters[static::ENTITY_TYPE_ID] = [
      'type' => static::ROUTE_PARAM_TYPE_PREFIX . static::ENTITY_TYPE_ID,
      static::ROUTE_PARAM_LANGUAGE_OPTION => static::DEFAULT_LANGCODE,
    ];
    $route->setOption('parameters', $parameters);

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRouteRequirements($method): array {
    $requirements = [];

    // User must be able to edit the entity in order to get its data.
    $requirements['_entity_access'] = static::ENTITY_TYPE_ID . '.update';

    return $requirements;
  }

}
