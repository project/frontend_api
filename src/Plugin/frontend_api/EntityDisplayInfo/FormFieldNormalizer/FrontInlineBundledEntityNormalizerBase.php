<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\frontend_api\Exception\DenormalizationException;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Drupal\frontend_api\Rest\EntityReference\Denormalizer\InlineItemDenormalizationContextInterface;

/**
 * Base normalizer for inline entity editing of multiple bundles.
 *
 * The widget provides management of multiple entity bundles on the same form.
 */
abstract class FrontInlineBundledEntityNormalizerBase extends FrontInlineEntityNormalizerBase {

  /**
   * The referenced entity bundle ID key in the item data.
   */
  public const ITEM_BUNDLE_KEY = 'bundle_id';

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeField($field, $format, $context);

    $result['bundle_forms'] = $this
      ->normalizeInlineEntityBundleForms($field, $format, $context);

    return $result;
  }

  /**
   * Normalizes the list of inline entity forms for every available bundle.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The entity display info field to normalize.
   * @param string $format
   *   The serialization format.
   * @param array $context
   *   The normalization context.
   *
   * @return array
   *   The normalized list of form info keyed by bundle ID.
   */
  protected function normalizeInlineEntityBundleForms(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): array {
    $field_definition = $field->getFieldDefinition();
    $target_type_id = $this->entityReferenceSettingsReader
      ->getTargetEntityTypeId($field_definition);
    $target_bundle_ids = $this->entityReferenceSettingsReader
      ->getTargetBundleIds($field_definition);
    $form_mode_name = $this->getInlineEntityFormModeName($field);

    $result = [];
    foreach ($target_bundle_ids as $target_bundle_id) {
      $form_info = $this->formInfoBuilder
        ->build($target_type_id, $target_bundle_id, $form_mode_name);
      $normalized_form = $this->normalizer
        ->normalize($form_info, $format, $context);

      $result[$target_bundle_id] = $normalized_form;
      $this->addCacheableDependency($context, $form_info);
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function createItemDenormalizationContext(
    FormDataFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): InlineItemDenormalizationContextInterface {
    $item_context = parent::createItemDenormalizationContext(
      $field,
      $format,
      $context
    );

    $field_definition = $field->getFieldDefinition();
    $bundle_ids = $this->entityReferenceSettingsReader
      ->getTargetBundleIds($field_definition) ?: NULL;
    $item_context->setTargetBundleIds($bundle_ids);

    return $item_context;
  }

  /**
   * {@inheritdoc}
   */
  protected function denormalizeNewInlineEntityItem(
    FormDataFieldInterface $field,
    array $item,
    InlineItemDenormalizationContextInterface $item_context
  ): array {
    $bundle_id = $item[static::ITEM_BUNDLE_KEY] ?? NULL;
    if ($bundle_id === NULL) {
      throw new DenormalizationException([
        '' => $this->t('Item bundle is required for new entities.'),
      ]);
    }
    if (!$item_context->isTargetBundleIdAllowed($bundle_id)) {
      throw new DenormalizationException([
        '' => $this->t('Unknown bundle was specified.'),
      ]);
    }
    $item_context->setTargetBundleId($bundle_id);

    return parent::denormalizeNewInlineEntityItem(
      $field,
      $item,
      $item_context
    );
  }

}
