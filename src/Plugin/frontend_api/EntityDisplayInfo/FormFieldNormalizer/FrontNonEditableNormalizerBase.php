<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides base class for a normalizer of a non-editable widget.
 *
 * Such widgets don't allow user to make any changes and only display values.
 */
abstract class FrontNonEditableNormalizerBase extends DefaultNormalizer {

  /**
   * The widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::NON_EDITABLE;

  /**
   * {@inheritdoc}
   */
  protected function isFieldReadOnly(FormFieldInterface $field): bool {
    return TRUE;
  }

}
