<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;

/**
 * Provides base class for normalizers of various boolean checkbox widgets.
 */
abstract class BooleanCheckboxNormalizerBase extends DefaultNormalizer {

  /**
   * The setting that controls if the checkbox should use the field label.
   *
   * In case it's FALSE, we add on/off labels to the form info.
   */
  public const DISPLAY_LABEL_SETTING = 'display_label';

  /**
   * The checkbox on/off labels key in the normalized field info.
   */
  public const CHECKBOX_LABELS_KEY = 'checkbox_labels';

  /**
   * The map of field setting to the key in normalized list of checkbox labels.
   */
  public const CHECKBOX_LABELS_SETTING_MAP = [
    'on_label' => 'on',
    'off_label' => 'off',
  ];

  /**
   * Returns field property name that holds the boolean value.
   *
   * The value of this property is normalized to boolean.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   *
   * @return string
   *   The property name.
   */
  abstract protected function getBooleanValuePropertyName(
    DisplayFieldInterface $field
  ): string;

  /**
   * Normalizes boolean property on the field value literal.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   * @param array $value
   *   The value literal (before flattening).
   *
   * @return array
   *   The updated value literal.
   *
   * @see getBooleanValuePropertyName()
   */
  protected function normalizeBooleanValueLiteral(
    DisplayFieldInterface $field,
    array $value
  ): array {
    // Convert strings/integers into booleans.
    $value_property = $this->getBooleanValuePropertyName($field);
    foreach ($value as &$item) {
      if (!isset($item[$value_property])) {
        continue;
      }

      $item[$value_property] = (bool) $item[$value_property];
    }
    unset($item);

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeValueLiteral(
    DisplayFieldInterface $field,
    array $value
  ) {
    $value = $this->normalizeBooleanValueLiteral($field, $value);
    return parent::normalizeValueLiteral($field, $value);
  }

  /**
   * Adds checkbox labels to the normalized field/property info.
   *
   * It only adds checkbox labels if they are needed according to the widget
   * settings.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   * @param array $result
   *   The field/property info to add the labels to.
   *
   * @return array
   *   The updated field/property info.
   */
  protected function addCheckboxLabels(
    DisplayFieldInterface $field,
    array $result
  ): array {
    $display_field_label = $this->getWidgetSetting(
      $field,
      static::DISPLAY_LABEL_SETTING
    );
    if (!$display_field_label) {
      $labels = [];
      $field_definition = $field->getFieldDefinition();
      foreach (static::CHECKBOX_LABELS_SETTING_MAP as $setting_key => $result_key) {
        $labels[$result_key] = $field_definition->getSetting($setting_key);
      }
      $result[static::CHECKBOX_LABELS_KEY] = $labels;
    }

    return $result;
  }

}
