<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionWithAutocreateInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;

/**
 * Trait for normalizers of the entity reference widgets with auto-create.
 */
trait EntityReferenceAutocreateNormalizerTrait {

  use EntityReferenceSelectionNormalizerTrait;

  /**
   * Checks if entity auto-creation is enabled on the selection handler.
   *
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface $handler
   *   The selection handler.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   *
   * @return bool
   *   TRUE if entities should be automatically created.
   *
   * @todo This method assumes that any other handler has auto-create option
   *   with the same key as default one, but it may be different. However, there
   *   is no better way to check if auto-create is enabled.
   */
  protected function isAutocreateEnabledOnSelectionHandler(
    SelectionInterface $handler,
    DisplayFieldInterface $field
  ): bool {
    if (!$handler instanceof SelectionWithAutocreateInterface) {
      return FALSE;
    }

    $handler_settings = $this->getSelectionHandlerSettings($field);
    if (empty($handler_settings['auto_create'])) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns bundle ID of the target automatically created entity.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   *
   * @return string|null
   *   The bundle ID or NULL in case it can't be determined.
   *
   * @see \Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget::getAutocreateBundle()
   *
   * @todo Figure out how is it done on the user selection handler if we need it
   *   and implement the same here. For now we don't support target entities
   *   without bundles.
   *
   * @todo This method assumes that any other handler has the same settings key
   *   as default one. However, there is no better way to get it.
   */
  protected function getAutocreateTargetBundleId(
    DisplayFieldInterface $field
  ): ?string {
    $settings = $this->getSelectionHandlerSettings($field);
    $target_bundles = $settings['target_bundles'] ?? [];
    if (empty($target_bundles)) {
      return NULL;
    }

    // If there's only one target bundle, use it.
    if (count($target_bundles) === 1) {
      return reset($target_bundles);
    }

    // Otherwise use the target bundle stored in selection handler settings.
    // We have to normalize empty value to NULL, as it may be an empty string.
    $bundle = $settings['auto_create_bundle'] ?? NULL;
    if (empty($bundle)) {
      return NULL;
    }

    return $bundle;
  }

}
