<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\frontend_api\Exception\DenormalizationException;
use Drupal\frontend_api\Rest\EntityReference\Denormalizer\ItemDenormalizationContext;
use Drupal\frontend_api\Rest\EntityReference\Denormalizer\ItemDenormalizationContextInterface;
use Drupal\frontend_api\Rest\Denormalizer\DenormalizerResult;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides normalizer for entity reference autocomplete field.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "entity_reference_autocomplete",
 *   field_types = {
 *     "entity_reference",
 *   },
 *   widget_types = {
 *     "entity_reference_autocomplete",
 *   }
 * )
 */
class EntityReferenceAutocompleteNormalizer extends DefaultNormalizer implements ContainerFactoryPluginInterface {

  use EntityReferenceAutocreateNormalizerTrait;

  /**
   * The autocomplete widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::AUTOCOMPLETE;

  /**
   * The entity ID key on the normalized item.
   */
  public const ITEM_ID_KEY = 'id';

  /**
   * The entity label key on the normalized item.
   */
  public const ITEM_LABEL_KEY = 'value';

  /**
   * The entity ID property name of the field item.
   */
  public const ENTITY_ID_PROPERTY = 'target_id';

  /**
   * The computed property name of the field item that holds the entity.
   */
  public const ENTITY_PROPERTY = 'entity';

  /**
   * Read only settings that are initialized with the class.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * URL generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * The key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValue;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    $instance = new static(
      $configuration,
      $pluginId,
      $pluginDefinition
    );

    $instance->settings = $container->get('settings');
    $instance->urlGenerator = $container->get('url_generator');
    $instance->keyValue = $container->get('keyvalue')
      ->get('frontend_api_entity_autocomplete');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->selectionManager = $container->get(
      'plugin.manager.entity_reference_selection'
    );
    $instance->currentUser = $container->get('current_user');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeField($field, $format, $context);

    $handler = $this->getSelectionHandler($field);
    $autoCreate = $this
      ->isAutocreateEnabledOnSelectionHandler($handler, $field);

    $handlerSettings = $this->buildSelectionHandlerSettings($field);
    $selectionKey = Crypt::hmacBase64(
      serialize($handlerSettings),
      $this->settings->getHashSalt()
    );

    if (!$this->keyValue->has($selectionKey)) {
      $this->keyValue->set($selectionKey, $handlerSettings);
    }
    $queryUrl = $this->urlGenerator->generate(
      'rest.frontend_api_entity_autocomplete.GET',
      ['selection_key' => $selectionKey],
      UrlGeneratorInterface::ABSOLUTE_PATH
    );

    $result += [
      'query_url' => $queryUrl,
      'auto_create' => $autoCreate,
    ];

    return $result;
  }

  /**
   * Builds the entity reference selection handler settings to be stored.
   *
   * The settings returned here are stored for the autocomplete REST resource.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   */
  protected function buildSelectionHandlerSettings(
    DisplayFieldInterface $field
  ): array {
    $componentData = $field->getComponentData();
    $fieldSettings = $field->getFieldDefinition()
      ->getSettings();

    // Get info about target field in order to create query link.
    $targetType = $fieldSettings['target_type'];
    $handlerId = $fieldSettings['handler'];
    $handlerSettings = $fieldSettings['handler_settings'];
    $handlerSettings += [
      'target_type' => $targetType,
      'handler' => $handlerId,
      'match_operator' => $componentData['settings']['match_operator'],
    ];
    return $handlerSettings;
  }

  /**
   * Returns definitions of the properties copied to/from (de-)normalized data.
   *
   * The list of returned property definitions shouldn't include computed or
   * internal properties. It also shouldn't list properties that need special
   * handling.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field info.
   *
   * @return \Drupal\Core\TypedData\DataDefinitionInterface[]
   *   The property definitions.
   */
  protected function getCopiedFieldPropertyDefinitions(
    DisplayFieldInterface $field
  ): array {
    $definitions = $field->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getPropertyDefinitions();

    $result = [];
    foreach ($definitions as $property_name => $definition) {
      if ($definition->isComputed() || $definition->isInternal()) {
        continue;
      }

      $result[$property_name] = $definition;
    }

    // Drop the entity ID property too, as its value is (de-)normalized
    // separately from the properties that are just copied.
    unset($result[static::ENTITY_ID_PROPERTY]);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeValueLiteral(
    DisplayFieldInterface $field,
    array $value
  ) {
    $idsDeltaMap = $this->getEntityReferenceIdDeltasMap($field, $value);
    if (empty($idsDeltaMap)) {
      return [];
    }

    $entities = $this->getReferenceableEntitiesOfValueLiteral($field, $value);
    if (empty($entities)) {
      return [];
    }

    $propertyDefinitions = $this->getCopiedFieldPropertyDefinitions($field);

    $result = [];
    foreach ($idsDeltaMap as $id => $deltas) {
      $entity = $entities[$id] ?? NULL;
      if ($entity === NULL) {
        continue;
      }

      foreach ($deltas as $delta) {
        $item = $value[$delta];

        $normalized = $this->normalizeEntityReferenceItemLiteral(
          $propertyDefinitions,
          $item,
          $entity
        );

        $result[$delta] = $normalized;
      }
    }

    // Keep results sorted by delta, re-index them to keep serializing it as
    // an array, not an object.
    ksort($result, SORT_NUMERIC);
    $result = array_values($result);

    return $this->flattenValueLiteralItemList($field, $result);
  }

  /**
   * Normalizes value literal of a single field item.
   *
   * @param array $propertyDefinitions
   *   Definitions of the properties that should be copied to the normalization
   *   result.
   * @param array $item
   *   The field item.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity referenced by the item.
   *
   * @return array
   *   The normalized item.
   */
  protected function normalizeEntityReferenceItemLiteral(
    array $propertyDefinitions,
    array $item,
    EntityInterface $entity
  ): array {
    return [
      static::ITEM_ID_KEY => $entity->id(),
      static::ITEM_LABEL_KEY => $entity->label(),
    ];
  }

  /**
   * Denormalizes a single item of the entity reference field.
   *
   * @param array $propertyDefinitions
   *   The property definitions that should be copied from the submitted data to
   *   the de-normalized item.
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface $handler
   *   The selection handler.
   * @param array $item
   *   The submitted item data.
   * @param \Drupal\frontend_api\Rest\EntityReference\Denormalizer\ItemDenormalizationContextInterface $context
   *   The item denormalization context.
   *
   * @return array
   *   The field item value.
   *
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  protected function denormalizeEntityReferenceItemLiteral(
    array $propertyDefinitions,
    SelectionInterface $handler,
    array $item,
    ItemDenormalizationContextInterface $context
  ): array {
    $result = [];

    $id = $item[static::ITEM_ID_KEY] ?? NULL;
    if (isset($id)) {
      $result[static::ENTITY_ID_PROPERTY] = $id;
    }
    else {
      if (!$context->isAutocreateEnabled()) {
        throw new DenormalizationException([
          '' => $this->t('The entity creation is not allowed.'),
        ]);
      }

      $label = $item[static::ITEM_LABEL_KEY] ?? NULL;
      if ($label === NULL || $label === '') {
        throw new DenormalizationException([
          '' => $this->t("The entity label can't be empty."),
        ]);
      }

      $result[static::ENTITY_PROPERTY] = $handler->createNewEntity(
        $context->getAutocreateEntityTypeId(),
        $context->getAutocreateBundleId(),
        $label,
        $context->getAutocreateAuthorId()
      );
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \InvalidArgumentException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  protected function denormalizeValueLiteral(
    FormDataFieldInterface $field,
    $value,
    string $format = NULL,
    array $context = []
  ) {
    $value = $this->roughenValueLiteralItemList($field, $value);

    // Return NULL if value is not array because all further methods work
    // with arrays or NULL.
    if (is_array($value)) {
      return $this->denormalizeAutocompleteEntities($field, $value);
    }

    return NULL;
  }

  /**
   * Creates a context object for the entity reference item de-normalization.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field info.
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface $handler
   *   The entity reference selection handler.
   *
   * @return \Drupal\frontend_api\Rest\EntityReference\Denormalizer\ItemDenormalizationContextInterface
   *   The context created.
   */
  protected function createEntityReferenceItemDenormalizationContext(
    FormDataFieldInterface $field,
    SelectionInterface $handler
  ): ItemDenormalizationContextInterface {
    // The callback is called to extract all the data required to create new
    // entity. It is called only once per context and only if the auto-create
    // is enabled for the field.
    $autocreateInitCallback = function (
      ItemDenormalizationContextInterface $context
    ) use (
      $field
    ) {
      /** @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionWithAutocreateInterface $handler */
      $autocreateBundleId = $this->getAutocreateTargetBundleId($field);
      if (!isset($autocreateBundleId)) {
        throw new \InvalidArgumentException(sprintf(
          'Unable to get bundle ID for auto-created %s on %s field.',
          $this->getEntityReferenceTargetType($field)
            ->getSingularLabel(),
          $field->getFieldDefinition()
            ->getName()
        ));
      }

      $autocreateUid = $this->currentUser->id();
      $autocteateEntityTypeId = $this->getEntityReferenceTargetTypeId($field);

      $context
        ->setAutocreateBundleId($autocreateBundleId)
        ->setAutocreateAuthorId($autocreateUid)
        ->setAutocreateEntityTypeId($autocteateEntityTypeId);
    };
    $isAutocreateEnabled = $this
      ->isAutocreateEnabledOnSelectionHandler($handler, $field);

    return new ItemDenormalizationContext(
      $isAutocreateEnabled,
      $autocreateInitCallback
    );
  }

  /**
   * Denormalizes the value literal into a list of field item values.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   * @param array $value
   *   The value literal.
   *
   * @return array
   *   The list of values to set on the field.
   *
   * @throws \InvalidArgumentException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  protected function denormalizeAutocompleteEntities(
    FormDataFieldInterface $field,
    array $value
  ): array {
    if (empty($value)) {
      return [];
    }

    $handler = $this->getSelectionHandler($field);
    $propertyDefinitions = $this->getCopiedFieldPropertyDefinitions($field);
    $context = $this
      ->createEntityReferenceItemDenormalizationContext($field, $handler);

    $result = [];
    $errors = new DenormalizerResult();
    foreach ($value as $delta => $item) {
      try {
        $item_result = $this->denormalizeEntityReferenceItemLiteral(
          $propertyDefinitions,
          $handler,
          $item,
          $context
        );
      }
      catch (DenormalizationException $e) {
        $errors->addErrorsAt($delta, $e->getErrors());
        continue;
      }

      $result[] = $item_result;
    }

    if ($errors->hasErrors()) {
      $fieldName = $field->getFieldDefinition()
        ->getName();
      $errors->throwErrors($fieldName);
    }

    return $result;
  }

}
