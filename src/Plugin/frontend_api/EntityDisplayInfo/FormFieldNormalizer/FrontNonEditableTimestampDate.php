<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\frontend_api\Plugin\Field\FieldWidget\FrontNonEditableDateWidget;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Widget normalizer for a timestamp field displayed as non-editable date.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "frontend_api_non_editable_timestamp_date",
 *   priority = 10,
 *   field_types = {
 *     "timestamp",
 *     "created",
 *     "changed",
 *   },
 *   widget_types = {
 *     "frontend_api_non_editable_date",
 *   },
 * )
 */
class FrontNonEditableTimestampDate extends FrontNonEditableNormalizerBase implements ContainerFactoryPluginInterface {

  /**
   * The date format widget setting.
   */
  protected const DATE_FORMAT_SETTING = FrontNonEditableDateWidget::DATE_FORMAT_SETTING;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->dateFormatter = $container->get('date.formatter');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeValueLiteral(
    DisplayFieldInterface $field,
    array $value
  ) {
    $value = $this->normalizeValueLiteralDate($field, $value);
    return parent::normalizeValueLiteral($field, $value);
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeValueLiteralDate(
    DisplayFieldInterface $field,
    array $value
  ): array {
    $main_property = $this->getFieldMainPropertyName($field);
    if ($main_property === NULL) {
      return $value;
    }

    $date_format_id = $this->getWidgetSetting($field, static::DATE_FORMAT_SETTING);
    if ($date_format_id === NULL) {
      return $value;
    }

    foreach ($value as &$item) {
      $timestamp = $item[$main_property] ?? NULL;
      if ($timestamp === NULL) {
        continue;
      }

      $item[$main_property] = $this->dateFormatter
        ->format($timestamp, $date_format_id);
    }
    unset($item);

    return $value;
  }

}
