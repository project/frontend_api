<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Url;
use Drupal\frontend_api\Exception\DenormalizationException;
use Drupal\frontend_api\Rest\Denormalizer\DenormalizerResult;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Base class for entity autocomplete normalizers on different fields search.
 *
 * It shows suggestions as basic entity list, using the same format as default
 * entity autocomplete does, but retrieving results using Search API.
 *
 * It doesn't support auto-creation.
 */
abstract class FrontEntityFieldAutocompleteNormalizerBase extends SearchApiEntityAutocompleteNormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::AUTOCOMPLETE;

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\frontend_api\Plugin\rest\resource\SearchApiBasicEntityAutocompleteResource
   */
  public const AUTOCOMPLETE_ROUTE_NAME = 'rest.frontend_api_search_api_entity_basic_autocomplete.GET';

  /**
   * The field name we search on.
   *
   * Must be overridden in child classes.
   */
  protected const FIELD_NAME = NULL;

  /**
   * The entity ID key on the normalized item.
   */
  protected const ITEM_ID_KEY = EntityReferenceAutocompleteNormalizer::ITEM_ID_KEY;

  /**
   * The entity label key on the normalized item.
   */
  protected const ITEM_LABEL_KEY = EntityReferenceAutocompleteNormalizer::ITEM_LABEL_KEY;

  /**
   * {@inheritdoc}
   */
  protected function normalizeEntityReferenceFieldItem(
    FormDataFieldInterface $field,
    ContentEntityInterface $entity,
    array $item_value,
    string $format = NULL,
    array $context = []
  ) {
    return [
      static::ITEM_ID_KEY => $entity->id(),
      static::ITEM_LABEL_KEY => $entity->get(static::FIELD_NAME)->value,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeField($field, $format, $context);

    $this->addAutocomplete($result, $field);

    return $result;
  }

  /**
   * Adds autocomplete properties to the normalized widget info.
   *
   * @param array $result
   *   The value widget data.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   Field element.
   */
  protected function addAutocomplete(
    array &$result,
    DisplayFieldInterface $field
  ): void {
    $component_data = $field->getComponentData();
    $widget_settings = $component_data['settings'];

    $route_name = static::AUTOCOMPLETE_ROUTE_NAME;
    $route_parameters = $this
      ->buildAutocompleteRouteParameters($field, $widget_settings);
    $url = Url::fromRoute($route_name, $route_parameters);

    $result['query_url'] = $url->toString();

    // We don't support auto-creation by this widget.
    $result['auto_create'] = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function denormalizeValueLiteral(
    FormDataFieldInterface $field,
    $value,
    string $format = NULL,
    array $context = []
  ) {
    $value = $this->roughenValueLiteralItemList($field, $value);
    if (!is_array($value)) {
      throw new DenormalizationException([
        '' => 'Wrong data format, expected an array.',
      ]);
    }

    $value = $this->denormalizeAutocompleteItems($field, $value);

    return $value;
  }

  /**
   * Denormalizes an array of values submitted by the front widget.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field info.
   * @param array $value
   *   The list of items.
   *
   * @return array
   *   The de-normalized list of values for the field.
   *
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  protected function denormalizeAutocompleteItems(
    FormDataFieldInterface $field,
    array $value
  ): array {
    $result = [];

    $errors = new DenormalizerResult();
    $id_key = static::ITEM_ID_KEY;
    $column = $this->getFieldMainPropertyName($field) ?? 'target_id';
    foreach ($value as $index => $item) {
      $id = $item[$id_key] ?? NULL;
      if ($id === NULL) {
        $errors->addErrors([
          "$index.$id_key" => $this->t('ID of every item should be specified.'),
        ]);
        continue;
      }

      $result[] = [
        $column => $id,
      ];
    }
    if ($errors->hasErrors()) {
      $field_name = $field->getFieldDefinition()
        ->getName();
      $errors->throwErrors($field_name);
    }

    return $result;
  }

}
