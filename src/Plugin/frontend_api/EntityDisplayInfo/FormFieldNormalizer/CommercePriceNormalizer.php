<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;
use Drupal\frontend_api\Dictionary\EntityTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides field definition normalizer for the commerce price fields.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "commerce_price",
 *   field_types = {
 *     "commerce_price"
 *   },
 *   widget_types = {
 *     "*"
 *   }
 * )
 *
 * @TODO NOW Move to another module.
 */
class CommercePriceNormalizer extends DefaultNormalizer implements ContainerFactoryPluginInterface {

  /**
   * Field setting that stores the list of available currencies.
   */
  public const AVAILABLE_CURRENCIES_SETTING = 'available_currencies';

  /**
   * The commerce currency entity type ID.
   */
  protected const CURRENCY_ENTITY_TYPE_ID = EntityTypes::CURRENCY;

  /**
   * The widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::PRICE;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeField($field, $format, $context);

    $result['currencies'] = $this
      ->normalizeAvailableCurrencies($field->getFieldDefinition());

    return $result;
  }

  /**
   * Returns the list of normalized currencies available for the field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return array
   *   The list of normalized currencies.
   *
   * @see \Drupal\commerce_price\Element\Price::processElement()
   * @see \Drupal\commerce_price\Plugin\Field\FieldType\PriceItem
   */
  protected function normalizeAvailableCurrencies(
    FieldDefinitionInterface $field_definition
  ): array {
    $available_currencies = $field_definition
      ->getSetting(static::AVAILABLE_CURRENCIES_SETTING);
    $available_currencies = array_filter($available_currencies);

    /** @var \Drupal\commerce_price\Entity\Currency[] $currencies */
    $currencies = $this->entityTypeManager
      ->getStorage(static::CURRENCY_ENTITY_TYPE_ID)
      ->loadMultiple($available_currencies ?: NULL);

    $options = [];
    foreach ($currencies as $currency_code => $currency) {
      $options[$currency_code] = [
        'name' => $currency->getName(),
        'symbol' => $currency->getSymbol(),
      ];
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeValueLiteral(
    DisplayFieldInterface $field,
    array $value
  ) {
    foreach ($value as &$item) {
      $number = $item['number'];
      $item['number'] = number_format($number, 2, '.', '');
    }

    return parent::normalizeValueLiteral($field, $value);
  }

}
