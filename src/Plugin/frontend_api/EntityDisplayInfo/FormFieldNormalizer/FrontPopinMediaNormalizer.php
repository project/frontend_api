<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\frontend_api\Plugin\Field\FieldWidget\FrontPopinMediaWidget;
use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides normalizer for a pop-in media widget.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "frontend_api_popin_media",
 *   field_types = {
 *     "entity_reference",
 *   },
 *   widget_types = {
 *     "frontend_api_popin_media",
 *   },
 * )
 *
 * @see \Drupal\frontend_api\Plugin\Field\FieldWidget\FrontPopinMediaWidget
 */
class FrontPopinMediaNormalizer extends FrontInlineBundledEntityNormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::POPIN_MEDIA;

  /**
   * {@inheritdoc}
   */
  protected const FORM_MODE_KEY = FrontPopinMediaWidget::FORM_MODE_KEY;

  /**
   * {@inheritdoc}
   */
  protected const DELETE_ORPHAN_ENTITIES_KEY = FrontPopinMediaWidget::DELETE_ORPHAN_ENTITIES_KEY;

}
