<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;

/**
 * Trait for a normalizer of the entity reference widget.
 */
trait EntityReferenceNormalizerTrait {

  /**
   * Returns column of the field that stores the ID of the referenced entity.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   *
   * @return string
   *   The property name.
   */
  protected function getEntityReferenceTargetColumn(
    DisplayFieldInterface $field
  ): string {
    $main_property = $this->getFieldMainPropertyName($field);
    if (isset($main_property)) {
      return $main_property;
    }

    // Fallback to default one.
    return 'target_id';
  }

  /**
   * Returns the target entity type ID of the reference.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   *
   * @return string
   *   The target entity type ID.
   */
  protected function getEntityReferenceTargetTypeId(
    DisplayFieldInterface $field
  ): string {
    return $field->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getSetting('target_type');
  }

  /**
   * Returns map of entity ID to the item deltas that reference it.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   * @param array $value
   *   The value literal.
   *
   * @return array
   *   Keys are entity IDs, values are arrays of item deltas.
   */
  protected function getEntityReferenceIdDeltasMap(
    DisplayFieldInterface $field,
    array $value
  ): array {
    $id_delta_map = [];

    $column_name = $this->getEntityReferenceTargetColumn($field);
    foreach ($value as $delta => $item) {
      $id = $item[$column_name] ?? NULL;
      if (empty($id)) {
        continue;
      }

      $id_delta_map[$id][] = $delta;
    }

    return $id_delta_map;
  }

}
