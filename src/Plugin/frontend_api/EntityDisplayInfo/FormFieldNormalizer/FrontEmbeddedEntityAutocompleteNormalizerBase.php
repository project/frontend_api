<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Url;
use Drupal\frontend_api\Plugin\Field\FieldWidget\FrontEmbeddedEntityAutocompleteWidgetBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Drupal\frontend_api\Rest\NormalizerContextKeys;
use Drupal\frontend_api\Rest\SearchApi\Autocomplete\SuggestionListBuilderBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Provides base normalizer for the embedded entity autocomplete widget.
 *
 * @see \Drupal\frontend_api\Plugin\Field\FieldWidget\FrontEmbeddedEntityAutocompleteWidgetBase
 */
abstract class FrontEmbeddedEntityAutocompleteNormalizerBase extends SearchApiEntityAutocompleteNormalizerBase implements NormalizerAwareInterface {

  use NormalizerAwareTrait;

  /**
   * Query variable name of the autocomplete suggestion keys.
   */
  protected const AUTOCOMPLETE_KEYWORDS_QUERY_VAR = SuggestionListBuilderBase::KEYWORDS_QUERY_VAR;

  /**
   * The context key of the view mode name.
   */
  protected const VIEW_MODE_CONTEXT = NormalizerContextKeys::VIEW_MODE_CONTEXT;

  /**
   * The view mode setting.
   */
  protected const VIEW_MODE_SETTING = FrontEmbeddedEntityAutocompleteWidgetBase::VIEW_MODE_SETTING;

  /**
   * {@inheritdoc}
   */
  public const AUTOCOMPLETE_ROUTE_NAME = 'rest.frontend_api_search_api_autocomplete.GET';

  /**
   * Normalizer of the formatted entity fields.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface|\Symfony\Component\Serializer\SerializerAwareInterface
   */
  protected $formattedFieldsNormalizer;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->formattedFieldsNormalizer = $container->get(
      'frontend_api.serializer.normalizer.formatted_fields'
    );

    return $instance;
  }

  /**
   * Retrieve render view mode from widget settings.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The entity view info field.
   *
   * @return string
   *   Widget view mode setting.
   *
   * @throws \Exception
   */
  protected function getRenderViewMode(FormDataFieldInterface $field) {
    $component_data = $field->getComponentData();
    $view_mode = $component_data['settings'][static::VIEW_MODE_SETTING] ?? NULL;
    if (empty($view_mode)) {
      throw new \Exception("View mode doesn't present in widget settings");
    }

    return $view_mode;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareFieldItemNormalization(
    FormDataFieldInterface $field,
    array &$context
  ): void {
    $serializer = $this->normalizer;
    if (!$serializer instanceof SerializerInterface) {
      throw new \LogicException(sprintf(
        'Serializer should implement %s',
        SerializerInterface::class
      ));
    }

    $context[static::VIEW_MODE_CONTEXT] = $this->getRenderViewMode($field);
    $this->formattedFieldsNormalizer->setSerializer($serializer);
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeEntityReferenceFieldItem(
    FormDataFieldInterface $field,
    ContentEntityInterface $entity,
    array $item_value,
    string $format = NULL,
    array $context = []
  ) {
    $normalized_item = $this
      ->normalizeBooleanValues($field, $item_value);
    $normalized_item['fields'] = $this->formattedFieldsNormalizer
      ->normalize($entity, $format, $context);
    return $normalized_item;
  }

  /**
   * Converts boolean properties' values from string to bool.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field the value is normalized for.
   * @param array $values
   *   Array with field values.
   *
   * @return array
   *   Values array with corrected booleans.
   */
  protected function normalizeBooleanValues(
    DisplayFieldInterface $field,
    array $values
  ): array {
    $property_definitions = $field->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getPropertyDefinitions();
    foreach ($property_definitions as $property => $definition) {
      if ($definition->getDataType() === 'boolean') {
        $values[$property] = (bool) $values[$property];
      }
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeField($field, $format, $context);

    $this->addAutocomplete($result, $field);

    return $result;
  }

  /**
   * Adds autocomplete properties to the normalized value widget data.
   *
   * @param array $result
   *   The value widget data.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   Field element.
   */
  protected function addAutocomplete(
    array &$result,
    DisplayFieldInterface $field
  ): void {
    $component_data = $field->getComponentData();
    $widget_settings = $component_data['settings'];
    $autocomplete_min_length = $this
      ->getAutocompleteMinInputLength($widget_settings);

    $route_name = static::AUTOCOMPLETE_ROUTE_NAME;
    $route_parameters = $this
      ->buildAutocompleteRouteParameters($field, $widget_settings);
    $url = Url::fromRoute($route_name, $route_parameters);

    $result['autocomplete_url'] = $url->toString();
    $result['autocomplete_keywords_var'] = static::AUTOCOMPLETE_KEYWORDS_QUERY_VAR;
    $result['autocomplete_min_length'] = $autocomplete_min_length;
  }

  /**
   * {@inheritdoc}
   */
  protected function denormalizeValueLiteral(
    FormDataFieldInterface $field,
    $value,
    string $format = NULL,
    array $context = []
  ) {
    // Front widgets send values as an array even for single-value fields, so
    // we don't want to wrap it again, just try to de-normalize the item column.
    $value = $this->roughenValueLiteralItems($field, $value);

    return $value;
  }

}
