<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides normalizer language field.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "frontend_api_language_select",
 *   field_types = {
 *     "language",
 *   },
 *   widget_types = {
 *     "language_select",
 *   }
 * )
 */
class LanguageSelectNormalizer extends ListOptionsNormalizer implements ContainerFactoryPluginInterface {

  /**
   * The map of Drupal to React widget type.
   */
  public const WIDGET_TYPE_MAP = [
    'language_select' => FrontWidgetTypes::SELECT,
  ];

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition
    );

    $instance->languageManager = $container->get('language_manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAllowedValues(
    DisplayFieldInterface $field,
    FieldableEntityInterface $entity,
    array $context = []
  ): array {
    $componentData = $field->getComponentData();
    $includeLocked = $componentData['settings']['include_locked'];
    $flag = $includeLocked
      ? LanguageInterface::STATE_ALL
      : LanguageInterface::STATE_CONFIGURABLE;
    $languages = $this->languageManager->getLanguages($flag);
    $options = [];
    foreach ($languages as $key => $language) {
      $options[$key] = $language->getName();
    }

    return $options;
  }

}
