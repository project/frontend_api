<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides form field normalizer for the number widget.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "number",
 *   field_types = {
 *     "integer",
 *   },
 *   widget_types = {
 *     "number",
 *   }
 * )
 */
class NumberNormalizer extends DefaultNormalizer {

  /**
   * The widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::NUMBER;

  /**
   * Maps key of the setting to the key it is exposed under.
   */
  public const EXPOSED_SETTINGS_MAP = [
    'min' => 'min_number',
    'max' => 'max_number',
    'prefix' => 'prefix',
    'suffix' => 'suffix',
  ];

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeField($field, $format, $context);

    // Expose min & max to the API, if specified for the field.
    $field_definition = $field->getFieldDefinition();
    foreach (static::EXPOSED_SETTINGS_MAP as $setting_key => $result_key) {
      $value = $field_definition->getSetting($setting_key);
      if (!isset($value)) {
        continue;
      }

      $result[$result_key] = $value;
    }

    return $result;
  }

}
