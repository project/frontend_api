<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\frontend_api\Exception\DenormalizationException;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Drupal\frontend_api\Field\ListOptions\OptionsProviderAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base class for normalizers of various list fields with options.
 *
 * It doesn't perform the actual normalization/de-normalization to give child
 * classes more flexibility.
 */
abstract class ListOptionsNormalizerBase extends DefaultNormalizer implements ContainerFactoryPluginInterface {

  /**
   * The options key in the normalized form info.
   */
  public const OPTIONS_KEY = 'options';

  /**
   * The map of React widget types to a React type depending on cardinality.
   */
  public const CARDINALITY_WIDGETS_MAP = [
    'select' => [
      -1 => 'multiselect',
      1 => 'select',
    ],
    'buttons' => [
      -1  => 'checkboxes',
      1 => 'radiobuttons',
    ],
  ];

  /**
   * The map of Drupal to React widget type.
   */
  public const WIDGET_TYPE_MAP = [
    'options_select' => 'select',
    'chosen_select' => 'select',
    'address_country_default' => 'select',
    'options_buttons' => 'buttons',
  ];

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->currentUser = $container->get('current_user');

    return $instance;
  }

  /**
   * Returns a property name the options are returned for.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   *
   * @return string
   *   The property name.
   */
  abstract protected function getOptionsPropertyName(
    DisplayFieldInterface $field
  ): string;

  /**
   * Returns cardinality for the options widget.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   *
   * @return int
   *   The cardinality.
   */
  abstract protected function getOptionsWidgetCardinality(
    DisplayFieldInterface $field
  ): int;

  /**
   * Normalizes the options available for a select list.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   * @param array $context
   *   The context.
   *
   * @return array
   *   The list of normalized options to add to the normalization result.
   */
  protected function normalizeOptions(
    DisplayFieldInterface $field,
    array $context = []
  ): array {
    $entity = $this->getFieldEntity($field);
    $allowed_values = $this->getAllowedValues($field, $entity, $context);
    return $this->normalizeAllowedValues($allowed_values);
  }

  /**
   * Returns allowed values for the passed field.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The entity display info field.
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   Current entity or its sample if it is form.
   * @param array $context
   *   Context options for the normalizer.
   *
   * @return string[]
   *   The list of allowed values:
   *   - keys are the option values,
   *   - values are the plain text option labels.
   */
  protected function getAllowedValues(
    DisplayFieldInterface $field,
    FieldableEntityInterface $entity,
    array $context = []
  ): array {
    $main_property = $this->getOptionsPropertyName($field);
    $field_definition = $field->getFieldDefinition();
    $provider = $field_definition
      ->getFieldStorageDefinition()
      ->getOptionsProvider($main_property, $entity);

    if ($provider instanceof OptionsProviderAwareInterface) {
      $metadata = new CacheableMetadata();
      $options = $provider->getOptionsProvider()
        ->getOptions($field_definition, $entity, $metadata);
      $this->addCacheableDependency($context, $metadata);
    }
    else {
      $options = $provider->getSettableOptions($this->currentUser);
    }

    return $options;
  }

  /**
   * Normalizes allowed values.
   *
   * In order to keep the order of options we have to convert them into a
   * numerically indexed array, since JSON objects don't guarantee the order of
   * properties.
   *
   * @param string[] $values
   *   Allowed values as returned by the getAllowedValues() method.
   *
   * @return array
   *   The normalized values.
   */
  protected function normalizeAllowedValues(array $values): array {
    $result = [];

    foreach ($values as $key => $value) {
      $result[] = [
        'key' => $key,
        'label' => $value,
      ];
    }

    return $result;
  }

  /**
   * Returns the front widget type for the options select.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   *
   * @return string|null
   *   The front widget type or NULL if it can't be determined.
   */
  protected function getOptionsWidgetType(
    DisplayFieldInterface $field
  ): ?string {
    $widget_data = $field->getComponentData();
    $source_type = $widget_data['type'];
    $result = static::WIDGET_TYPE_MAP[$source_type] ?? NULL;
    if (!isset($result)) {
      return NULL;
    }

    // We may need a different front widget type depending on cardinality.
    $cardinality = $this->getOptionsWidgetCardinality($field);
    return static::CARDINALITY_WIDGETS_MAP[$result][$cardinality] ?? $result;
  }

  /**
   * Validates options property on the value literal.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   * @param array $value
   *   The value literal after restoring the flattened items.
   * @param array $context
   *   The context.
   *
   * @return array
   *   The validated value literal.
   *
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  protected function validateValueLiteralOptions(
    FormDataFieldInterface $field,
    array $value,
    array $context = []
  ): array {
    $entity = $this->getFieldEntity($field);
    $options_property = $this->getOptionsPropertyName($field);
    $allowed_values = $this->getAllowedValues($field, $entity, $context);

    // Make sure submitted values are actually allowed at the moment.
    $invalid_values = [];
    foreach ($value as $item) {
      $property_value = $item[$options_property] ?? NULL;

      // We ignore empty values. Some fields submit empty string instead of
      // NULL, so we have to exclude it too.
      if (!isset($property_value) || $property_value === '') {
        continue;
      }

      if (!isset($allowed_values[$property_value])) {
        $invalid_values[$property_value] = TRUE;
      }
    }
    if (!empty($invalid_values)) {
      $field_name = $field->getItemList()
        ->getName();
      throw new DenormalizationException([
        $field_name => $this->t('Invalid values submitted: @values', [
          '@values' => implode(', ', array_keys($invalid_values)),
        ]),
      ]);
    }

    return $value;
  }

}
