<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;
use Drupal\frontend_api\Exception\DenormalizationException;
use Drupal\frontend_api\Rest\Denormalizer\DenormalizerResult;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;
use Drupal\frontend_api\Dictionary\EntityTypes;
use Drupal\frontend_api\Exception\MissingImageStyleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides form field normalizer for Media entity reference fields.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "frontend_api_media_image_upload",
 *   field_types = {
 *     "entity_reference",
 *   },
 *   widget_types = {
 *     "frontend_api_media_image_upload",
 *   }
 * )
 *
 * @todo Replace it with a pop-in media widget once we implement universal
 *   image upload widget.
 */
class FrontMediaImageUploadNormalizer extends DefaultNormalizer implements ContainerFactoryPluginInterface {

  use EntityReferenceSelectionNormalizerTrait;

  /**
   * The widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::IMAGE;

  /**
   * Target media bundle for product.
   */
  public const PRODUCT_IMAGE = 'product_image';

  /**
   * Medium image style id.
   *
   * @todo Use configurable image style instead of a hard-coded one.
   */
  public const MEDIUM_IMAGE_STYLE_ID = 'medium';

  /**
   * The entity type ID of an image style.
   */
  protected const STYLE_ENTITY_TYPE_ID = EntityTypes::IMAGE_STYLE;

  /**
   * The media fields that are accepted and validated.
   *
   * @todo Instead of hard-coding the field list we should provide form mode
   *   setting and use embedded entity denormalization.
   */
  public const IMAGE_MEDIA_FIELDS = [
    'field_media_image' => TRUE,
    'bundle' => TRUE,
    'name' => TRUE,
  ];

  /**
   * The media entity type ID.
   */
  protected const MEDIA_ENTITY_TYPE = EntityTypes::MEDIA;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The resource validator.
   *
   * @var \Drupal\frontend_api\Rest\ResourceValidator\ResourceValidatorInterface
   */
  protected $resourceValidator;

  /**
   * The entity reference field settings reader.
   *
   * @var \Drupal\frontend_api\Field\EntityReference\EntityReferenceSettingsReaderInterface
   */
  protected $entityReferenceFieldSettingsReader;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->selectionManager = $container->get(
      'plugin.manager.entity_reference_selection'
    );
    $instance->resourceValidator = $container->get(
      'frontend_api.rest.resource_validator'
    );
    $instance->entityReferenceFieldSettingsReader = $container->get(
      'frontend_api.field.entity_reference.settings_reader'
    );

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeField(DisplayFieldInterface $field, string $format = NULL, array $context = []) {
    $result = parent::normalizeField($field, $format, $context);
    $field_definition = $field->getFieldDefinition();
    $target_bundles = $this->entityReferenceFieldSettingsReader
      ->getTargetBundleIds($field_definition);
    $media_bundle = reset($target_bundles);
    if ($media_bundle) {
      $result['media_bundle'] = $media_bundle;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\frontend_api\Exception\MissingImageStyleException
   */
  protected function normalizeValueLiteral(DisplayFieldInterface $field, array $value) {
    $style_storage = $this->entityTypeManager
      ->getStorage(static::STYLE_ENTITY_TYPE_ID);
    $style_id = static::MEDIUM_IMAGE_STYLE_ID;
    $style = $style_storage->load($style_id);
    if (!isset($style)) {
      throw new MissingImageStyleException(sprintf(
        'The %s image style is missing.',
        $style_id
      ));
    }

    $results = [];
    $entities = $this->getReferenceableEntitiesOfValueLiteral($field, $value);
    foreach ($entities as $item) {
      // @todo Use configurable entity view mode instead of hard-coding fields.
      $result = [
        'mid' => $item->id(),
        'name' => $item->label(),
        'bundle' => $item->bundle(),
      ];

      if ($item->bundle() === static::PRODUCT_IMAGE) {
        $image_item_list = $item->get('field_media_image');
        $result['field_media_image'] = [
          $image_item_list->target_id,
        ];

        $file = $image_item_list->entity;
        if ($file instanceof FileInterface) {
          $image_uri = $file->getFileUri();
          $result['image_preview_url'] = $style->buildUrl($image_uri);
        }
      }

      $results[] = $result;
    }

    return $this->flattenValueLiteralItemList($field, $results);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  protected function denormalizeValueLiteral(
    FormDataFieldInterface $field,
    $value,
    string $format = NULL,
    array $context = []
  ) {
    $value = $this->roughenValueLiteralItemList($field, $value);
    return $this->denormalizeMediaEntities($field, $value);
  }

  /**
   * Denormalizes value literal into a set of media entities.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   * @param array $value
   *   The value literal.
   *
   * @return array
   *   The list of values to set on the field.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  protected function denormalizeMediaEntities(
    FormDataFieldInterface $field,
    array $value
  ): array {
    $allowed_fields = static::IMAGE_MEDIA_FIELDS;
    $media_storage = $this->entityTypeManager
      ->getStorage(static::MEDIA_ENTITY_TYPE);
    $field_definition = $field->getFieldDefinition();
    // For security reasons we can't allow to apply any media to this field
    // so additional check was added. Media should be already used in target
    // entity type (i.e was created for this entity type).
    $submitted_media_ids = array_column($value, 'mid');
    if (!empty($submitted_media_ids)) {
      $field_name = $field_definition->getName();
      $entity_type_id = $field_definition->getTargetEntityTypeId();
      $existing_media_ids = $this->entityTypeManager->getStorage($entity_type_id)
        ->getAggregateQuery()
        ->accessCheck(FALSE)
        ->groupBy($field_name)
        ->condition($field_name, $submitted_media_ids, 'IN')
        ->execute();
      $existing_media_ids = array_column(
        $existing_media_ids,
        "${field_name}_target_id"
      );
      $existing_media_ids = array_flip($existing_media_ids);
    }

    $media_entities = [];
    $errors = new DenormalizerResult();
    foreach ($value as $delta => $image_data) {
      $media_id = $image_data['mid'] ?? NULL;
      unset($image_data['mid']);

      $image_data = array_intersect_key($image_data, $allowed_fields);

      // Checks that media bundle can be reference to the field.
      $allowed_bundles = $this->entityReferenceFieldSettingsReader
        ->getTargetBundleIds($field_definition);
      $bundle = $image_data['bundle'] ?? NULL;
      if (!isset($allowed_bundles[$bundle])) {
        $message = $this->t(
          'The given @bundle bundle cannot be referenced.',
          ['@bundle' => $bundle]
        );
        $errors->addErrors([
          $delta => $message,
        ]);
        continue;
      }

      // Update existing media if its id was submitted.
      // @todo This should probably use embedded entity denormalization
      //   in order to apply all the processing, data validation, etc.
      if (isset($media_id)) {
        if (!isset($existing_media_ids[$media_id])) {
          $errors->addErrors([
            $delta => $this->t(
              "The media #@id doesn't have reference in any product and can't be used.",
              ['@id' => $media_id]
            ),
          ]);
          continue;
        }

        $media = $media_storage->load($media_id);
        if (!$media instanceof MediaInterface) {
          $errors->addErrors([
            $delta => $this->t(
              "The media #@id doesn't exist.",
              ['@id' => $media_id]
            ),
          ]);
          continue;
        }

        // @todo Should we really allow updates of the media fields? Are they
        //   saved at all? That's just a port of legacy code.
        foreach ($image_data as $media_field => $media_field_value) {
          $media->set($media_field, $media_field_value);
        }
      }
      else {
        try {
          $media = $media_storage->create($image_data);
        }
        catch (EntityStorageException $e) {
          watchdog_exception('frontend_api', $e);
          $errors->addErrors([
            $delta => $this->t(
              'Unable to create media entity, see log for details.'
            ),
          ]);
          continue;
        }
      }

      // Validate the received field image data.
      $item_errors = $this->resourceValidator->validate(
        $media,
        array_keys($image_data),
        array_keys($allowed_fields)
      );
      if (!empty($item_errors)) {
        $errors->addErrorsAt($delta, $item_errors);
        continue;
      }

      $media_entities[$delta] = $media;
    }

    if ($errors->hasErrors()) {
      $field_name = $field->getFieldDefinition()
        ->getName();
      $errors->addPrefixToErrorPaths($field_name);
      throw new DenormalizationException($errors->getErrors());
    }

    return $media_entities;
  }

}
