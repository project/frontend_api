<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides field definition normalizer for the fields draggable table widget.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "frontend_api_draggable_table",
 *   field_types = {
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *   },
 *   widget_types = {
 *     "options_table",
 *   }
 * )
 *
 * @TODO NOW Move to a separate module.
 */
class FrontDraggableTableNormalizer extends FrontListOptionsNormalizer {

  /**
   * {@inheritdoc}
   */
  public const WIDGET_TYPE_MAP = [
    'options_table' => FrontWidgetTypes::DRAGGABLE_TABLE,
  ];

}
