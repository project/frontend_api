<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides field definition normalizer for email fields.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "email_default",
 *   field_types = {
 *     "email",
 *   },
 *   widget_types={
 *     "email_default"
 *   }
 * )
 */
class EmailNormalizer extends StringNormalizer {

  /**
   * The React widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::EMAIL;

}
