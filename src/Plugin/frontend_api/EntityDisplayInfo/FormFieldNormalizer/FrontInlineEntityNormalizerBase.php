<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\frontend_api\Exception\DenormalizationException;
use Drupal\frontend_api\Rest\Denormalizer\DenormalizerResult;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Drupal\frontend_api\Rest\EntityReference\Denormalizer\InlineItemDenormalizationContext;
use Drupal\frontend_api\Rest\EntityReference\Denormalizer\InlineItemDenormalizationContextInterface;
use Drupal\frontend_api\Rest\NormalizerContextKeys;
use Drupal\frontend_api\Field\EntityReference\InlineEditingAwareFieldItemListInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;

/**
 * Base normalizer for widgets that allow inline editing of referenced entity.
 *
 * @see \Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer\FrontInlineSimpleEntityNormalizerBase
 * @see \Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer\FrontInlineBundledEntityNormalizerBase
 */
abstract class FrontInlineEntityNormalizerBase extends DefaultNormalizer implements ContainerFactoryPluginInterface, NormalizerAwareInterface {

  use EntityReferenceSelectionNormalizerTrait,
    NormalizerAwareTrait;

  /**
   * The form mode key in the widget settings.
   *
   * Must be overridden in child class.
   */
  protected const FORM_MODE_KEY = NULL;

  /**
   * The widget settings that allows the normalizer to delete orphan entities.
   *
   * Could be overridden in child class to support entity deletion.
   */
  protected const DELETE_ORPHAN_ENTITIES_KEY = NULL;

  /**
   * The referenced entity ID key in the item data.
   */
  public const ITEM_ID_KEY = 'entity_id';

  /**
   * The fields key in the item data.
   */
  public const ITEM_FIELDS_KEY = 'fields';

  /**
   * The computed property name of the field item that holds the entity.
   */
  protected const ENTITY_PROPERTY = EntityReferenceAutocompleteNormalizer::ENTITY_PROPERTY;

  /**
   * The context key of the entity validation boolean flag.
   */
  protected const ENTITY_VALIDATION_CONTEXT_KEY = NormalizerContextKeys::ENTITY_VALIDATION;

  /**
   * The form info builder.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormInfoBuilderInterface
   */
  protected $formInfoBuilder;

  /**
   * The form data builder.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataBuilderInterface
   */
  protected $formDataBuilder;

  /**
   * The entity denormalizer factory.
   *
   * @var \Drupal\frontend_api\Rest\EntityDenormalizer\EntityDenormalizerFactoryInterface
   */
  protected $denormalizerFactory;

  /**
   * The settings reader of the entity reference field.
   *
   * @var \Drupal\frontend_api\Field\EntityReference\EntityReferenceSettingsReaderInterface
   */
  protected $entityReferenceSettingsReader;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->selectionManager = $container->get(
      'plugin.manager.entity_reference_selection'
    );
    $instance->formInfoBuilder = $container->get(
      'frontend_api.rest.entity_form_info_builder'
    );
    $instance->formDataBuilder = $container->get(
      'frontend_api.rest.entity_form_data_builder'
    );
    $instance->entityReferenceSettingsReader = $container->get(
      'frontend_api.field.entity_reference.settings_reader'
    );
    $instance->denormalizerFactory = $container->get(
      'frontend_api.rest.entity_denormalizer_factory'
    );

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldValue(
    FormDataFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $item_list */
    $value = $this->getValueLiteral($field);
    if ($value === NULL) {
      return NULL;
    }

    $entities = $this
      ->getReferenceableEntitiesOfValueLiteral($field, $value);
    $id_deltas_map = $this->getEntityReferenceIdDeltasMap($field, $value);
    $form_mode_name = $this->getInlineEntityFormModeName($field);

    $result = [];
    foreach ($id_deltas_map as $entity_id => $deltas) {
      $entity = $entities[$entity_id] ?? NULL;
      if ($entity === NULL) {
        continue;
      }

      $normalized_entity = $this
        ->normalizeInlineEntity($entity, $form_mode_name, $format, $context);

      foreach ($deltas as $delta) {
        $result[$delta] = $normalized_entity;
      }
    }

    // Keep the order of deltas.
    ksort($result, SORT_NUMERIC);

    return $this->flattenValueLiteralItemList($field, $result);
  }

  /**
   * Normalizes the inline entity for the form data.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The referenced entity to normalize.
   * @param string $form_mode_name
   *   The form mode of the referenced entity to use.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   *
   * @return array|null
   *   The normalized entity.
   */
  protected function normalizeInlineEntity(
    ContentEntityInterface $entity,
    string $form_mode_name,
    string $format = NULL,
    array $context = []
  ): ?array {
    $form_data = $this->formDataBuilder->build($entity, $form_mode_name);
    return $this->normalizer
      ->normalize($form_data, $format, $context);
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeValueLiteral(
    DisplayFieldInterface $field,
    array $value
  ) {
    // @todo Implement default value support.
    if (!empty($value)) {
      throw new \LogicException(sprintf(
        "The %s normalizer doesn't support default values yet.",
        $this->getPluginId()
      ));
    }

    return NULL;
  }

  /**
   * Returns form mode name of the inline form chosen on the widget settings.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface $field
   *   The field.
   *
   * @return string
   *   The form mode name.
   */
  protected function getInlineEntityFormModeName(
    FormFieldInterface $field
  ): string {
    $form_mode_name = $this->getWidgetSetting($field, static::FORM_MODE_KEY);
    if ($form_mode_name === NULL) {
      throw new \LogicException(sprintf(
        'The form mode is required for the %s normalizer.',
        $this->getPluginId()
      ));
    }

    return $form_mode_name;
  }

  /**
   * {@inheritdoc}
   */
  protected function denormalizeValueLiteral(
    FormDataFieldInterface $field,
    $value,
    string $format = NULL,
    array $context = []
  ) {
    $value = $this->roughenValueLiteralItemList($field, $value);

    if (!is_array($value)) {
      return NULL;
    }

    return $this->denormalizeInlineEntityItems($field, $value, $format, $context);
  }

  /**
   * Creates context object for the field item de-normalization.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   *
   * @return \Drupal\frontend_api\Rest\EntityReference\Denormalizer\InlineItemDenormalizationContextInterface
   *   The item de-normalization context object.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function createItemDenormalizationContext(
    FormDataFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): InlineItemDenormalizationContextInterface {
    $field_definition = $field->getFieldDefinition();
    $target_type_id = $this->entityReferenceSettingsReader
      ->getTargetEntityTypeId($field_definition);

    $form_mode_name = $this->getInlineEntityFormModeName($field);
    $is_validation_enabled = $context[static::ENTITY_VALIDATION_CONTEXT_KEY]
      ?? FALSE;

    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $item_list */
    $item_list = $field->getItemList();
    $entities = $item_list->referencedEntities();

    return (new InlineItemDenormalizationContext($target_type_id))
      ->setFormat($format)
      ->setContext($context)
      ->setValidation($is_validation_enabled)
      ->setFormModeName($form_mode_name)
      ->setReferencedEntities($entities);
  }

  /**
   * De-normalizes field item values from the inline form data.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   * @param array $value
   *   The list of data for every field item.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   *
   * @return array
   *   The list of values suitable for setting to the field.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  protected function denormalizeInlineEntityItems(
    FormDataFieldInterface $field,
    array $value,
    string $format = NULL,
    array $context = []
  ): array {
    $item_context = $this
      ->createItemDenormalizationContext($field, $format, $context);

    $denormalized_items = [];
    $errors = new DenormalizerResult();
    foreach ($value as $delta => $item) {
      try {
        $item = $this->denormalizeInlineEntityItem($field, $item, $item_context);
      }
      catch (DenormalizationException $e) {
        $errors->addErrorsAt($delta, $e->getErrors());
        continue;
      }

      $denormalized_items[] = $item;
    }

    $field_name = $field->getFieldDefinition()
      ->getName();
    $errors->throwErrors($field_name);

    $this->enforceOrphanInlineEntitiesDeletion($field, $item_context);

    return $denormalized_items;
  }

  /**
   * De-normalizes values of a single field item from the inline form data.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   * @param array $item
   *   The item data.
   * @param \Drupal\frontend_api\Rest\EntityReference\Denormalizer\InlineItemDenormalizationContextInterface $item_context
   *   The item de-normalization context.
   *
   * @return array
   *   The de-normalized item data suitable for setting to the field.
   */
  protected function denormalizeInlineEntityItem(
    FormDataFieldInterface $field,
    array $item,
    InlineItemDenormalizationContextInterface $item_context
  ): array {
    $id = $item[static::ITEM_ID_KEY] ?? NULL;
    if ($id !== NULL) {
      return $this->denormalizeExistingInlineEntityItem(
        $field,
        $item,
        $id,
        $item_context
      );
    }
    else {
      return $this->denormalizeNewInlineEntityItem(
        $field,
        $item,
        $item_context
      );
    }
  }

  /**
   * De-normalizes new inline entity.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   * @param array $item
   *   The item to de-normalize into an entity.
   * @param \Drupal\frontend_api\Rest\EntityReference\Denormalizer\InlineItemDenormalizationContextInterface $item_context
   *   The item de-normalization context.
   *
   * @return array
   *   The field item value with the referenced entity.
   *
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  protected function denormalizeNewInlineEntityItem(
    FormDataFieldInterface $field,
    array $item,
    InlineItemDenormalizationContextInterface $item_context
  ): array {
    $field_values = $item[static::ITEM_FIELDS_KEY] ?? [];
    $entity = $this
      ->denormalizeInlineEntity($field, $field_values, $item_context);

    return [
      static::ENTITY_PROPERTY => $entity,
    ];
  }

  /**
   * De-normalizes existing inline entity.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   * @param array $item
   *   The item to de-normalize into an entity.
   * @param int $entity_id
   *   The ID of the existing entity.
   * @param \Drupal\frontend_api\Rest\EntityReference\Denormalizer\InlineItemDenormalizationContextInterface $item_context
   *   The item de-normalization context.
   *
   * @return array
   *   The field item value with the referenced entity.
   *
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  protected function denormalizeExistingInlineEntityItem(
    FormDataFieldInterface $field,
    array $item,
    int $entity_id,
    InlineItemDenormalizationContextInterface $item_context
  ): array {
    if (!$item_context->hasReferencedEntity($entity_id)) {
      throw new DenormalizationException([
        '' => $this->t(
          "The referenced entity #@id doesn't belong to this host entity.",
          ['@id' => $entity_id]
        ),
      ]);
    }
    $entity = $item_context->getReferencedEntity($entity_id);

    $field_values = $item[static::ITEM_FIELDS_KEY] ?? [];
    $entity = $this
      ->denormalizeInlineEntity($field, $field_values, $item_context, $entity);

    $this->enforceInlineEntitySaving($field, $entity);
    $item_context->meetReferencedEntity($entity_id);

    return [
      static::ENTITY_PROPERTY => $entity,
    ];
  }

  /**
   * De-normalizes values of the inline entity form into an entity.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   * @param array $field_values
   *   The field values submitted from the form.
   * @param \Drupal\frontend_api\Rest\EntityReference\Denormalizer\InlineItemDenormalizationContextInterface $item_context
   *   The item de-normalization context.
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $entity
   *   The entity to de-normalize the values to or NULL to create one.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity with de-normalized values set to it.
   *
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  protected function denormalizeInlineEntity(
    FormDataFieldInterface $field,
    array $field_values,
    InlineItemDenormalizationContextInterface $item_context,
    ContentEntityInterface $entity = NULL
  ): ContentEntityInterface {
    $denormalizer = $this->denormalizerFactory
      ->create($item_context->getTargetTypeId(), $entity)
      ->setFormModeName($item_context->getFormModeName())
      ->setRequestData($field_values)
      ->setFormat($item_context->getFormat())
      ->setContext($item_context->getContext());

    if ($item_context->hasTargetBundleId()) {
      $denormalizer->setBundleId($item_context->getTargetBundleId());
    }
    if ($item_context->isValidationEnabled()) {
      $denormalizer->enableValidation();
    }

    $result = $denormalizer->denormalize();
    $result->throwErrors();

    return $result->getEntity();
  }

  /**
   * Returns the field item list checking that it supports inline entities.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   *
   * @return \Drupal\frontend_api\Field\EntityReference\InlineEditingAwareFieldItemListInterface
   *   The field item list.
   */
  protected function getInlineEditingFieldItemList(
    FormDataFieldInterface $field
  ): InlineEditingAwareFieldItemListInterface {
    $item_list = $field->getItemList();
    if (!$item_list instanceof InlineEditingAwareFieldItemListInterface) {
      $field_definition = $field->getFieldDefinition();
      throw new \LogicException(sprintf(
        'The %s normalizer requires the %s field of the %s entity to implement %s.',
        $this->getPluginId(),
        $field_definition->getName(),
        $field_definition->getTargetEntityTypeId(),
        InlineEditingAwareFieldItemListInterface::class
      ));
    }

    return $item_list;
  }

  /**
   * Makes sure that inline entity is saved when the main entity is saved.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   * @param \Drupal\Core\Entity\ContentEntityInterface $inline_entity
   *   The inline entity.
   */
  protected function enforceInlineEntitySaving(
    FormDataFieldInterface $field,
    ContentEntityInterface $inline_entity
  ): void {
    $this->getInlineEditingFieldItemList($field)
      ->addInlineUpdatedEntity($inline_entity);
  }

  /**
   * Makes sure that orphan inline entities are deleted on main entity saving.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   * @param \Drupal\frontend_api\Rest\EntityReference\Denormalizer\InlineItemDenormalizationContextInterface $item_context
   *   The item de-normalization context to get the list of orphan inline
   *   entities from.
   */
  protected function enforceOrphanInlineEntitiesDeletion(
    FormDataFieldInterface $field,
    InlineItemDenormalizationContextInterface $item_context
  ): void {
    $setting_key = static::DELETE_ORPHAN_ENTITIES_KEY;
    if ($setting_key === NULL) {
      return;
    }

    $should_delete = $this->getWidgetSetting($field, $setting_key);
    if (empty($should_delete)) {
      return;
    }

    $item_list = $this->getInlineEditingFieldItemList($field);
    foreach ($item_context->getReferencedEntities(TRUE) as $entity) {
      $item_list->addInlineDeletedEntity($entity);
    }
  }

}
