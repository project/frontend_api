<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;

/**
 * Provides field definition normalizer for the fields with options.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "options",
 *   field_types = {
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *   },
 *   widget_types = {
 *     "options_select",
 *     "chosen_select",
 *     "options_buttons",
 *     "address_country_default",
 *   }
 * )
 */
class ListOptionsNormalizer extends ListOptionsNormalizerBase {

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeField($field, $format, $context);

    $result[static::OPTIONS_KEY] = $this->normalizeOptions($field, $context);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function getOptionsPropertyName(
    DisplayFieldInterface $field
  ): string {
    return $this->getFieldMainPropertyName($field);
  }

  /**
   * {@inheritdoc}
   */
  protected function getOptionsWidgetCardinality(
    DisplayFieldInterface $field
  ): int {
    return $this->getFieldCardinality($field);
  }

  /**
   * {@inheritdoc}
   */
  protected function getWidgetType(
    DisplayFieldInterface $field,
    array $context = []
  ): ?string {
    return $this->getOptionsWidgetType($field);
  }

  /**
   * {@inheritdoc}
   */
  protected function validateDenormalizedValueLiteral(
    FormDataFieldInterface $field,
    ?array $value,
    array $context = []
  ): ?array {
    if ($value !== NULL) {
      $value = $this->validateValueLiteralOptions($field, $value, $context);
    }

    return $value;
  }

}
