<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Drupal\frontend_api\Rest\EntityReference\Denormalizer\InlineItemDenormalizationContextInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;

/**
 * Base normalizer for inline entity editing of a single bundle.
 *
 * The widget provides management of inline entities of a single bundle.
 */
abstract class FrontInlineSimpleEntityNormalizerBase extends FrontInlineEntityNormalizerBase implements ContainerFactoryPluginInterface, NormalizerAwareInterface {

  use EntityReferenceAutocreateNormalizerTrait;

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeField($field, $format, $context);

    $result['form'] = $this->normalizeInlineFormInfo($field, $format, $context);

    return $result;
  }

  /**
   * Normalizes information of the inline entity form.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   *
   * @return array
   *   The normalized form info.
   */
  protected function normalizeInlineFormInfo(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): array {
    $field_definition = $field->getFieldDefinition();
    $target_type_id = $this->entityReferenceSettingsReader
      ->getTargetEntityTypeId($field_definition);

    /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface $field */
    $form_mode_name = $this->getInlineEntityFormModeName($field);
    $bundle_id = $this->getInlineEntityBundleId($target_type_id, $field);

    $form_info = $this->formInfoBuilder
      ->build($target_type_id, $bundle_id, $form_mode_name);
    $result = $this->normalizer
      ->normalize($form_info, $format, $context);

    // Add form info as cacheable dependency after normalization, just in case
    // its metadata has been updated by the process.
    $this->addCacheableDependency($context, $form_info);

    return $result;
  }

  /**
   * Returns the bundle ID to use for the entity created from the inline form.
   *
   * @param string $target_type_id
   *   The entity type ID of the referenced entity.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface $field
   *   The field.
   *
   * @return string|null
   *   The bundle ID or NULL if entity doesn't have bundles or it's impossible
   *   to detect the one for entity creation.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getInlineEntityBundleId(
    string $target_type_id,
    FormFieldInterface $field
  ): ?string {
    $target_type = $this->entityTypeManager->getDefinition($target_type_id);

    if ($target_type->hasKey('bundle')) {
      // @todo That's probably not the best way, as it returns the bundle ID
      //   for auto-creation and we don't do that here.
      $bundle_id = $this->getAutocreateTargetBundleId($field);
    }
    else {
      $bundle_id = NULL;
    }

    return $bundle_id;
  }

  /**
   * {@inheritdoc}
   */
  protected function createItemDenormalizationContext(
    FormDataFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): InlineItemDenormalizationContextInterface {
    $item_context = parent::createItemDenormalizationContext(
      $field,
      $format,
      $context
    );

    // Specify fixed bundle ID for new entities, if necessary.
    $field_definition = $field->getFieldDefinition();
    $target_type_id = $this->entityReferenceSettingsReader
      ->getTargetEntityTypeId($field_definition);
    $target_bundle_id = $this->getInlineEntityBundleId($target_type_id, $field);
    $item_context->setTargetBundleId($target_bundle_id);

    return $item_context;
  }

}
