<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\frontend_api\Plugin\rest\resource\FileUploadResource;
use Drupal\frontend_api\Rest\Denormalizer\DenormalizerResult;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Drupal\frontend_api\Rest\File\FileNormalizer;
use Drupal\frontend_api\Rest\FrontWidgetTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Normalizer for reusable file upload widget.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "frontend_api_file_upload",
 *   field_types = {
 *     "file",
 *   },
 *   widget_types = {
 *     "frontend_api_file_upload",
 *   },
 * )
 *
 * @see \Drupal\frontend_api\Plugin\Field\FieldWidget\FrontFileUploadWidget
 */
class FrontFileUploadNormalizer extends DefaultNormalizer implements ContainerFactoryPluginInterface {

  use EntityReferenceNormalizerTrait;

  /**
   * {@inheritdoc}
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::FILE_UPLOAD;

  /**
   * The upload URL key in the normalized form info.
   */
  public const UPLOAD_URL_KEY = 'upload_url';

  /**
   * The max file size key in the normalized form info.
   */
  public const MAX_FILE_SIZE_KEY = 'max_file_size';

  /**
   * The REST resource ID the upload should be done to.
   */
  protected const UPLOAD_RESOURCE = FileUploadResource::PLUGIN_ID;

  /**
   * The HTTP method used for file upload.
   */
  protected const UPLOAD_HTTP_METHOD = Request::METHOD_POST;

  /**
   * The prefix of the REST resource route name.
   */
  public const RESOURCE_ROUTE_PREFIX = 'rest.';

  /**
   * The file ID key in the denormalized item data.
   */
  protected const FILE_ID_ITEM_KEY = FileNormalizer::ID_KEY;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file normalizer used for normalizing field items.
   *
   * @var \Drupal\frontend_api\Rest\File\FileNormalizerInterface
   */
  protected $fileNormalizer;

  /**
   * The file resource helper.
   *
   * @var \Drupal\frontend_api\Rest\File\FileResourceHelperInterface
   */
  protected $fileResourceHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->fileNormalizer = $container->get(
      'frontend_api.rest.file_normalizer'
    );
    $instance->fileResourceHelper = $container->get(
      'frontend_api.rest.file_resource_helper'
    );

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeField($field, $format, $context);

    $result[static::UPLOAD_URL_KEY] = $this->getFileUploadUrl($field, $context);
    $field_settings = $field->getFieldDefinition()
      ->getSettings();
    $result[static::MAX_FILE_SIZE_KEY] = $this->fileResourceHelper
      ->getMaxFileSize($field_settings);

    return $result;
  }

  /**
   * Returns the file upload URL.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   * @param array $context
   *   The normalization context.
   *
   * @return string
   *   The URL string.
   */
  protected function getFileUploadUrl(
    DisplayFieldInterface $field,
    array $context = []
  ): string {
    $field_definition = $field->getFieldDefinition();

    $route_name = 'rest.'
      . static::UPLOAD_RESOURCE
      . '.'
      . static::UPLOAD_HTTP_METHOD;
    $route_parameters = [
      'entity_type_id' => $field_definition->getTargetEntityTypeId(),
      'bundle' => $field_definition->getTargetBundle(),
      'field_name' => $field_definition->getName(),
    ];

    $generated_url = Url::fromRoute($route_name, $route_parameters)
      ->toString(TRUE);
    $this->addCacheableDependency($context, $generated_url);

    return $generated_url->getGeneratedUrl();
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeValueLiteral(
    DisplayFieldInterface $field,
    array $value
  ) {
    $file_id_deltas_map = $this->getEntityReferenceIdDeltasMap($field, $value);
    if (empty($file_id_deltas_map)) {
      return NULL;
    }

    $target_type_id = $this->getEntityReferenceTargetTypeId($field);
    $target_column = $this->getEntityReferenceTargetColumn($field);
    $files = $this->entityTypeManager
      ->getStorage($target_type_id)
      ->loadMultiple(array_keys($file_id_deltas_map));
    if (empty($files)) {
      return NULL;
    }

    $result = [];
    foreach ($value as $item) {
      $file_id = $item[$target_column] ?? NULL;
      if (empty($file_id)) {
        continue;
      }

      /** @var \Drupal\file\FileInterface|null $file */
      $file = $files[$file_id] ?? NULL;
      if ($file === NULL) {
        continue;
      }

      $result[] = $this->fileNormalizer->normalizeFile($file);
    }
    if (empty($result)) {
      return NULL;
    }

    return $this->flattenValueLiteralItemList($field, $result);
  }

  /**
   * {@inheritdoc}
   */
  protected function denormalizeValueLiteral(
    FormDataFieldInterface $field,
    $value,
    string $format = NULL,
    array $context = []
  ) {
    if ($value === NULL) {
      return NULL;
    }
    $value = $this->roughenValueLiteralItemList($field, $value);

    $result = [];
    $errors = new DenormalizerResult();
    $column_name = $this->getFieldMainPropertyName($field);
    foreach ($value as $delta => $item) {
      $file_id = $item[static::FILE_ID_ITEM_KEY] ?? NULL;
      if ($file_id === NULL || !is_numeric($file_id)) {
        $errors->addErrors([
          $delta => $this->t('The file ID is required and must be numeric.'),
        ]);
        continue;
      }

      $result[] = [
        $column_name => $file_id,
      ];
    }
    if ($errors->hasErrors()) {
      $field_name = $field->getFieldDefinition()
        ->getName();
      $errors->throwErrors($field_name);
    }

    return $result;
  }

}
