<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides form field normalizer for the boolean checkbox.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "boolean_checkbox",
 *   field_types = {
 *     "boolean",
 *   },
 *   widget_types = {
 *     "boolean_checkbox",
 *   },
 * )
 */
class BooleanCheckboxNormalizer extends BooleanCheckboxNormalizerBase {

  /**
   * The widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::CHECKBOX;

  /**
   * {@inheritdoc}
   */
  protected function getBooleanValuePropertyName(
    DisplayFieldInterface $field
  ): string {
    return $this->getFieldMainPropertyName($field);
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeField($field, $format, $context);
    $result = $this->addCheckboxLabels($field, $result);
    return $result;
  }

}
