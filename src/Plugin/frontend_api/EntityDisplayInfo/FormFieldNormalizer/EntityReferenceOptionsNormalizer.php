<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides field definition normalizer for the entity reference with options.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "entity_reference_options",
 *   field_types = {
 *     "entity_reference",
 *   },
 *   widget_types = {
 *     "options_select",
 *     "chosen_select"
 *   }
 * )
 */
class EntityReferenceOptionsNormalizer extends ListOptionsNormalizer implements ContainerFactoryPluginInterface {

  use EntityReferenceSelectionNormalizerTrait;

  /**
   * The entity reference options provider.
   *
   * @var \Drupal\frontend_api\Field\ListOptions\OptionsProviderInterface
   */
  protected $optionsProvider;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->selectionManager = $container->get(
      'plugin.manager.entity_reference_selection'
    );
    $instance->optionsProvider = $container->get(
      'frontend_api.field.entity_reference_options_provider'
    );

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAllowedValues(
    DisplayFieldInterface $field,
    FieldableEntityInterface $entity,
    array $context = []
  ): array {
    $metadata = new CacheableMetadata();
    $field_definition = $field->getFieldDefinition();
    $options = $this->optionsProvider
      ->getOptions($field_definition, $entity, $metadata);
    $this->addCacheableDependency($context, $metadata);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeValueLiteral(
    DisplayFieldInterface $field,
    array $value
  ) {
    $value = $this->filterValueLiteralForReferenceableEntities($field, $value);
    return parent::normalizeValueLiteral($field, $value);
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\Core\Entity\Plugin\Validation\Constraint\ValidReferenceConstraintValidator
   */
  protected function validateDenormalizedValueLiteral(
    FormDataFieldInterface $field,
    ?array $value,
    array $context = []
  ): ?array {
    // Override the parent method with no validation, as constraint does the
    // actual validation and it's not necessary to duplicate it here.
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  protected function denormalizeValueLiteral(
    FormDataFieldInterface $field,
    $value,
    string $format = NULL,
    array $context = []
  ) {
    $denormalized_values = parent::denormalizeValueLiteral($field, $value);
    $main_property = $this->getFieldMainPropertyName($field);

    // Filter empty values from demormalized array.
    foreach ($denormalized_values as $key => $item) {
      if (empty($item[$main_property])) {
        unset($denormalized_values[$key]);
      }
    }

    return $denormalized_values;
  }

}
