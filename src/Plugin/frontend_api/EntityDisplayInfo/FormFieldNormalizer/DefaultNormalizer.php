<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\frontend_api\Exception\DenormalizationException;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldNormalizerInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormInfoInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Drupal\frontend_api\Rest\FieldConstraint\FieldConstraintList;
use Drupal\frontend_api\Rest\FieldConstraint\FieldConstraintListInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;

/**
 * Provides default field definition normalizer plugin.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "default",
 *   priority = -100,
 *   field_types = {
 *     "*"
 *   },
 *   widget_types = {
 *     "*"
 *   }
 * )
 */
class DefaultNormalizer extends DisplayFieldNormalizerBase implements FormFieldNormalizerInterface, NormalizerAwareInterface {

  use StringTranslationTrait,
    NormalizerAwareTrait;

  /**
   * Map of the value matching type to a machine name for the normalization.
   *
   * @TODO NOW This looks like a dependency.
   */
  public const CONDITION_VALUES_SET_MAP = [
    CONDITIONAL_FIELDS_DEPENDENCY_VALUES_WIDGET => 'equal',
    CONDITIONAL_FIELDS_DEPENDENCY_VALUES_AND => 'and',
    CONDITIONAL_FIELDS_DEPENDENCY_VALUES_OR => 'or',
    CONDITIONAL_FIELDS_DEPENDENCY_VALUES_XOR => 'xor',
    CONDITIONAL_FIELDS_DEPENDENCY_VALUES_NOT => 'not',
    CONDITIONAL_FIELDS_DEPENDENCY_VALUES_REGEX => 'regex',
  ];

  /**
   * The constraints key in the normalized field result.
   */
  public const CONSTRAINTS_KEY = 'constraints';

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface $field */
    $field_definition = $field->getFieldDefinition();
    $widget_data = $field->getComponentData();

    $result = [
      'label' => $this->getFieldLabel($field),
      'description' => $field_definition->getDescription(),
      'required' => $this->isFieldRequired($field),
      'readonly' => $this->isFieldReadOnly($field),
      'field_type' => $field_definition->getType(),
      'cardinality' => $this->getFieldCardinality($field),
      'weight' => $widget_data['weight'],
    ] + $this->getThirdPartySettings($field);

    $main_property_name = $this->getFieldMainPropertyName($field);
    if (isset($main_property_name)) {
      $result['main_property'] = $main_property_name;
    }

    $widget_type = $this->getWidgetType($field, $context);
    if (isset($widget_type)) {
      $result[static::WIDGET_TYPE_KEY] = $widget_type;
    }

    $default_value = $this->getDefaultValue($field);
    if (isset($default_value)) {
      $result['default_value'] = $default_value;
    }

    $constraints = $this->normalizeConstraints($field, $format, $context);
    if (isset($constraints)) {
      $result[static::CONSTRAINTS_KEY] = $constraints;
    }

    return $result;
  }

  /**
   * Creates a field constraint list wrapper for the field.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface $field
   *   The field.
   *
   * @return \Drupal\frontend_api\Rest\FieldConstraint\FieldConstraintListInterface|null
   *   The constraints list wrapper or NULL in case there are no constraints on
   *   the field.
   */
  protected function createFieldConstraintList(
    FormFieldInterface $field
  ): ?FieldConstraintListInterface {
    $constraints = $field->getConstraints();
    if (empty($constraints)) {
      return NULL;
    }

    return new FieldConstraintList($field, $constraints);
  }

  /**
   * Normalizes the field constraints.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface $field
   *   The field.
   * @param string|null $format
   *   The format.
   * @param array $context
   *   The context.
   *
   * @return array|null
   *   The normalized list of constraints or NULL to hide it completely from the
   *   normalized result.
   */
  protected function normalizeConstraints(
    FormFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): ?array {
    $constraint_list = $this->createFieldConstraintList($field);
    if ($constraint_list === NULL) {
      return NULL;
    }

    return $this->normalizer->normalize($constraint_list, $format, $context);
  }

  /**
   * Provides the field label.
   *
   * Returns overridden label from form mode (if it was), otherwise
   * takes it from field definition.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   Field to extract data.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   The field label.
   */
  protected function getFieldLabel(
    DisplayFieldInterface $field
  ) {
    $widget_data = $field->getComponentData();
    $widget_label = $widget_data['third_party_settings']['field_widget_label']['label'] ?? NULL;
    if ($widget_label !== NULL) {
      return $widget_label;
    }

    return $field->getFieldDefinition()
      ->getLabel();
  }

  /**
   * Get additional form field settings.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   Field to extract data.
   *
   * @return array
   *   List of settings.
   *
   * @TODO NOW Move to Conditional Fields module.
   */
  protected function getThirdPartySettings(DisplayFieldInterface $field) : array {
    /*
     * Right now added only conditional fields, we can add more later.
     */
    $component_data = $field->getComponentData();
    $third_party_settings = $component_data['third_party_settings'];
    $result = [
      'conditional_fields' => $this->getConditionalFieldSettings(
        $field,
        $third_party_settings
      ),
    ];

    return $result;
  }

  /**
   * Checks if field is required.
   *
   * Performs check both in field definition and in third party settings.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface $field
   *   Field to extract data.
   *
   * @return bool
   *   Result whether field is required.
   */
  protected function isFieldRequired(FormFieldInterface $field) {
    // @todo Use WidgetHelper for this check.
    $field_definition = $field->getFieldDefinition();
    if ($field_definition->isRequired()) {
      return TRUE;
    }

    if ($field->isForcedRequired()) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Checks if the field is read-only.
   *
   * Read-only fields ignore submitted data.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface $field
   *   The field to check.
   *
   * @return bool
   *   TRUE if the field is read-only.
   */
  protected function isFieldReadOnly(FormFieldInterface $field): bool {
    return $field->isForcedReadOnly();
  }

  /**
   * Returns normalized settings for the conditions of the field.
   *
   * !!!Important!!! Conditional fields module stores its settings on a default
   * display, so you must manually sync it after any UI changes to the
   * corresponding form mode config.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   Field to extract data.
   * @param array $third_party_settings
   *   Third party settings of the field.
   *
   * @return array
   *   The normalized settings of the conditional fields.
   */
  protected function getConditionalFieldSettings(
    DisplayFieldInterface $field,
    array $third_party_settings
  ): array {
    $result = [];
    $conditional_fields = $third_party_settings['conditional_fields'] ?? [];
    foreach ($conditional_fields as $condition) {
      $result[] = $this
        ->normalizeFieldCondition($field, $condition);
    }

    return $result;
  }

  /**
   * Normalizes a single field condition.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field condition is specified for.
   * @param array $condition
   *   The condition data.
   *
   * @return array
   *   The normalized condition.
   */
  protected function normalizeFieldCondition(
    DisplayFieldInterface $field,
    array $condition
  ): array {
    $settings = $condition['settings'] ?? [];
    $condition_type = ltrim($settings['condition'], '!');

    $item = [
      'depends' => $condition['dependee'],
      'state' => $settings['state'],
      'condition' => $settings['condition'],
      'grouping' => $settings['grouping'],
    ];

    if ($condition_type === 'value') {
      // @todo Support other cases, e.g. regex or specific value.
      // @see conditional_fields_form_after_build()
      $source_setting = $settings['values'] ?? NULL;
      if (is_string($source_setting)) {
        $item['values'] = explode("\r\n", $settings['values']);
      }

      $source_setting = $settings['values_set'] ?? NULL;
      if (!empty($source_setting)) {
        $values_set_map = static::CONDITION_VALUES_SET_MAP;
        $normalized_setting = $values_set_map[$source_setting] ?? NULL;

        if (isset($normalized_setting)) {
          $item['values_set'] = $normalized_setting;
        }

        $applicable_values = $settings['value_form'] ?? NULL;
        if (!isset($applicable_values)) {
          return $item;
        }
        $dependee_field_name = $item['depends'];
        $main_property = $this->getDependeeFieldMainProperty(
          $field,
          $dependee_field_name
        );

        if (!empty($main_property)) {
          $item['applicable_values'] = array_column(
            $applicable_values,
            $main_property
          );
        }
      }
    }

    return $item;
  }

  /**
   * Returns main property of field current one depends on.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   Currently normalized field.
   * @param string $dependee_field_name
   *   Name of the field current one depends on.
   *
   * @return string|null
   *   Main property of dependee field or null if it is disabled in form mode.
   */
  protected function getDependeeFieldMainProperty(
    DisplayFieldInterface $field,
    string $dependee_field_name
  ): ?string {
    $display_info = $field->getDisplayInfo();
    $fields = $display_info->getFields();
    $dependee_field = $fields[$dependee_field_name] ?? NULL;
    if (!$dependee_field) {
      return NULL;
    }
    $dependee_field_storage_definition = $dependee_field->getFieldDefinition()
      ->getFieldStorageDefinition();
    return $dependee_field_storage_definition->getMainPropertyName();
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldValue(
    FormDataFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $value = $this->getValueLiteral($field, $context);
    if ($value === NULL) {
      return NULL;
    }

    return $this->normalizeValueLiteral($field, $value);
  }

  /**
   * Returns value literal extracted from the field item list.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field to get the item list from.
   * @param array $context
   *   The context.
   *
   * @return array|null
   *   The value literal or NULL in case the field is empty.
   */
  protected function getValueLiteral(
    FormDataFieldInterface $field,
    array $context = []
  ): ?array {
    $items_list = $field->getItemList();
    if ($items_list->isEmpty()) {
      return NULL;
    }

    return $items_list->getValue();
  }

  /**
   * Returns default value of the field.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   *
   * @return mixed|null
   *   Default value or NULL in case there is no default value.
   *
   * @todo Call getDefaultValue() on field definition instead.
   */
  protected function getDefaultValue(DisplayFieldInterface $field) {
    $field_definition = $field->getFieldDefinition();

    $default = $field_definition->getDefaultValueLiteral();
    if (empty($default)) {
      return NULL;
    }

    return $this->normalizeValueLiteral($field, $default);
  }

  /**
   * {@inheritdoc}
   */
  public function supportsDenormalization(
    FormDataFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function denormalizeFieldValue(
    FormDataFieldInterface $field,
    array $data,
    string $format = NULL,
    array $context = []
  ): void {
    if ($this->isFieldReadOnly($field)) {
      // Read-only widget should never set the value, even if it's submitted.
      return;
    }

    $field_name = $field->getFieldDefinition()
      ->getName();
    if (array_key_exists($field_name, $data)) {
      $value = $data[$field_name];
      $value = $this->acceptFieldInput($field, $value);
      $value = $this
        ->denormalizeValueLiteral($field, $value, $format, $context);
      $value = $this->validateDenormalizedValueLiteral($field, $value, $context);
      $this->setDenormalizedFieldValue($field, $value);
    }

    if ($this->isFieldRequired($field)) {
      $this->checkRequiredValue($field);
    }
  }

  /**
   * Initial check on field input value.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   * @param mixed $value
   *   Field value.
   *
   * @return mixed
   *   Field value if no errors occurred.
   *
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  protected function acceptFieldInput(FormDataFieldInterface $field, $value) {
    $field_name = $field->getFieldDefinition()->getName();
    $cardinality = $this->getFieldCardinality($field);
    if ($cardinality !== 1) {
      if (!is_array($value)) {
        throw new DenormalizationException([
          $field_name => $this->t('Not valid item format.'),
        ]);
      }
    }

    return $value;
  }

  /**
   * Validates the denormalized value literal of the field.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   * @param array|null $value
   *   The de-normalized value literal.
   * @param array $context
   *   The de-normalizer context.
   *
   * @return array|null
   *   The filtered value literal.
   */
  protected function validateDenormalizedValueLiteral(
    FormDataFieldInterface $field,
    ?array $value,
    array $context = []
  ): ?array {
    // Do nothing by default.
    return $value;
  }

  /**
   * Sets de-normalized value to the field.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field to set the value to.
   * @param mixed $value
   *   The value to set.
   */
  protected function setDenormalizedFieldValue(
    FormDataFieldInterface $field,
    $value
  ): void {
    $item_list = $field->getItemList();
    $item_list->setValue([]);

    // Avoid warning just in case child class produced NULL instead of an empty
    // array.
    if (!isset($value)) {
      return;
    }

    foreach ($value as $item_value) {
      $item_list->appendItem()
        ->setValue($item_value);
    }
  }

  /**
   * De-normalizes the value literal.
   *
   * The value passed is the one received from the request body after decoding.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The form data field.
   * @param mixed $value
   *   The value retrieved from the request body.
   * @param string $format
   *   The format.
   * @param array $context
   *   The context.
   *
   * @return array
   *   The de-normalized value ready to be set on the item list.
   */
  protected function denormalizeValueLiteral(
    FormDataFieldInterface $field,
    $value,
    string $format = NULL,
    array $context = []
  ) {
    $value = $this->roughenValueLiteralItemList($field, $value);
    $value = $this->roughenValueLiteralItems($field, $value);

    return $value;
  }

  /**
   * Restores flattened value of every field item, if necessary.
   *
   * In case of a single-property field, it wraps the value with an array
   * having the value under the main property key.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field the value is restored for.
   * @param array $values
   *   Value to restore.
   *
   * @return array
   *   The restored value.
   */
  protected function roughenValueLiteralItems(
    FormDataFieldInterface $field,
    array $values
  ) {
    $main_property = $this->getFieldMainPropertyName($field);
    if (!isset($main_property)) {
      return $values;
    }

    $result = [];
    foreach ($values as $delta => $value) {
      // The app may send non-flattened data already, we shouldn't do anything
      // on such items.
      if (!is_array($value)) {
        $value = [$main_property => $value];
      }

      $result[$delta] = $value;
    }

    return $result;
  }

  /**
   * Restores flattened field item list, if applicable.
   *
   * Value of a single-value field is wrapped with an array.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   * @param mixed $value
   *   Possibly flatten value to restore.
   *
   * @return array
   *   The restored value.
   */
  protected function roughenValueLiteralItemList(
    FormDataFieldInterface $field,
    $value
  ) {
    $cardinality = $this->getFieldCardinality($field);
    if ($cardinality !== 1) {
      return $value;
    }

    // Don't create an item if the value is NULL.
    if (!isset($value)) {
      return [];
    }

    return [
      0 => $value,
    ];
  }

  /**
   * Checks field value on emptiness if field is required.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   Field which value is checked.
   */
  protected function checkRequiredValue(FormDataFieldInterface $field): void {
    $is_empty = $field->getItemList()
      ->isEmpty();
    if ($is_empty) {
      $definition = $field->getFieldDefinition();
      $field_name = $definition->getName();
      $field_label = $definition->getLabel();
      throw new DenormalizationException([
        $field_name => $this->t(
          "Field '@label' is required",
          ['@label' => $field_label]
        ),
      ]);
    }
  }

  /**
   * Returns the field entity.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field to extract the value from.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface
   *   The entity or its sample.
   *
   * @throws \InvalidArgumentException
   */
  protected function getFieldEntity(
    DisplayFieldInterface $field
  ): FieldableEntityInterface {
    if ($field instanceof FormDataFieldInterface) {
      return $field->getItemList()
        ->getEntity();
    }

    $display_info = $field->getDisplayInfo();
    if ($display_info instanceof FormInfoInterface) {
      if ($display_info->hasEntity()) {
        return $display_info->getEntity();
      }

      return $display_info->getSampleEntity();
    }

    throw new \InvalidArgumentException(sprintf(
      'Unable to get entity for the %s field.',
      $field->getFieldDefinition()->getName()
    ));
  }

  /**
   * Returns the value of the specified widget setting.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   * @param array|string $key
   *   The setting key.
   *
   * @return mixed|null
   *   The setting value or NULL in case the value doesn't exist.
   */
  protected function getWidgetSetting(
    DisplayFieldInterface $field,
    $key
  ) {
    $settings = $field->getComponentData()['settings'] ?? [];
    return NestedArray::getValue($settings, (array) $key);
  }

}
