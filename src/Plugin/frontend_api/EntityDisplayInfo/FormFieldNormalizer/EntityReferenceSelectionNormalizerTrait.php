<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;

/**
 * Trait for normalizers of the entity reference selection widgets.
 */
trait EntityReferenceSelectionNormalizerTrait {

  use EntityReferenceNormalizerTrait;

  /**
   * The entity reference selection plugin manager.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected $selectionManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Returns the target entity type definition of the reference.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   *   The target entity type.
   *
   * @throws \InvalidArgumentException
   */
  protected function getEntityReferenceTargetType(
    DisplayFieldInterface $field
  ): EntityTypeInterface {
    $entity_type_id = $this->getEntityReferenceTargetTypeId($field);
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    if (!isset($entity_type)) {
      throw new \InvalidArgumentException(sprintf(
        'The %s target entity type of the %s field is not defined.',
        $entity_type_id,
        $field->getFieldDefinition()
          ->getName()
      ));
    }

    return $entity_type;
  }

  /**
   * Returns selections handler for entity reference field.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The entity display info field.
   * @param \Drupal\Core\Entity\FieldableEntityInterface|null $entity
   *   Current entity or its sample. Pass NULL to get entity from the field.
   *
   * @return \Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface
   *   Selection handler instance.
   */
  protected function getSelectionHandler(
    DisplayFieldInterface $field,
    FieldableEntityInterface $entity = NULL
  ) {
    if (!isset($entity)) {
      $entity = $this->getFieldEntity($field);
    }

    $field_definition = $field->getFieldDefinition();
    $selection_handler = $this->selectionManager
      ->getSelectionHandler($field_definition, $entity);

    return $selection_handler;
  }

  /**
   * Returns selection handler settings found on the field definition.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   *
   * @return array
   *   The selection handler settings.
   */
  protected function getSelectionHandlerSettings(
    DisplayFieldInterface $field
  ): array {
    $settings = $field->getFieldDefinition()
      ->getSetting('handler_settings');
    return $settings ?? [];
  }

  /**
   * Returns referenceable entities found in the items of the value literal.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   * @param array $value
   *   The value literal. It's an array of items, every item has property data.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   The entity list. Keys are entity IDs, not item deltas.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getReferenceableEntitiesOfValueLiteral(
    DisplayFieldInterface $field,
    array $value
  ): array {
    $column_name = $this->getEntityReferenceTargetColumn($field);
    $ids = array_column($value, $column_name);

    $entity = $this->getFieldEntity($field);
    $selection_handler = $this->getSelectionHandler($field, $entity);
    $ids = $selection_handler->validateReferenceableEntities($ids);

    $entity_type_id = $this->getEntityReferenceTargetTypeId($field);
    return $this->entityTypeManager
      ->getStorage($entity_type_id)
      ->loadMultiple($ids);
  }

  /**
   * Filters value literal for the ones that refer to valid entities.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   * @param array $value
   *   The value literal. It's an array of items, every item has property data.
   *
   * @return array
   *   The filtered value literal with deltas re-indexed.
   *
   * @throws \InvalidArgumentException
   */
  protected function filterValueLiteralForReferenceableEntities(
    DisplayFieldInterface $field,
    array $value
  ): array {
    $id_delta_map = $this->getEntityReferenceIdDeltasMap($field, $value);
    if (empty($id_delta_map)) {
      return [];
    }

    $entity = $this->getFieldEntity($field);
    $selection_handler = $this->getSelectionHandler($field, $entity);
    $ids = array_keys($id_delta_map);
    $valid_ids = $selection_handler->validateReferenceableEntities($ids);
    if (empty($valid_ids)) {
      return [];
    }

    $result = [];
    foreach ($valid_ids as $valid_id) {
      foreach ($id_delta_map[$valid_id] ?? [] as $delta) {
        $result[$delta] = $value[$delta];
      }
    }

    // Keep results sorted by delta.
    ksort($result, SORT_NUMERIC);

    // Reindex filtered items to keep it an array.
    return array_values($result);
  }

  /**
   * Returns target entity bundles.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The entity reference field.
   *
   * @return array
   *   The bundles.
   */
  protected function getEntityReferenceTargetBundles(
    DisplayFieldInterface $field
  ): array {
    $handler_settings = $this->getSelectionHandlerSettings($field);
    return $handler_settings['target_bundles'];
  }

}
