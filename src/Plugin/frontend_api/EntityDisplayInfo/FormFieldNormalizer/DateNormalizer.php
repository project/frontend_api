<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;

/**
 * Provides field definition normalizer for the date & time fields.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "datetime",
 *   field_types = {
 *     "datetime",
 *   },
 *   widget_types = {
 *     "*",
 *     "datetime_default",
 *   },
 * )
 */
class DateNormalizer extends DefaultNormalizer {

  /**
   * Date storage type setting.
   */
  public const DATE_TIME_TYPE_SETTING = 'datetime_type';

  /**
   * Maps date storage with it's callback method.
   */
  public const DATE_TYPE_CALLBACK_MAP = [
    'now' => 'static::getCurrentDateString',
    'relative' => 'static::getRelativeDateString',
  ];

  /**
   * Field properties with their settings map.
   *
   * The content structure is:
   *   - first level keys are property names,
   *   - second level keys are setting keys we need,
   *   - second level values are setting keys the field uses for the property.
   */
  public const FIELD_PROPS_SETTINGS_MAP = [
    'value' => [
      'type' => 'default_date_type',
      'value' => 'default_date',
    ],
  ];

  /**
   * {@inheritdoc}
   */
  protected function getWidgetType(
    DisplayFieldInterface $field,
    array $context = []
  ): ?string {
    $field_definition = $field->getFieldDefinition();
    $storage_definition = $field_definition->getFieldStorageDefinition();
    $datetime_type = $storage_definition
      ->getSetting(static::DATE_TIME_TYPE_SETTING);
    return $datetime_type;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultValue(DisplayFieldInterface $field) {
    $field_definition = $field->getFieldDefinition();

    $default = $field_definition->getDefaultValueLiteral();
    if (empty($default)) {
      return NULL;
    }

    $default_value = [];
    foreach ($default as $delta => $default_settings) {
      $default_value[$delta] = $this->calculateValueFromSettings($default_settings);
    }

    return $this->normalizeValueLiteral($field, $default_value);
  }

  /**
   * Calculates default date value from field settings.
   *
   * @param array $default_settings
   *   The field settings.
   *
   * @return array
   *   Array in "date prop => calculated date" format.
   */
  protected function calculateValueFromSettings(
    array $default_settings
  ): array {
    $default_value = [];
    foreach (static::FIELD_PROPS_SETTINGS_MAP as $prop => $settings) {
      $type_key = $settings['type'];
      $date_type = $default_settings[$type_key] ?? NULL;
      if (!isset(static::DATE_TYPE_CALLBACK_MAP[$date_type])) {
        continue;
      }

      $value_key = $settings['value'];
      $default_date_value = $default_settings[$value_key];
      $value = call_user_func_array(
        static::DATE_TYPE_CALLBACK_MAP[$date_type],
        [$default_date_value]
      );
      $default_value[$prop] = $value;
    }

    return $default_value;
  }

  /**
   * Gets current date, formatted as string.
   *
   * @return string
   *   The formatted current date.
   */
  protected function getCurrentDateString(): string {
    $date = new DrupalDateTime();
    return $date->format('Y-m-d');
  }

  /**
   * Returns formatted current date + offset.
   *
   * @param string $offset
   *   The date offset.
   *
   * @return string
   *   Current date + offset.
   */
  protected function getRelativeDateString(string $offset): string {
    $relative_date = new DrupalDateTime($offset);

    return $relative_date->format('Y-m-d');
  }

}
