<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\search_api_autocomplete\SearchInterface;
use Drupal\frontend_api\Plugin\Field\FieldWidget\FrontEmbeddedEntityAutocompleteWidgetBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Drupal\frontend_api\Dictionary\EntityTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base normalizer for Search API based entity autocomplete widgets.
 *
 * @see \Drupal\frontend_api\Plugin\Field\FieldWidget\SearchApiEntityAutocompleteWidgetBase
 */
abstract class SearchApiEntityAutocompleteNormalizerBase extends DefaultNormalizer implements ContainerFactoryPluginInterface {

  use EntityReferenceSelectionNormalizerTrait;

  /**
   * Autocomplete display setting.
   */
  protected const VIEW_DISPLAY_SETTING = FrontEmbeddedEntityAutocompleteWidgetBase::VIEW_DISPLAY_SETTING;

  /**
   * Autocomplete filter setting.
   */
  protected const VIEW_FILTER_SETTING = FrontEmbeddedEntityAutocompleteWidgetBase::VIEW_FILTER_SETTING;

  /**
   * Min length option of the Search API autocomplete entity.
   */
  protected const MIN_LENGTH_OPTION = 'min_length';

  /**
   * The route name of the REST resource that provides autocomplete suggestions.
   *
   * Must be overridden in child classes.
   */
  protected const AUTOCOMPLETE_ROUTE_NAME = NULL;

  /**
   * The Search API autocomplete entity type ID.
   */
  protected const AUTOCOMPLETE_ENTITY_TYPE = EntityTypes::SEARCH_API_AUTOCOMPLETE;

  /**
   * The entity reference field settings reader.
   *
   * @var \Drupal\frontend_api\Field\EntityReference\EntityReferenceSettingsReaderInterface
   */
  protected $fieldSettingsReader;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->selectionManager = $container->get(
      'plugin.manager.entity_reference_selection'
    );
    $instance->fieldSettingsReader = $container->get(
      'frontend_api.field.entity_reference.settings_reader'
    );

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldValue(
    FormDataFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $item_list = $field->getItemList();
    if ($item_list->isEmpty()) {
      return NULL;
    }

    return $this->normalizeFieldItemList($field, $item_list, $format, $context);
  }

  /**
   * Prepares field item normalization once before iterating over items.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field info.
   * @param array $context
   *   The context that is later passed to every item normalization.
   *
   * @see normalizeEntityReferenceFieldItem()
   */
  protected function prepareFieldItemNormalization(
    FormDataFieldInterface $field,
    array &$context
  ): void {
    // Do nothing by default.
  }

  /**
   * Normalizes a single field item.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field info.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity referenced from the normalized item.
   * @param array $item_value
   *   The normalized field item value.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   *
   * @return mixed
   *   The normalized item.
   *
   * @see prepareFieldItemNormalization()
   */
  abstract protected function normalizeEntityReferenceFieldItem(
    FormDataFieldInterface $field,
    ContentEntityInterface $entity,
    array $item_value,
    string $format = NULL,
    array $context = []
  );

  /**
   * Normalizes the field item list.
   *
   * Child classes could use it to change the literal value passed to the
   * flattening process.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The entity view info field.
   * @param \Drupal\Core\Field\FieldItemListInterface $item_list
   *   The item list of a field.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   *
   * @return mixed
   *   The normalization result.
   *
   * @throws \Exception
   */
  protected function normalizeFieldItemList(
    FormDataFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $this->prepareFieldItemNormalization($field, $context);

    $values = $item_list->getValue();
    $id_deltas_map = $this->getEntityReferenceIdDeltasMap($field, $values);
    $entities = $this->getReferenceableEntitiesOfValueLiteral($field, $values);

    $result = [];
    foreach ($entities as $entity_id => $entity) {
      $deltas = $id_deltas_map[$entity_id] ?? [];
      foreach ($deltas as $delta) {
        $result[$delta] = $this->normalizeEntityReferenceFieldItem(
          $field,
          $entity,
          $values[$delta],
          $format,
          $context
        );
      }
    }

    // Keep results sorted by delta.
    ksort($result, SORT_NUMERIC);

    // Reindex filtered items to keep it serialized as an array.
    $result = array_values($result);

    // Make sure we provide cacheable metadata, as we use embedded entities
    // here.
    $this->addItemListCacheMetadata($field, $item_list, $context);

    return $result;
  }

  /**
   * Adds cache metadata of the field item list to the normalization context.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field info.
   * @param \Drupal\Core\Field\FieldItemListInterface $item_list
   *   The field item list.
   * @param array $context
   *   The normalization context.
   */
  protected function addItemListCacheMetadata(
    FormDataFieldInterface $field,
    FieldItemListInterface $item_list,
    array $context = []
  ): void {
    $entity_ids = array_column($item_list->getValue(), 'target_id');
    if (empty($entity_ids)) {
      return;
    }

    $field_definition = $field->getFieldDefinition();
    $entity_type_id = $this->fieldSettingsReader
      ->getTargetEntityTypeId($field_definition);

    $cache_tags = [];
    foreach ($entity_ids as $entity_id) {
      $cache_tags[$entity_type_id . ':' . $entity_id] = TRUE;
    }
    $this->addCacheTags($context, array_keys($cache_tags));
  }

  /**
   * Loads the Search API autocomplete entity using passed widget settings.
   *
   * @param array $widget_settings
   *   The widget settings.
   *
   * @return \Drupal\search_api_autocomplete\SearchInterface
   *   The Search API autocomplete entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @see \Drupal\search_api_autocomplete\Plugin\search_api_autocomplete\search\ViewsDeriver::getDerivativeDefinitions()
   */
  protected function getAutocompleteEntity(
    array $widget_settings
  ): SearchInterface {
    // Search API Autocomplete uses view ID as autocomplete ID.
    list($view_id) = $this->getAutocompleteViewDisplayIds($widget_settings);
    $autocomplete = $this->entityTypeManager
      ->getStorage(static::AUTOCOMPLETE_ENTITY_TYPE)
      ->load($view_id);
    if (!$autocomplete instanceof SearchInterface) {
      throw new \LogicException(sprintf(
        'The %s Search API autocomplete not found.',
        $view_id
      ));
    }

    return $autocomplete;
  }

  /**
   * Returns minimal input length that should trigger autocomplete.
   *
   * @param array $widget_settings
   *   The widget settings.
   *
   * @return int
   *   The minimal length of the input string.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getAutocompleteMinInputLength(
    array $widget_settings
  ): int {
    $autocomplete = $this->getAutocompleteEntity($widget_settings);
    return $autocomplete->getOption(static::MIN_LENGTH_OPTION) ?? 1;
  }

  /**
   * Returns view and its display IDs set on the widget settings.
   *
   * @param array $widget_settings
   *   The widget settings.
   *
   * @return array
   *   An array with exactly two items:
   *   - view ID,
   *   - view display ID.
   */
  protected function getAutocompleteViewDisplayIds(
    array $widget_settings
  ): array {
    $autocomplete_display_setting =
      $widget_settings[static::VIEW_DISPLAY_SETTING];
    return explode(':', $autocomplete_display_setting);
  }

  /**
   * Builds route parameters for the autocomplete route.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field info.
   * @param array $widget_settings
   *   The widget settings.
   *
   * @return array
   *   The route parameters.
   */
  protected function buildAutocompleteRouteParameters(
    DisplayFieldInterface $field,
    array $widget_settings
  ): array {
    list($view_id, $display_id) = $this
      ->getAutocompleteViewDisplayIds($widget_settings);
    $autocomplete_filter = $widget_settings[static::VIEW_FILTER_SETTING];

    return [
      'search_api_autocomplete_search' => $view_id,
      'display' => $display_id,
      'filter' => $autocomplete_filter,
    ];
  }

}
