<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\frontend_api\Dictionary\Field\EntityReference\SelectionHandlerSettings;
use Drupal\frontend_api\Plugin\Field\FieldWidget\FrontEntityAutocompleteQueryWidget;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;

/**
 * Provides normalizer for entity autocomplete field with query parameters.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "frontend_api_entity_autocomplete_query",
 *   field_types = {
 *     "entity_reference",
 *   },
 *   widget_types = {
 *     "frontend_api_entity_autocomplete_query",
 *   },
 * )
 *
 * @see \Drupal\frontend_api\Plugin\Field\FieldWidget\FrontEntityAutocompleteQueryWidget
 */
class FrontEntityAutocompleteQueryNormalizer extends EntityReferenceAutocompleteNormalizer {

  /**
   * The widget setting name of the query parameter to the field name map.
   */
  protected const WIDGET_QUERY_MAP_SETTING = FrontEntityAutocompleteQueryWidget::QUERY_FIELD_MAP_SETTING;

  /**
   * The map of the query parameters and their keys in the selection settings.
   */
  protected const HANDLER_QUERY_MAP_KEY = SelectionHandlerSettings::QUERY_PARAMETERS_MAP;

  /**
   * The normalized result key of the query parameter to field name map.
   */
  public const RESULT_QUERY_MAP_KEY = 'query_fields';

  /**
   * {@inheritdoc}
   */
  protected function buildSelectionHandlerSettings(
    DisplayFieldInterface $field
  ): array {
    $result = parent::buildSelectionHandlerSettings($field);

    // Add query parameters map to the handler settings, so that autocomplete
    // REST resource could read it and pass query parameters to the handler.
    $map = $this->getWidgetSetting($field, static::WIDGET_QUERY_MAP_SETTING);
    if (!empty($map)) {
      $result[static::HANDLER_QUERY_MAP_KEY] = $map;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeField($field, $format, $context);

    $map = $this->getWidgetSetting($field, static::WIDGET_QUERY_MAP_SETTING);
    if (!empty($map)) {
      $result[static::RESULT_QUERY_MAP_KEY] = $map;
    }

    return $result;
  }

}
