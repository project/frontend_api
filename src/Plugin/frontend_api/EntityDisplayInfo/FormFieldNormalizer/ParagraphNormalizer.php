<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\frontend_api\Exception\DenormalizationException;
use Drupal\frontend_api\Rest\Denormalizer\DenormalizerResult;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;
use Drupal\frontend_api\Rest\NormalizerContextKeys;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;

/**
 * Provides form field normalizer for paragraphs fields.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "frontend_api_paragraph",
 *   field_types = {
 *     "entity_reference_revisions"
 *   },
 *   widget_types = {
 *     "entity_reference_paragraphs",
 *   }
 * )
 *
 * @TODO NOW Implement custom widget type and move both into separate module.
 */
class ParagraphNormalizer extends DefaultNormalizer implements ContainerFactoryPluginInterface, NormalizerAwareInterface {

  use EntityReferenceSelectionNormalizerTrait, NormalizerAwareTrait;

  /**
   * The paragraph front widget type.
   */
  public const WIDGET_TYPE = FrontWidgetTypes::PARAGRAPH;

  /**
   * The paragraph widget form mode setting.
   */
  public const PARAGRAPH_WIDGET_FORM_MODE_SETTING = 'form_display_mode';

  /**
   * The paragraph revision creation on change boolean flag.
   */
  protected const PARAGRAPH_REVISION_ON_CHANGE_KEY = NormalizerContextKeys::PARAGRAPH_REVISION_ON_CHANGE;

  /**
   * The form info builder.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormInfoBuilderInterface
   */
  protected $formInfoBuilder;

  /**
   * The form data builder.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataBuilderInterface
   */
  protected $formDataBuilder;

  /**
   * The entity denormalizer factory.
   *
   * @var \Drupal\frontend_api\Rest\EntityDenormalizer\EntityDenormalizerFactoryInterface
   */
  protected $denormalizerFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    $instance = new static(
      $configuration,
      $pluginId,
      $pluginDefinition
    );
    $instance->formInfoBuilder = $container->get(
      'frontend_api.rest.entity_form_info_builder'
    );
    $instance->formDataBuilder = $container->get(
      'frontend_api.rest.entity_form_data_builder'
    );
    $instance->denormalizerFactory = $container->get(
      'frontend_api.rest.entity_denormalizer_factory'
    );

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeField(
      $field,
      $format,
      $context
    );

    $targetEntityTypeId = $this->getEntityReferenceTargetTypeId($field);
    $targetBundle = $this->getEntityReferenceTargetBundle($field);
    $formMode = $this->getFormModeFromWidget($field);

    $formInfo = $this->formInfoBuilder->build(
      $targetEntityTypeId,
      $targetBundle,
      $formMode
    );
    $result['fields'] = $this->normalizer->normalize(
      $formInfo,
      $format,
      $context
    );

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldValue(
    FormDataFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = [];
    $formMode = $this->getFormModeFromWidget($field);

    /** @var \Drupal\paragraphs\ParagraphInterface[] $paragraphs */
    $paragraphs = $field->getItemList()
      ->referencedEntities();
    foreach ($paragraphs as $paragraph) {
      $formData = $this->formDataBuilder->build($paragraph, $formMode);
      $result[] = $this->normalizer->normalize(
        $formData,
        $format,
        $context
      );
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function denormalizeValueLiteral(
    FormDataFieldInterface $field,
    $value,
    string $format = NULL,
    array $context = []
  ) {
    $paragraphs = $this->denormalizeParagraphs($field, $value, $context);

    $value = [];
    foreach ($paragraphs as $delta => $paragraph) {
      $value[$delta]['entity'] = $paragraph;
    }

    return $value;
  }

  /**
   * Denormalizes request data and creates/updates paragraphs.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The entity field that references paragraphs.
   * @param array $paragraphsData
   *   The array with paragraphs data.
   * @param array $context
   *   The context.
   *
   * @return \Drupal\paragraphs\ParagraphInterface[]
   *   The paragraphs list.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  protected function denormalizeParagraphs(
    FormDataFieldInterface $field,
    array $paragraphsData,
    array $context = []
  ) {
    $targetEntityTypeId = $this->getEntityReferenceTargetTypeId($field);
    $fieldName = $field->getFieldDefinition()
      ->getName();
    $bundle = $this->getEntityReferenceTargetBundle($field);
    $formMode = $this->getFormModeFromWidget($field);

    $referencedEntities = $this->getReferencedParagraphs($field);
    $savedParagraphs = [];
    $errors = new DenormalizerResult();
    foreach ($paragraphsData as $delta => $paragraphInfo) {
      if (!isset($paragraphInfo['fields'])) {
        throw new DenormalizationException(
          [$this->t('Malformed data submitted.')]
        );
      }
      $paragraphId = $paragraphInfo['entity_id'] ?? NULL;
      $denormalizer = $this->denormalizerFactory->create(
        $targetEntityTypeId,
        $paragraphId ? ($referencedEntities[$paragraphId] ?? NULL) : NULL
      );

      $denormalizerResult = $denormalizer->setBundleId($bundle)
        ->setFormModeName($formMode)
        ->setRequestData($paragraphInfo['fields'])
        ->enableValidation()
        ->denormalize();
      if (!$denormalizerResult->hasErrors()) {
        $paragraph = $denormalizerResult->getEntity();
        if (!$paragraph->isNew()) {
          $revisionOnChange = $context[static::PARAGRAPH_REVISION_ON_CHANGE_KEY]
            ?? FALSE;
          if ($revisionOnChange && $paragraph->isChanged()) {
            $paragraph->setNewRevision();
          }
          $paragraph->save();
        }
        $savedParagraphs[] = $paragraph;

        continue;
      }
      $errors->addErrorsAt(
        "{$fieldName}.{$delta}.fields",
        $denormalizerResult->getErrors()
      );
    }

    $errors->throwErrors();

    return $savedParagraphs;
  }

  /**
   * Returns paragraphs that are currently attached to field keyed by their IDs.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   *
   * @return array
   *   Paragraphs keyed by IDs.
   */
  protected function getReferencedParagraphs(FormDataFieldInterface $field): array {
    /** @var \Drupal\paragraphs\ParagraphInterface[] $paragraphs */
    $paragraphs = $field->getItemList()
      ->referencedEntities();
    $referencedParagraphs = [];
    foreach ($paragraphs as $paragraph) {
      $referencedParagraphs[$paragraph->id()] = $paragraph;
    }

    return $referencedParagraphs;
  }

  /**
   * Returns target entity bundle.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The entity field that has references to paragraphs.
   *
   * @return string
   *   The bundle.
   */
  protected function getEntityReferenceTargetBundle(
    DisplayFieldInterface $field
  ): string {
    $targetBundles = $this->getEntityReferenceTargetBundles($field);
    return reset($targetBundles);
  }

  /**
   * Return paragraph form mode from widget settings.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   *
   * @return string
   *   The form mode.
   */
  protected function getFormModeFromWidget(DisplayFieldInterface $field): string {
    $fieldSettings = $field->getComponentData();
    return $fieldSettings['settings'][static::PARAGRAPH_WIDGET_FORM_MODE_SETTING];
  }

}
