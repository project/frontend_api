<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\frontend_api\Exception\DenormalizationException;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides normalizer for text fields that use HTML editors.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "frontend_api_formatted_text",
 *   field_types = {
 *     "text_with_summary",
 *   },
 *   widget_types = {
 *     "frontend_api_formatted_text"
 *   },
 * )
 */
class FormattedTextNormalizer extends DefaultNormalizer {

  /**
   * The React widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::HTML_EDITOR;

  /**
   * {@inheritdoc}
   */
  protected function denormalizeValueLiteral(
    FormDataFieldInterface $field,
    $value,
    string $format = NULL,
    array $context = []
  ) {
    $value = parent::denormalizeValueLiteral($field, $value, $format, $context);
    $format = $this->getFilterFormatId($field);
    foreach ($value as $delta => $item) {
      $value[$delta]['format'] = $format;
    }

    return $value;
  }

  /**
   * Returns HTML format ID from widget settings.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   *
   * @return string
   *   The filter format ID.
   *
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  protected function getFilterFormatId(DisplayFieldInterface $field): string {
    $componentData = $field->getComponentData();
    $formatId = $componentData['settings']['format'] ?? NULL;
    if ($formatId === NULL) {
      $fieldName = $field->getFieldDefinition()
        ->getName();
      throw new DenormalizationException([
        $fieldName => $this->t('No filter format used'),
      ]);
    }

    return $formatId;
  }

}
