<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides field definition normalizer for textarea.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "textarea",
 *   field_types = {
 *     "string_long",
 *   },
 *   widget_types={
 *     "string_textarea"
 *   }
 * )
 */
class TextareaNormalizer extends DefaultNormalizer {

  /**
   * The React widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::TEXTAREA;

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $fieldDefinition = $field->getFieldDefinition();
    $componentData = $field->getComponentData();
    $placeholder = $componentData['settings']['placeholder'];

    // Replace placeholder from widget if exists.
    if (!empty($placeholder)) {
      $fieldDefinition->setDescription($placeholder);
    }

    return parent::normalizeField($field, $format, $context);
  }

}
