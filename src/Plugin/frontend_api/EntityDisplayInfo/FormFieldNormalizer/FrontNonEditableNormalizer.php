<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

/**
 * Provides form field normalizer for the non-editable integer/e-mail fields.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "frontend_api_non_editable",
 *   priority = 10,
 *   field_types = {
 *     "integer",
 *     "email",
 *     "string",
 *   },
 *   widget_types = {
 *     "frontend_api_non_editable",
 *   },
 * )
 */
class FrontNonEditableNormalizer extends FrontNonEditableNormalizerBase {

}
