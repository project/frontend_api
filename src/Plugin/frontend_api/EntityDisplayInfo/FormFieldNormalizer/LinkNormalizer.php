<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\Component\Utility\UrlHelper;
use Drupal\frontend_api\Rest\Denormalizer\DenormalizerResult;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides form field normalizer for the link widget.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "frontend_api_link",
 *   field_types = {
 *     "link",
 *   },
 *   widget_types = {
 *     "link_default",
 *   },
 * )
 */
class LinkNormalizer extends DefaultNormalizer {

  /**
   * The React widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::LINK;

  /**
   * The link title field property.
   */
  public const LINK_TITLE_PROPERTY = 'title';

  /**
   * Link title modes with states map.
   */
  public const LINK_TITLE_MODES_MAP = [
    DRUPAL_DISABLED => 'disabled',
    DRUPAL_OPTIONAL => 'optional',
    DRUPAL_REQUIRED => 'required',
  ];

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeField($field, $format, $context);

    $componentData = $field->getComponentData();
    $result['title_label'] = $componentData['settings']['placeholder_title']
      ?: $this->t('Link title');
    $result['uri_label'] = $componentData['settings']['placeholder_url']
      ?: $this->t('Link URI');

    $fieldDefinition = $field->getFieldDefinition();
    $titleMode = $fieldDefinition->getSetting('title');
    $result['title'] = static::LINK_TITLE_MODES_MAP[$titleMode];

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeValueLiteral(DisplayFieldInterface $field, array $value) {
    // We need both title and uri properties on form data result.
    // Link item has uri as main property, and ::flattenValueLiteralItems
    // method returns only it.
    // Also remove useless 'options' property from result.
    foreach ($value as $delta => $item) {
      if (isset($item['options'])) {
        unset($value[$delta]['options']);
      }
    }

    return $this->flattenValueLiteralItemList($field, $value);
  }

  /**
   * {@inheritdoc}
   */
  protected function validateDenormalizedValueLiteral(
    FormDataFieldInterface $field,
    ?array $value,
    array $context = []
  ): ?array {
    $mainProperty = $this->getFieldMainPropertyName($field);
    $linkTitleProperty = static::LINK_TITLE_PROPERTY;
    $fieldDefinition = $field->getFieldDefinition();
    $fieldName = $fieldDefinition->getName();
    $cardinality = $this->getFieldCardinality($field);
    $titleMode = $fieldDefinition->getSetting('title');

    $denormalizedResult = new DenormalizerResult();
    $errors = [];
    foreach ($value as $delta => $item) {
      $title = $item[$linkTitleProperty];
      $url = $item[$mainProperty];
      $urlPath = $cardinality === 1 ? $mainProperty : "{$delta}.{$mainProperty}";
      $titlePath = $cardinality === 1 ? $linkTitleProperty : "{$delta}.{$linkTitleProperty}";

      // We need to implement here all these checks because our resource
      // validator will not return proper paths for errors due to due to
      // specificity of the link field. It has 'uri' as main property and the
      // check of 'title' one is implemented at formatter level.
      $isValidUrl = UrlHelper::isValid($url, TRUE);
      if (!$isValidUrl) {
        $errors[$urlPath] = $this->t('This value should be of the correct primitive type.');
      }

      if (empty($title) && $titleMode === DRUPAL_REQUIRED) {
        $errors[$titlePath] = $this->t("The link title is required.");
      }

      // Throw an error if user sent link title without URL.
      if (empty($url) && !empty($linkTitleProperty)) {
        $errors[$urlPath] = $this->t("Could not set link title without URL.");
      }
    }
    $denormalizedResult->addErrorsAt($fieldName, $errors);
    $denormalizedResult->throwErrors();

    return $value;
  }

}
