<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\frontend_api\Plugin\Field\FieldWidget\FrontOptionsSelectWidget;
use Drupal\frontend_api\Rest\Denormalizer\DenormalizerResult;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides field definition normalizer for the fields with options.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "frontend_api_options",
 *   field_types = {
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *   },
 *   widget_types = {
 *     "frontend_api_options_select",
 *   },
 * )
 */
class FrontListOptionsNormalizer extends ListOptionsNormalizer {

  /**
   * {@inheritdoc}
   */
  public const WIDGET_TYPE_MAP = [
    'frontend_api_options_select' => FrontWidgetTypes::SELECT,
  ];

  /**
   * {@inheritdoc}
   */
  protected const DISABLED_OPTIONS = FrontOptionsSelectWidget::DISABLED_OPTIONS;

  /**
   * Returns disabled options for the passed field.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The entity display info field.
   *
   * @return array
   *   The list of disabled options
   */
  protected function getDisabledOptions(DisplayFieldInterface $field) {
    $disabled_options = [];
    $component_data = $field->getComponentData();
    $settings = $component_data['settings'];
    if (!empty($settings[static::DISABLED_OPTIONS])) {
      $disabled_options = array_fill_keys($settings[static::DISABLED_OPTIONS], TRUE);
    }

    return $disabled_options;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeField($field, $format, $context);

    $disabled_options = $this->getDisabledOptions($field);
    if (!empty($disabled_options)) {
      foreach ($result['options'] as &$option) {
        if (isset($disabled_options[$option['key']])) {
          $option['disabled'] = TRUE;
        }
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function validateDenormalizedValueLiteral(
    FormDataFieldInterface $field,
    ?array $value,
    array $context = []
  ): ?array {
    $value = parent::validateDenormalizedValueLiteral(
      $field,
      $value,
      $context
    );

    if ($value !== NULL) {
      $value = $this->validateValueLiteralDisabledOptions($field, $value);
    }

    return $value;
  }

  /**
   * Validates the submitted option values against the list of disabled options.
   *
   * It makes sure desabled options can't be set to the entity if they aren't
   * already on the field value.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field.
   * @param array $value
   *   The submitted value literal after restoring the flattened value.
   *
   * @return array
   *   The submitted value, possibly updated.
   *
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  protected function validateValueLiteralDisabledOptions(
    FormDataFieldInterface $field,
    array $value
  ): array {
    $disabled_options = array_keys($this->getDisabledOptions($field));
    if (empty($disabled_options)) {
      return $value;
    }

    $options_property = $this->getOptionsPropertyName($field);
    $existing_value = $field->getItemList()
      ->getValue();
    $existing_options = array_column($existing_value, $options_property);
    $submitted_options = array_column($value, $options_property);

    $errors = new DenormalizerResult();

    $added_options = array_diff($submitted_options, $existing_options);
    $added_disabled_options = array_intersect($disabled_options, $added_options);
    if (!empty($added_disabled_options)) {
      $errors->addErrors([
        '' => $this->t("You can't set disabled options."),
      ]);
    }

    // @todo We should probably split disabled and blocked options, but for now
    //   they are the same.
    $removed_options = array_diff($existing_options, $submitted_options);
    $removed_disabled_options = array_intersect($disabled_options, $removed_options);
    if (!empty($removed_disabled_options)) {
      $errors->addErrors([
        '' => $this->t("You can't change locked options."),
      ]);
    }

    $field_name = $field->getFieldDefinition()
      ->getName();
    $errors->throwErrors($field_name);

    return $value;
  }

}
