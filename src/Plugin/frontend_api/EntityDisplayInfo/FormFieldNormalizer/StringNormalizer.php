<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer;

use Drupal\frontend_api\Exception\DenormalizationException;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides field definition normalizer for simple string fields.
 *
 * @EntityFormInfoFieldNormalizer(
 *   id = "string",
 *   field_types = {
 *     "string",
 *   },
 *   widget_types = {
 *     "*",
 *     "string_textfield"
 *   }
 * )
 */
class StringNormalizer extends DefaultNormalizer {

  /**
   * The React widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::INPUT;

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeField($field, $format, $context);

    $settings = $field->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getSettings();
    if (isset($settings['max_length'])) {
      $result['max_length'] = $settings['max_length'];
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkRequiredValue(FormDataFieldInterface $field): void {
    parent::checkRequiredValue($field);

    $property = $this->getFieldMainPropertyName($field);

    foreach ($field->getItemList()->getValue() as $value) {
      if (trim($value[$property]) === '') {
        $field_name = $field->getFieldDefinition()
          ->getName();

        throw new DenormalizationException([
          $field_name => $this->t('This field is required.'),
        ]);
      }
    }
  }

}
