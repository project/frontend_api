<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;

/**
 * Provides normalizer for a Front populated formatter.
 *
 * Displays raw boolean values.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "frontend_api_populated",
 *   field_types = {},
 *   formatter_types = {
 *     "frontend_api_populated",
 *   },
 * )
 */
class FrontPopulatedNormalizer extends DefaultNormalizer {

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    return $item_list->isEmpty() ? FALSE : TRUE;
  }

}
