<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides normalizer for a default price formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "price_default",
 *   field_types = {
 *     "commerce_price",
 *   },
 *   formatter_types = {
 *     "commerce_price_default",
 *     "commerce_price_plain",
 *   }
 * )
 */
class PriceDefaultNormalizer extends DefaultNormalizer {

  /**
   * The mapped front widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::PRICE;

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $itemList,
    string $format = NULL,
    array $context = []
  ) {
    foreach ($itemList as $item) {
      $price = $item->toPrice();
      $number = $price->getNumber();
      $item->set('number', number_format($number, 2, '.', ''));
    }

    return parent::normalizeFieldItemList($field, $itemList, $format, $context);
  }

}
