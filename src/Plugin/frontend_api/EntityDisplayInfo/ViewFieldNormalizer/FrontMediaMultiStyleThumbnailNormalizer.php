<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Dictionary\Field\MediaFields;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;

/**
 * Provides normalizer for a front-only multi-style media thumbnails formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "frontend_api_media_multistyle_thumbnails",
 *   field_types = {
 *     "entity_reference",
 *   },
 *   formatter_types={
 *     "frontend_api_media_multistyle_thumbnails",
 *   },
 * )
 *
 * @see \Drupal\frontend_api\Plugin\Field\FieldFormatter\FrontMediaMultiStyleThumbnailFormatter
 */
class FrontMediaMultiStyleThumbnailNormalizer extends FrontMediaMultiStyleNormalizerBase {

  /**
   * The media thumbnail field.
   */
  protected const MEDIA_THUMBNAIL_FIELD = MediaFields::THUMBNAIL;

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $values = $item_list->getValue();
    if (empty($values)) {
      return NULL;
    }

    $media_entities = $this
      ->getReferencedEntitiesToDisplay($item_list, $context);
    if (empty($media_entities)) {
      return NULL;
    }

    $styles = $this->getFormatterImageStyles($field);

    $normalized = [];
    foreach ($media_entities as $delta => $media) {
      /** @var \Drupal\media\MediaInterface $media */
      $image_item = $media->get(static::MEDIA_THUMBNAIL_FIELD)->first();
      if (!$image_item instanceof FieldItemInterface) {
        throw new \InvalidArgumentException(sprintf(
          'Media #%d has broken thumbnail.',
          $media->id()
        ));
      }

      $normalized[$delta] = $this
        ->normalizeImageItem($image_item, $styles, $format, $context);
      $this->addCacheableDependency($context, $media);
    }

    return $this->flattenValueLiteralItemList($field, $normalized);
  }

}
