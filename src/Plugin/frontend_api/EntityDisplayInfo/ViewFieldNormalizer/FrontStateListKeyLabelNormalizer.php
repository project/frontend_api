<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;
use Drupal\state_machine\Plugin\Field\FieldType\StateItem;

/**
 * Provides normalizer for a state field exposed as a key + label pair.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "frontend_api_state_key_label",
 *   field_types = {
 *     "state",
 *   },
 *   formatter_types = {
 *     "frontend_api_state_list_key_label",
 *   },
 * )
 */
class FrontStateListKeyLabelNormalizer extends ListNormalizerBase {

  /**
   * The mapped front widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::TEXT;

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $mainProperty = $this->getFieldMainPropertyName($field);

    $normalized = [];
    foreach ($item_list as $item) {
      if ($item instanceof StateItem) {
        $stateId = $item->getString();
        $states = $item->getPossibleOptions();
        $normalized[$mainProperty] = [
          'key' => $stateId,
          'label' => $states[$stateId] ?? $this->t('N/A'),
        ];
      }
    }

    return $this->flattenValueLiteralItemList($field, $normalized);
  }

}
