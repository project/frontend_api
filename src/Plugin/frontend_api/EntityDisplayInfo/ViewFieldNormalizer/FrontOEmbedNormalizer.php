<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\media\OEmbed\Resource;
use Drupal\media\OEmbed\ResourceException;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides normalizer for the oEmbed media field formatter.
 *
 * It only supports videos inserted as an iframe at the moment.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "frontend_api_oembed",
 *   field_types = {
 *     "string",
 *   },
 *   formatter_types = {
 *     "frontend_api_oembed",
 *   },
 * )
 *
 * @see \Drupal\frontend_api\Plugin\Field\FieldFormatter\FrontOEmbedFormatter
 */
class FrontOEmbedNormalizer extends DefaultNormalizer implements ContainerFactoryPluginInterface {

  /**
   * The video oEmbed resource type.
   */
  protected const RESOURCE_TYPE_VIDEO = Resource::TYPE_VIDEO;

  /**
   * The type key in the normalized field item.
   */
  public const TYPE_ITEM_KEY = 'type';

  /**
   * The HTML key in the normalized field item.
   */
  public const HTML_ITEM_KEY = 'html';

  /**
   * The list of allowed HTML tags for the oEmbed content.
   */
  public const ALLOWED_HTML_TAGS = ['iframe'];

  /**
   * The oEmbed URL resolver.
   *
   * @var \Drupal\media\OEmbed\UrlResolverInterface
   */
  protected $urlResolver;

  /**
   * The oEmbed resource fetcher.
   *
   * @var \Drupal\media\OEmbed\ResourceFetcherInterface
   */
  protected $resourceFetcher;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->urlResolver = $container->get('media.oembed.url_resolver');
    $instance->resourceFetcher = $container->get('media.oembed.resource_fetcher');
    $instance->logger = $container->get('logger.channel.frontend_api');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $value = $item_list->getValue();
    if (empty($value)) {
      return NULL;
    }

    $result = [];
    $column_name = $this->getFieldMainPropertyName($field);
    foreach ($value as $item) {
      $item_url = $item[$column_name] ?? NULL;
      if ($item_url === NULL) {
        continue;
      }

      try {
        $resource_url = $this->urlResolver->getResourceUrl($item_url);
        $resource = $this->resourceFetcher->fetchResource($resource_url);
      }
      catch (ResourceException $exception) {
        $this->logger->error(
          'Could not retrieve the remote URL (@url).',
          ['@url' => $value]
        );
        continue;
      }

      $this->addCacheableDependency($context, $resource);

      $resource_type = $resource->getType();
      if ($resource_type !== static::RESOURCE_TYPE_VIDEO) {
        $this->logger->error(
          'Unsupported oEmbed resource type @type received for the %url URL.',
          [
            '@type' => $resource->getType(),
            '%url' => $item_url,
          ]
        );
        continue;
      }

      $html = $resource->getHtml();
      $html = Xss::filter($html, static::ALLOWED_HTML_TAGS);

      $result[] = [
        static::TYPE_ITEM_KEY => $resource_type,
        static::HTML_ITEM_KEY => $html,
      ];
    }

    return $this->flattenValueLiteralItemList($field, $result);
  }

}
