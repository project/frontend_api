<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Dictionary\Field\MediaFields;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides normalizer for a front multi-style first media thumbnail formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "frontend_api_media_multistyle_first_thumbnail",
 *   field_types = {
 *     "entity_reference",
 *   },
 *   formatter_types = {
 *     "frontend_api_media_multistyle_first_thumbnail",
 *   },
 * )
 *
 * @see \Drupal\frontend_api\Plugin\Field\FieldFormatter\FrontMediaMultiStyleFirstThumbnailFormatter
 */
class FrontMediaMultiStyleFirstThumbnailNormalizer extends FrontMediaMultiStyleNormalizerBase {

  /**
   * The media thumbnail field.
   */
  protected const MEDIA_THUMBNAIL_FIELD = MediaFields::THUMBNAIL;

  /**
   * The field item list slicer.
   *
   * @var \Drupal\frontend_api\Field\FieldItemListSlicerInterface
   */
  protected $itemListSlicer;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->itemListSlicer = $container->get(
      'frontend_api.field.item_list_slicer'
    );

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $item_list */
    $item_list = $this->itemListSlicer->sliceItemList($item_list, 0, 1);
    if ($item_list->isEmpty()) {
      return NULL;
    }

    $entities = $this->getReferencedEntitiesToDisplay($item_list, $context);
    if (empty($entities)) {
      return NULL;
    }

    /** @var \Drupal\media\MediaInterface $media */
    $media = reset($entities);
    $this->addCacheableDependency($context, $media);

    $image_item = $media->get(static::MEDIA_THUMBNAIL_FIELD)->first();
    if (!$image_item instanceof FieldItemInterface) {
      throw new \InvalidArgumentException(sprintf(
        'Media #%d has broken thumbnail.',
        $media->id()
      ));
    }

    $styles = $this->getFormatterImageStyles($field);
    return $this->normalizeImageItem($image_item, $styles, $format, $context);
  }

}
