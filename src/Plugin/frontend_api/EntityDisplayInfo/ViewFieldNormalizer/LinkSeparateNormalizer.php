<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;

/**
 * Provides normalizer for a frontend_api_link field formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "link_separate",
 *   field_types = {
 *     "link",
 *   },
 *   formatter_types = {
 *     "link_separate",
 *   }
 * )
 */
class LinkSeparateNormalizer extends DefaultNormalizer {

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $serializerFormat = NULL,
    array $context = []
  ) {
    $values = $item_list->getValue();
    if (empty($values)) {
      return NULL;
    }

    $result = [];
    foreach ($item_list as $delta => $item) {
      $url = $item->getUrl();
      $url->setAbsolute(TRUE);

      $result[$delta]['url'] = $url->toString();
      $result[$delta]['title'] = $item->title;
    }
    return $this->flattenValueLiteralItemList($field, $result);
  }

}
