<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Drupal\frontend_api\Rest\NormalizerContextKeys;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base normalizer for formatters displaying entity reference fields.
 */
abstract class EntityReferenceNormalizerBase extends DefaultNormalizer implements ContainerFactoryPluginInterface {

  /**
   * The entity type key in the normalized data.
   */
  public const ENTITY_TYPE_KEY = 'entity_type_id';

  /**
   * The field setting name of the target entity type.
   */
  public const ENTITY_TYPE_SETTING = 'target_type';

  /**
   * The context key of the content language code.
   */
  protected const CONTENT_LANGCODE_CONTEXT = NormalizerContextKeys::CONTENT_LANGCODE_CONTEXT;

  /**
   * The language type used for the entities displayed.
   */
  protected const ENTITY_LANGUAGE_TYPE = LanguageInterface::TYPE_CONTENT;

  /**
   * The operation for the entity access check.
   */
  protected const ENTITY_ACCESS_OPERATION = 'view';

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
      $container->get('entity.repository')
    );
  }

  /**
   * A constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    LanguageManagerInterface $language_manager,
    EntityRepositoryInterface $entity_repository
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->languageManager = $language_manager;
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldValue(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeFieldValue(
      $field,
      $item_list,
      $format,
      $context
    );

    $target_entity_type = $field
      ->getFieldDefinition()
      ->getSetting(static::ENTITY_TYPE_SETTING);
    if (isset($target_entity_type)) {
      $result[static::ENTITY_TYPE_KEY] = $target_entity_type;
    }

    return $result;
  }

  /**
   * Returns the content langcode.
   *
   * Useful to get the rendering language code for displayed entities.
   *
   * @param array $context
   *   The serialization context.
   *
   * @return string
   *   The language code.
   */
  protected function getContentLangcode(array $context): string {
    $langcode = $context[static::CONTENT_LANGCODE_CONTEXT] ?? NULL;

    if (!isset($langcode)) {
      $langcode = $this->languageManager
        ->getCurrentLanguage(static::ENTITY_LANGUAGE_TYPE)
        ->getId();
    }

    return $langcode;
  }

  /**
   * Returns referenced entities that are visible to current user.
   *
   * @param \Drupal\Core\Field\EntityReferenceFieldItemListInterface $item_list
   *   The field item list to retrieve the entities from.
   * @param array $context
   *   The serialization context.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   The list of entities indexed by the delta.
   *
   * @see \Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase::getEntitiesToView()
   */
  protected function getReferencedEntitiesToDisplay(
    EntityReferenceFieldItemListInterface $item_list,
    array $context
  ) {
    $result = [];

    $langcode = $this->getContentLangcode($context);
    $entities = $item_list->referencedEntities();
    foreach ($entities as $delta => $entity) {
      // Set the entity in the correct language for display.
      if ($entity instanceof TranslatableInterface) {
        $entity = $this->entityRepository
          ->getTranslationFromContext($entity, $langcode);
      }
      $this->addCacheableDependency($context, $entity);

      $access = $this->checkEntityAccess($entity);
      $this->addCacheableDependency($context, $access);
      if (!$access->isAllowed()) {
        continue;
      }

      $result[$delta] = $entity;
    }

    return $result;
  }

  /**
   * Checks access to the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check access for.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  protected function checkEntityAccess(
    EntityInterface $entity
  ): AccessResultInterface {
    return $entity->access(static::ENTITY_ACCESS_OPERATION, NULL, TRUE);
  }

  /**
   * Returns items of the field having loadable and accessible entities.
   *
   * @param \Drupal\Core\Field\EntityReferenceFieldItemListInterface $item_list
   *   The field item list.
   * @param array $context
   *   The serialization context.
   *
   * @return \Drupal\Core\Field\FieldItemInterface[]
   *   The items that contain existing entities that are accessible by current
   *   user.
   */
  protected function getFieldItemsToDisplay(
    EntityReferenceFieldItemListInterface $item_list,
    array $context
  ): array {
    if ($item_list->isEmpty()) {
      return [];
    }

    $entities_by_delta = $this
      ->getReferencedEntitiesToDisplay($item_list, $context);

    $items = [];
    foreach ($item_list as $delta => $item) {
      if (!isset($entities_by_delta[$delta])) {
        continue;
      }

      $items[$delta] = $item;
    }
    return array_values($items);
  }

}
