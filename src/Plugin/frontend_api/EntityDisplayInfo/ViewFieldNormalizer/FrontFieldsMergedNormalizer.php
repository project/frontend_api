<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Plugin\Field\FieldFormatter\FrontFieldsMergedFormatter;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Drupal\frontend_api\Rest\FieldMerger\FieldMergerInterface;
use Drupal\frontend_api\Rest\NormalizerContextKeys;

/**
 * Provides normalizer for the Entity Fields Merged formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "frontend_api_entity_fields_merged",
 *   field_types = {
 *     "entity_reference",
 *   },
 *   formatter_types = {
 *     "frontend_api_entity_fields_merged",
 *   },
 * )
 */
class FrontFieldsMergedNormalizer extends EntityReferenceNormalizerBase {

  /**
   * The field name prefix setting on the formatter.
   */
  protected const PREFIX_SETTING = FrontFieldsMergedFormatter::PREFIX_SETTING;

  /**
   * The context key of the field merger.
   */
  protected const FIELD_MERGER_CONTEXT = NormalizerContextKeys::FIELD_MERGER_CONTEXT;

  /**
   * Returns the field merger found in the context.
   *
   * @param array $context
   *   The context.
   *
   * @return \Drupal\frontend_api\Rest\FieldMerger\FieldMergerInterface
   *   The field merger.
   */
  protected function getFieldMerger(
    array $context
  ): FieldMergerInterface {
    $field_merger = $context[static::FIELD_MERGER_CONTEXT] ?? NULL;
    if (!isset($field_merger)) {
      throw new \InvalidArgumentException(
        'The field merger must be provided by the formatted fields normalizer.'
      );
    }

    return $field_merger;
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\frontend_api\Plugin\Field\FieldFormatter\FrontFieldsMergedFormatter::isApplicable()
   */
  public function prepareFieldValue(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ): void {
    if ($item_list->isEmpty()) {
      return;
    }

    // It only supports fields of the first entity for now.
    $entity_id = $item_list->first()
      ->target_id;
    if (empty($entity_id)) {
      return;
    }

    $formatter_data = $field->getComponentData();
    $view_mode_name = $formatter_data['settings']['view_mode'] ?? NULL;
    if (!isset($view_mode_name)) {
      return;
    }

    $prefix = $formatter_data['settings'][static::PREFIX_SETTING] ?? NULL;
    if (!isset($prefix)) {
      return;
    }

    $target_type_id = $field->getFieldDefinition()
      ->getSetting(static::ENTITY_TYPE_SETTING);
    if (!isset($target_type_id)) {
      return;
    }

    $this->getFieldMerger($context)
      ->addEntity($target_type_id, $entity_id, $view_mode_name, $prefix);
  }

}
