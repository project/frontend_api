<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;

/**
 * Provides normalizer for a front-only image formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "frontend_api_image",
 *   field_types = {
 *     "image",
 *   },
 *   formatter_types = {
 *     "frontend_api_image",
 *   },
 * )
 *
 * @see \Drupal\frontend_api\Plugin\Field\FieldFormatter\FrontMediaThumbnailsFormatter
 */
class FrontImageNormalizer extends FrontMediaNormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    if ($item_list->isEmpty()) {
      return NULL;
    }

    $style = $this->getFormatterImageStyle($field);
    $items = $this->getFieldItemsToDisplay($item_list, $context);

    $normalized = [];
    foreach ($items as $delta => $image_item) {
      $normalized[$delta] = $this
        ->normalizeImageItem($image_item, $style, $format, $context);
    }

    return $this->flattenValueLiteralItemList($field, $normalized);
  }

}
