<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\image\ImageStyleInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Drupal\frontend_api\Rest\Media\ImageStylistInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;

/**
 * Provides base class for a normalizer of a media reference field.
 */
abstract class FrontMediaNormalizerBase extends EntityReferenceNormalizerBase implements NormalizerAwareInterface {

  use NormalizerAwareTrait;

  /**
   * The image style setting of the formatter.
   */
  public const IMAGE_STYLE_SETTING = 'image_style';

  /**
   * The image stylist.
   *
   * @var \Drupal\frontend_api\Rest\Media\ImageStylistInterface
   */
  protected $stylist;

  /**
   * A constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\frontend_api\Rest\Media\ImageStylistInterface $stylist
   *   The image stylist.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    LanguageManagerInterface $language_manager,
    EntityRepositoryInterface $entity_repository,
    ImageStylistInterface $stylist
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $language_manager,
      $entity_repository
    );

    $this->stylist = $stylist;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
      $container->get('entity.repository'),
      $container->get('frontend_api.rest.image_stylist')
    );
  }

  /**
   * Returns the image style specified on the formatter settings.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface $field
   *   The entity view info field.
   *
   * @return \Drupal\image\ImageStyleInterface|null
   *   The image style or NULL.
   */
  protected function getFormatterImageStyle(
    ViewFieldInterface $field
  ): ?ImageStyleInterface {
    $formatter_data = $field->getComponentData();

    $style_name = $formatter_data['settings'][static::IMAGE_STYLE_SETTING] ?: NULL;
    return $this->stylist->getImageStyle($style_name);
  }

  /**
   * Normalizes a single field item of an image field.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The image field item.
   * @param \Drupal\image\ImageStyleInterface|null $style
   *   The image style or NULL to normalize original image.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   *
   * @return mixed
   *   The styled and normalized image.
   */
  protected function normalizeImageItem(
    FieldItemInterface $item,
    ImageStyleInterface $style = NULL,
    string $format = NULL,
    array $context = []
  ) {
    $styled_image = $this->stylist->buildStyledImage($item, $style);
    return $this->normalizer->normalize($styled_image, $format, $context);
  }

}
