<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides normalizer for a default date time formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "datetime_default",
 *   field_types = {
 *     "datetime",
 *   },
 *   formatter_types = {
 *     "datetime_default",
 *   },
 * )
 */
class DateTimeDefaultNormalizer extends DefaultNormalizer {

  /**
   * The mapped front widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::DATE;

}
