<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Drupal\state_machine\Plugin\Field\FieldType\StateItem;

/**
 * Provides normalizer for a state field displayed as plain text.
 *
 * Converts the machine key value to a human-readable state name.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "state_default",
 *   field_types = {
 *     "state"
 *   },
 *   formatter_types = {
 *     "list_default"
 *   }
 * )
 */
class StateDefaultNormalizer extends StringNormalizer {

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $itemList,
    string $format = NULL,
    array $context = []
  ) {
    $mainProperty = $this->getFieldMainPropertyName($field);

    // Replace machine keys with readable labels.
    $normalized = [];
    foreach ($itemList as $item) {
      if ($item instanceof StateItem) {
        $stateId = $item->getString();
        $states = $item->getPossibleOptions();
        $normalized[$mainProperty] = $states[$stateId] ?? $this->t('N/A');
      }
    }

    return $this->flattenValueLiteralItemList($field, $normalized);
  }

}
