<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides normalizer for a Front Boolean formatter.
 *
 * It displays raw boolean values.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "frontend_api_boolean",
 *   field_types = {
 *     "boolean",
 *   },
 *   formatter_types = {
 *     "frontend_api_boolean",
 *   }
 * )
 */
class FrontBooleanNormalizer extends DefaultNormalizer {

  /**
   * The mapped front widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::CHECKBOX;

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    // Convert stored numerical string values into booleans.
    $result = [];
    foreach ($item_list as $delta => $item) {
      $result[$delta] = (bool) $item->value;
    }

    return $this->flattenValueLiteralItemList($field, $result);
  }

}
