<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\commerce_price\Price;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides normalizer for a "commerce_order_total_summary" formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "order_total_summary",
 *   field_types = {
 *     "commerce_price",
 *   },
 *   formatter_types={
 *     "commerce_order_total_summary",
 *   }
 * )
 */
class OrderTotalSummaryNormalizer extends DefaultNormalizer implements ContainerFactoryPluginInterface {

  /**
   * The order total summary service.
   *
   * @var \Drupal\commerce_order\OrderTotalSummaryInterface
   */
  protected $orderTotalSummary;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->orderTotalSummary = $container->get('commerce_order.order_total_summary');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    /* @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $item_list->getEntity();
    $totals = $this->orderTotalSummary->buildTotals($order);

    $adjustments = new Price(0, $totals['total']->getCurrencyCode());
    foreach ($totals['adjustments'] as $adjustment) {
      $adjustments = $adjustments->add($adjustment['amount']);
    }

    $totals['adjustments'] = $adjustments;

    /* @var \Drupal\commerce_price\Price $price */
    foreach ($totals as $key => $price) {
      $totals[$key] = $price->toArray();
      $totals[$key]['number'] = number_format($totals[$key]['number'], 2, '.', '');
    }

    return $this->normalizeValueLiteral($field, [$totals]);
  }

}
