<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;

/**
 * Provides normalizer for a list option displayed as plain text.
 *
 * Converts the machine key value to a human-readable option name.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "list_default",
 *   field_types = {
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *   },
 *   formatter_types = {
 *     "list_default",
 *     "address_country_default",
 *   },
 * )
 */
class ListDefaultNormalizer extends ListNormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected function getWidgetType(
    DisplayFieldInterface $field,
    array $context = []
  ): ?string {
    $cardinality = $this->getFieldCardinality($field);
    $widget_type = 'text';

    if ($cardinality > 1) {
      $widget_type = 'list';
    }

    return $widget_type;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    // Replace machine keys with readable labels.
    $entity = $item_list->getEntity();
    $options = $this->getAllowedValues($field, $entity);
    $main_property = $this->getFieldMainPropertyName($field);

    $normalized = [];
    foreach ($item_list->getValue() as $delta => $item) {
      if (!isset($item[$main_property])) {
        continue;
      }

      $raw_value = $item[$main_property];
      $formatted_value = $options[$raw_value] ?? $raw_value;
      $normalized[$delta] = $formatted_value;
    }

    return $this->flattenValueLiteralItemList($field, $normalized);
  }

}
