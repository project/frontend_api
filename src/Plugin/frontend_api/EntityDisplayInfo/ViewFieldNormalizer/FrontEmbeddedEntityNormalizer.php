<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Plugin\Field\FieldFormatter\FrontEmbeddedEntityFormatter;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Drupal\frontend_api\Rest\NormalizerContextKeys;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;

/**
 * Provides normalizer for the Embedded Entity formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "frontend_api_embedded_entity",
 *   field_types = {
 *     "entity_reference",
 *   },
 *   formatter_types = {
 *     "frontend_api_embedded_entity",
 *   },
 * )
 *
 * @see \Drupal\frontend_api\Plugin\Field\FieldFormatter\FrontEmbeddedEntityFormatter
 */
class FrontEmbeddedEntityNormalizer extends EntityReferenceNormalizerBase implements NormalizerAwareInterface {

  use NormalizerAwareTrait;

  /**
   * The context key of the view mode name.
   */
  protected const VIEW_MODE_CONTEXT = NormalizerContextKeys::VIEW_MODE_CONTEXT;

  /**
   * The context key of the field merger.
   */
  protected const FIELD_MERGER_CONTEXT = NormalizerContextKeys::FIELD_MERGER_CONTEXT;

  /**
   * The key in the normalized data of a single embedded entity.
   */
  public const EMBEDDED_ENTITY_KEY = 'entity';

  /**
   * The key in the normalized data of multiple embedded entities.
   */
  public const EMBEDDED_ENTITIES_KEY = 'entities';

  /**
   * The context normalizer uses to temporarily store the list of entities.
   */
  public const EMBEDDED_ENTITIES_CONTEXT = 'embedded_entities';

  /**
   * The formatter setting key of the access check flag.
   */
  protected const ACCESS_CHECK_SETTING = FrontEmbeddedEntityFormatter::ACCESS_CHECK_SETTING;

  /**
   * The formatter setting key of the target entity field to use for sorting.
   */
  protected const SORT_BY_SETTING = FrontEmbeddedEntityFormatter::SORT_BY_SETTING;

  /**
   * The formatter setting key of the sort direction.
   */
  protected const SORT_DIRECTION_SETTING = FrontEmbeddedEntityFormatter::SORT_DIRECTION_SETTING;

  /**
   * Default sort direction.
   */
  protected const DEFAULT_SORT_DIRECTION = FrontEmbeddedEntityFormatter::DEFAULT_SORT_DIRECTION;

  /**
   * Normalizes embedded entities.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface $field
   *   The field of the entity view info.
   * @param \Drupal\Core\Field\FieldItemListInterface $item_list
   *   The item list to normalize the values of.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   *
   * @return array|null
   *   Normalized entity or entities list. NULL in case the entities can't be
   *   normalized.
   */
  protected function normalizeEmbeddedEntities(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $formatter_data = $field->getComponentData();
    $view_mode_name = $formatter_data['settings']['view_mode'] ?? NULL;
    if (!isset($view_mode_name)) {
      return NULL;
    }

    if (!$item_list instanceof EntityReferenceFieldItemListInterface) {
      return NULL;
    }

    $access_check_setting = $formatter_data['settings'][static::ACCESS_CHECK_SETTING]
      ?? NULL;
    if (!empty($access_check_setting)) {
      // Get accessible entities if access check setting is checked.
      $referenced_entities = $this->getReferencedEntitiesToDisplay(
        $item_list,
        $context
      );
    }
    else {
      $referenced_entities = $item_list->referencedEntities();
    }

    // Specify view mode selected on the widget and erase the field merger, as
    // merging shouldn't bubble up through an embedded entity.
    $context[static::VIEW_MODE_CONTEXT] = $view_mode_name;
    unset($context[static::FIELD_MERGER_CONTEXT]);

    // @todo Find a way to optimize loading on nested embedded entities.
    $entities = [];
    foreach ($referenced_entities as $entity) {
      $entities[$entity->id()] = $entity;
    }
    if (empty($entities)) {
      return NULL;
    }

    $result = $this->normalizer->normalize($entities, $format, $context);
    return $result;
  }

  /**
   * Adds normalized embedded entities to the normalization result.
   *
   * It adds embedded entities under a different key depending on the field
   * cardinality.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface $field
   *   The field of the entity view info.
   * @param array $result
   *   The normalization result to add the embedded entities.
   * @param array $normalized_entities
   *   The normalized entities to embed.
   *
   * @return array
   *   The normalization result with embedded entities added.
   */
  protected function addEmbeddedEntities(
    ViewFieldInterface $field,
    array $result,
    array $normalized_entities
  ): array {
    $cardinality = $this->getFieldCardinality($field);
    if ($cardinality === 1) {
      if (empty($normalized_entities)) {
        $entity = NULL;
      }
      else {
        $entity = reset($normalized_entities);
      }

      $result[static::EMBEDDED_ENTITY_KEY] = $entity;
    }
    else {
      $result[static::EMBEDDED_ENTITIES_KEY] = $normalized_entities;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldValue(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    // Normalize embedded entities and store them in context for the item list
    // normalization to be able to filter-out invalid references.
    $entities = $this
      ->normalizeEmbeddedEntities($field, $item_list, $format, $context);
    $context[static::EMBEDDED_ENTITIES_CONTEXT] = $entities;

    $result = parent::normalizeFieldValue($field, $item_list, $format, $context);

    if (isset($entities)) {
      $result = $this->addEmbeddedEntities($field, $result, $entities);
    }

    return $result;
  }

  /**
   * Filters item list value to only include loaded entities.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface $field
   *   The field of the entity view info.
   * @param array $value
   *   The value literal extracted from the item list.
   * @param array $context
   *   The context.
   *
   * @return array
   *   The filtered value literal.
   */
  protected function filterEmbeddedEntityItemListValue(
    ViewFieldInterface $field,
    array $value,
    array $context
  ): array {
    $main_property = $field->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getMainPropertyName();
    if (!isset($main_property)) {
      return $value;
    }

    $entities = $context[static::EMBEDDED_ENTITIES_CONTEXT] ?? NULL;
    if (empty($entities)) {
      return [];
    }

    $filtered = [];
    foreach ($value as $item_value) {
      $entity_id = $item_value[$main_property] ?? NULL;
      if (!isset($entity_id) || !isset($entities[$entity_id])) {
        continue;
      }

      $filtered[] = $item_value;
    }
    return $filtered;
  }

  /**
   * Sorts the passed values by configured field values of the target entity.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface $field
   *   The field info.
   * @param \Drupal\Core\Field\FieldItemListInterface $item_list
   *   The field item list.
   * @param array $value
   *   The values to sort. They must be already filtered to only include items
   *   that reference existing entities.
   *
   * @return array
   *   The sorted values, re-indexed.
   */
  protected function sortItemListValues(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    array $value
  ): array {
    if (empty($value)) {
      return $value;
    }

    $formatter_settings = $field->getComponentData()['settings'] ?? [];
    $sort_by = $formatter_settings[static::SORT_BY_SETTING] ?? NULL;
    if (empty($sort_by)) {
      return $value;
    }

    if (!$item_list instanceof EntityReferenceFieldItemListInterface) {
      return $value;
    }

    /** @var \Drupal\Core\Entity\FieldableEntityInterface[] $entities */
    $entities = $item_list->referencedEntities();
    if (empty($entities)) {
      $field_name = $field->getFieldDefinition()
        ->getName();
      throw new \LogicException(sprintf(
        'The list of entities is empty on %s field.',
        $field_name
      ));
    }

    $sort_direction = $formatter_settings[static::SORT_DIRECTION_SETTING]
      ?? static::DEFAULT_SORT_DIRECTION;
    $sort_multiplier = $sort_direction === 'ASC' ? 1 : -1;
    $sort_callback = function (
      int $a_key,
      int $b_key
    ) use (
      $sort_by,
      $sort_multiplier,
      $entities
    ): int {
      $a_entity = $entities[$a_key];
      $b_entity = $entities[$b_key];

      $a_value = $a_entity->get($sort_by)
        ->getString();
      $b_value = $b_entity->get($sort_by)
        ->getString();

      $result = strnatcasecmp($a_value, $b_value);
      $result *= $sort_multiplier;
      return $result;
    };

    // Sort and re-index to still keep it serialized as an array.
    uksort($value, $sort_callback);
    $value = array_values($value);

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $value = $item_list->getValue();
    $value = $this->filterEmbeddedEntityItemListValue($field, $value, $context);
    $value = $this->sortItemListValues($field, $item_list, $value);
    $value = $this->normalizeValueLiteral($field, $value);
    return $value;
  }

}
