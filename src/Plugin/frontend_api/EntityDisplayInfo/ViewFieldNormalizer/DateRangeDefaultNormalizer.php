<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides normalizer for a default date range formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "daterange_default",
 *   field_types = {
 *     "daterange",
 *   },
 *   formatter_types={
 *     "daterange_default",
 *   }
 * )
 */
class DateRangeDefaultNormalizer extends DefaultNormalizer implements ContainerFactoryPluginInterface {

  /**
   * The mapped front widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::DATERANGE;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    $instance = new static(
      $configuration,
      $pluginId,
      $pluginDefinition
    );
    $instance->dateFormatter = $container->get('date.formatter');

    return $instance;
  }

  /**
   * Formats the date.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime|\DateTimeInterface|null $date
   *   The date to format. NULL results in empty string returned.
   * @param string $formatType
   *   The format name.
   *
   * @return string
   *   The formatted date or empty string if the date was NULL.
   */
  protected function formatDate(
    $date,
    string $formatType
  ): string {
    if ($date === NULL) {
      return '';
    }

    $timestamp = $date->getTimestamp();
    return $this->dateFormatter
      ->format($timestamp, $formatType);
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $itemList,
    string $serializerFormat = NULL,
    array $context = []
  ) {
    $componentData = $field->getComponentData();
    $formatType = $componentData['settings']['format_type'] ?? '';

    $normalized = [];
    foreach ($itemList as $delta => $item) {
      if (!$item->isEmpty()) {
        $normalized[$delta]['value'] = $this->formatDate(
          $item->start_date,
          $formatType
        );
        $normalized[$delta]['end_value'] = $this->formatDate(
          $item->end_date,
          $formatType
        );
      }
    }

    return $this->flattenValueLiteralItemList($field, $normalized);
  }

}
