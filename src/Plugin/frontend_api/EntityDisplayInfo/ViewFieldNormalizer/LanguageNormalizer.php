<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides normalizer for a language field.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "language",
 *   field_types = {
 *     "language"
 *   },
 *   formatter_types = {
 *     "language",
 *   },
 * )
 */
class LanguageNormalizer extends StringNormalizer implements ContainerFactoryPluginInterface {

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * A constructor.
   */
  public function __construct(
    array $configuration,
    string $pluginId,
    $pluginDefinition,
    LanguageManagerInterface $languageManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $itemList,
    string $format = NULL,
    array $context = []
  ) {
    $normalized = [];
    $languages = $this->languageManager->getLanguages();

    // Replace machine keys with readable labels.
    foreach ($itemList as $delta => $item) {
      $langCode = $item->getString();
      if (isset($languages[$langCode])) {
        $normalized[$delta] = $languages[$langCode]->getName();
      }
    }

    return $this->flattenValueLiteralItemList($field, $normalized);
  }

}
