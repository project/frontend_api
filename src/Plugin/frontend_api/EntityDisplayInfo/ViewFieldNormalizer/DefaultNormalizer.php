<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldNormalizerInterface;

/**
 * Provides default normalizer for the entity view field.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "default",
 *   priority = -100,
 *   field_types = {
 *     "*"
 *   },
 *   formatter_types={
 *     "*"
 *   }
 * )
 */
class DefaultNormalizer extends DisplayFieldNormalizerBase implements ViewFieldNormalizerInterface {

  use StringTranslationTrait;

  /**
   * The formatter label settings that makes field label to be exposed.
   */
  public const LABEL_DISPLAY_EXPOSED = [
    'above',
    'inline',
  ];

  /**
   * The label key in the normalized data.
   */
  public const LABEL_KEY = 'label';

  /**
   * The main property key in the normalized data.
   */
  public const MAIN_PROPERTY_KEY = 'main_property';

  /**
   * The cardinality key in the normalized data.
   */
  public const CARDINALITY_KEY = 'cardinality';

  /**
   * The value key in the normalized data.
   */
  public const VALUE_KEY = 'value';

  /**
   * {@inheritdoc}
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ) {
    $result = [
      static::CARDINALITY_KEY => $this->getFieldCardinality($field),
    ];

    $label = $this->getFieldLabel($field);
    if (isset($label)) {
      $result[static::LABEL_KEY] = $label;
    }

    $main_property_name = $this->getFieldMainPropertyName($field);
    if (isset($main_property_name)) {
      $result[static::MAIN_PROPERTY_KEY] = $main_property_name;
    }

    $widget_type = $this->getWidgetType($field, $context);
    if (isset($widget_type)) {
      $result[static::WIDGET_TYPE_KEY] = $widget_type;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareFieldValue(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ): void {
    // Do nothing by default.
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldValue(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $result = [];

    $label = $this->getFieldLabel($field);
    if (isset($label)) {
      $result[static::LABEL_KEY] = $label;
    }

    $widget_type = $this->getWidgetType($field, $context);
    if (isset($widget_type)) {
      $result[static::WIDGET_TYPE_KEY] = $widget_type;
    }

    $result[static::VALUE_KEY] = $this->normalizeFieldItemList(
      $field,
      $item_list,
      $format,
      $context
    );

    return $result;
  }

  /**
   * Normalizes the field item list.
   *
   * Child classes could use it to change the literal value passed to the
   * flattening process.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface $field
   *   The entity view info field.
   * @param \Drupal\Core\Field\FieldItemListInterface $item_list
   *   The item list of a field.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   *
   * @return mixed
   *   The normalization result.
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $value = $item_list->getValue();
    $value = $this->normalizeValueLiteral($field, $value);
    return $value;
  }

  /**
   * Returns label of the field.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface $field
   *   The entity view info field.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string|null
   *   The label or NULL in case it shouldn't be exposed.
   */
  protected function getFieldLabel(
    ViewFieldInterface $field
  ) {
    $field_definition = $field->getFieldDefinition();
    $formatter_data = $field->getComponentData();

    if (!in_array($formatter_data['label'], static::LABEL_DISPLAY_EXPOSED, TRUE)) {
      return NULL;
    }

    $custom_settings = $formatter_data['third_party_settings']['frontend_api'] ?? [];

    $label_override = $custom_settings['label'] ?? NULL;
    if (isset($label_override)) {
      $label = $label_override;
    }
    else {
      $label = $field_definition->getLabel();
    }

    return $label;
  }

}
