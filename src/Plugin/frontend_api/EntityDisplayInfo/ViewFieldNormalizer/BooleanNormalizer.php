<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;

/**
 * Provides normalizer for a boolean field formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "boolean",
 *   field_types = {
 *     "boolean",
 *   },
 *   formatter_types={
 *     "boolean",
 *   }
 * )
 */
class BooleanNormalizer extends DefaultNormalizer {

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $serializerFormat = NULL,
    array $context = []
  ) {
    $component_data = $field->getComponentData();

    $field_settings = $item_list->getFieldDefinition()
      ->getSettings();
    $formats = $this->getOutputFormats($field_settings);

    $formatter_settings = $component_data['settings'];
    $format = $formatter_settings['format'];

    $result = [];
    foreach ($item_list as $delta => $item) {
      if ($format == 'custom') {
        $result[$delta] = $item->value ?
          $formatter_settings['format_custom_true'] :
          $formatter_settings['format_custom_false'];
      }
      else {
        $result[$delta] = $item->value ?
          $formats[$format][0] :
          $formats[$format][1];
      }
    }
    return $this->flattenValueLiteralItemList($field, $result);
  }

  /**
   * Gets the available format options.
   *
   * @param array $field_settings
   *   Field settings.
   *
   * @return array
   *   A list of output formats.
   *
   * @see \Drupal\Core\Field\Plugin\Field\FieldFormatter\BooleanFormatter::getOutputFormats()
   */
  protected function getOutputFormats(array $field_settings): array {
    $formats = [
      'default' => [
        $field_settings['on_label'],
        $field_settings['off_label'],
      ],
      'yes-no' => [
        $this->t('Yes'),
        $this->t('No'),
      ],
      'true-false' => [
        $this->t('True'),
        $this->t('False'),
      ],
      'on-off' => [
        $this->t('On'),
        $this->t('Off'),
      ],
      'enabled-disabled' => [
        $this->t('Enabled'),
        $this->t('Disabled'),
      ],
      'boolean' => [1, 0],
      'unicode-yes-no' => ['✔', '✖'],
    ];

    return $formats;
  }

}
