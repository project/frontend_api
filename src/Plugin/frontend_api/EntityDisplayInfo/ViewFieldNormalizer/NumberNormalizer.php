<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides normalizer for a simple number displayed as plain text.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "number",
 *   priority = -50,
 *   field_types = {
 *     "integer",
 *     "decimal",
 *     "float",
 *   },
 *   formatter_types = {
 *     "number_unformatted",
 *     "number_decimal",
 *     "number_integer",
 *   },
 * )
 */
class NumberNormalizer extends DefaultNormalizer {

  /**
   * The front widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::NUMBER;

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldValue(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeFieldValue(
      $field,
      $item_list,
      $format,
      $context
    );

    $field_definition = $field->getFieldDefinition();
    $prefix = $field_definition->getSetting('prefix');
    $suffix = $field_definition->getSetting('suffix');
    if (isset($prefix)) {
      $result['prefix'] = $prefix;
    }
    if (isset($suffix)) {
      $result['suffix'] = $suffix;
    }

    return $result;
  }

}
