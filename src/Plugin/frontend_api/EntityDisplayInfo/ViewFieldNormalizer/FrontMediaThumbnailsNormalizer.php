<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;

/**
 * Provides normalizer for a front-only media thumbnails formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "frontend_api_media_thumbnails",
 *   field_types = {
 *     "entity_reference",
 *   },
 *   formatter_types = {
 *     "frontend_api_media_thumbnails",
 *   }
 * )
 *
 * @see \Drupal\frontend_api\Plugin\Field\FieldFormatter\FrontMediaThumbnailsFormatter
 */
class FrontMediaThumbnailsNormalizer extends FrontMediaNormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $values = $item_list->getValue();
    if (empty($values)) {
      return NULL;
    }

    $media_entities = $this->getReferencedEntitiesToDisplay($item_list, $context);
    if (empty($media_entities)) {
      return NULL;
    }

    $style = $this->getFormatterImageStyle($field);

    /** @var \Drupal\media\MediaInterface $media */

    $normalized = [];
    foreach ($media_entities as $delta => $media) {
      $image_item = $media->get('thumbnail')->first();
      if (!$image_item instanceof FieldItemInterface) {
        throw new \InvalidArgumentException(sprintf(
          'Media #%d has broken thumbnail.',
          $media->id()
        ));
      }

      $normalized[$delta] = $this->normalizeImageItem($image_item, $style, $format, $context);
      $this->addCacheableDependency($context, $media);
    }

    return $this->flattenValueLiteralItemList($field, $normalized);
  }

}
