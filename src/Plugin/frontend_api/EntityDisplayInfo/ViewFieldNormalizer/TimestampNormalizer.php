<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides normalizer for a timestamp field formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "timestamp",
 *   field_types = {
 *     "timestamp",
 *     "created",
 *     "changed",
 *   },
 *   formatter_types = {
 *     "timestamp",
 *   },
 * )
 */
class TimestampNormalizer extends DefaultNormalizer implements ContainerFactoryPluginInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    DateFormatterInterface $date_formatter
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $component_data = $field->getComponentData();

    $formatter_settings = $component_data['settings'];
    $date_format = $formatter_settings['date_format'];
    $custom_date_format = $formatter_settings['custom_date_format'];
    $timezone = $formatter_settings['timezone'] ?: NULL;

    // Convert timestamp field value into date format which we set in
    // field formatter settings.
    $result = [];
    foreach ($item_list as $delta => $item) {
      $result[$delta] = $this->dateFormatter->format(
        $item->value,
        $date_format,
        $custom_date_format,
        $timezone
      );
    }

    return $this->flattenValueLiteralItemList($field, $result);
  }

}
