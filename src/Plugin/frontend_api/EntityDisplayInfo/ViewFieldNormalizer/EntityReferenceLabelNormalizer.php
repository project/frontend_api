<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Dictionary\EntityOperations;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;

/**
 * Provides normalizer for an entity label formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "entity_reference_label",
 *   field_types = {
 *     "entity_reference",
 *   },
 *   formatter_types = {
 *     "entity_reference_label",
 *   },
 * )
 */
class EntityReferenceLabelNormalizer extends EntityReferenceNormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected const ENTITY_ACCESS_OPERATION = EntityOperations::VIEW_LABEL;

  /**
   * {@inheritdoc}
   */
  protected function normalizeValueLiteral(
    DisplayFieldInterface $field,
    array $value
  ) {
    return $this->flattenValueLiteralItemList($field, $value);
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $result = [];

    $entities = $this->getReferencedEntitiesToDisplay($item_list, $context);
    foreach ($entities as $entity) {
      $label = $entity->label();
      $result[] = $label;
    }

    return $this->normalizeValueLiteral($field, $result);
  }

}
