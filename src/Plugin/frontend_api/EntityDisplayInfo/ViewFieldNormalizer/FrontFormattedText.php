<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides normalizer for formatted text fields.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "frontend_api_formatted_text",
 *   field_types = {
 *     "text_with_summary",
 *   },
 *   formatter_types = {
 *     "text_default",
 *   },
 * )
 */
class FrontFormattedText extends DefaultNormalizer {

  /**
   * The FE app widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::RAW_HTML;

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $format = $item_list->format;
    $mainProperty = $this->getFieldMainPropertyName($field);
    $normalized = [];
    $value = $item_list->getValue();
    foreach ($value as $delta => $item) {
      $processed = check_markup($item[$mainProperty], $format);
      $normalized[$delta][$mainProperty] = (string) $processed;
    }

    return $this->normalizeValueLiteral($field, $normalized);
  }

}
