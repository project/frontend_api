<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides normalizer for a list field exposed as a key + label pair.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "frontend_api_list_key_label",
 *   field_types = {
 *     "list_string",
 *   },
 *   formatter_types = {
 *     "frontend_api_list_key_label",
 *   },
 * )
 */
class FrontListKeyLabelNormalizer extends ListNormalizerBase {

  /**
   * The mapped front widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::TEXT;

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $entity = $item_list->getEntity();
    $options = $this->getAllowedValues($field, $entity);

    // Replace machine keys with readable labels.
    $normalized = [];
    $main_property = $this->getFieldMainPropertyName($field);
    foreach ($item_list as $delta => $item) {
      /** @var \Drupal\Core\Field\FieldItemInterface $item */
      if ($item->isEmpty()) {
        continue;
      }

      $raw_value = $item->{$main_property};
      $formatted_value = $options[$raw_value] ?? $raw_value;
      $normalized[$delta] = [
        'key' => $raw_value,
        'label' => $formatted_value,
      ];
    }

    return $this->flattenValueLiteralItemList($field, $normalized);
  }

}
