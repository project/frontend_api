<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Drupal\frontend_api\Rest\NormalizerContextKeys;
use Drupal\frontend_api\Rest\Relationship\RelationshipCollectorInterface;

/**
 * Provides normalizer for the Front Relationship formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "frontend_api_relationship",
 *   field_types = {
 *     "entity_reference",
 *   },
 *   formatter_types = {
 *     "frontend_api_relationship",
 *   },
 * )
 */
class FrontRelationshipNormalizer extends EntityReferenceNormalizerBase {

  /**
   * The context key of the relationship collector.
   */
  protected const RELATIONSHIP_COLLECTOR_CONTEXT = NormalizerContextKeys::RELATIONSHIP_COLLECTOR_CONTEXT;

  /**
   * Returns the relationship collector found in the context.
   *
   * @param array $context
   *   The context.
   *
   * @return \Drupal\frontend_api\Rest\Relationship\RelationshipCollectorInterface
   *   The relationship collector.
   */
  protected function getRelationshipCollector(
    array $context
  ): RelationshipCollectorInterface {
    $collector = $context[static::RELATIONSHIP_COLLECTOR_CONTEXT] ?? NULL;
    if (!isset($collector)) {
      throw new \InvalidArgumentException(
        'The relationship collector must be provided by the formatted entity normalizer.'
      );
    }

    return $collector;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareFieldValue(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ): void {
    if ($item_list->isEmpty()) {
      return;
    }

    $entity_ids_map = [];
    foreach ($item_list as $item) {
      /** @var \Drupal\Core\Field\FieldItemInterface $item */

      if ($item->isEmpty()) {
        continue;
      }

      $entity_id = $item->get('target_id')
        ->getValue();
      $entity_ids_map[$entity_id] = TRUE;
    }
    if (empty($entity_ids_map)) {
      return;
    }

    $formatter_data = $field->getComponentData();
    $view_mode_name = $formatter_data['settings']['view_mode'] ?? NULL;
    if (!isset($view_mode_name)) {
      return;
    }

    $target_type_id = $field->getFieldDefinition()
      ->getSetting('target_type');
    if (!isset($target_type_id)) {
      return;
    }

    $this->getRelationshipCollector($context)
      ->setViewMode($target_type_id, $view_mode_name)
      ->addEntityIds($target_type_id, array_keys($entity_ids_map));
  }

}
