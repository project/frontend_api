<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides normalizer for a custom date range formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "daterange_custom",
 *   field_types = {
 *     "daterange",
 *   },
 *   formatter_types = {
 *     "daterange_custom",
 *   },
 * )
 */
class DateRangeCustomNormalizer extends DefaultNormalizer {

  /**
   * The mapped front widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::DATERANGE;

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $itemList,
    string $serializerFormat = NULL,
    array $context = []
  ) {
    $componentData = $field->getComponentData();
    $format = $componentData['settings']['date_format'];

    $normalized = [];
    foreach ($itemList as $delta => $item) {
      if (!$item->isEmpty()) {
        $normalized[$delta]['value'] = $item->start_date->format($format);
        $normalized[$delta]['end_value'] = $item->end_date->format($format);
      }
    }

    return $this->flattenValueLiteralItemList($field, $normalized);
  }

}
