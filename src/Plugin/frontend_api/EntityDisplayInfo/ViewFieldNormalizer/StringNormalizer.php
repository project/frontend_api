<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\frontend_api\Rest\FrontWidgetTypes;

/**
 * Provides normalizer for a simple primitive displayed as plain text.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "string",
 *   priority = -50,
 *   field_types = {
 *     "*"
 *   },
 *   formatter_types = {
 *     "string",
 *     "machine_name",
 *     "link",
 *     "basic_string",
 *   },
 * )
 */
class StringNormalizer extends DefaultNormalizer {

  /**
   * The mapped front widget type.
   */
  protected const WIDGET_TYPE = FrontWidgetTypes::TEXT;

}
