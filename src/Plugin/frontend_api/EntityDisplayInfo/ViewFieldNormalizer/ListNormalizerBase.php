<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base class for different list field normalizers.
 */
abstract class ListNormalizerBase extends DefaultNormalizer implements ContainerFactoryPluginInterface {

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->currentUser = $container->get('current_user');

    return $instance;
  }

  /**
   * Returns allowed values for the passed field.
   *
   * Allowed values are cached per entity + field + user.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The entity display info field.
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   Current entity.
   *
   * @return string[]
   *   The list of allowed values:
   *   - keys are the option values,
   *   - values are the plain text option labels.
   */
  protected function getAllowedValues(
    DisplayFieldInterface $field,
    FieldableEntityInterface $entity
  ): array {
    $storage_definition = $field->getFieldDefinition()
      ->getFieldStorageDefinition();
    $main_property = $storage_definition->getMainPropertyName();
    if (!isset($main_property)) {
      $field_name = $field->getFieldDefinition()
        ->getName();
      throw new \InvalidArgumentException(sprintf(
        'The normalizer needs main property defined on the %s field.',
        $field_name
      ));
    }

    return $storage_definition
      ->getOptionsProvider($main_property, $entity)
      ->getSettableOptions($this->currentUser);
  }

}
