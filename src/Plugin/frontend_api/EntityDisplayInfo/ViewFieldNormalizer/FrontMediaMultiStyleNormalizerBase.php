<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;

/**
 * Base normalizer for the multi-style image formatter.
 *
 * @see \Drupal\frontend_api\Plugin\Field\FieldFormatter\FrontMediaMultiStyleFormatterBase
 */
abstract class FrontMediaMultiStyleNormalizerBase extends EntityReferenceNormalizerBase implements NormalizerAwareInterface {

  use NormalizerAwareTrait;

  /**
   * The image stylist.
   *
   * @var \Drupal\frontend_api\Rest\Media\ImageStylistInterface
   */
  protected $stylist;

  /**
   * The formatter settings reader.
   *
   * @var \Drupal\frontend_api\Field\Formatter\MultiStyleImageSettingsReaderInterface
   */
  protected $formatterSettingsReader;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
      $container->get('entity.repository')
    );

    $instance->stylist = $container->get('frontend_api.rest.image_stylist');
    $instance->formatterSettingsReader = $container->get(
      'frontend_api.field.formatter.multi_style_image_settings_reader'
    );

    return $instance;
  }

  /**
   * Returns the image style specified on the formatter settings.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface $field
   *   The entity view info field.
   *
   * @return \Drupal\image\ImageStyleInterface[]|null[]
   *   The image styles or NULL in case it's an original image.
   */
  protected function getFormatterImageStyles(ViewFieldInterface $field): array {
    $formatter_settings = $field->getComponentData()['settings'] ?? [];
    $style_names = $this->formatterSettingsReader
      ->getStyleNames($formatter_settings);
    return $this->stylist->getImageStyles($style_names);
  }

  /**
   * Normalizes a single field item of an image field.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The image field item.
   * @param \Drupal\image\ImageStyleInterface[]|null[] $styles
   *   The image styles. NULL value of the item means it must be an original
   *   image.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   *
   * @return mixed
   *   The styled and normalized image.
   */
  protected function normalizeImageItem(
    FieldItemInterface $item,
    array $styles,
    string $format = NULL,
    array $context = []
  ) {
    $result = [];

    foreach ($styles as $key => $style) {
      $styled_image = $this->stylist->buildStyledImage($item, $style);
      $result[$key] = $this->normalizer
        ->normalize($styled_image, $format, $context);
    }

    return $result;
  }

}
