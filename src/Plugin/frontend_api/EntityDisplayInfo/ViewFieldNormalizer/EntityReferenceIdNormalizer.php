<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;

/**
 * Provides normalizer for an entity id formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "entity_reference_entity_id",
 *   field_types = {
 *     "entity_reference",
 *   },
 *   formatter_types = {
 *     "entity_reference_entity_id",
 *   }
 * )
 */
class EntityReferenceIdNormalizer extends DefaultNormalizer {

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldValue(
    ViewFieldInterface $field,
    FieldItemListInterface $itemList,
    string $format = NULL,
    array $context = []
  ) {
    $result = parent::normalizeFieldValue($field, $itemList, $format, $context);

    $fieldDefinition = $field->getFieldDefinition();
    $entityType = $fieldDefinition->getFieldStorageDefinition()
      ->getSettings()['target_type'];
    $result['entity_type_id'] = $entityType;

    return $result;
  }

}
