<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;

/**
 * Provides normalizer for an file URL formatter.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "frontend_api_file_url",
 *   field_types = {
 *     "file",
 *   },
 *   formatter_types = {
 *     "frontend_api_file_url",
 *   },
 * )
 */
class FrontFileUrlNormalizer extends EntityReferenceNormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected function normalizeValueLiteral(
    DisplayFieldInterface $field,
    array $value
  ) {
    return $this->flattenValueLiteralItemList($field, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldValue(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $result = [];

    $label = $this->getFieldLabel($field);
    if (isset($label)) {
      $result[static::LABEL_KEY] = $label;
    }

    $result[static::VALUE_KEY] = $this->normalizeFieldItemList(
      $field,
      $item_list,
      $format,
      $context
    );

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ) {
    $result = [];

    $files = $this->getReferencedEntitiesToDisplay($item_list, $context);
    foreach ($files as $file) {
      $result[] = $file->createFileUrl(FALSE);
    }

    return $this->normalizeValueLiteral($field, $result);
  }

}
