<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;

/**
 * Provides normalizer for a multiline plain text field.
 *
 * @EntityViewInfoFieldNormalizer(
 *   id = "plain_text",
 *   field_types = {
 *     "string_long",
 *   },
 *   formatter_types = {
 *     "basic_string",
 *   }
 * )
 */
class PlainTextNormalizer extends StringNormalizer {

  /**
   * {@inheritdoc}
   */
  protected function normalizeFieldItemList(
    ViewFieldInterface $field,
    FieldItemListInterface $itemList,
    string $format = NULL,
    array $context = []
  ) {
    $normalized = [];
    foreach ($itemList as $delta => $item) {
      $normalized[$delta] = $item->getString();
    }

    return $this->flattenValueLiteralItemList($field, $normalized);
  }

}
