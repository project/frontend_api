<?php

namespace Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FieldConstraintNormalizer;

use Drupal\Core\Plugin\PluginBase;
use Drupal\field_constraints\ConfigurableFieldConstraintInterface;
use Drupal\field_constraints\FieldConstraintInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface;
use Drupal\frontend_api\Rest\FieldConstraint\FieldConstraintNormalizerInterface;

/**
 * Provides base class for the field constraint normalizer.
 */
abstract class FieldConstraintNormalizerBase extends PluginBase implements FieldConstraintNormalizerInterface {

  /**
   * The constraint type key in the normalized result.
   */
  public const TYPE_KEY = 'type';

  /**
   * The error message key in the normalized result.
   */
  public const MESSAGE_KEY = 'message';

  /**
   * The front constraint type.
   *
   * Must be overridden in child classes.
   */
  protected const FRONT_CONSTRAINT_TYPE = NULL;

  /**
   * The map of constraint configuration keys exposed to the front.
   *
   * The keys are field constraint config keys, values are keys in the
   * normalized result.
   *
   * Could be specified in child classes if they are OK about just exposing the
   * config keys as-is.
   *
   * @see normalizeConstraintConfig()
   */
  protected const EXPOSED_CONFIG_MAP = [];

  /**
   * Returns the front constraint type.
   *
   * @return string
   *   The front constraint type.
   */
  protected function getFrontConstraintType(): string {
    return static::FRONT_CONSTRAINT_TYPE;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeConstraint(
    FormFieldInterface $field,
    FieldConstraintInterface $constraint
  ): ?array {
    $config = $this->getConstraintConfig($constraint);
    $result = $this->normalizeConstraintConfig($field, $constraint, $config);
    if ($result === NULL) {
      return NULL;
    }

    $result += [
      static::TYPE_KEY => $this->getFrontConstraintType(),
    ];
    return $result;
  }

  /**
   * Returns constraint configuration array.
   *
   * @param \Drupal\field_constraints\FieldConstraintInterface $constraint
   *   The field constraint.
   *
   * @return array
   *   The configuration or empty array for non-configurable constraints.
   */
  protected function getConstraintConfig(
    FieldConstraintInterface $constraint
  ): array {
    if (!$constraint instanceof ConfigurableFieldConstraintInterface) {
      return [];
    }

    return $constraint->getConfiguration();
  }

  /**
   * Normalizes the field constraint config.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface $field
   *   The field.
   * @param \Drupal\field_constraints\FieldConstraintInterface $constraint
   *   The constraint instance.
   * @param array $config
   *   The constraint config to normalize.
   *
   * @return array|null
   *   The normalized constraint config or NULL to completely hide this
   *   constraint from the front API.
   */
  protected function normalizeConstraintConfig(
    FormFieldInterface $field,
    FieldConstraintInterface $constraint,
    array $config
  ): ?array {
    $result = [];
    foreach (static::EXPOSED_CONFIG_MAP as $config_key => $exposed_key) {
      $result[$exposed_key] = $config[$config_key] ?? NULL;
    }
    return $result;
  }

}
