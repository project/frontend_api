<?php

namespace Drupal\frontend_api\Plugin\frontend_api\Views\ExposedHandlerNormalizer;

use Drupal\frontend_api\Rest\FrontWidgetTypes;
use Drupal\frontend_api\Rest\Views\ExposedFilterNormalizerBase;
use Drupal\views\Plugin\views\ViewsHandlerInterface;

/**
 * Provides normalizer of the Search API text Views filter.
 *
 * @ViewsExposedHandlerNormalizer(
 *   id = "filter_search_api_text",
 *   exposed_forms = {"*"},
 *   handlers = {
 *     "search_api_text",
 *   },
 * )
 */
class SearchApiTextFilterNormalizer extends ExposedFilterNormalizerBase {

  /**
   * The widget type of the filter value.
   */
  protected const VALUE_WIDGET_TYPE = FrontWidgetTypes::INPUT;

  /**
   * {@inheritdoc}
   */
  protected function normalizeValueFormElement(
    ViewsHandlerInterface $handler,
    array $element,
    array $exposed_info
  ): array {
    return [
      'widget_type' => static::VALUE_WIDGET_TYPE,
      'label' => $exposed_info['label'],
      'required' => $element['#required'] ?? FALSE,
      'value' => $exposed_info['#value'] ?? NULL,
      'conditional_fields' => [],
      'description' => $exposed_info['description'] ?? '',
    ];
  }

}
