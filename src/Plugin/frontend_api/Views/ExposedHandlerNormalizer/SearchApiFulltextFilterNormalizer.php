<?php

namespace Drupal\frontend_api\Plugin\frontend_api\Views\ExposedHandlerNormalizer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\search_api_autocomplete\SearchInterface;
use Drupal\frontend_api\Rest\SearchApi\Autocomplete\SuggestionListBuilderBase;
use Drupal\frontend_api\Rest\Views\ExposedFilterNormalizerBase;
use Drupal\frontend_api\Dictionary\EntityTypes;
use Drupal\views\Plugin\views\ViewsHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides exposed Views filter normalizer for the Search API fulltext.
 *
 * @ViewsExposedHandlerNormalizer(
 *   id = "filter_search_api_fulltext",
 *   exposed_forms = {"*"},
 *   handlers = {
 *     "search_api_fulltext",
 *   },
 * )
 */
class SearchApiFulltextFilterNormalizer extends ExposedFilterNormalizerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity type ID of the Search API Autocomplete search entity.
   */
  protected const SEARCH_ENTITY_TYPE_ID = EntityTypes::SEARCH_API_AUTOCOMPLETE;

  /**
   * Query variable name of the autocomplete suggestion keys.
   */
  protected const AUTOCOMPLETE_KEYWORDS_QUERY_VAR = SuggestionListBuilderBase::KEYWORDS_QUERY_VAR;

  /**
   * Default route name of the autocomplete.
   */
  public const AUTOCOMPLETE_DEFAULT_ROUTE_NAME = 'search_api_autocomplete.autocomplete';

  /**
   * The route name of the REST resource that provides autocomplete suggestions.
   */
  public const AUTOCOMPLETE_REST_ROUTE_NAME = 'rest.frontend_api_search_api_autocomplete.GET';

  /**
   * Widget type of the value widget exposed to the API.
   */
  public const VALUE_WIDGET_TYPE = 'search';

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    return $instance;
  }

  /**
   * Sets the entity type manager.
   *
   * Protected setter for the factory method.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @return static
   */
  protected function setEntityTypeManager(
    EntityTypeManagerInterface $entity_type_manager
  ): SearchApiFulltextFilterNormalizer {
    $this->entityTypeManager = $entity_type_manager;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function normalizeValueFormElement(
    ViewsHandlerInterface $handler,
    array $element,
    array $exposed_info
  ): array {
    $result = [
      'widget_type' => static::VALUE_WIDGET_TYPE,
      'placeholder' => $element['#attributes']['placeholder'] ?? NULL,
      'title' => $exposed_info['label'] ?? NULL,
    ];

    $this->addAutocompleteToValueWidget($result, $element);

    return $result;
  }

  /**
   * Adds autocomplete properties to the normalized value widget data.
   *
   * @param array $result
   *   The value widget data.
   * @param array $element
   *   The source form element.
   */
  protected function addAutocompleteToValueWidget(
    array &$result,
    array $element
  ): void {
    if ($element['#type'] !== 'search_api_autocomplete') {
      return;
    }

    $search_id = $element['#search_id'] ?? NULL;
    if (!isset($search_id)) {
      return;
    }

    $search = $this->entityTypeManager
      ->getStorage(static::SEARCH_ENTITY_TYPE_ID)
      ->load($search_id);
    if (!$search instanceof SearchInterface) {
      return;
    }

    // Replace original route with a custom one that returns results in expected
    // format.
    $default_route_name = static::AUTOCOMPLETE_DEFAULT_ROUTE_NAME;
    $route_name = $element['#autocomplete_route_name'] ?? $default_route_name;
    if ($route_name === $default_route_name) {
      $route_name = static::AUTOCOMPLETE_REST_ROUTE_NAME;
    }

    // Quit if autocomplete route not specified.
    if ($route_name === FALSE) {
      return;
    }

    $route_parameters = $element['#autocomplete_route_parameters'] ?? [];

    // @todo Take care about the URL cacheable metadata.
    $url = Url::fromRoute($route_name, $route_parameters);
    $result['autocomplete_url'] = $url->toString();
    $result['autocomplete_keywords_var'] = static::AUTOCOMPLETE_KEYWORDS_QUERY_VAR;
    $result['autocomplete_min_length'] = $search->getOption('min_length') ?? 1;
  }

}
