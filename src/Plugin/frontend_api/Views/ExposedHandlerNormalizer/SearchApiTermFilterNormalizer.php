<?php

namespace Drupal\frontend_api\Plugin\frontend_api\Views\ExposedHandlerNormalizer;

use Drupal\frontend_api\Rest\Views\ExposedFilterNormalizerBase;
use Drupal\views\Plugin\views\ViewsHandlerInterface;

/**
 * Provides exposed filter normalizer for the Search API term filters.
 *
 * @ViewsExposedHandlerNormalizer(
 *   id = "filter_search_api_term",
 *   exposed_forms = {"*"},
 *   handlers = {
 *     "search_api_term",
 *   },
 * )
 */
class SearchApiTermFilterNormalizer extends ExposedFilterNormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected function normalizeValueFormElement(
    ViewsHandlerInterface $handler,
    array $element,
    array $exposed_info
  ): array {
    $options = [];
    foreach ($element['#options'] as $key => $value) {
      if ($key != 'All') {
        $options[] = ['key' => $key, 'label' => strval($value)];
      }
    }
    return [
      'widget_type' => 'multiselect',
      'label' => $exposed_info['label'],
      'options' => $options,
      'required' => $element['#required'] ?? FALSE,
      'value' => $element['#value'] ?? NULL,
      'conditional_fields' => [],
      'description' => $exposed_info['description'] ?? '',
    ];
  }

}
