<?php

namespace Drupal\frontend_api\Plugin\frontend_api\Views\ExposedHandlerNormalizer;

use Drupal\frontend_api\Rest\FrontWidgetTypes;
use Drupal\frontend_api\Rest\Views\ExposedFilterNormalizerBase;
use Drupal\views\Plugin\views\ViewsHandlerInterface;

/**
 * Provides normalizer of the string Views filter.
 *
 * @ViewsExposedHandlerNormalizer(
 *   id = "filter_string",
 *   exposed_forms = {"*"},
 *   handlers = {
 *     "string",
 *   },
 * )
 */
class StringFilterNormalizer extends ExposedFilterNormalizerBase {

  /**
   * The widget type of the filter value.
   */
  protected const VALUE_WIDGET_TYPE = FrontWidgetTypes::INPUT;

  /**
   * {@inheritdoc}
   */
  protected function normalizeValueFormElement(
    ViewsHandlerInterface $handler,
    array $element,
    array $exposed_info
  ): array {
    return [
      'widget_type' => static::VALUE_WIDGET_TYPE,
      'label' => $exposed_info['label'],
      'required' => $element['#required'] ?? FALSE,
      'value' => $element['#value'] ?? NULL,
      'conditional_fields' => [],
      'description' => $exposed_info['description'] ?? '',
    ];
  }

}
