<?php

namespace Drupal\frontend_api\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\frontend_api\Dictionary\EntityTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base class for Search API based entity autocomplete widgets.
 *
 * It provides configuration options for view + display and its exposed filter
 * handler to use for autocomplete suggestions.
 *
 * @see \Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer\SearchApiEntityAutocompleteNormalizerBase
 */
class SearchApiEntityAutocompleteWidgetBase extends WidgetBase implements ContainerFactoryPluginInterface {

  use FrontOnlyWidgetTrait,
    StringTranslationTrait;

  /**
   * Autocomplete view display setting.
   *
   * The setting contains a view and its display name separated with ":". The
   * display specified here is used by the widget to retrieve the autocomplete
   * results through Search API Autocomplete.
   */
  public const VIEW_DISPLAY_SETTING = 'autocomplete_display';

  /**
   * The view filter setting.
   *
   * The setting contains an exposed filter identifier of the view that is used
   * by Search API Autocomplete when retrieving the autocomplete suggestions for
   * the widget.
   */
  public const VIEW_FILTER_SETTING = 'autocomplete_filter';

  /**
   * The list of Views filter plugin IDs that could be used as autocomplete.
   */
  public const SUPPORTED_FILTER_PLUGINS = [
    'search_api_fulltext' => TRUE,
  ];

  /**
   * The view entity type ID.
   */
  protected const VIEW_ENTITY_TYPE = EntityTypes::VIEW;

  /**
   * The Search API index entity type ID.
   */
  protected const INDEX_ENTITY_TYPE = EntityTypes::SEARCH_API_INDEX;

  /**
   * The Search API autocomplete entity type ID.
   */
  protected const AUTOCOMPLETE_ENTITY_TYPE = EntityTypes::SEARCH_API_AUTOCOMPLETE;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings']
    );

    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = [
      static::VIEW_DISPLAY_SETTING => '',
      static::VIEW_FILTER_SETTING => '',
    ];

    $settings += parent::defaultSettings();

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element[static::VIEW_DISPLAY_SETTING] = [
      '#title' => $this->t('Autocomplete view display'),
      '#type' => 'select',
      '#chosen' => TRUE,
      '#required' => TRUE,
      '#default_value' => $this->getSetting(
        static::VIEW_DISPLAY_SETTING
      ),
      '#empty_value' => '',
      '#options' => $this->generateViewDisplayOptions(),
      '#ajax' => [
        'callback' => [$this, 'buildAjaxFilter'],
        'wrapper' => 'autocomplete-filter',
      ],
    ];

    $element[static::VIEW_FILTER_SETTING] = [
      '#title' => $this->t('Search filter'),
      '#type' => 'select',
      '#required' => TRUE,
      '#default_value' => $this->getSetting(
        static::VIEW_FILTER_SETTING
      ),
      '#options' => $this->generateViewFilterOptions($form_state),
      '#empty_value' => '',

      // @todo Find a way to avoid hard-coded ID.
      '#prefix' => '<div id="autocomplete-filter">',
      '#suffix' => '</div>',
    ];

    return $element;
  }

  /**
   * Ajax callback for autocomplete filter.
   *
   * @param array $form
   *   Built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Form element to replace.
   */
  public function buildAjaxFilter(array $form, FormStateInterface $form_state) {
    $element = NestedArray::getValue($form, [
      'fields',
      $this->fieldDefinition->getName(),
      'plugin',
      'settings_edit_form',
      'settings',
      static::VIEW_FILTER_SETTING,
    ]);

    return $element;
  }

  /**
   * Get value depending on form input.
   *
   * @param array $input_values
   *   Form input.
   *
   * @return string|null
   *   Key value.
   */
  protected function extractSelectedViewDisplayValue(array $input_values) {
    $settings_form = NestedArray::getValue(
      $input_values,
      [
        'fields',
        $this->fieldDefinition->getName(),
        'settings_edit_form',
      ]
    );

    if (empty($settings_form)) {
      return $this->getSetting(static::VIEW_DISPLAY_SETTING);
    }

    return NestedArray::getValue(
      $settings_form,
      [
        'settings',
        static::VIEW_DISPLAY_SETTING,
      ]
    );
  }

  /**
   * Get filter options.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Filter options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function generateViewFilterOptions(FormStateInterface $form_state) {
    $form_values = $form_state->getUserInput();

    $view_display_value = $this->extractSelectedViewDisplayValue($form_values);
    if (!isset($view_display_value)) {
      return [];
    }

    [$view_id, $display_id] = explode(':', $view_display_value);
    return $this->getExposedFilters($view_id, $display_id);
  }

  /**
   * Extract fulltext exposed filters from view display.
   *
   * @param string $view_id
   *   View id.
   * @param string $selected_display_id
   *   View display.
   *
   * @return array
   *   Possible filter options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getExposedFilters($view_id, $selected_display_id) {
    $view = $this->entityTypeManager
      ->getStorage(static::VIEW_ENTITY_TYPE)
      ->load($view_id);
    if (empty($view)) {
      return [];
    }

    /* @var \Drupal\views\ViewEntityInterface $view */
    $displays = $view->get('display');

    $filters = [];
    $allowed_display_ids = [
      'default' => TRUE,
      $selected_display_id => TRUE,
    ];
    foreach ($displays as $display) {
      $display_id = $display['id'];
      if (!isset($allowed_display_ids[$display_id])) {
        continue;
      }

      $display_filters = $display['display_options']['filters'] ?? NULL;
      if (!is_array($display_filters)) {
        continue;
      }

      $filters = array_merge($filters, $display_filters);
    }

    // Extract only exposed fulltext search filters.
    $options = [];
    foreach ($filters as $filter) {
      if (empty($filter['exposed'])) {
        continue;
      }

      $plugin_id = $filter['plugin_id'] ?? '';
      if (!isset(static::SUPPORTED_FILTER_PLUGINS[$plugin_id])) {
        continue;
      }

      $identifier = $filter['expose']['identifier'];
      $label = $filter['expose']['label'];
      $options[$identifier] = $label;
    }

    return $options;
  }

  /**
   * Returns target entity type ID.
   *
   * @return string
   *   The referenced entity type ID.
   */
  protected function getTargetEntityTypeId(): string {
    return $this->fieldDefinition
      ->getFieldStorageDefinition()
      ->getSetting('target_type');
  }

  /**
   * Get display options for autocomplete.
   *
   * @return array
   *   List of options keyed as %view_id%:%display_id% => %label.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function generateViewDisplayOptions() {
    // Generate autocomplete view options.
    // Right now only 'solr search api' views counted.
    // 1) get search indexes that has entity as datasource;
    // 2) filter indexes that has autocomplete feature;
    // 3) load views based on this data;
    // 4) generate keys %view_id%:%display_id% for select.
    $data_source_index = $this->entityTypeManager
      ->getStorage(static::INDEX_ENTITY_TYPE)
      ->getQuery()
      ->accessCheck(FALSE)
      ->exists(
        'datasource_settings.entity:' . $this->getTargetEntityTypeId()
      )
      ->execute();

    if (empty($data_source_index)) {
      return [];
    }

    $indexes_autocomplete = $this->entityTypeManager
      ->getStorage(static::AUTOCOMPLETE_ENTITY_TYPE)
      ->loadByProperties(['index_id' => $data_source_index]);

    $allowed_indexes = [];
    foreach ($indexes_autocomplete as $index) {
      /* @var \Drupal\search_api_autocomplete\Entity\Search $index */
      $allowed_indexes[] = 'search_api_index_' . $index->getIndexId();
    }

    if (empty($allowed_indexes)) {
      return [];
    }

    $view_storage = $this->entityTypeManager
      ->getStorage(static::VIEW_ENTITY_TYPE);
    $search_api_views = $view_storage->loadByProperties([
      'base_table' => $allowed_indexes,
    ]);

    $options = [];
    foreach ($search_api_views as $view) {
      $displays = $view->get('display');
      $view_id = $view->id();

      foreach ($displays as $display) {
        $key = $view_id . ':' . $display['id'];
        $options[$key] = $view->label() . ' - ' . $display['display_title'];
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $summary[] = $this->t('Views display: @id', [
      '@id' => $this->getSetting(static::VIEW_DISPLAY_SETTING),
    ]);
    $summary[] = $this->t('Filter: @id', [
      '@id' => $this->getSetting(static::VIEW_FILTER_SETTING),
    ]);

    return $summary;
  }

}
