<?php

namespace Drupal\frontend_api\Plugin\Field\FieldWidget;

use Drupal\frontend_api\Dictionary\EntityTypes;
use Drupal\frontend_api\Field\EntityReference\TargetTypeLimitedComponentTrait;

/**
 * Provides front-only widget with media list editable through a pop-in.
 *
 * @FieldWidget(
 *   id = "frontend_api_popin_media",
 *   label = @Translation("Front-end API: pop-in media"),
 *   field_types = {
 *     "entity_reference",
 *   },
 * )
 *
 * @see \Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer\FrontPopinMediaNormalizer
 */
class FrontPopinMediaWidget extends FrontInlineEntityWidgetBase {

  use TargetTypeLimitedComponentTrait;

  /**
   * The referenced entity type ID this widget supports.
   */
  protected const TARGET_ENTITY_TYPE = EntityTypes::MEDIA;

}
