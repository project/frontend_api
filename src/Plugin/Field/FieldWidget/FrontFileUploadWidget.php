<?php

namespace Drupal\frontend_api\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;

/**
 * Front-only file upload widget.
 *
 * @FieldWidget(
 *   id = "frontend_api_file_upload",
 *   label = @Translation("Front-end API: File upload"),
 *   field_types = {
 *     "file",
 *   },
 * )
 *
 * @see \Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer\FrontFileUploadNormalizer
 */
class FrontFileUploadWidget extends WidgetBase {

  use FrontOnlyWidgetTrait;

}
