<?php

namespace Drupal\frontend_api\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides front widget displayed as entity autocomplete with query parameters.
 *
 * An admin configures the map of query parameters to the field names. The map
 * is exposed to the front app, so that it could collect query parameters and
 * pass them to the autocomplete REST resource. The rest resource reads all the
 * configured query parameters and passes their values to the selection handler
 * as an array in the handler settings.
 *
 * @FieldWidget(
 *   id = "frontend_api_entity_autocomplete_query",
 *   label = @Translation("Front-end API: Autocomplete with query parameters"),
 *   description = @Translation("An autocomplete text field with field values passed as query parameters."),
 *   field_types = {
 *     "entity_reference",
 *   },
 * )
 *
 * @see \Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer\FrontEntityAutocompleteQueryNormalizer
 */
class FrontEntityAutocompleteQueryWidget extends EntityReferenceAutocompleteWidget {

  use FrontOnlyWidgetTrait,
    StringTranslationTrait;

  /**
   * The setting name of the query parameter to the field name map.
   */
  public const QUERY_FIELD_MAP_SETTING = 'query_field_map';

  /**
   * The separator between the query parameter name and the field name.
   */
  public const QUERY_FIELD_SEPARATOR = ':';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $config = parent::defaultSettings();

    $config[static::QUERY_FIELD_MAP_SETTING] = [];

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $default_value = [];
    $map = $this->getSetting(static::QUERY_FIELD_MAP_SETTING);
    foreach ($map as $parameter_name => $field_name) {
      $default_value[] = $parameter_name
        . static::QUERY_FIELD_SEPARATOR
        . $field_name;
    }
    $default_value = implode("\n", $default_value);

    $form[static::QUERY_FIELD_MAP_SETTING] = [
      '#type' => 'textarea',
      '#title' => $this->t('Query parameters'),
      '#description' => $this->t(
        'Put one query parameter per line. Every line should specify the query parameter name + the corresponding field name. The value of the field on the form is then sent to the autocomplete endpoint and passed to the selection handler. Use the format: <code>parameter_name:field_name</code>.'
      ),
      '#default_value' => $default_value,
      '#element_validate' => [
        [static::class, 'validateQueryFieldMap'],
      ],
    ];

    // Hide irrelevant settings.
    $form['size']['#access'] = FALSE;
    $form['placeholder']['#access'] = FALSE;

    return $form;
  }

  /**
   * The element validate callback for the query parameter to field name map.
   *
   * It parses the textarea input into an array for storage.
   *
   * @param array $element
   *   The textarea form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @see settingsForm()
   */
  public static function validateQueryFieldMap(
    array $element,
    FormStateInterface $form_state
  ) {
    $result_value = [];
    $input_value = trim($element['#value']);
    if ($input_value !== '') {
      $input_lines = preg_split('@\R@', $input_value, -1, PREG_SPLIT_NO_EMPTY);
      foreach ($input_lines as $input_line) {
        $input_line = trim($input_line);
        if ($input_line === '') {
          continue;
        }

        $parts = explode(static::QUERY_FIELD_SEPARATOR, $input_line, 2);
        if (count($parts) !== 2) {
          $form_state->setError($element, t('Invalid line: @line', [
            '@line' => $input_line,
          ]));
          continue;
        }

        [$parameter_name, $field_name] = $parts;
        $result_value[$parameter_name] = $field_name;
      }
    }

    $value_path = $element['#parents'];
    $form_state->setValue($value_path, $result_value);
  }

}
