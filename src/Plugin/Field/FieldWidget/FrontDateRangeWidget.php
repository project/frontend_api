<?php

namespace Drupal\frontend_api\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Special widget that can limit user input.
 *
 * @FieldWidget(
 *   id = "frontend_api_date_range",
 *   label = @Translation("Front-end API: Date range"),
 *   field_types = {
 *     "daterange",
 *   },
 * )
 */
class FrontDateRangeWidget extends WidgetBase {

  use FrontOnlyWidgetTrait,
    StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'disabled_before' => 'now',
      'date' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];
    $elements['disabled_before'] = [
      '#type' => 'select',
      '#title' => $this->t('Disabled before'),
      '#description' => $this->t('Dates before given value will be disabled on FE datepicker'),
      '#default_value' => $this->getSetting('disabled_before'),
      '#required' => TRUE,
      '#id' => 'disabled_before',
      '#options' => [
        'now' => $this->t('Now'),
        'custom' => $this->t('Custom'),
      ],
    ];
    $elements['date'] = [
      '#type' => 'date',
      '#title' => $this->t('Date'),
      '#default_value' => $this->getSetting('date'),
      '#states' => [
        'visible' => [
          ':input[id="disabled_before"]' => ['value' => 'custom'],
        ],
        'required' => [
          ':input[id="disabled_before"]' => ['value' => 'custom'],
        ],
      ],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $disabledBefore = $this->getSetting('disabled_before') === 'now'
      ? $this->t('Now')
      : $this->getSetting('date');

    $summary[] = $this->t(
      'Disabled before: @disabled_before',
      ['@disabled_before' => $disabledBefore]
    );

    return $summary;
  }

}
