<?php

namespace Drupal\frontend_api\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\frontend_api\Exception\FrontOnlyComponentException;

/**
 * Provides trait for a widget that is only usable on the front app.
 *
 * Such a widget handles settings that are then exposed to the front app by a
 * normalizer. It can't be displayed on a Drupal form.
 */
trait FrontOnlyWidgetTrait {

  /**
   * Returns the form for a single field widget.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   Array of default values for this field.
   * @param int $delta
   *   The order of this item in the array of sub-elements.
   * @param array $element
   *   A form element array containing basic properties for the widget.
   * @param array $form
   *   The form structure where widgets are being attached to.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    throw new FrontOnlyComponentException();
  }

}
