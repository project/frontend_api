<?php

namespace Drupal\frontend_api\Plugin\Field\FieldWidget;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Datetime\DateFormatInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\frontend_api\Dictionary\EntityTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides widget that displays date/timestamp as non-editable date.
 *
 * @FieldWidget(
 *   id = "frontend_api_non_editable_date",
 *   label = @Translation("Front-end API: Non-editable date"),
 *   field_types = {
 *     "timestamp",
 *     "created",
 *     "changed",
 *   },
 * )
 */
class FrontNonEditableDateWidget extends WidgetBase {

  use FrontOnlyWidgetTrait;

  /**
   * The date format setting key.
   */
  public const DATE_FORMAT_SETTING = 'date_format';

  /**
   * The date format entity type ID.
   */
  protected const DATE_FORMAT_ENTITY_TYPE = EntityTypes::DATE_FORMAT;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->time = $container->get('datetime.time');
    $instance->dateFormatter = $container->get('date.formatter');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    $settings[static::DATE_FORMAT_SETTING] = NULL;

    return $settings;
  }

  /**
   * Returns the date format configured through settings.
   *
   * @return \Drupal\Core\Datetime\DateFormatInterface|null
   *   The date format or NULL if it isn't set or can't be loaded.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getDateFormat(): ?DateFormatInterface {
    // The widget depends on selected date format.
    $date_format_id = $this->getSetting(static::DATE_FORMAT_SETTING);
    if ($date_format_id === NULL) {
      return NULL;
    }

    /** @var \Drupal\Core\Datetime\DateFormatInterface $date_format */
    $date_format = $this->entityTypeManager
      ->getStorage(static::DATE_FORMAT_ENTITY_TYPE)
      ->load($date_format_id);
    return $date_format;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();

    // The widget depends on selected date format.
    $date_format = $this->getDateFormat();
    if ($date_format) {
      $dependency_key = $date_format->getConfigDependencyKey();
      $dependencies[$dependency_key][] = $date_format->getConfigDependencyName();
    }

    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element[static::DATE_FORMAT_SETTING] = [
      '#required' => TRUE,
      '#type' => 'select',
      '#title' => $this->t('Date format'),
      '#options' => $this->getDateFormatOptions(),
      '#default_value' => $this->getSetting(
        static::DATE_FORMAT_SETTING
      ),
    ];

    return $element;
  }

  /**
   * Returns options for the date format setting.
   *
   * @return array
   *   The options list.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getDateFormatOptions(): array {
    $options = [];

    $formats = $this->entityTypeManager
      ->getStorage(EntityTypes::DATE_FORMAT)
      ->loadMultiple();
    $timestamp = $this->time->getRequestTime();
    foreach ($formats as $format) {
      $format_id = $format->id();
      $sample = $this->dateFormatter
        ->format($timestamp, $format_id);
      $options[$format_id] = new FormattableMarkup(
        '@label (@sample)', [
          '@label' => $format->label(),
          '@sample' => $sample,
        ]
      );
    }
    asort($options);

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $date_format = $this->getDateFormat();
    if ($date_format) {
      $summary[] = $this->t('Date format: @format', [
        '@format' => $date_format->label(),
      ]);
    }

    return $summary;
  }

}
