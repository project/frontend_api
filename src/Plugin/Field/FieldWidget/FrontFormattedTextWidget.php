<?php

namespace Drupal\frontend_api\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides front-only widget for formatted text field.
 *
 * Forces front app to use text format that was set here in settings.
 *
 * @FieldWidget(
 *   id = "frontend_api_formatted_text",
 *   label = @Translation("Front-end API: formatted text"),
 *   field_types = {
 *     "text_with_summary",
 *   },
 * )
 */
class FrontFormattedTextWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  use FrontOnlyWidgetTrait,
    StringTranslationTrait;

  /**
   * The format setting key.
   */
  public const FORMAT_KEY = 'format';

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition
    );
    $instance->currentUser = $container->get('current_user');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = [
      static::FORMAT_KEY => NULL,
    ];

    $settings += parent::defaultSettings();

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $summary[] = $this->t('Text format: @id', [
      '@id' => $this->getSetting(static::FORMAT_KEY),
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $formState) {
    $formats = filter_formats($this->currentUser);
    $options = [];
    foreach ($formats as $id => $filterFormat) {
      $options[$id] = $filterFormat->label();
    }
    $form[static::FORMAT_KEY] = [
      '#type' => 'select',
      '#title' => $this->t('Filter format'),
      '#default_value' => $this->getSetting(static::FORMAT_KEY),
      '#options' => $options,
      '#required' => TRUE,
    ];

    return $form;
  }

}
