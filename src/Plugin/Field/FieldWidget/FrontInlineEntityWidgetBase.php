<?php

namespace Drupal\frontend_api\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityFormModeInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\frontend_api\Dictionary\EntityTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class of front widgets that allow inline editing of referenced entities.
 *
 * @see \Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer\FrontInlineEntityNormalizerBase
 */
abstract class FrontInlineEntityWidgetBase extends WidgetBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait,
    FrontOnlyWidgetTrait;

  /**
   * The form mode key in the widget settings.
   */
  public const FORM_MODE_KEY = 'form_mode';

  /**
   * The the widget settings that enables orphan inline entities deletion.
   */
  public const DELETE_ORPHAN_ENTITIES_KEY = 'delete_orphan_entities';

  /**
   * The form mode entity type ID.
   */
  protected const FORM_MODE_ENTITY_TYPE = EntityTypes::ENTITY_FORM_MODE;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity reference settings reader.
   *
   * @var \Drupal\frontend_api\Field\EntityReference\EntityReferenceSettingsReaderInterface
   */
  protected $entityReferenceSettingsReader;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings']
    );

    $instance->entityDisplayRepository = $container->get(
      'entity_display.repository'
    );
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityReferenceSettingsReader = $container->get(
      'frontend_api.field.entity_reference.settings_reader'
    );

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    $settings[static::FORM_MODE_KEY] = 'default';
    $settings[static::DELETE_ORPHAN_ENTITIES_KEY] = FALSE;

    return $settings;
  }

  /**
   * Returns target entity type ID of the reference field.
   *
   * @return string
   *   The entity type ID.
   */
  protected function getTargetEntityTypeId(): string {
    return $this->entityReferenceSettingsReader
      ->getTargetEntityTypeId($this->fieldDefinition);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form_mode_options = $this->entityDisplayRepository
      ->getFormModeOptions($this->getTargetEntityTypeId());
    $form[static::FORM_MODE_KEY] = [
      '#type' => 'select',
      '#title' => $this->t('Form mode'),
      '#default_value' => $this->getSetting(static::FORM_MODE_KEY),
      '#options' => $form_mode_options,
      '#required' => TRUE,
    ];

    $form[static::DELETE_ORPHAN_ENTITIES_KEY] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete orphan entities'),
      '#description' => $this->t("Enable to delete referenced entities that stop being referenced. It doesn't check other references to this entity."),
      '#default_value' => $this->getSetting(static::DELETE_ORPHAN_ENTITIES_KEY),
    ];

    return $form;
  }

  /**
   * Returns the entity form mode selected on the widget settings.
   *
   * @return \Drupal\Core\Entity\EntityFormModeInterface|null
   *   The form mode entity, or NULL if it doesn't exist.
   */
  protected function getEntityFormMode(): ?EntityFormModeInterface {
    $form_mode_name = $this->getSetting(static::FORM_MODE_KEY);
    $target_type_id = $this->getTargetEntityTypeId();
    $form_mode_id = "{$target_type_id}.{$form_mode_name}";

    /** @var \Drupal\Core\Entity\EntityFormModeInterface|null $result */
    $result = $this->entityTypeManager
      ->getStorage(static::FORM_MODE_ENTITY_TYPE)
      ->load($form_mode_id);
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $result = parent::settingsSummary();

    $entity_form_mode = $this->getEntityFormMode();
    if ($entity_form_mode !== NULL) {
      $form_mode_label = $entity_form_mode->label();
    }
    else {
      $form_mode_label = $this->t('Unknown');
    }
    $result[] = $this->t('Form mode: @mode', ['@mode' => $form_mode_label]);

    if ($this->getSetting(static::DELETE_ORPHAN_ENTITIES_KEY)) {
      $message = $this->t('Orphan entities are <b>deleted</b>.');
    }
    else {
      $message = $this->t('Orphan entities are <b>not deleted</b>.');
    }
    $result[] = $message;

    return $result;
  }

}
