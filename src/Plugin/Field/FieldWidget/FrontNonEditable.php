<?php

namespace Drupal\frontend_api\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\NumberWidget;

/**
 * Indicates field that can't be edited on front or from BO.
 *
 * @FieldWidget(
 *   id = "frontend_api_non_editable",
 *   label = @Translation("Front-end API: Non-editable field"),
 *   field_types = {
 *     "integer",
 *     "email",
 *     "string",
 *   },
 * )
 */
class FrontNonEditable extends NumberWidget {

  use FrontOnlyWidgetTrait;

}
