<?php

namespace Drupal\frontend_api\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;

/**
 * Provides front-only image upload widget for the media reference.
 *
 * @FieldWidget(
 *   id = "frontend_api_media_image_upload",
 *   label = @Translation("Front-end API: Media image upload"),
 *   field_types = {
 *     "entity_reference",
 *   },
 * )
 */
class FrontMediaImageUploadWidget extends WidgetBase {

  use FrontOnlyWidgetTrait;

}
