<?php

namespace Drupal\frontend_api\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides front-only autocomplete widget base that displays embedded entities.
 *
 * @see \Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer\FrontEmbeddedEntityAutocompleteNormalizerBase
 */
abstract class FrontEmbeddedEntityAutocompleteWidgetBase extends SearchApiEntityAutocompleteWidgetBase {

  use FrontOnlyWidgetTrait,
    StringTranslationTrait;

  /**
   * The entity view mode setting.
   *
   * The view mode is used to render entity in default value. The view mode of
   * the autocomplete should be configured on the Search API autocomplete.
   */
  public const VIEW_MODE_SETTING = 'view_mode';

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->entityDisplayRepository = $container->get(
      'entity_display.repository'
    );

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = [
      static::VIEW_MODE_SETTING => '',
    ];

    $settings += parent::defaultSettings();

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element[static::VIEW_MODE_SETTING] = [
      '#title' => $this->t('Entity view mode'),
      '#type' => 'select',
      '#chosen' => TRUE,
      '#required' => TRUE,
      '#default_value' => $this->getSetting(static::VIEW_MODE_SETTING),
      '#empty_value' => '',
      '#options' => $this->generateViewModeOptions(),
      '#description' => $this->t(
        'The view mode is used to render the entity in a default value. The view mode of the autocomplete should be configured on the view and should match the one selected here in most cases.'
      ),
    ];

    return $element;
  }

  /**
   * Get entity view modes as select options.
   *
   * @return array
   *   List of possible view modes for entity to render.
   */
  protected function generateViewModeOptions() {
    $target_entity_type_id = $this->getTargetEntityTypeId();
    $entity_view_modes = $this->entityDisplayRepository
      ->getViewModes($target_entity_type_id);

    $view_mode_options = [];
    foreach ($entity_view_modes as $key => $view_mode) {
      $view_mode_options[$key] = $view_mode['label'];
    }

    return $view_mode_options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $summary[] = $this->t('View mode: @id', [
      '@id' => $this->getSetting(static::VIEW_MODE_SETTING),
    ]);

    return $summary;
  }

}
