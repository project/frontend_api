<?php

namespace Drupal\frontend_api\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Special widget that can disable select options.
 *
 * @FieldWidget(
 *   id = "frontend_api_options_select",
 *   label = @Translation("Front-end API: Options select"),
 *   field_types = {
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *   },
 * )
 */
class FrontOptionsSelectWidget extends OptionsSelectWidget {

  use FrontOnlyWidgetTrait,
    StringTranslationTrait;

  /**
   * The disabled options widget setting.
   */
  public const DISABLED_OPTIONS = 'disabled_options';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      static::DISABLED_OPTIONS => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];
    $elements[static::DISABLED_OPTIONS] = [
      '#type' => 'textarea',
      '#title' => $this->t('Disabled options'),
      '#default_value' => implode("\n", $this->getSetting(static::DISABLED_OPTIONS)),
      '#element_validate' => [
        [static::class, 'optionsDisabledElementValidate'],
      ],
    ];

    return $elements;
  }

  /**
   * The element validate callback for the disabled options textarea.
   *
   * Parses textarea value into an array.
   *
   * @param array $element
   *   The textarea element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function optionsDisabledElementValidate(
    array $element,
    FormStateInterface $form_state
  ): void {
    $value_path = $element['#parents'];
    $value = $form_state->getValue($value_path, '');
    $value = preg_split('@\R@', $value);
    $value = array_map('trim', $value);
    $value = array_filter($value, 'strlen');
    $form_state->setValue($value_path, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t(
      'Disabled options: @list',
      ['@list' => implode(', ', $this->getSetting(static::DISABLED_OPTIONS))]
    );

    return $summary;
  }

}
