<?php

namespace Drupal\frontend_api\Plugin\Field\FieldFormatter;

/**
 * Formatter that displays the thumbnail of the first media entity.
 *
 * @FieldFormatter(
 *   id = "frontend_api_media_first_thumbnail",
 *   label = @Translation("Front-end API: First media thumbnail"),
 *   field_types = {
 *     "entity_reference",
 *   },
 * )
 */
class FrontMediaFirstThumbnailFormatter extends FrontMediaFormatterBase {

}
