<?php

namespace Drupal\frontend_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Formatter that merges fields of the referenced entity to the parent fields.
 *
 * It only support single-value fields for now.
 *
 * @FieldFormatter(
 *   id = "frontend_api_entity_fields_merged",
 *   label = @Translation("Front: Entity fields merged (first only)"),
 *   field_types = {
 *     "entity_reference",
 *   }
 * )
 */
class FrontFieldsMergedFormatter extends EntityReferenceEntityFormatter {

  use FrontOnlyFormatterTrait, StringTranslationTrait;

  /**
   * Field name prefix setting.
   */
  public const PREFIX_SETTING = 'field_name_prefix';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $settings = parent::defaultSettings();

    $settings['field_name_prefix'] = NULL;

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $form = parent::settingsForm($form, $form_state);

    $form[static::PREFIX_SETTING] = [
      '#required' => TRUE,
      '#type' => 'machine_name',
      '#title' => $this->t('Field name prefix'),
      '#default_value' => $this->getSetting(static::PREFIX_SETTING),
      '#machine_name' => [
        'exists' => [static::class, 'fieldNamePrefixExists'],
        'source' => [],
      ],
    ];

    return $form;
  }

  /**
   * Machine name existence callback.
   *
   * @return bool
   *   Always TRUE, as we don't make any checks.
   */
  public static function fieldNamePrefixExists(): bool {
    // Just allow any value, as we don't care about uniqueness.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();

    $summary[] = $this->t('Field name prefix: @prefix', [
      '@prefix' => $this->getSetting(static::PREFIX_SETTING),
    ]);

    return $summary;
  }

}
