<?php

namespace Drupal\frontend_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;

/**
 * Formatter that adds referenced entity to the root list of relationships.
 *
 * @FieldFormatter(
 *   id = "frontend_api_relationship",
 *   label = @Translation("Front-end API: Relationship"),
 *   field_types = {
 *     "entity_reference",
 *   },
 * )
 */
class FrontRelationshipFormatter extends EntityReferenceEntityFormatter {

  use FrontOnlyFormatterTrait;

}
