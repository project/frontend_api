<?php

namespace Drupal\frontend_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\frontend_api\Dictionary\EntityTypes;
use Drupal\frontend_api\Field\HostTypeLimitedComponentTrait;

/**
 * Provides formatter for the oEmbed media field.
 *
 * It only supports videos inserted as an iframe at the moment.
 *
 * @FieldFormatter(
 *   id = "frontend_api_oembed",
 *   label = @Translation("Front-end API: oEmbed URL"),
 *   field_types = {
 *     "string",
 *   },
 * )
 *
 * @see \Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer\FrontOEmbedNormalizer
 */
class FrontOEmbedFormatter extends FormatterBase {

  use FrontOnlyFormatterTrait,
    HostTypeLimitedComponentTrait;

  /**
   * The entity type this formatter must be attached to.
   */
  protected const HOST_ENTITY_TYPE = EntityTypes::MEDIA;

}
