<?php

namespace Drupal\frontend_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;

/**
 * Front-end API formatter that displays key + label of the state fields.
 *
 * @FieldFormatter(
 *   id = "frontend_api_state_list_key_label",
 *   label = @Translation("Front: Key + label"),
 *   field_types = {
 *     "state",
 *   }
 * )
 *
 * @todo Merge with the FrontListKeyLabelFormatter ?
 */
class FrontStateListKeyLabelFormatter extends FormatterBase {

  use FrontOnlyFormatterTrait;

}
