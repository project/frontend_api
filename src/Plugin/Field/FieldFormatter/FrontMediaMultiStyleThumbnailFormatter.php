<?php

namespace Drupal\frontend_api\Plugin\Field\FieldFormatter;

/**
 * Formatter that displays the media thumbnails in multiple styles.
 *
 * @FieldFormatter(
 *   id = "frontend_api_media_multistyle_thumbnails",
 *   label = @Translation("Front: Multi-style media thumbnails"),
 *   field_types = {
 *     "entity_reference",
 *   },
 * )
 *
 * @see \Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer\FrontMediaMultiStyleThumbnailNormalizer
 */
class FrontMediaMultiStyleThumbnailFormatter extends FrontMediaMultiStyleFormatterBase {

}
