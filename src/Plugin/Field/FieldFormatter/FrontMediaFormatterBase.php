<?php

namespace Drupal\frontend_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Provides base formatter for the media reference front-only formatters.
 */
abstract class FrontMediaFormatterBase extends ImageFormatter {

  use FrontOnlyFormatterTrait;

  /**
   * The setting name of the referenced entity type on the field storage.
   */
  public const TARGET_TYPE_SETTING = 'target_type';

  /**
   * The media entity type ID.
   */
  public const MEDIA_ENTITY_TYPE_ID = 'media';

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    // Don't allow to select the link type, as it isn't supported by
    // normalizers.
    $form['image_link'] = [
      '#type' => 'value',
      '#value' => '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\media\Plugin\Field\FieldFormatter\MediaThumbnailFormatter::isApplicable()
   */
  public static function isApplicable(
    FieldDefinitionInterface $field_definition
  ): bool {
    $target_entity_type = $field_definition->getFieldStorageDefinition()
      ->getSetting(static::TARGET_TYPE_SETTING);

    // This formatter is only available for fields that reference media entity.
    return ($target_entity_type == static::MEDIA_ENTITY_TYPE_ID);
  }

}
