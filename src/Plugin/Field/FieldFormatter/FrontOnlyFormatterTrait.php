<?php

namespace Drupal\frontend_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Exception\FrontOnlyComponentException;

/**
 * Trait for the formatter that is only suitable for the front app.
 */
trait FrontOnlyFormatterTrait {

  /**
   * Builds a renderable array for a field value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field values to be rendered.
   * @param string $langcode
   *   The language that should be used to render the field.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    throw new FrontOnlyComponentException();
  }

}
