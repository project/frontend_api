<?php

namespace Drupal\frontend_api\Plugin\Field\FieldFormatter;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\frontend_api\Field\EntityReference\TargetTypeLimitedComponentTrait;
use Drupal\frontend_api\Field\Formatter\MultiStyleImageSettingsReader;
use Drupal\frontend_api\Dictionary\EntityTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for front formatters that render image in multiple image styles.
 *
 * @see \Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer\FrontMediaMultiStyleNormalizerBase
 */
abstract class FrontMediaMultiStyleFormatterBase extends FormatterBase {

  use FrontOnlyFormatterTrait,
    TargetTypeLimitedComponentTrait,
    StringTranslationTrait;

  /**
   * The referenced entity type this widget is applicable to.
   *
   * @see \Drupal\frontend_api\Field\EntityReference\TypeLimitedComponentTrait::isApplicable()
   */
  protected const TARGET_ENTITY_TYPE = EntityTypes::MEDIA;

  /**
   * The image style entity type.
   */
  protected const IMAGE_STYLE_ENTITY_TYPE = EntityTypes::IMAGE_STYLE;

  /**
   * The image styles setting key.
   */
  protected const STYLES_SETTING = MultiStyleImageSettingsReader::STYLES_SETTING;

  /**
   * The name of the fake style to use for the original image in UI.
   *
   * This value is then converted to/from NULL.
   */
  public const ORIGINAL_STYLE = '-';

  /**
   * The separator between key of the image and style to use in the input field.
   */
  public const KEY_STYLE_SEPARATOR = '|';

  /**
   * The settings reader.
   *
   * @var \Drupal\frontend_api\Field\Formatter\MultiStyleImageSettingsReaderInterface
   */
  protected $settingsReader;

  /**
   * The image stylist.
   *
   * @var \Drupal\frontend_api\Rest\Media\ImageStylistInterface
   */
  protected $stylist;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->settingsReader = $container->get(
      'frontend_api.field.formatter.multi_style_image_settings_reader'
    );
    $instance->stylist = $container->get('frontend_api.rest.image_stylist');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();

    $style_names = $this->settingsReader->getStyleNames($this->getSettings());
    $style_names = array_unique($style_names);
    try {
      $styles = $this->stylist->getImageStyles($style_names);

      foreach ($styles as $style) {
        if ($style === NULL) {
          continue;
        }

        $key = $style->getConfigDependencyKey();
        $dependencies[$key][] = $style->getConfigDependencyName();
      }
    }
    catch (\Exception $e) {
      // Silently ignore if there are broken styles. It's safer for now.
    }

    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    try {
      $style_names = $this->settingsReader->getStyleNames($this->getSettings());
      $styles = $this->stylist->getImageStyles($style_names);

      $items = [];
      foreach ($styles as $key => $style) {
        if ($style === NULL) {
          $items[] = new FormattableMarkup('<code>@key</code> => @label', [
            '@key' => $key,
            '@label' => $this->t('Original image'),
          ]);
        }
        else {
          $items[] = new FormattableMarkup('<code>@key</code> => @label (@name)', [
            '@key' => $key,
            '@label' => $style->label(),
            '@name' => $style->id(),
          ]);
        }
      }

      $summary[] = $this->t('Image styles:');
      $summary[] = [
        '#theme' => 'item_list',
        '#items' => $items,
      ];
    }
    catch (\Exception $e) {
      $summary[] = $this->t('!! Broken styles are configured !!');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    $settings[static::STYLES_SETTING] = [];

    return $settings;
  }

  /**
   * {@inheritdoc}
   *
   * @see validateImageStyles()
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $default_value = [];
    $styles = $this->settingsReader->getStyleNames($this->getSettings());
    foreach ($styles as $key => $style_name) {
      if ($style_name === NULL) {
        $style_name = static::ORIGINAL_STYLE;
      }

      $default_value[] = $key . static::KEY_STYLE_SEPARATOR . $style_name;
    }
    $default_value = implode("\n", $default_value);

    $form[static::STYLES_SETTING] = [
      '#title' => $this->t('Image styles'),
      '#description' => $this->t('Put image style per line. The format of every line is: <code>key@separatorstyle_machine_name</code>. The styled image is exported under the key specified. Use "<code>@original</code>" (dash) as image style to expose the original image.', [
        '@separator' => static::KEY_STYLE_SEPARATOR,
        '@original' => static::ORIGINAL_STYLE,
      ]),
      '#element_validate' => [
        [static::class, 'validateImageStyles'],
      ],
      '#type' => 'textarea',
      '#required' => TRUE,
      '#default_value' => $default_value,
    ];

    return $form;
  }

  /**
   * The element validate callback for the image styles textarea.
   *
   * It parses the textarea input into an array for storage. Also, it checks the
   * image styles to exist in the system.
   *
   * @param array $element
   *   The textarea form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @see settingsForm()
   */
  public static function validateImageStyles(
    array $element,
    FormStateInterface $form_state
  ) {
    // @todo Use DI when possible.
    $storage = \Drupal::entityTypeManager()
      ->getStorage(static::IMAGE_STYLE_ENTITY_TYPE);

    $result_value = [];
    $input_value = trim($element['#value']);
    if ($input_value !== '') {
      $input_lines = preg_split('@\R@', $input_value, -1, PREG_SPLIT_NO_EMPTY);
      foreach ($input_lines as $input_line) {
        $parts = explode(static::KEY_STYLE_SEPARATOR, $input_line, 2);
        if (count($parts) !== 2) {
          $form_state->setError($element, t('Invalid line: @line', [
            '@line' => $input_line,
          ]));
          continue;
        }

        [$key, $style_name] = $parts;
        if (isset($result_value[$key])) {
          $form_state->setError($element, t('Duplicate key on line: @line', [
            '@line' => $input_line,
          ]));
          continue;
        }

        if ($style_name === static::ORIGINAL_STYLE) {
          $style_name = NULL;
        }
        else {
          $style = $storage->load($style_name);
          if (!$style) {
            $error = t("The @style image style doesn't exist.", [
              '@style' => $style_name,
            ]);
            $form_state->setError($element, $error);
            continue;
          }
        }

        $result_value[$key] = $style_name;
      }
    }

    $value_path = $element['#parents'];
    $form_state->setValue($value_path, $result_value);
  }

}
