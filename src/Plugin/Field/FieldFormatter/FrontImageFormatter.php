<?php

namespace Drupal\frontend_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Formatter that displays the image field.
 *
 * @FieldFormatter(
 *   id = "frontend_api_image",
 *   label = @Translation("Front: Image"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class FrontImageFormatter extends ImageFormatter {
  use FrontOnlyFormatterTrait;

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    // Don't allow to select the link type, as it isn't supported by
    // normalizers.
    $form['image_link'] = [
      '#type' => 'value',
      '#value' => '',
    ];

    return $form;
  }

}
