<?php

namespace Drupal\frontend_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;

/**
 * Front formatter that displays raw boolean values.
 *
 * @FieldFormatter(
 *   id = "frontend_api_list_key_label",
 *   label = @Translation("Front-end API: Key + label"),
 *   field_types = {
 *     "list_string",
 *   },
 * )
 */
class FrontListKeyLabelFormatter extends FormatterBase {

  use FrontOnlyFormatterTrait;

}
