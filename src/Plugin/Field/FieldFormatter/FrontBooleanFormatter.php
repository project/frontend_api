<?php

namespace Drupal\frontend_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\BooleanFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Front formatter that displays raw boolean values.
 *
 * It also works on Drupal side and renders value as Yes / No .
 *
 * @FieldFormatter(
 *   id = "frontend_api_boolean",
 *   label = @Translation("Front: Boolean"),
 *   field_types = {
 *     "boolean",
 *   }
 * )
 */
class FrontBooleanFormatter extends BooleanFormatter {

  /**
   * The yes-no output format.
   */
  public const YES_NO_FORMAT = 'yes-no';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();
    $settings['format'] = static::YES_NO_FORMAT;

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    // Do not show any form here.
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [];
  }

}
