<?php

namespace Drupal\frontend_api\Plugin\Field\FieldFormatter;

/**
 * Formatter that displays the thumbnail of the first media in multiple styles.
 *
 * @FieldFormatter(
 *   id = "frontend_api_media_multistyle_first_thumbnail",
 *   label = @Translation("Front: First multi-style media thumbnail"),
 *   field_types = {
 *     "entity_reference",
 *   },
 * )
 *
 * @see \Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer\FrontMediaMultiStyleFirstThumbnailNormalizer
 */
class FrontMediaMultiStyleFirstThumbnailFormatter extends FrontMediaMultiStyleFormatterBase {

}
