<?php

namespace Drupal\frontend_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Formatter that embeds referenced entities into the field.
 *
 * It provides optional sorting of values and optional access check.
 *
 * @FieldFormatter(
 *   id = "frontend_api_embedded_entity",
 *   label = @Translation("Front: Embedded entities"),
 *   field_types = {
 *     "entity_reference",
 *   },
 * )
 *
 * @see \Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\ViewFieldNormalizer\FrontEmbeddedEntityNormalizer
 */
class FrontEmbeddedEntityFormatter extends EntityReferenceEntityFormatter {

  use FrontOnlyFormatterTrait;
  use StringTranslationTrait;

  /**
   * Setting key of the access check flag.
   */
  public const ACCESS_CHECK_SETTING = 'access_check';

  /**
   * Setting key of the target entity field to use for sorting.
   */
  public const SORT_BY_SETTING = 'sort_by';

  /**
   * Setting key of the sort direction.
   */
  public const SORT_DIRECTION_SETTING = 'sort_direction';

  /**
   * Default sort direction.
   */
  public const DEFAULT_SORT_DIRECTION = 'ASC';

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * The entity reference field settings reader.
   *
   * @var \Drupal\frontend_api\Field\EntityReference\EntityReferenceSettingsReaderInterface
   */
  protected $settingsReader;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var static $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->fieldManager = $container->get('entity_field.manager');
    $instance->settingsReader = $container->get(
      'frontend_api.field.entity_reference.settings_reader'
    );

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      static::ACCESS_CHECK_SETTING => FALSE,
      static::SORT_BY_SETTING => '',
      static::SORT_DIRECTION_SETTING => static::DEFAULT_SORT_DIRECTION,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $elements = parent::settingsForm($form, $form_state);

    $elements[static::ACCESS_CHECK_SETTING] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Access check'),
      '#description' => $this->t('Enable or disable access check for embedded entities'),
      '#default_value' => $this->getSetting(static::ACCESS_CHECK_SETTING),
    ];

    $elements[static::SORT_BY_SETTING] = [
      '#type' => 'select',
      '#title' => $this->t('Sort by'),
      '#options' => $this->getSortByOptions(),
      '#default_value' => $this->getSetting(static::SORT_BY_SETTING),
    ];
    $elements[static::SORT_DIRECTION_SETTING] = [
      '#type' => 'select',
      '#title' => $this->t('Sort direction'),
      '#options' => $this->getSortDirectionOptions(),
      '#default_value' => $this->getSetting(static::SORT_DIRECTION_SETTING),
    ];

    return $elements;
  }

  /**
   * Returns options for the sort direction setting.
   *
   * @return array
   *   The options.
   */
  protected function getSortDirectionOptions(): array {
    return [
      'ASC' => $this->t('Ascending'),
      'DESC' => $this->t('Descending'),
    ];
  }

  /**
   * Returns options for the "sort by" setting.
   *
   * @return array
   *   The options.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getSortByOptions(): array {
    $options = [
      '' => $this->t('- None (delta) -'),
    ];

    $target_entity_type_id = $this->settingsReader
      ->getTargetEntityTypeId($this->fieldDefinition);

    $target_entity_type = $this->entityTypeManager
      ->getDefinition($target_entity_type_id);
    if ($target_entity_type->hasKey('bundle')) {
      $target_bundle_ids = $this->settingsReader
        ->getTargetBundleIds($this->fieldDefinition);
    }
    else {
      $target_bundle_ids = [$target_entity_type_id];
    }

    $field_labels = [];
    foreach ($target_bundle_ids as $bundle_id) {
      $bundle_fields = $this->fieldManager
        ->getFieldDefinitions($target_entity_type_id, $bundle_id);
      foreach ($bundle_fields as $field_name => $field_definition) {
        if (isset($field_labels[$field_name])) {
          continue;
        }

        $field_labels[$field_name] = $field_definition->getLabel();
      }
    }

    $sort_callback = function ($a, $b): int {
      return strnatcasecmp((string) $a, (string) $b);
    };
    uasort($field_labels, $sort_callback);

    return $options + $field_labels;
  }

  /**
   * Returns field label of the specified target entity field.
   *
   * @param string $field_name
   *   The field name of the target entity.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   *   The field label.
   */
  protected function getTargetEntityFieldLabel(string $field_name) {
    $field_map = $this->fieldManager->getFieldMap();
    $target_entity_type_id = $this->settingsReader
      ->getTargetEntityTypeId($this->fieldDefinition);

    $field_info = $field_map[$target_entity_type_id][$field_name] ?? NULL;
    if ($field_info === NULL) {
      return $field_name;
    }

    $bundle_ids = $field_info['bundles'] ?? [];
    if (empty($bundle_ids)) {
      return $field_name;
    }

    $bundle_id = reset($bundle_ids);
    $field_definitions = $this->fieldManager
      ->getFieldDefinitions($target_entity_type_id, $bundle_id);
    $field_definition = $field_definitions[$field_name] ?? NULL;
    if ($field_definition === NULL) {
      return $field_name;
    }

    return $field_definition->getLabel();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $value = !empty($this->getSetting(static::ACCESS_CHECK_SETTING)) ?
      $this->t('Enabled') : $this->t('Disabled');
    $summary[] = $this->t('Access check: @value', ['@value' => $value]);

    $sort_by = $this->getSetting(static::SORT_BY_SETTING);
    if (!empty($sort_by)) {
      $sort_direction = $this->getSetting(static::SORT_DIRECTION_SETTING);
      $sort_direction_label = $this->getSortDirectionOptions()[$sort_direction]
        ?? $sort_direction;

      $summary[] = $this->t('Sort by @field, @direction', [
        '@field' => $this->getTargetEntityFieldLabel($sort_by),
        '@direction' => $sort_direction_label,
      ]);
    }

    return $summary;
  }

}
