<?php

namespace Drupal\frontend_api\Plugin\Field\FieldFormatter;

/**
 * Formatter that displays the thumbnails of the all referenced media entities.
 *
 * @FieldFormatter(
 *   id = "frontend_api_media_thumbnails",
 *   label = @Translation("Front: All media thumbnails"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class FrontMediaThumbnailsFormatter extends FrontMediaFormatterBase {

}
