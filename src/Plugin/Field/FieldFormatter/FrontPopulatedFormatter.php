<?php

namespace Drupal\frontend_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;

/**
 * Formatter for populated fields.
 *
 * Shows raw boolean value.
 *
 * @FieldFormatter(
 *   id = "frontend_api_populated",
 *   label = @Translation("Front-end API: Populated"),
 *   field_types = {}
 * )
 */
class FrontPopulatedFormatter extends FormatterBase {

  use FrontOnlyFormatterTrait;

}
