<?php

namespace Drupal\frontend_api\Exception;

/**
 * Thrown in case the widget/formatter plugin is only suitable for a front API.
 */
class FrontOnlyComponentException extends \Exception {

}
