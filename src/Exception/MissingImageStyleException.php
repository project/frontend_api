<?php

namespace Drupal\frontend_api\Exception;

/**
 * Thrown in case image style is missing.
 */
class MissingImageStyleException extends \Exception {

}
