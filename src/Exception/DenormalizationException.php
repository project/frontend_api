<?php

namespace Drupal\frontend_api\Exception;

/**
 * Thrown when de-normalization fails, including failed validation.
 */
class DenormalizationException extends \Exception {

  /**
   * The list of errors.
   *
   * @var array
   */
  protected $errors = [];

  /**
   * A constructor.
   *
   * @param array $errors
   *   The list of validation errors. Keys are property paths, values are
   *   validation errors. Errors may be displayed to the end user.
   * @param string $message
   *   The exception message. It isn't displayed to the end user.
   */
  public function __construct(
    array $errors,
    $message = ''
  ) {
    parent::__construct($message);

    $this->errors = $errors;
  }

  /**
   * Returns validation errors.
   *
   * @return array
   *   Keys are property paths, values are validation errors.
   */
  public function getErrors(): array {
    return $this->errors;
  }

}
