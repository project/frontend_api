<?php

namespace Drupal\frontend_api\Exception;

/**
 * Thrown in case max entity relationship depth has been exceeded.
 */
class MaxRelationshipDepthExceeded extends \Exception {

}
