<?php

namespace Drupal\frontend_api\Event;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormInfoInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event that allows to change the structure of the form info.
 *
 * @todo Fix deprecated parent.
 * @todo Provide events for view info & form data.
 */
class FormInfoBuildEvent extends Event {

  /**
   * Form info.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormInfoInterface
   */
  protected $formInfo;

  /**
   * The entity operation.
   *
   * @var string|null
   */
  protected $operation;

  /**
   * A constructor.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormInfoInterface $formInfo
   *   The form info.
   * @param string|null $operation
   *   The entity operation.
   */
  public function __construct(FormInfoInterface $formInfo, $operation = NULL) {
    $this->formInfo = $formInfo;
    $this->operation = $operation;
  }

  /**
   * Returns form info.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormInfoInterface
   *   The form info.
   */
  public function getFormInfo(): FormInfoInterface {
    return $this->formInfo;
  }

  /**
   * Returns entities' operation name or null if not set.
   *
   * @return string|null
   *   Operation name or null.
   */
  public function getOperation(): ?string {
    return $this->operation;
  }

}
