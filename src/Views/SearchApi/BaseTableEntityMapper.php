<?php

namespace Drupal\frontend_api\Views\SearchApi;

use Drupal\frontend_api\Views\BaseTableEntityMapperBase;
use Drupal\frontend_api\Dictionary\EntityTypes;

/**
 * Provides mapping of Search API base tables to entity type IDs.
 *
 * @see \Drupal\frontend_api\Views\BaseTableEntityMapperInterface
 */
class BaseTableEntityMapper extends BaseTableEntityMapperBase {

  /**
   * Entity type ID of the Search API index.
   */
  protected const INDEX_ENTITY_TYPE_ID = EntityTypes::SEARCH_API_INDEX;

  /**
   * {@inheritdoc}
   */
  public function getMap(): array {
    $map = [];

    /** @var \Drupal\search_api\IndexInterface[] $indexes */
    $indexes = $this->entityTypeManager
      ->getStorage(static::INDEX_ENTITY_TYPE_ID)
      ->loadMultiple();
    foreach ($indexes as $index) {
      // The index must have just a single datasource, so that all items are of
      // the same type.
      $datasource_ids = $index->getDatasourceIds();
      if (count($datasource_ids) !== 1) {
        continue;
      }

      // The only datasource of the index must provide items of a single entity
      // type.
      $entity_types = $index->getEntityTypes();
      if (count($entity_types) !== 1) {
        continue;
      }

      $map['search_api_index_' . $index->id()] = reset($entity_types);
    }

    return $map;
  }

}
