<?php

namespace Drupal\frontend_api\Views;

/**
 * Interface for a mapper of a Views base table to an entity type ID.
 *
 * Its purpose is to provide information about the Views base tables that store
 * entries of a single entity type.
 */
interface BaseTableEntityMapperInterface {

  /**
   * Returns a map of base table name to the entity type ID.
   *
   * @return string[]
   *   The map where:
   *   - keys are Views base table names,
   *   - values are corresponding entity type IDs.
   */
  public function getMap(): array;

  /**
   * Returns base tables having an entity type.
   *
   * @return string[]
   *   Base table names.
   */
  public function getBaseTables(): array;

}
