<?php

namespace Drupal\frontend_api\Views\Database;

use Drupal\frontend_api\Views\BaseTableEntityMapperBase;

/**
 * Provides mapping of database base tables to entity type IDs.
 *
 * @see \Drupal\frontend_api\Views\BaseTableEntityMapperInterface
 */
class BaseTableEntityMapper extends BaseTableEntityMapperBase {

  /**
   * {@inheritdoc}
   */
  public function getMap(): array {
    $map = [];

    $entity_types = $this->entityTypeManager->getDefinitions();
    foreach ($entity_types as $entity_type_id => $entity_type) {
      $tables = array_filter([
        $entity_type->getBaseTable(),
        $entity_type->getDataTable(),
        $entity_type->getRevisionTable(),
        $entity_type->getRevisionDataTable(),
      ]);

      $map += array_fill_keys($tables, $entity_type_id);
    }

    return $map;
  }

}
