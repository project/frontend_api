<?php

namespace Drupal\frontend_api\Views;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Base class for a base table to entity type mapper.
 *
 * @see \Drupal\frontend_api\Views\BaseTableEntityMapperInterface
 */
abstract class BaseTableEntityMapperBase implements BaseTableEntityMapperInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseTables(): array {
    return array_keys($this->getMap());
  }

}
