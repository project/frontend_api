<?php

namespace Drupal\frontend_api;

/**
 * Provides an interface of a generic error list.
 *
 * @see \Drupal\frontend_api\ErrorListTrait
 */
interface ErrorListInterface {

  /**
   * Returns the validation errors.
   *
   * @return array
   *   Keys are property paths, values are error messages. Messages may be
   *   displayed to the user. Value could be either string or an array if there
   *   are many errors for the same path.
   */
  public function getErrors(): array;

  /**
   * Adds validation errors to the result.
   *
   * @param array $errors
   *   The list of errors. Keys are property paths, values are error messages.
   *   Messages may be displayed to the user. Dots in the keys separate path
   *   parts, e.g. "field_name.0.target_id".
   *
   * @return static
   */
  public function addErrors(array $errors): ErrorListInterface;

  /**
   * Returns TRUE if there are validation errors.
   *
   * @return bool
   *   TRUE in case there are errors.
   */
  public function hasErrors(): bool;

  /**
   * Returns TRUE if there are any errors at the specified property path.
   *
   * It also returns TRUE if there are errors for the nested property path.
   * For example, check for "my.path" returns TRUE if there are errors for
   * "my.path.below.specified".
   *
   * @param string $path
   *   The property path to check errors at.
   *
   * @return bool
   *   TRUE if there are any errors at the specified path.
   */
  public function hasErrorsAt(string $path): bool;

  /**
   * Adds errors at specific path.
   *
   * @param string $path_prefix
   *   The prefix that should be added to the path of all added errors.
   * @param array $errors
   *   The errors to add.
   *
   * @return static
   */
  public function addErrorsAt(
    string $path_prefix,
    array $errors
  ): ErrorListInterface;

  /**
   * Adds prefix to the paths of errors currently in the result.
   *
   * It doesn't affect errors added after this call.
   *
   * @param string $path_prefix
   *   The path prefix to add.
   *
   * @return static
   *   Self.
   */
  public function addPrefixToErrorPaths(
    string $path_prefix
  ): ErrorListInterface;

  /**
   * Adds a single error with empty path.
   *
   * @param string|\Drupal\Component\Render\MarkupInterface|array $error
   *   The error or a list of errors to add.
   *
   * @return static
   *   Self.
   */
  public function addError($error): ErrorListInterface;

  /**
   * Adds an error to the specified path.
   *
   * @param string $path
   *   The path of the error.
   * @param string|\Drupal\Component\Render\MarkupInterface $error
   *   The error to add.
   *
   * @return static
   *   Self.
   */
  public function addErrorAt(string $path, $error): ErrorListInterface;

}
