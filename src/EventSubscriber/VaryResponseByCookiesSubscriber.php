<?php

namespace Drupal\frontend_api\EventSubscriber;

use Drupal\Core\Cache\CacheableResponseInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Conditionally adds the Cookies header to the Vary response header.
 *
 * Browsers may still take even the non-cacheable responses from disk cache in
 * case of tab duplication (and probably session restore). This may result in
 * 403 responses be cached and then restored even for already authenticated
 * user. Adding proper Vary header to the response stops that weird browser
 * behavior and forces it to make a network request.
 *
 * @todo Make sure we still need it.
 */
class VaryResponseByCookiesSubscriber implements EventSubscriberInterface {

  /**
   * Priority of the response event handler.
   *
   * It goes after most of the subscribers, so that we get all the cacheable
   * metadata.
   */
  public const RESPONSE_PRIORITY = -10;

  /**
   * Cache contexts of the response that trigger Cookie added to Vary header.
   */
  public const AFFECTING_CONTEXTS = [
    'user',
    'cookies',
    'session',
  ];

  /**
   * Delimiter between parents/childs of the cache context IDs.
   */
  public const CONTEXT_PARENT_DELIMITER = '.';

  /**
   * The cookie header to add to the Vary response header.
   */
  public const COOKIE_HEADER = 'Cookie';

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];

    $events[KernelEvents::RESPONSE][] = ['onRespond', static::RESPONSE_PRIORITY];

    return $events;
  }

  /**
   * Returns TRUE in case the response should vary by cookies.
   *
   * @param \Drupal\Core\Cache\CacheableResponseInterface $response
   *   The response to check.
   *
   * @return bool
   *   Whether the response should vary by cookie or not.
   */
  protected function shouldVaryByCookie(
    CacheableResponseInterface $response
  ): bool {
    $delimiter = static::CONTEXT_PARENT_DELIMITER;
    $delimiter_length = strlen($delimiter);

    $contexts = $response->getCacheableMetadata()
      ->getCacheContexts();

    foreach (static::AFFECTING_CONTEXTS as $affecting_context) {
      // The context ID could either be a full match or a child context.
      $affecting_prefixes = [
        $affecting_context,
        $affecting_context . $delimiter,
      ];
      $comparison_length = strlen($affecting_context) + $delimiter_length;

      foreach ($contexts as $context) {
        $prefix = substr($context, 0, $comparison_length);
        if (in_array($prefix, $affecting_prefixes, TRUE)) {
          // We got a match.
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Handles the response core event.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   The event to handle.
   */
  public function onRespond(FilterResponseEvent $event): void {
    $response = $event->getResponse();
    if (!$response instanceof CacheableResponseInterface) {
      return;
    }

    if (!$this->shouldVaryByCookie($response)) {
      return;
    }

    $response->setVary(static::COOKIE_HEADER, FALSE);
  }

}
