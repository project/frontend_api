<?php

namespace Drupal\frontend_api\EventSubscriber;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\serialization\Normalizer\CacheableNormalizerInterface;
use Drupal\frontend_api\Rest\ResourceResponseInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Response subscriber that serializes and removes ResourceResponses' data.
 *
 * Rewritten core subscriber.
 *
 * @todo Re-validate that it's necessary.
 */
class ResourceResponseSubscriber implements EventSubscriberInterface {

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a ResourceResponseSubscriber object.
   *
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   The serializer.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(
    SerializerInterface $serializer,
    RendererInterface $renderer,
    RouteMatchInterface $route_match
  ) {
    $this->serializer = $serializer;
    $this->renderer = $renderer;
    $this->routeMatch = $route_match;
  }

  /**
   * Serializes ResourceResponse responses' data, and removes that data.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   The event to process.
   */
  public function onResponse(FilterResponseEvent $event) {
    $response = $event->getResponse();
    if (!$response instanceof ResourceResponseInterface) {
      return;
    }

    $request = $event->getRequest();
    $format = $this->getResponseFormat($this->routeMatch, $request);
    $this->renderResponseBody($request, $response, $this->serializer, $format);
    $event->setResponse($this->flattenResponse($response));
  }

  /**
   * Determines the format to respond in.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return string
   *   The response format.
   */
  public function getResponseFormat(RouteMatchInterface $route_match, Request $request) {
    $route = $route_match->getRouteObject();
    $acceptable_response_formats = $route->hasRequirement('_format') ?
      explode('|', $route->getRequirement('_format')) : [];
    $acceptable_request_formats = $route->hasRequirement('_content_type_format') ?
      explode('|', $route->getRequirement('_content_type_format')) : [];
    $acceptable_formats = $request->isMethodCacheable() ?
      $acceptable_response_formats : $acceptable_request_formats;

    $requested_format = $request->getRequestFormat();
    $content_type_format = $request->getContentType();

    // If an acceptable response format is requested, then use that. Otherwise,
    // including and particularly when the client forgot to specify a response
    // format, then use heuristics to select the format that is most likely
    // expected.
    if (in_array($requested_format, $acceptable_response_formats, TRUE)) {
      return $requested_format;
    }

    // If a request body is present, then use the format corresponding to the
    // request body's Content-Type for the response, if it's an acceptable
    // format for the request.
    if (!empty($request->getContent()) && in_array(
      $content_type_format, $acceptable_request_formats, TRUE)) {
      return $content_type_format;
    }

    // Otherwise, use the first acceptable format.
    if (!empty($acceptable_formats)) {
      return $acceptable_formats[0];
    }

    // Sometimes, there are no acceptable formats, e.g. DELETE routes.
    return NULL;
  }

  /**
   * Renders a resource response body.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param \Drupal\frontend_api\Rest\ResourceResponseInterface $response
   *   The response from the REST resource.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   The serializer to use.
   * @param string|null $format
   *   The response format, or NULL in case the response does not need a format,
   *   for example for the response to a DELETE request.
   */
  protected function renderResponseBody(
    Request $request,
    ResourceResponseInterface $response,
    SerializerInterface $serializer,
    $format
  ) {
    $data = $response->getResponseData();

    // If there is data to send, serialize and set it as the response body.
    if ($data !== NULL) {
      $serialization_context = [
        'request' => $request,
        CacheableNormalizerInterface::SERIALIZATION_CONTEXT_CACHEABILITY => new CacheableMetadata(),
      ];
      $serialization_context += $response->getContext();

      // @deprecated In Drupal 8.5.0, will be removed before Drupal 9.0.0. Use
      // explicit cacheability metadata bubbling instead. (The wrapping call to
      // executeInRenderContext() will be removed before Drupal 9.0.0.)
      $context = new RenderContext();
      $output = $this->renderer
        ->executeInRenderContext($context, function () use ($serializer, $data, $format, $serialization_context) {
          return $serializer->serialize($data, $format, $serialization_context);
        });
      if ($response instanceof CacheableResponseInterface) {
        if (!$context->isEmpty()) {
          @trigger_error('Implicit cacheability metadata bubbling (onto the global render context) in normalizers is deprecated in drupal:8.5.0 and will be removed in drupal:9.0.0. Use the "cacheability" serialization context instead, for explicit cacheability metadata bubbling. See https://www.drupal.org/node/2918937', E_USER_DEPRECATED);
          $response->addCacheableDependency($context->pop());
        }
        $response->addCacheableDependency(
          $serialization_context[CacheableNormalizerInterface::SERIALIZATION_CONTEXT_CACHEABILITY]
        );
      }

      $response->setContent($output);
      $response->headers->set('Content-Type', $request->getMimeType($format));
    }
  }

  /**
   * Flattens a fully rendered resource response.
   *
   * @param \Drupal\frontend_api\Rest\ResourceResponseInterface $response
   *   A fully rendered resource response.
   *
   * @return \Drupal\Core\Cache\CacheableResponse|\Symfony\Component\HttpFoundation\Response
   *   The flattened response.
   */
  protected function flattenResponse(ResourceResponseInterface $response) {
    $final_response = ($response instanceof CacheableResponseInterface) ?
      new CacheableResponse() : new Response();
    $final_response->setContent($response->getContent());
    $final_response->setStatusCode($response->getStatusCode());
    $final_response->setProtocolVersion($response->getProtocolVersion());
    $final_response->setCharset($response->getCharset());
    $final_response->headers = clone $response->headers;
    if ($final_response instanceof CacheableResponseInterface) {
      $final_response->addCacheableDependency($response->getCacheableMetadata());
    }
    return $final_response;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Run before DynamicPageCacheSubscriber (priority 100),
    // so that Dynamic Page Cache can cache flattened responses.
    // 129 runs before core ResourceResponseSubscriber.
    $events[KernelEvents::RESPONSE][] = ['onResponse', 129];
    return $events;
  }

}
