<?php

namespace Drupal\frontend_api\Dictionary;

final class CacheContexts {

  /**
   * The language cache context prefix.
   */
  public const LANGUAGE_PREFIX = 'languages:';

}
