<?php

namespace Drupal\frontend_api\Dictionary;

// @TODO NOW Cleanup.
final class CacheTags {

  /**
   * The config tag prefix.
   *
   * @see \Drupal\Core\Config\ConfigBase::getCacheTags()
   */
  public const CONFIG_PREFIX = 'config:';

  /**
   * The cache tag marking the cached field info.
   *
   * This tag must be invalidated on field config changes in order to actually
   * see the updates on the field info.
   */
  public const FIELD_INFO = 'entity_field_info';

  /**
   * Entity bundles cache tag.
   */
  public const BUNDLES = 'entity_bundles';

}
