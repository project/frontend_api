<?php

namespace Drupal\frontend_api\Dictionary\Event;

/**
 * Contains list of events for display info.
 */
final class DisplayInfoEvents {

  /**
   * The event fired during the form info build.
   */
  public const FORM_INFO_BUILD = 'frontend_api.form_info_build';

  /**
   * The event is fired during entity normalization process.
   */
  public const ENTITY_DISPLAY_INFO_NORMALIZE = 'frontend_api.entity_dislpay_info_nomalize';

}
