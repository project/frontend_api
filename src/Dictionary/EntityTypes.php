<?php

namespace Drupal\frontend_api\Dictionary;

final class EntityTypes {

  /**
   * The entity type ID of the Search API index.
   */
  public const SEARCH_API_INDEX = 'search_api_index';

  /**
   * The entity form mode.
   */
  public const ENTITY_FORM_MODE = 'entity_form_mode';

  /**
   * The entity form display.
   */
  public const ENTITY_FORM_DISPLAY = 'entity_form_display';

  /**
   * The entity view display.
   */
  public const ENTITY_VIEW_DISPLAY = 'entity_view_display';

  /**
   * The media.
   */
  public const MEDIA = 'media';

  /**
   * The file.
   */
  public const FILE = 'file';

  /**
   * The view.
   */
  public const VIEW = 'view';

  /**
   * The search api autocomplete.
   */
  public const SEARCH_API_AUTOCOMPLETE = 'search_api_autocomplete_search';

  /**
   * The currency.
   */
  public const CURRENCY = 'commerce_currency';

  /**
   * The image style.
   */
  public const IMAGE_STYLE = 'image_style';

  /**
   * The date format config entity.
   */
  public const DATE_FORMAT = 'date_format';

}
