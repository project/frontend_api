<?php

namespace Drupal\frontend_api\Dictionary;

final class EntityOperations {

  /**
   * The "view" operation.
   */
  public const VIEW = 'view';

  /**
   * The "view label" operation.
   */
  public const VIEW_LABEL = 'view label';

}
