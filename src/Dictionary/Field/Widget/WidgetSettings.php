<?php

namespace Drupal\frontend_api\Dictionary\Field\Widget;

/**
 * Dictionary class for common widget setting keys.
 */
final class WidgetSettings {

  /**
   * The third-party settings key in the widget data stored in the form display.
   */
  public const THIRD_PARTY = 'third_party_settings';

}
