<?php

namespace Drupal\frontend_api\Dictionary\Field\EntityReference;

/**
 * Dictionary class with setting keys of the entity reference field definition.
 */
final class EntityReferenceFieldSettings {

  /**
   * The selection handler setting key.
   */
  public const SELECTION_HANLDER = 'handler';

  /**
   * The target entity type ID key in the field definition settings.
   */
  public const TARGET_TYPE = 'target_type';

  /**
   * The target bundles option of a selection handler.
   */
  public const TARGET_BUNDLES_OPTION = 'target_bundles';

  /**
   * The handler options key on the field definition settings.
   */
  public const HANDLER_OPTIONS = 'handler_settings';

}
