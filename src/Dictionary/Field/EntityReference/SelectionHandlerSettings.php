<?php

namespace Drupal\frontend_api\Dictionary\Field\EntityReference;

/**
 * Dictionary class for setting keys of entity reference selection handlers.
 */
final class SelectionHandlerSettings {

  /**
   * The map of the query parameters and their keys.
   *
   * The keys are names of the request query parameters, values are their keys
   * in the query parameters array passed to the handler settings on actual
   * request.
   *
   * @see QUERY_PARAMETERS
   */
  public const QUERY_PARAMETERS_MAP = 'query_parameters_map';

  /**
   * The request query parameters mapped from the actual request.
   *
   * @see QUERY_PARAMETERS_MAP
   */
  public const QUERY_PARAMETERS = 'query_parameters';

}
