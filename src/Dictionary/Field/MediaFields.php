<?php

namespace Drupal\frontend_api\Dictionary\Field;

final class MediaFields {

  /**
   * The bundle ID.
   */
  public const BUNDLE = 'bundle';

  /**
   * The thumbnail field name.
   */
  public const THUMBNAIL = 'thumbnail';

}
