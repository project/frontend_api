<?php

namespace Drupal\frontend_api\Routing;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Customized route parameter converter for a single entity.
 *
 * Supports the following options:
 * - language: the language code to retrieve the entity in, could also be a
 *   special "x-default" code in order to always retrieve default translation.
 *
 * @TODO NOW Merge with the base class.
 */
class EntityParamConverter extends EntityParamConverterBase {

  /**
   * {@inheritdoc}
   */
  public const TYPE_PREFIX = 'frontend_api_entity:';

  /**
   * The option of a language code used for the entity translation.
   */
  public const LANGUAGE_OPTION = 'language';

  /**
   * The language code that specifies default entity translation.
   */
  protected const DEFAULT_LANGUAGE = LanguageInterface::LANGCODE_DEFAULT;

  /**
   * The operation value submitted to the entity repository on translation.
   */
  public const TRANSLATION_OPERATION = 'entity_upcast';

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * A constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityRepositoryInterface $entity_repository
  ) {
    parent::__construct($entity_type_manager);

    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function convert($value, $definition, $name, array $defaults) {
    $entity_type_id = $this->getEntityTypeIdFromDefinition($definition);
    if (!isset($entity_type_id)) {
      return NULL;
    }

    $entity = $this->entityTypeManager
      ->getStorage($entity_type_id)
      ->load($value);
    if (!isset($entity)) {
      return NULL;
    }

    $langcode = $definition[static::LANGUAGE_OPTION] ?? NULL;
    if ($langcode === static::DEFAULT_LANGUAGE) {
      return $entity;
    }

    $context = [
      'operation' => static::TRANSLATION_OPERATION,
    ];
    return $this->entityRepository
      ->getTranslationFromContext($entity, $langcode, $context);
  }

}
