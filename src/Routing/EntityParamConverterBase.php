<?php

namespace Drupal\frontend_api\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\Routing\Route;

/**
 * Provides base class for the entity route parameter converters.
 */
abstract class EntityParamConverterBase implements ParamConverterInterface {

  /**
   * The parameter type prefix.
   *
   * Must be overridden in child classes.
   */
  protected const TYPE_PREFIX = NULL;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Returns entity type ID parsed from a parameter definition.
   *
   * @param array $definition
   *   The parameter definition.
   *
   * @return string|null
   *   The entity type ID or NULL if it's not possible to find the entity type
   *   ID.
   */
  protected function getEntityTypeIdFromDefinition(array $definition): ?string {
    $type = $definition['type'] ?? NULL;
    if (!isset($type)) {
      return NULL;
    }

    $prefix = static::TYPE_PREFIX;
    $prefix_length = strlen($prefix);
    $starts_with = (substr($type, 0, $prefix_length) === $prefix);
    if (!$starts_with) {
      return NULL;
    }

    return substr($type, $prefix_length);
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    $entity_type_id = $this->getEntityTypeIdFromDefinition($definition);
    if (!isset($entity_type_id)) {
      return FALSE;
    }

    return $this->entityTypeManager->hasDefinition($entity_type_id);
  }

}
