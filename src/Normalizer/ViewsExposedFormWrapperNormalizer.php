<?php

namespace Drupal\frontend_api\Normalizer;

use Drupal\Core\Session\AccountInterface;
use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\frontend_api\Rest\Views\AccessibleExposedFilterInterface;
use Drupal\frontend_api\Rest\Views\ExposedHandlerNormalizerManagerInterface;
use Drupal\frontend_api\Rest\Views\ExposedFormWrapper;

/**
 * Exposed form normalizer.
 */
class ViewsExposedFormWrapperNormalizer extends NormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = ExposedFormWrapper::class;

  /**
   * Exposed normalizers plugin manager.
   *
   * @var \Drupal\frontend_api\Rest\Views\ExposedHandlerNormalizerManagerInterface
   */
  protected $normalizerManager;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * A constructor.
   *
   * @param \Drupal\frontend_api\Rest\Views\ExposedHandlerNormalizerManagerInterface $normalizer_manager
   *   Exposed normalizers plugin manager.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Current user.
   */
  public function __construct(
    ExposedHandlerNormalizerManagerInterface $normalizer_manager,
    AccountInterface $account
  ) {
    $this->normalizerManager = $normalizer_manager;
    $this->currentUser = $account;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($wrapper, $format = NULL, array $context = []) {
    $exposed_form = $wrapper->exposedFormPlugin;
    $form_array = $wrapper->exposedForm;

    // Go through each handler and build info about their exposed widget.
    $result = [];
    foreach ($exposed_form->displayHandler->handlers as $type => $value) {
      $type_result = [];

      /** @var \Drupal\views\Plugin\views\ViewsHandlerInterface $handler */
      foreach ($exposed_form->view->$type as $handler) {
        if ($handler->canExpose() && $handler->isExposed()) {
          // Remove filter from exposed_form if user doesn't have access to it.
          if ($handler instanceof AccessibleExposedFilterInterface) {
            $access_result = $handler->access($this->currentUser);
            if (!$access_result->isAllowed()) {
              continue;
            }
          }

          $normalizer = $this->normalizerManager->createInstanceByHandler(
            $exposed_form,
            $type,
            $handler
          );
          $type_result += $normalizer->normalizeHandler(
            $handler,
            $form_array,
            $format,
            $context
          );
        }
      }

      if (!empty($type_result)) {
        $result[$type] = $type_result;
      }
    }
    return $result;
  }

}
