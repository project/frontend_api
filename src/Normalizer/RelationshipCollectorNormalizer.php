<?php

namespace Drupal\frontend_api\Normalizer;

use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\frontend_api\Exception\MaxRelationshipDepthExceeded;
use Drupal\frontend_api\Rest\NormalizerContextKeys;
use Drupal\frontend_api\Rest\Relationship\RelationshipCollectorInterface;

/**
 * Provides normalizer of a relationship collector into a grouped entities list.
 */
class RelationshipCollectorNormalizer extends NormalizerBase {

  /**
   * Max depth of referenced entities.
   */
  public const MAX_DEPTH = 10;

  /**
   * The default view mode.
   */
  public const DEFAULT_VIEW_MODE = 'default';

  /**
   * The view mode context.
   */
  protected const VIEW_MODE_CONTEXT = NormalizerContextKeys::VIEW_MODE_CONTEXT;

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = RelationshipCollectorInterface::class;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface|\Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * {@inheritdoc}
   */
  public function normalize($collector, $format = NULL, array $context = []) {
    /** @var \Drupal\frontend_api\Rest\Relationship\RelationshipCollectorInterface $collector */

    // Perform recursive entity loading and normalization. As soon as there are
    // no entities left to load, we're done.
    $result = [];
    $depth = 0;
    while ($entities = $collector->loadEntities()) {
      if ($depth++ >= static::MAX_DEPTH) {
        throw new MaxRelationshipDepthExceeded();
      }

      $view_modes = $collector->getViewModes();
      foreach ($entities as $entity_type_id => $type_entities) {
        $view_mode = $view_modes[$entity_type_id] ?? static::DEFAULT_VIEW_MODE;
        $context[static::VIEW_MODE_CONTEXT] = $view_mode;

        $result += [
          $entity_type_id => [],
        ];
        $result[$entity_type_id] += $this->serializer->normalize($type_entities, $format, $context);
      }
    }

    if (empty($result)) {
      return NULL;
    }

    return $result;
  }

}
