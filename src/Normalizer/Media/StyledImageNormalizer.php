<?php

namespace Drupal\frontend_api\Normalizer\Media;

use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\frontend_api\Rest\Media\StyledImageInterface;

/**
 * Provides normalizer for a styled image.
 */
class StyledImageNormalizer extends NormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = StyledImageInterface::class;

  /**
   * {@inheritdoc}
   */
  public function normalize(
    $styled_image,
    $format = NULL,
    array $context = []
  ): array {
    /** @var \Drupal\frontend_api\Rest\Media\StyledImageInterface $styled_image */

    $result = [
      'id' => $styled_image->getFileId(),
      'url' => $styled_image->getUrl(),
    ];

    $attributes = $styled_image->getAttributes();
    if (!empty($attributes)) {
      $result['attributes'] = $attributes;
    }

    $this->addCacheableDependency($context, $styled_image);

    return $result;
  }

}
