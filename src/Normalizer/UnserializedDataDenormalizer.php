<?php

namespace Drupal\frontend_api\Normalizer;

use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\frontend_api\Rest\UnserializedData;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Provides de-normalizer for an un-serialized data wrapper.
 *
 * @see \Drupal\frontend_api\Rest\UnserializedDataInterface
 */
class UnserializedDataDenormalizer extends NormalizerBase implements DenormalizerInterface {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = UnserializedData::class;

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL): bool {
    // Disallow normalization, as it's a de-normalizer.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    // Drupal doesn't provide a separate base class for a de-normalizer, so we
    // have to throw an exception here in order to re-use some methods of base
    // normalizer.
    throw new \LogicException("Denormalizer can't normalize.");
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize(
    $data,
    $class,
    $format = NULL,
    array $context = []
  ) {
    return new UnserializedData($data);
  }

}
