<?php

namespace Drupal\frontend_api\Normalizer;

use Drupal\frontend_api\Rest\NormalizerContextKeys;
use Drupal\frontend_api\Rest\Relationship\RelationshipCollectorFactoryInterface;
use Drupal\frontend_api\Rest\Relationship\RelationshipCollectorInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Provides normalizer for an entity formatted in a view mode.
 *
 * It isn't registered as a normalizer, but gets called by a content entity
 * normalizer if there is a view mode passed.
 */
class FormattedEntityNormalizer extends ManuallyCalledNormalizerBase {

  /**
   * The context key of the relationship collector.
   */
  protected const RELATIONSHIP_COLLECTOR_CONTEXT = NormalizerContextKeys::RELATIONSHIP_COLLECTOR_CONTEXT;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface|\Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * The relationship collector factory.
   *
   * @var \Drupal\frontend_api\Rest\Relationship\RelationshipCollectorFactoryInterface
   */
  protected $relationshipCollectorFactory;

  /**
   * Normalizer of the formatted entity fields.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface|\Symfony\Component\Serializer\SerializerAwareInterface
   */
  protected $formattedFieldsNormalizer;

  /**
   * A constructor.
   *
   * @param \Drupal\frontend_api\Rest\Relationship\RelationshipCollectorFactoryInterface $relationship_collector_factory
   *   The relationship collector factory.
   * @param \Symfony\Component\Serializer\Normalizer\NormalizerInterface $formatted_fields_normalizer
   *   Normalizer of the formatted entity fields.
   */
  public function __construct(
    RelationshipCollectorFactoryInterface $relationship_collector_factory,
    NormalizerInterface $formatted_fields_normalizer
  ) {
    $this->relationshipCollectorFactory = $relationship_collector_factory;
    $this->formattedFieldsNormalizer = $formatted_fields_normalizer;
  }

  /**
   * Creates relationship collector, if no instance found in the context.
   *
   * @param array $context
   *   The context.
   *
   * @return \Drupal\frontend_api\Rest\Relationship\RelationshipCollectorInterface|null
   *   The relationship collector.
   */
  protected function createRelationshipCollector(
    array $context
  ): ?RelationshipCollectorInterface {
    if (isset($context[static::RELATIONSHIP_COLLECTOR_CONTEXT])) {
      // There is a relationship collector in the context already, so we don't
      // have to set up another one and the relationships must be handled on a
      // parent level.
      return NULL;
    }

    return $this->relationshipCollectorFactory->get();
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */

    $relationship_collector = $this->createRelationshipCollector($context);
    if (isset($relationship_collector)) {
      $context[static::RELATIONSHIP_COLLECTOR_CONTEXT] = $relationship_collector;
    }

    $result = [
      'entity_type_id' => $entity->getEntityTypeId(),
      'bundle_id' => $entity->bundle(),
      'entity_id' => $entity->id(),
    ];

    // @todo Find a way to set serializer through container.
    $this->formattedFieldsNormalizer->setSerializer($this->serializer);
    $result['fields'] = $this->formattedFieldsNormalizer
      ->normalize($entity, $format, $context);

    if (isset($relationship_collector)) {
      $entities = $this->serializer
        ->normalize($relationship_collector, $format, $context);

      // Avoid adding empty array to the response, as it breaks data validation
      // on the front app.
      if (isset($entities)) {
        $result['entities'] = $entities;
      }
    }

    return $result;
  }

}
