<?php

namespace Drupal\frontend_api\Normalizer\SearchApi\Autocomplete;

use Drupal\Core\Entity\EntityInterface;
use Drupal\search_api_autocomplete\Suggestion\SuggestionInterface;
use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\frontend_api\Rest\NormalizerContextKeys;
use Drupal\frontend_api\Rest\SearchApi\Autocomplete\GroupableSuggestionInterface;
use Drupal\frontend_api\Rest\SearchApi\Autocomplete\DefaultSuggestionList;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Normalizer for the Search API Autocomplete suggestion list.
 */
class DefaultSuggestionListNormalizer extends NormalizerBase {

  /**
   * The view mode key on the serialization context.
   */
  protected const VIEW_MODE_CONTEXT = NormalizerContextKeys::VIEW_MODE_CONTEXT;

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = DefaultSuggestionList::class;

  /**
   * The normalizer of the entity formatted in a view mode.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface|\Symfony\Component\Serializer\SerializerAwareInterface
   */
  protected $formattedEntityNormalizer;

  /**
   * A constructor.
   */
  public function __construct(
    NormalizerInterface $formatted_entity_normalizer
  ) {
    $this->formattedEntityNormalizer = $formatted_entity_normalizer;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($list, $format = NULL, array $context = []) {
    /** @var \Drupal\frontend_api\Rest\SearchApi\Autocomplete\SuggestionListInterface $list */

    $search = $list->getSearch();
    $show_count = $search->getOption('show_count');
    $suggestions = $list->getSuggestions();
    $groups = [];
    $items = [];
    foreach ($suggestions as $suggestion) {
      $value = $suggestion->getSuggestedKeys();
      if (!isset($value)) {
        $value = $suggestion->getLabel();
      }

      $prefix = $suggestion->getSuggestionPrefix();
      $input = $suggestion->getUserInput();
      $suffix = $suggestion->getSuggestionSuffix();
      if (isset($prefix) || isset($input) || isset($suffix)) {
        $label = $prefix . $input . $suffix;
      }
      else {
        $label = $suggestion->getLabel();
      }

      $item = [
        'label' => $label,
        'value' => $value,
      ];

      if ($show_count) {
        $item['count'] = $suggestion->getResultsCount();
      }

      if ($entity = $this->getFormattedEntity($suggestion, $format)) {
        $item['entity'] = $entity;
      }

      // @todo Check if suggestion has an URL and expose it to the API somehow.
      // Change results structure if suggestions should be grouped.
      if ($suggestion instanceof GroupableSuggestionInterface) {
        $group_id = $suggestion->getGroupId();

        if (!isset($groups[$group_id]['name'])) {
          $groups[$group_id]['name'] = $suggestion->getGroupName();
        }
        $groups[$group_id]['items'][] = $item;
      }
      $items[] = $item;
    }

    return [
      'groups' => $groups,
      'items' => $items,
    ];
  }

  /**
   * Gets formatted entity for specified view mode.
   *
   * @param \Drupal\search_api_autocomplete\Suggestion\SuggestionInterface $suggestion
   *   Single autocompletion suggestion.
   * @param string $format
   *   Serialization format.
   *
   * @return array|null
   *   Formatted entity or null.
   */
  protected function getFormattedEntity(
    SuggestionInterface $suggestion,
    string $format
  ) {
    $render = $suggestion->getRender();
    if (!$render) {
      return NULL;
    }
    // Entity comes in render array with key '#entity_type'
    // so we will get it from Url object.
    // @todo Find a better way to get entity.
    $url = $suggestion->getUrl();
    if (!$url) {
      return NULL;
    }
    $options = $url->getOptions();
    $entity = $options['entity'] ?? NULL;
    if (!$entity instanceof EntityInterface) {
      return NULL;
    }

    $view_mode = $render['#view_mode'];
    $this->formattedEntityNormalizer->setSerializer($this->serializer);
    $formatted_entity = $this->formattedEntityNormalizer->normalize(
      $entity,
      $format,
      [static::VIEW_MODE_CONTEXT => $view_mode]
    );

    return $formatted_entity;
  }

}
