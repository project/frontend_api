<?php

namespace Drupal\frontend_api\Normalizer\SearchApi\Autocomplete;

use Drupal\Core\Entity\EntityInterface;
use Drupal\search_api_autocomplete\Suggestion\SuggestionInterface;
use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer\EntityReferenceAutocompleteNormalizer;
use Drupal\frontend_api\Rest\SearchApi\Autocomplete\BasicEntitySuggestionList;

/**
 * Provides normalizer for the basic entity suggestion list.
 *
 * It uses different format comparing to the default suggestion list normalizer
 * to match the one expected by the basic autocomplete widget.
 *
 * @see \Drupal\frontend_api\Rest\SearchApi\Autocomplete\BasicEntitySuggestionList
 */
class BasicEntitySuggestionListNormalizer extends NormalizerBase {

  /**
   * The entity ID key on the normalized suggestion.
   */
  protected const ITEM_ID_KEY = EntityReferenceAutocompleteNormalizer::ITEM_ID_KEY;

  /**
   * The entity label key on the normalized suggestion.
   */
  protected const ITEM_LABEL_KEY = EntityReferenceAutocompleteNormalizer::ITEM_LABEL_KEY;

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = BasicEntitySuggestionList::class;

  /**
   * Extracts the entity from the autocomplete suggestion.
   *
   * @param \Drupal\search_api_autocomplete\Suggestion\SuggestionInterface $suggestion
   *   The suggestion.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity or NULL if it wasn't found in the suggestion.
   */
  protected function getSuggestionEntity(
    SuggestionInterface $suggestion
  ): ?EntityInterface {
    // Entity comes in render array with key '#entity_type'
    // so we will get it from Url object.
    // @todo Find a better way to get entity.
    $url = $suggestion->getUrl();
    if (!$url) {
      return NULL;
    }
    $options = $url->getOptions();
    $entity = $options['entity'] ?? NULL;
    if (!$entity instanceof EntityInterface) {
      return NULL;
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($list, $format = NULL, array $context = []) {
    /** @var \Drupal\frontend_api\Rest\SearchApi\Autocomplete\SuggestionListInterface $list */
    $suggestions = $list->getSuggestions();
    $items = [];
    foreach ($suggestions as $suggestion) {
      $prefix = $suggestion->getSuggestionPrefix();
      $input = $suggestion->getUserInput();
      $suffix = $suggestion->getSuggestionSuffix();
      if (isset($prefix) || isset($input) || isset($suffix)) {
        $label = $prefix . $input . $suffix;
      }
      else {
        $label = $suggestion->getLabel();
      }
      if ($label === NULL) {
        continue;
      }

      $entity = $this->getSuggestionEntity($suggestion);
      if ($entity === NULL || $entity->isNew()) {
        continue;
      }

      $items[] = [
        static::ITEM_ID_KEY => $entity->id(),
        static::ITEM_LABEL_KEY => $label,
      ];
    }

    return $items;
  }

}
