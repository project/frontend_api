<?php

namespace Drupal\frontend_api\Normalizer\EntityDisplayInfo;

use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraFieldNormalizerManagerInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataExtraFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;

/**
 * Provides normalizer for an extra field of the entity form data.
 */
class FormDataExtraFieldNormalizer extends NormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = FormDataExtraFieldInterface::class;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface|\Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * The plugin manager of the field definition normalizers.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraFieldNormalizerManagerInterface
   */
  protected $normalizerManager;

  /**
   * A constructor.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraFieldNormalizerManagerInterface $normalizer_manager
   *   The plugin manager of the field normalizers.
   */
  public function __construct(
    FormExtraFieldNormalizerManagerInterface $normalizer_manager
  ) {
    $this->normalizerManager = $normalizer_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($field, $format = NULL, array $context = []) {
    /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataExtraFieldInterface $field */
    /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraFieldNormalizerInterface $normalizer */
    $normalizer = $this->normalizerManager
      ->createNormalizerForField(
        $field,
        $format,
        $context
      );

    if ($normalizer instanceof NormalizerAwareInterface) {
      $normalizer->setNormalizer($this->serializer);
    }

    $form_data = $field->getDisplayInfo();
    if (!$form_data instanceof FormDataInterface) {
      throw new \InvalidArgumentException(sprintf(
        'The %s extra field must be attached to the form data.',
        $field->getFieldName()
      ));
    }

    $entity = $form_data->getEntity();
    return $normalizer->normalizeFieldValue($entity, $field, $format, $context);
  }

}
