<?php

namespace Drupal\frontend_api\Normalizer\EntityDisplayInfo;

use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\frontend_api\Rest\FieldConstraint\FieldConstraintListInterface;
use Drupal\frontend_api\Rest\FieldConstraint\FieldConstraintNormalizerManagerInterface;

/**
 * Provides system normalizer for the field constraint list.
 *
 * @see \Drupal\frontend_api\Rest\FieldConstraint\FieldConstraintListInterface
 */
class FieldConstraintListNormalizer extends NormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = FieldConstraintListInterface::class;

  /**
   * The plugin manager of the field constraint normalizers.
   *
   * @var \Drupal\frontend_api\Rest\FieldConstraint\FieldConstraintNormalizerManagerInterface
   */
  protected $normalizerManager;

  /**
   * A constructor.
   */
  public function __construct(
    FieldConstraintNormalizerManagerInterface $normalizer_manager
  ) {
    $this->normalizerManager = $normalizer_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($constraint_list, $format = NULL, array $context = []) {
    /** @var \Drupal\frontend_api\Rest\FieldConstraint\FieldConstraintListInterface $constraint_list */
    $constraints = $constraint_list->getConstraints();
    if (empty($constraints)) {
      return [];
    }

    $field = $constraint_list->getField();
    $normalizers = $this->normalizerManager
      ->createNormalizersForFieldConstraints($field, $constraints);

    $result = [];
    foreach ($normalizers as $constraint_id => $normalizer) {
      $constraint = $constraints[$constraint_id] ?? NULL;
      if ($constraint === NULL) {
        $field_name = $field->getFieldDefinition()
          ->getName();
        throw new \LogicException(sprintf(
          'The %s constraint normalizer was returned for unknown %s constraint of the %s field.',
          $normalizer->getPluginId(),
          $constraint_id,
          $field_name
        ));
      }

      $normalized = $normalizer->normalizeConstraint($field, $constraint);
      if ($normalized === NULL) {
        continue;
      }

      $result[] = $normalized;
    }
    return $result;
  }

}
