<?php

namespace Drupal\frontend_api\Normalizer\EntityDisplayInfo;

use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraFieldNormalizerManagerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;

/**
 * Provides normalizer for a extra field of the entity form info.
 */
class FormExtraFieldNormalizer extends NormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = FormExtraFieldInterface::class;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface|\Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * The plugin manager of the field definition normalizers.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraFieldNormalizerManagerInterface
   */
  protected $normalizerManager;

  /**
   * A constructor.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraFieldNormalizerManagerInterface $normalizer_manager
   *   The plugin manager of the field normalizers.
   */
  public function __construct(
    FormExtraFieldNormalizerManagerInterface $normalizer_manager
  ) {
    $this->normalizerManager = $normalizer_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($field, $format = NULL, array $context = []) {
    /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface $field */
    /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraFieldNormalizerInterface $normalizer */
    $normalizer = $this->normalizerManager
      ->createNormalizerForField(
        $field,
        $format,
        $context
      );

    if ($normalizer instanceof NormalizerAwareInterface) {
      $normalizer->setNormalizer($this->serializer);
    }

    return $normalizer->normalizeField($field, $format, $context);
  }

}
