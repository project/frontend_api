<?php

namespace Drupal\frontend_api\Normalizer\EntityDisplayInfo;

use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface;

/**
 * Provides normalizer for a field of the entity view info.
 */
class ViewFieldNormalizer extends FieldNormalizerBase {

  /**
   * Name of the supported interface or class.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = ViewFieldInterface::class;

}
