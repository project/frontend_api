<?php

namespace Drupal\frontend_api\Normalizer\EntityDisplayInfo;

use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataInterface;

/**
 * Provides normalizer for the entity form data.
 */
class FormDataNormalizer extends NormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = FormDataInterface::class;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface|\Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected $serializer;

  /**
   * {@inheritdoc}
   */
  public function normalize(
    $object,
    $format = NULL,
    array $context = []
  ): array {
    /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataInterface $object */
    $entity = $object->getEntity();
    $result = [
      'entity_type_id' => $entity->getEntityTypeId(),
      'bundle_id' => $entity->bundle(),
      'entity_id' => $entity->id(),
    ];

    $result['fields'] = $this->normalizeFields($object, $format, $context);
    $result['fields'] += $this->normalizeExtraFields($object, $format, $context);

    // Add cacheable dependencies after fields are normalized, as they may add
    // some more dependencies. Although entity is already added as a dependency
    // to the form data, we have to do it once again, as computed fields may add
    // some cacheability metadata to the entity and they're not aware about the
    // form data.
    $this->addCacheableDependency($context, $object);
    $this->addCacheableDependency($context, $entity);

    return $result;
  }

  /**
   * Normalizes fields of the form data.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataInterface $form_data
   *   The entity form data.
   * @param string|null $format
   *   The format.
   * @param array $context
   *   The context.
   *
   * @return array
   *   The normalization result.
   */
  protected function normalizeFields(
    FormDataInterface $form_data,
    string $format = NULL,
    array $context = []
  ): array {
    /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface[] $fields */
    $result = [];
    $fields = $form_data->getFields();
    foreach ($fields as $key => $field) {
      $normalized = $this->serializer
        ->normalize($field, $format, $context);
      if (!isset($normalized)) {
        continue;
      }

      $result[$key] = $normalized;
    }
    return $result;
  }

  /**
   * Normalizes extra fields of the form data.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataInterface $form_data
   *   The entity form data.
   * @param string|null $format
   *   The format.
   * @param array $context
   *   The context.
   *
   * @return array
   *   The normalization result.
   */
  protected function normalizeExtraFields(
    FormDataInterface $form_data,
    string $format = NULL,
    array $context = []
  ): array {
    /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface[] $extra_fields */
    $result = [];
    $extra_fields = $form_data->getExtraFields();
    foreach ($extra_fields as $extra_field) {
      $normalized = $this->serializer
        ->normalize($extra_field, $format, $context);
      if (!isset($normalized)) {
        continue;
      }

      $result += $normalized;
    }
    return $result;
  }

}
