<?php

namespace Drupal\frontend_api\Normalizer\EntityDisplayInfo;

use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerManagerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;

/**
 * Provides base normalizer for a field of the entity form/view info.
 */
abstract class FieldNormalizerBase extends NormalizerBase {

  /**
   * The plugin manager of the field definition normalizers.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerManagerInterface
   */
  protected $normalizerManager;

  /**
   * The serializer/normalizer.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected $serializer;

  /**
   * A constructor.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerManagerInterface $normalizer_manager
   *   The plugin manager of the field normalizers.
   */
  public function __construct(
    DisplayFieldNormalizerManagerInterface $normalizer_manager
  ) {
    $this->normalizerManager = $normalizer_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($field, $format = NULL, array $context = []) {
    /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field */
    $this->addCacheableDependency($context, $field->getFieldDefinition());

    $normalizer = $this->normalizerManager
      ->createNormalizerForField(
        $field,
        $format,
        $context
      );

    if ($normalizer instanceof NormalizerAwareInterface) {
      $normalizer->setNormalizer($this->serializer);
    }

    return $normalizer->normalizeField($field, $format, $context);
  }

}
