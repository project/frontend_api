<?php

namespace Drupal\frontend_api\Normalizer\EntityDisplayInfo;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings\DisplaySettingsNormalizerManagerInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormInfoInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provides normalizer for the entity form info.
 */
class FormInfoNormalizer extends DisplayInfoNormalizer {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = FormInfoInterface::class;

  /**
   * The form third-party settings normalizer manager.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings\DisplaySettingsNormalizerManagerInterface
   */
  protected $settingsNormalizerManager;

  /**
   * A constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_bundle_info_provider
   *   The entity bundle info provider.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings\DisplaySettingsNormalizerManagerInterface $settings_normalizer_manager
   *   The form third-party settings normalizer manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_bundle_info_provider,
    EventDispatcherInterface $event_dispatcher,
    DisplaySettingsNormalizerManagerInterface $settings_normalizer_manager
  ) {
    parent::__construct(
      $entity_type_manager,
      $entity_bundle_info_provider,
      $event_dispatcher
    );

    $this->settingsNormalizerManager = $settings_normalizer_manager;
  }

  /**
   * Normalizes third-party settings of the form info.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $display_info
   *   The entity form/view info.
   * @param string|null $format
   *   The format.
   * @param array $context
   *   The context.
   *
   * @return array
   *   The normalization result.
   */
  protected function normalizeThirdPartySettings(
    DisplayInfoInterface $display_info,
    string $format = NULL,
    array $context = []
  ): array {
    $result = [];
    $settings = $display_info->getThirdPartySettings();
    $field_context = $this->getFieldContext($display_info, $context);
    foreach ($settings as $item) {
      $normalizers = $this->settingsNormalizerManager
        ->createNormalizersForItem($item);

      foreach ($normalizers as $normalizer_plugin) {
        $normalizer_plugin->normalizeItem(
          $item,
          $result,
          $format,
          $field_context
        );
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize(
    $object,
    $format = NULL,
    array $context = []
  ): array {
    $result = parent::normalize($object, $format, $context);

    // @TODO Move to parent class if view info support will be added.
    $third_party_settings = $this->normalizeThirdPartySettings(
      $object,
      $format,
      $context
    );
    $result['third_party_settings'] = $third_party_settings;

    /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormInfoInterface $object */
    if ($object->hasEntity()) {
      $entity = $object->getEntity();
      if (!$entity->isNew()) {
        $entityId = $entity->id();
        $result['entity_id'] = $entityId;
      }

      // Although entity is already added as a dependency to the form info, we
      // have to do it once again, as computed fields may add some cacheability
      // metadata to the entity and they're not aware about the form info.
      $this->addCacheableDependency($context, $entity);
    }

    return $result;
  }

}
