<?php

namespace Drupal\frontend_api\Normalizer\EntityDisplayInfo;

use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewExtraFieldInterface;

/**
 * Provides normalizer for a extra field of the entity view info.
 */
class ViewExtraFieldNormalizer extends NormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = ViewExtraFieldInterface::class;

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    // @todo Implement support, if needed. For now we don't use the entity view
    //   info.
    throw new \Exception(
      'Normalization of the entity view info with extra fields is not yet supported.'
    );
  }

}
