<?php

namespace Drupal\frontend_api\Normalizer\EntityDisplayInfo;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\frontend_api\Rest\NormalizerContextKeys;
use Drupal\frontend_api\Dictionary\CacheTags;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface;
use Drupal\frontend_api\Dictionary\CacheContexts;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Defines normalizer of an entity form/view info object.
 *
 * @see \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface
 */
class DisplayInfoNormalizer extends NormalizerBase {

  /**
   * The context key of the form info object added.
   */
  protected const DISPLAY_INFO_CONTEXT = NormalizerContextKeys::DISPLAY_INFO_CONTEXT;

  /**
   * The language cache context prefix.
   */
  protected const LANGUAGE_CONTEXT_PREFIX = CacheContexts::LANGUAGE_PREFIX;

  /**
   * The interface language type.
   */
  protected const LANGUAGE_TYPE_INTERFACE = LanguageInterface::TYPE_INTERFACE;

  /**
   * Entity bundles cache tag.
   */
  protected const BUNDLES_CACHE_TAG = CacheTags::BUNDLES;

  /**
   * Name of the supported interface or class.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = DisplayInfoInterface::class;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface|\Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected $serializer;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity bundle info provider.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityBundleInfoProvider;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * A constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_bundle_info_provider
   *   The entity bundle info provider.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_bundle_info_provider,
    EventDispatcherInterface $event_dispatcher
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityBundleInfoProvider = $entity_bundle_info_provider;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize(
    $object,
    $format = NULL,
    array $context = []
  ): array {
    /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $object */
    $entity_type_id = $object->getEntityTypeId();
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);

    $result = [
      'entity_type_id' => $entity_type_id,
    ];

    $bundle_key = $entity_type->getKey('bundle');
    if ($bundle_key) {
      $result['bundle_key'] = $bundle_key;
    }

    $bundle_id = $object->getEntityBundleId();
    if (isset($bundle_id)) {
      $result['bundle_id'] = $bundle_id;
    }

    $bundle_label = $this->getBundleLabel($object, $entity_type, $context);
    if ($bundle_label !== NULL) {
      $result['bundle_label'] = $bundle_label;
    }

    $result['fields'] = $this->normalizeFields($object, $format, $context);
    $result['fields'] += $this->normalizeExtraFields($object, $format, $context);

    // @todo Dispatch events.

    // Add cacheable dependencies after fields are normalized, as they may add
    // some more dependencies.
    $this->addCacheableDependency($context, $object);

    return $result;
  }

  /**
   * Returns the entity bundle label of the passed display info, if any.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $display_info
   *   The form/view info.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param array $context
   *   The serialization context.
   *
   * @return string|null
   *   The bundle label or NULL if entity doesn't have a bundle, it was not
   *   specified on the display info or is not loadable.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getBundleLabel(
    DisplayInfoInterface $display_info,
    EntityTypeInterface $entity_type,
    array $context
  ) {
    $bundle_id = $display_info->getEntityBundleId();
    if ($bundle_id === NULL) {
      return NULL;
    }

    $entity_type_id = $entity_type->id();
    $bundles_info = $this->entityBundleInfoProvider
      ->getBundleInfo($entity_type_id);
    $bundle_label = $bundles_info[$bundle_id]['label'] ?? NULL;
    if ($bundle_label === NULL) {
      return NULL;
    }

    $metadata = new CacheableMetadata();
    $metadata->addCacheContexts([
      static::LANGUAGE_CONTEXT_PREFIX . static::LANGUAGE_TYPE_INTERFACE,
    ]);
    $metadata->addCacheTags([static::BUNDLES_CACHE_TAG]);
    $bundle_entity_type_id = $entity_type->getBundleEntityType();
    if ($bundle_entity_type_id !== NULL) {
      $metadata->addCacheTags([
        $bundle_entity_type_id . ':' . $bundle_id,
      ]);
    }

    $this->addCacheableDependency($context, $metadata);

    return $bundle_label;
  }

  /**
   * Normalizes fields of the form/view info.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $display_info
   *   The entity form/view info.
   * @param string|null $format
   *   The format.
   * @param array $context
   *   The context.
   *
   * @return array
   *   The normalization result.
   */
  protected function normalizeFields(
    DisplayInfoInterface $display_info,
    string $format = NULL,
    array $context = []
  ): array {
    $result = [];
    $fields = $display_info->getFields();
    $field_context = $this->getFieldContext($display_info, $context);
    foreach ($fields as $key => $field_definition) {
      $result[$key] = $this->serializer
        ->normalize($field_definition, $format, $field_context);
    }

    return $result;
  }

  /**
   * Normalizes extra fields of the form/view info.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $display_info
   *   The entity form/view info.
   * @param string|null $format
   *   The format.
   * @param array $context
   *   The context.
   *
   * @return array
   *   The normalization result.
   */
  protected function normalizeExtraFields(
    DisplayInfoInterface $display_info,
    string $format = NULL,
    array $context = []
  ): array {
    $result = [];
    $extra_fields = $display_info->getExtraFields();
    $field_context = $this->getFieldContext($display_info, $context);
    foreach ($extra_fields as $field) {
      $result += $this->serializer
        ->normalize($field, $format, $field_context);
    }

    return $result;
  }

  /**
   * Returns array with field context.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $display_info
   *   The entity form/view info.
   * @param array $context
   *   The context.
   *
   * @return array
   *   The field context.
   */
  protected function getFieldContext(
    DisplayInfoInterface $display_info,
    array $context
  ): array {
    $field_context = [
      static::DISPLAY_INFO_CONTEXT => $display_info,
    ];
    $field_context += $context;

    return $field_context;
  }

}
