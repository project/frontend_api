<?php

namespace Drupal\frontend_api\Normalizer\EntityDisplayInfo;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface;

/**
 * Provides normalizer for a field of the entity form info.
 */
class FormFieldNormalizer extends FieldNormalizerBase {

  /**
   * Name of the supported interface or class.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = FormFieldInterface::class;

}
