<?php

namespace Drupal\frontend_api\Normalizer\EntityDisplayInfo;

use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerManagerInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;

/**
 * Provides normalizer for the field of the entity form data.
 */
class FormDataFieldNormalizer extends NormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = FormDataFieldInterface::class;

  /**
   * The plugin manager of the field definition normalizers.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerManagerInterface
   */
  protected $normalizerManager;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface|\Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * A constructor.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerManagerInterface $normalizer_manager
   *   The plugin manager of the field normalizers.
   */
  public function __construct(
    DisplayFieldNormalizerManagerInterface $normalizer_manager
  ) {
    $this->normalizerManager = $normalizer_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($field, $format = NULL, array $context = []) {
    /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field */
    $this->addCacheableDependency($context, $field->getFieldDefinition());

    /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldNormalizerInterface $normalizer */
    $normalizer = $this->normalizerManager
      ->createNormalizerForField(
        $field,
        $format,
        $context
      );

    if ($normalizer instanceof NormalizerAwareInterface) {
      $normalizer->setNormalizer($this->serializer);
    }

    return $normalizer->normalizeFieldValue($field, $format, $context);
  }

}
