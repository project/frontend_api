<?php

namespace Drupal\frontend_api\Normalizer;

use Drupal\serialization\Normalizer\NormalizerBase;

/**
 * Provides base class for normalizers that aren't registered in serializer.
 */
abstract class ManuallyCalledNormalizerBase extends NormalizerBase {

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsDenormalization($data, $type, $format = NULL): bool {
    return FALSE;
  }

}
