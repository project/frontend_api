<?php

namespace Drupal\frontend_api\Normalizer\Pager;

use Drupal\views\Plugin\views\pager\Mini;
use Drupal\serialization\Normalizer\NormalizerBase;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Converts the Mini pager object to a json array structure.
 */
class MiniPagerNormalizer extends NormalizerBase implements NormalizerInterface {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = Mini::class;

  /**
   * {@inheritdoc}
   */
  public function normalize($pager, $format = NULL, array $context = []) {
    /** @var \Drupal\views\Plugin\views\pager\Mini $pager */

    // We do not show total_pages and total_items for Mini pager because it
    // does not know about exact records of a view. As it forms only
    // previous/next links, it shows only the amount of records on
    // previous+current+next page, same is for pages count.
    // Further pages are out of its scope.
    return [
      'pager_type' => $pager->getPluginId(),
      'current_page' => $pager->getCurrentPage(),
      'items_per_page' => $pager->getItemsPerPage(),
    ];
  }

}
