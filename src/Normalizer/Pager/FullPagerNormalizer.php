<?php

namespace Drupal\frontend_api\Normalizer\Pager;

use Drupal\views\Plugin\views\pager\Full;
use Drupal\serialization\Normalizer\NormalizerBase;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Converts the Full pager object to a json array structure.
 */
class FullPagerNormalizer extends NormalizerBase implements NormalizerInterface {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = Full::class;

  /**
   * {@inheritdoc}
   */
  public function normalize($pager, $format = NULL, array $context = []) {
    /** @var \Drupal\views\Plugin\views\pager\Full $pager */

    return [
      'pager_type' => $pager->getPluginId(),
      'current_page' => $pager->getCurrentPage(),
      'total_items' => $pager->getTotalItems(),
      'total_pages' => $pager->getPagerTotal(),
      'items_per_page' => $pager->getItemsPerPage(),
    ];
  }

}
