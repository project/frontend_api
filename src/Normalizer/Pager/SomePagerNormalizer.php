<?php

namespace Drupal\frontend_api\Normalizer\Pager;

use Drupal\views\Plugin\views\pager\Some;
use Drupal\serialization\Normalizer\NormalizerBase;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Converts the Some pager object to a json array structure.
 */
class SomePagerNormalizer extends NormalizerBase implements NormalizerInterface {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = Some::class;

  /**
   * {@inheritdoc}
   */
  public function normalize($pager, $format = NULL, array $context = []) {
    /** @var \Drupal\views\Plugin\views\pager\Some $pager */

    // It shows only certain amount of records that was set in view,
    // so it doesn't take into account the total number of records and
    // doesn't have pages.
    return [
      'pager_type' => $pager->getPluginId(),
      'items_per_page' => $pager->getItemsPerPage(),
    ];
  }

}
