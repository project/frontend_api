<?php

namespace Drupal\frontend_api\Normalizer\Pager;

use Drupal\views\Plugin\views\pager\None;
use Drupal\serialization\Normalizer\NormalizerBase;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Converts the None pager object to a json array structure.
 */
class NonePagerNormalizer extends NormalizerBase implements NormalizerInterface {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = None::class;

  /**
   * {@inheritdoc}
   */
  public function normalize($pager, $format = NULL, array $context = []) {
    /** @var \Drupal\views\Plugin\views\pager\None $pager */

    return [
      'pager_type' => $pager->getPluginId(),
    ];
  }

}
