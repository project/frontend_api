<?php

namespace Drupal\frontend_api\Normalizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldNormalizerManagerInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerManagerInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoBuilderInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface;
use Drupal\frontend_api\Rest\FieldMerger\FieldMergerFactoryInterface;
use Drupal\frontend_api\Rest\FieldMerger\FieldMergerInterface;
use Drupal\frontend_api\Rest\NormalizerContextKeys;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;

/**
 * Provides normalizer for fields of an entity formatted in a view mode.
 *
 * It isn't registered as a normalizer, but gets called by a formatted entity
 * normalizer.
 */
class FormattedFieldsNormalizer extends ManuallyCalledNormalizerBase {

  /**
   * The context key of the view mode name.
   */
  protected const VIEW_MODE_CONTEXT = NormalizerContextKeys::VIEW_MODE_CONTEXT;

  /**
   * The context key of the field merger.
   */
  protected const FIELD_MERGER_CONTEXT = NormalizerContextKeys::FIELD_MERGER_CONTEXT;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface|\Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * The entity view info builder.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoBuilderInterface
   */
  protected $viewInfoBuilder;

  /**
   * The plugin manager of the entity view info normalizers.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerManagerInterface
   */
  protected $fieldNormalizerManager;

  /**
   * The plugin manager of the entity view info extra field normalizers.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldNormalizerManagerInterface
   */
  protected $extraFieldNormalizerManager;

  /**
   * The field merger factory.
   *
   * @var \Drupal\frontend_api\Rest\FieldMerger\FieldMergerFactoryInterface
   */
  protected $fieldMergerFactory;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * A constructor.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoBuilderInterface $view_info_builder
   *   The entity view info builder.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerManagerInterface $field_normalizer_manager
   *   The plugin manager of the entity view info normalizers.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldNormalizerManagerInterface $extra_field_normalizer_manager
   *   The plugin manager of the entity view info normalizers for extra fields.
   * @param \Drupal\frontend_api\Rest\FieldMerger\FieldMergerFactoryInterface $field_merger_factory
   *   The field merger factory.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(
    DisplayInfoBuilderInterface $view_info_builder,
    DisplayFieldNormalizerManagerInterface $field_normalizer_manager,
    DisplayExtraFieldNormalizerManagerInterface $extra_field_normalizer_manager,
    FieldMergerFactoryInterface $field_merger_factory,
    EntityRepositoryInterface $entity_repository
  ) {
    $this->viewInfoBuilder = $view_info_builder;
    $this->fieldNormalizerManager = $field_normalizer_manager;
    $this->extraFieldNormalizerManager = $extra_field_normalizer_manager;
    $this->fieldMergerFactory = $field_merger_factory;
    $this->entityRepository = $entity_repository;
  }

  /**
   * Returns the entity display info built for the passed entity and view mode.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $view_mode
   *   The view mode name.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface
   *   The entity display info.
   *
   * @todo Cache entity display info.
   */
  protected function getEntityDisplayInfo(
    ContentEntityInterface $entity,
    string $view_mode
  ): DisplayInfoInterface {
    return $this->viewInfoBuilder->build(
      $entity->getEntityTypeId(),
      $entity->bundle(),
      $view_mode
    );
  }

  /**
   * Returns the view mode name found on the context.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param array $context
   *   The serialization context.
   *
   * @return string
   *   The view mode name.
   */
  protected function getViewModeName(
    ContentEntityInterface $entity,
    array $context = []
  ): string {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $view_mode = $context[static::VIEW_MODE_CONTEXT] ?? NULL;
    if (!isset($view_mode)) {
      throw new \InvalidArgumentException(sprintf(
        'View mode must be passed to the %s #%s',
        $entity->getEntityTypeId(),
        $entity->id()
      ));
    }

    return $view_mode;
  }

  /**
   * Returns field normalizers.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $view_info
   *   The entity view info.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The context.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldNormalizerInterface[]
   *   Field normalizer keyed by the field name.
   */
  protected function getFieldNormalizers(
    DisplayInfoInterface $view_info,
    string $format = NULL,
    array $context = []
  ): array {
    $normalizers = [];

    foreach ($view_info->getFields() as $field_name => $field) {
      $normalizer = $this->fieldNormalizerManager
        ->createNormalizerForField($field, $format, $context);

      if ($normalizer instanceof NormalizerAwareInterface) {
        $normalizer->setNormalizer($this->serializer);
      }

      $normalizers[$field_name] = $normalizer;
    }

    return $normalizers;
  }

  /**
   * Returns extra field normalizers.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $view_info
   *   The entity view info.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The context.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewExtraFieldNormalizerInterface[]
   *   Field normalizer keyed by the field name.
   */
  protected function getExtraFieldNormalizers(
    DisplayInfoInterface $view_info,
    string $format = NULL,
    array $context = []
  ): array {
    $normalizers = [];

    foreach ($view_info->getExtraFields() as $field_name => $field) {
      $normalizer = $this->extraFieldNormalizerManager
        ->createNormalizerForField($field, $format, $context);

      if ($normalizer instanceof NormalizerAwareInterface) {
        $normalizer->setNormalizer($this->serializer);
      }

      $normalizers[$field_name] = $normalizer;
    }

    return $normalizers;
  }

  /**
   * Prepares the field values using the passed normalizers.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $view_info
   *   The entity view info.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldNormalizerInterface[] $normalizers
   *   The field normalizers.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The context.
   */
  protected function prepareFieldValues(
    ContentEntityInterface $entity,
    DisplayInfoInterface $view_info,
    array $normalizers,
    string $format = NULL,
    array $context = []
  ): void {
    $fields = $view_info->getFields();

    foreach ($normalizers as $field_name => $normalizer) {
      $field = $fields[$field_name];
      $item_list = $entity->get($field_name);

      $normalizer->prepareFieldValue(
        $field,
        $item_list,
        $format,
        $context
      );
    }
  }

  /**
   * Normalizes the entity field values.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $view_info
   *   The entity view info.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldNormalizerInterface[] $normalizers
   *   The normalizers keyed by the field name.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The context.
   *
   * @return array
   *   The normalized field values.
   */
  protected function normalizeFieldValues(
    ContentEntityInterface $entity,
    DisplayInfoInterface $view_info,
    array $normalizers,
    string $format = NULL,
    array $context = []
  ): array {
    $fields = $view_info->getFields();

    $result = [];
    foreach ($normalizers as $field_name => $normalizer) {
      $field = $fields[$field_name];
      $item_list = $entity->get($field_name);

      $result[$field_name] = $normalizer->normalizeFieldValue(
        $field,
        $item_list,
        $format,
        $context
      );
    }
    return $result;
  }

  /**
   * Creates field merger, if no instance found in the context.
   *
   * @param array $context
   *   The context.
   *
   * @return \Drupal\frontend_api\Rest\FieldMerger\FieldMergerInterface|null
   *   The field merger.
   */
  protected function createFieldMerger(array $context): ?FieldMergerInterface {
    if (isset($context[static::FIELD_MERGER_CONTEXT])) {
      // There is a field merger in the context already, so we don't want to
      // create another one. All the fields must be merged to the first level.
      return NULL;
    }

    return $this->fieldMergerFactory->get($this);
  }

  /**
   * Normalizes entity fields.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $view_info
   *   The entity view info.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The context.
   *
   * @return array
   *   The normalized fields.
   */
  protected function normalizeFields(
    ContentEntityInterface $entity,
    DisplayInfoInterface $view_info,
    string $format = NULL,
    array $context = []
  ): array {
    $normalizers = $this->getFieldNormalizers($view_info, $format, $context);

    $this->prepareFieldValues(
      $entity,
      $view_info,
      $normalizers,
      $format,
      $context
    );
    return $this->normalizeFieldValues(
      $entity,
      $view_info,
      $normalizers,
      $format,
      $context
    );
  }

  /**
   * Normalizes entity extra fields.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $view_info
   *   The entity view info.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The context.
   *
   * @return array
   *   The normalized extra fields.
   */
  protected function normalizeExtraFields(
    ContentEntityInterface $entity,
    DisplayInfoInterface $view_info,
    string $format = NULL,
    array $context = []
  ): array {
    $normalizers = $this->getExtraFieldNormalizers($view_info, $format, $context);
    $extra_fields = $view_info->getExtraFields();

    $result = [];
    foreach ($normalizers as $field_name => $normalizer) {
      $field = $extra_fields[$field_name];

      $result += $normalizer->normalizeField(
        $entity,
        $field,
        $format,
        $context
      );
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {
    // Set the entity in the correct language for display.
    if ($entity instanceof ContentEntityInterface) {
      $entity = $this->entityRepository
        ->getTranslationFromContext($entity);
    }

    $view_mode = $this->getViewModeName($entity, $context);
    $view_info = $this->getEntityDisplayInfo($entity, $view_mode);

    // It shouldn't be used by entities exposed with a relationship collector,
    // so we add it inside this function and it won't leak to relationships.
    $field_merger = $this->createFieldMerger($context);
    if (isset($field_merger)) {
      $context[static::FIELD_MERGER_CONTEXT] = $field_merger;
    }

    $fields = $this->normalizeFields($entity, $view_info, $format, $context);
    $fields += $this->normalizeExtraFields($entity, $view_info, $format, $context);

    if (isset($field_merger)) {
      $fields = $field_merger->mergeInto($fields, $format, $context);
    }

    // Add entity and display info as cacheable dependencies after all the
    // fields have been serialized, just to catch all the metadata possibly
    // added to the entity by the field normalizer plugins.
    $this->addCacheableDependency($context, $entity);
    $this->addCacheableDependency($context, $view_info);

    return $fields;
  }

}
