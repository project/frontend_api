<?php

namespace Drupal\frontend_api\Normalizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\serialization\Normalizer\FieldableEntityNormalizerTrait;
use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoBuilderInterface;
use Drupal\frontend_api\Rest\NormalizerContextKeys;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Converts the Drupal entity object structure to a json array structure.
 */
class ContentEntityNormalizer extends NormalizerBase implements NormalizerInterface {

  use FieldableEntityNormalizerTrait;

  /**
   * The view mode key on the serialization context.
   *
   * @todo Update to specify view mode per entity type, not global one.
   */
  protected const VIEW_MODE_CONTEXT = NormalizerContextKeys::VIEW_MODE_CONTEXT;

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = ContentEntityInterface::class;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface|\Symfony\Component\Serializer\Normalizer\DenormalizerInterface|\Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected $serializer;

  /**
   * ViewInfoBuilder service.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoBuilderInterface
   */
  protected $viewInfoBuilder;

  /**
   * The normalizer of the entity formatted in a view mode.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface|\Symfony\Component\Serializer\SerializerAwareInterface
   */
  protected $formattedEntityNormalizer;

  /**
   * The normalizer used in case no view mode specified.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface
   *
   * @todo Switch to a separate wrapper for a single entity and entity list, so
   *   that we don't need it.
   */
  protected $fallbackNormalizer;

  /**
   * A constructor.
   */
  public function __construct(
    DisplayInfoBuilderInterface $view_info_builder,
    NormalizerInterface $formatted_entity_normalizer,
    NormalizerInterface $fallback_normalizer
  ) {
    $this->viewInfoBuilder = $view_info_builder;
    $this->formattedEntityNormalizer = $formatted_entity_normalizer;
    $this->fallbackNormalizer = $fallback_normalizer;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */

    // Delegate to a different normalizer in case a view mode is provided.
    if (isset($context[static::VIEW_MODE_CONTEXT])) {
      // @todo Find a way to set serializer through container.
      $this->formattedEntityNormalizer->setSerializer($this->serializer);
      return $this->formattedEntityNormalizer->normalize(
        $entity,
        $format,
        $context
      );
    }

    return $this->fallbackNormalizer->normalize($entity, $format, $context);
  }

}
