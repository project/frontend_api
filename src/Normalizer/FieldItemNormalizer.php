<?php

namespace Drupal\frontend_api\Normalizer;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\serialization\Normalizer\FieldItemNormalizer as NormalizerBase;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Converts the field item object structure.
 *
 * @todo Make sure there are no use cases for both denormalization and
 *   normalization, then drop.
 */
class FieldItemNormalizer extends NormalizerBase implements NormalizerInterface {

  /**
   * List of formats which supports (de-)normalization.
   *
   * @var string|string[]
   */
  protected $format = ['json'];

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface|\Symfony\Component\Serializer\Normalizer\DenormalizerInterface|\Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected $serializer;

  /**
   * {@inheritdoc}
   */
  public function normalize($field_item, $format = NULL, array $context = []) {
    /** @var \Drupal\Core\Field\FieldItemInterface $field_item */
    $normalized = parent::normalize($field_item, $format, $context);

    $field_definition = $field_item->getFieldDefinition();
    $normalized['label'] = $field_definition->getLabel();

    if ($field_item->getFieldDefinition()->getType() === 'list_string' &&
      !$field_item->isEmpty()) {
      $field_list_options = $field_item->getSettableOptions();
      // Add readable select list field value to normalized array.
      $normalized['readable_value'] = $field_list_options[$field_item->getValue()['value']];
    }

    if ($field_item->getFieldDefinition()->getType() === 'daterange' &&
      !$field_item->isEmpty()) {
      // Add reformatted date values to normalized array.
      $normalized['value'] = $field_item->start_date->format('d/m/Y');
      $normalized['end_value'] = $field_item->start_date->format('d/m/Y');
    }

    return $normalized;
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize(
    $data,
    $class,
    $format = NULL,
    array $context = []
  ) {
    if (isset($data) && !is_array($data)) {
      $data = $this->denormalizePrimitive($data, $context);
    }

    return parent::denormalize($data, $class, $format, $context);
  }

  /**
   * Returns target instance (field item).
   *
   * @param array $context
   *   The context.
   *
   * @return \Drupal\Core\Field\FieldItemInterface
   *   The target field item.
   */
  protected function getTarget(array $context): FieldItemInterface {
    $field_item = $context['target_instance'] ?? NULL;

    if (!$field_item instanceof FieldItemInterface) {
      throw new \InvalidArgumentException(
        '$context["target_instance"] must be set to denormalize with the FieldItemNormalizer'
      );
    }

    return $field_item;
  }

  /**
   * De-normalizes the primitive value into an array with main property key.
   *
   * @param mixed $data
   *   The primitive value.
   * @param array $context
   *   The de-serialization context.
   *
   * @return array
   *   The value wrapped into [main_property => primitive].
   */
  protected function denormalizePrimitive($data, array $context) {
    $storage_definition = $this->getTarget($context)
      ->getFieldDefinition()
      ->getFieldStorageDefinition();
    $main_property = $storage_definition->getMainPropertyName();
    if (!isset($main_property)) {
      return $data;
    }

    return [$main_property => $data];
  }

}
