<?php

namespace Drupal\frontend_api\Normalizer;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\serialization\Normalizer\NormalizerBase;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * The normalizer that flattens single-value field items list.
 *
 * It removes unnecessary wrapping of a single-value fields with an array of a
 * single element.
 *
 * @todo Make sure there are no use cases for both denormalization and
 *   normalization, then drop.
 */
class FieldItemListNormalizer extends NormalizerBase implements NormalizerInterface, DenormalizerInterface {

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = FieldItemListInterface::class;

  /**
   * List of formats which supports (de-)normalization.
   *
   * @var string|string[]
   */
  protected $format = ['json'];

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface|\Symfony\Component\Serializer\Normalizer\DenormalizerInterface|\Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected $serializer;

  /**
   * {@inheritdoc}
   */
  public function normalize($field, $format = NULL, array $context = []): ?array {
    /* @var \Drupal\Core\Field\FieldItemListInterface $field */

    $normalized_items = [];
    if (!$field->isEmpty()) {
      foreach ($field as $field_item) {
        $normalized_items[] = $this->serializer->normalize($field_item, $format, $context);
      }
    }

    $cardinality = $field->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getCardinality();
    if ($cardinality === 1) {
      if (empty($normalized_items)) {
        return NULL;
      }

      return reset($normalized_items);
    }

    return $normalized_items;
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []): FieldItemListInterface {
    if (!isset($context['target_instance'])) {
      throw new InvalidArgumentException(
        '$context["target_instance"] must be set to denormalize with the FieldNormalizer'
      );
    }

    if ($context['target_instance']->getParent() == NULL) {
      throw new InvalidArgumentException(
        'The field passed in via $context["target_instance"] must have a parent set.'
      );
    }

    /** @var \Drupal\Core\Field\FieldItemListInterface $items */
    $items = $context['target_instance'];
    $item_class = $items->getItemDefinition()->getClass();

    $cardinality = $items->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getCardinality();
    if ($cardinality === 1) {
      if (!isset($data)) {
        $data = [];
      }
      else {
        $data = [
          0 => $data,
        ];
      }
    }

    if (!is_array($data)) {
      throw new UnexpectedValueException(sprintf('Field values for "%s" must use an array structure', $items->getName()));
    }

    foreach ($data as $item_data) {
      // Create a new item and pass it as the target for the unserialization of
      // $item_data. All items in field should have removed before this method
      // was called.
      // @see \Drupal\serialization\Normalizer\ContentEntityNormalizer::denormalize().
      $context['target_instance'] = $items->appendItem();
      $this->serializer->denormalize($item_data, $item_class, $format, $context);
    }
    return $items;
  }

}
