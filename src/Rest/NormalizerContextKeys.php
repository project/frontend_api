<?php

namespace Drupal\frontend_api\Rest;

/**
 * Dictionary class for the normalizer context keys.
 */
final class NormalizerContextKeys {

  /**
   * The entity operation.
   *
   * This may be set only in case the form display varies per operation.
   */
  public const ENTITY_OPERATION = 'entity_op';

  /**
   * The entity validation boolean flag.
   *
   * The embedded entities should only be validated in case this context option
   * is set to TRUE.
   */
  public const ENTITY_VALIDATION = 'entity_validation';

  /**
   * The context key of the view mode name.
   */
  public const VIEW_MODE_CONTEXT = 'frontend_api_view_mode';

  /**
   * The context key of the relationship collector.
   */
  public const RELATIONSHIP_COLLECTOR_CONTEXT = 'frontend_api_relationship_collector';

  /**
   * The context key of the field merger.
   */
  public const FIELD_MERGER_CONTEXT = 'frontend_api_field_merger';

  /**
   * The context key of the form info object added.
   */
  public const DISPLAY_INFO_CONTEXT = 'frontend_api_entity_display_info';

  /**
   * The context key of the content language code.
   */
  public const CONTENT_LANGCODE_CONTEXT = 'langcode';

  /**
   * The paragraph revision creation on change boolean flag.
   *
   * Sometimes we need to force to create new paragraph revisions.
   */
  public const PARAGRAPH_REVISION_ON_CHANGE = 'frontend_api_paragraph_revision_on_change';

}
