<?php

namespace Drupal\frontend_api\Rest;

/**
 * Provides wrapper class for an un-serialized data.
 *
 * See interface description for an explanation why the class exists.
 *
 * @see \Drupal\frontend_api\Rest\UnserializedDataInterface
 * @see \Drupal\frontend_api\Normalizer\UnserializedDataDenormalizer
 */
class UnserializedData implements UnserializedDataInterface {

  /**
   * The un-serialized data.
   *
   * @var mixed
   */
  protected $data;

  /**
   * A constructor.
   *
   * @param mixed $data
   *   The un-serialized data.
   */
  public function __construct($data) {
    $this->data = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    return $this->data;
  }

}
