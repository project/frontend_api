<?php

namespace Drupal\frontend_api\Rest\ResourceValidator;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Checks for valid input and throws array with errors if there are any.
 *
 * @todo Rewrite it or integrate into de-normalizer.
 */
class ResourceValidator implements ResourceValidatorInterface {

  use StringTranslationTrait;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(
    ContentEntityInterface $entity,
    array $submitted_fields,
    array $form_fields = NULL
  ) {
    $validated_response = [];
    if (isset($form_fields)) {
      $extra_fields = array_diff($submitted_fields, $form_fields);
      if (!empty($extra_fields)) {
        foreach ($extra_fields as $field) {
          // Set error for field which not exist in product form.
          $validated_response[$field] = $this->t('Field does not exist in form');
        }
      }
    }

    $violations = $entity->validate();
    // Remove violations of inaccessible fields as they cannot stem from our
    // changes.
    $violations->filterByFieldAccess();
    if ($form_fields) {
      // Exclude violations by provided array of field names.
      $violations->filterByFields(array_diff(array_keys(
        $entity->getFieldDefinitions()), $form_fields));
    }

    if ($violations->count() > 0) {
      foreach ($violations as $violation) {
        $property = $violation->getPropertyPath();
        $property_path = explode('.', $property, 3);

        if ($entity->hasField($property_path[0])) {
          $field = $entity->get($property_path[0]);
          $property = $this->normalizePropertyPath(
            $field,
            $property_path
          );
        }
        $validated_response[$property] =
          PlainTextOutput::renderFromHtml($violation->getMessage());
      }
    }

    return $validated_response;
  }

  /**
   * Normalizes field error path to convenient format.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   Entity field.
   * @param array $property_path
   *   Exploded property path.
   *
   * @return string
   *   Normalized field violation path.
   */
  protected function normalizePropertyPath(
    FieldItemListInterface $field,
    array $property_path
  ): string {
    $cardinality = $field->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getCardinality();

    // Check if field is multicolumn. If main property is null,
    // field is compound.
    $main_property = $field->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getMainPropertyName();
    if (isset($main_property)) {
      array_splice($property_path, 2);
    }

    // Adjust property path according to cardinality.
    if ($cardinality === 1) {
      unset($property_path[1]);
    }

    return implode('.', $property_path);
  }

}
