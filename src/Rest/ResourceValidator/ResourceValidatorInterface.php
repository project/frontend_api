<?php

namespace Drupal\frontend_api\Rest\ResourceValidator;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines a common interface for ResourceValidators.
 */
interface ResourceValidatorInterface {

  /**
   * Validate fields and return array with errors if exists.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity for validation.
   * @param array $submitted_fields
   *   Array with submitted fields.
   * @param array $form_fields
   *   Array with fields from entity form mode.
   *   NULL means the list of submitted fields won't be validated.
   *
   * @return array
   *   Return array with not valid properties.
   */
  public function validate(
    ContentEntityInterface $entity,
    array $submitted_fields,
    array $form_fields = NULL
  );

}
