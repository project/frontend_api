<?php

namespace Drupal\frontend_api\Rest\Denormalizer;

/**
 * Provides basic de-normalizer result.
 *
 * This class is only suitable for handling denormalizer errors in some cases,
 * in general you should extend the base class and add the de-normalized data.
 */
class DenormalizerResult extends DenormalizerResultBase {

}
