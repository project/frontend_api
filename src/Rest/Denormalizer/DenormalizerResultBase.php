<?php

namespace Drupal\frontend_api\Rest\Denormalizer;

use Drupal\frontend_api\Exception\DenormalizationException;
use Drupal\frontend_api\ErrorListTrait;

/**
 * Provides base class for a de-normalizer result.
 */
abstract class DenormalizerResultBase implements DenormalizerResultInterface {

  use ErrorListTrait;

  /**
   * {@inheritdoc}
   */
  public function throwErrors(string $path_prefix = NULL): void {
    if (!$this->hasErrors()) {
      return;
    }

    if (isset($path_prefix)) {
      $this->addPrefixToErrorPaths($path_prefix);
    }

    throw new DenormalizationException($this->getErrors());
  }

}
