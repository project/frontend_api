<?php

namespace Drupal\frontend_api\Rest\Denormalizer;

use Drupal\frontend_api\ErrorListInterface;

/**
 * Interface of a de-normalization result.
 */
interface DenormalizerResultInterface extends ErrorListInterface {

  /**
   * Throws a de-normalization exception with the errors, if any.
   *
   * @param string|null $path_prefix
   *   The optional path prefix to add to the errors before the exception is
   *   thrown.
   *
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  public function throwErrors(string $path_prefix = NULL): void;

}
