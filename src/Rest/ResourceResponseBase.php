<?php

namespace Drupal\frontend_api\Rest;

use Symfony\Component\HttpFoundation\Response;

/**
 * Base class for ResourceResponse classes.
 */
abstract class ResourceResponseBase extends Response implements ResourceResponseInterface {

  /**
   * Response data that should be serialized.
   *
   * @var mixed
   */
  protected $responseData;

  /**
   * Response context.
   *
   * @var mixed
   */
  protected $context = [];

  /**
   * Constructor for ResourceResponse objects.
   *
   * @param mixed $data
   *   Response data that should be serialized.
   * @param int $status
   *   The response status code.
   * @param array $headers
   *   An array of response headers.
   */
  public function __construct($data = NULL, $status = 200, array $headers = []) {
    $this->responseData = $data;
    parent::__construct('', $status, $headers);
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseData() {
    return $this->responseData;
  }

  /**
   * {@inheritdoc}
   */
  public function addResponseData(array $data) {
    if (!isset($this->responseData)) {
      $this->responseData = [];
    }
    $this->responseData += $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    return $this->context;
  }

  /**
   * {@inheritdoc}
   */
  public function setContext(array $context) {
    $this->context = $context;
  }

}
