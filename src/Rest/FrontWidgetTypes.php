<?php

namespace Drupal\frontend_api\Rest;

/**
 * Dictionary class for all the widget types of the front application.
 */
final class FrontWidgetTypes {

  /**
   * The select front widget name.
   */
  public const SELECT = 'select';

  /**
   * The searchable select.
   */
  public const SEARCHABLE_SELECT = 'searchable_select';

  /**
   * The hierarchical select front widget name.
   */
  public const HIERARCHICAL_SELECT = 'hierarchical_select';

  /**
   * The non editable field front widget name.
   */
  public const NON_EDITABLE = 'non_editable';

  /**
   * The non editable dimensions front widget name.
   */
  public const NON_EDITABLE_DIMENSIONS = 'non_editable_dimensions';

  /**
   * The quota list.
   */
  public const SELECTION_CARD = 'selection_card';

  /**
   * The basic autocomplete input field with plain list of suggestions.
   */
  public const AUTOCOMPLETE = 'autocomplete';

  /**
   * The autocomplete with quantity front widget name.
   */
  public const AUTOCOMPLETE_WITH_QUANTITY = 'autocomplete_with_quantity';

  /**
   * The billing entity autocomplete front widget name.
   */
  public const BILLING_ENTITY_AUTOCOMPLETE = 'billing_entity_autocomplete';

  /**
   * The daterange front widget name.
   */
  public const DATE = 'date';

  /**
   * The daterange front widget name.
   */
  public const DATERANGE = 'daterange';

  /**
   * The daterange front widget name.
   */
  public const DIMENSIONS = 'dimensions';

  /**
   * The link widget.
   */
  public const LINK = 'link';

  /**
   * The html editor front widget.
   */
  public const HTML_EDITOR = 'html_editor';

  /**
   * The inline form front widget.
   */
  public const INLINE_FORM = 'inline_form';

  /**
   * The inline form front widget.
   */
  public const POPIN_MEDIA = 'popin_media';

  /**
   * The address front widget name.
   */
  public const ADDRESS = 'address';

  /**
   * The address front widget name.
   */
  public const PRICE = 'price';

  /**
   * The input front widget name.
   */
  public const INPUT = 'input';

  /**
   * The address front widget name.
   */
  public const NUMBER = 'number';

  /**
   * The address front widget name.
   */
  public const EMAIL = 'email';

  /**
   * The text front widget name.
   */
  public const TEXT = 'text';

  /**
   * The text front widget name.
   */
  public const TEXTAREA = 'textarea';

  /**
   * The taxonomy categories front widget name.
   */
  public const TAXONOMY_CATEGORIES = 'taxonomy_categories';

  /**
   * The image front widget name.
   */
  public const IMAGE = 'image';

  /**
   * The checkbox front widget name.
   */
  public const CHECKBOX = 'checkbox';

  /**
   * The raw html front widget name.
   */
  public const RAW_HTML = 'raw_html';

  /**
   * The stock front widget name.
   */
  public const STOCK = 'stock';

  /**
   * The weight front widget name.
   */
  public const WEIGHT = 'weight';

  /**
   * The paragraph widget type name.
   */
  public const PARAGRAPH = 'paragraph';

  /**
   * The scheduled field widget name.
   */
  public const SCHEDULED_FIELD = 'scheduled_field';

  /**
   * The draggable table widget name.
   */
  public const DRAGGABLE_TABLE = 'draggable_table';

  /**
   * Generic file upload widget.
   */
  public const FILE_UPLOAD = 'file_upload';

}
