<?php

namespace Drupal\frontend_api\Rest\SearchApi\Autocomplete;

/**
 * Builds default suggestion list.
 */
class DefaultSuggestionListBuilder extends SuggestionListBuilderBase {

  /**
   * {@inheritdoc}
   */
  public const SUGGESTION_LIST_CLASS = DefaultSuggestionList::class;

}
