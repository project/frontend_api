<?php

namespace Drupal\frontend_api\Rest\SearchApi\Autocomplete;

use Drupal\search_api_autocomplete\SearchInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides an interface for a Search API autocomplete suggestion list builder.
 *
 * It executes the Search API query using configured suggesters and returns
 * suggestion list.
 */
interface SuggestionListBuilderInterface {

  /**
   * Builds suggestion list for the passed search entity and the request.
   *
   * @param \Drupal\search_api_autocomplete\SearchInterface $search
   *   The Search API Autocomplete search entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request to extract the query variables from.
   *
   * @return \Drupal\frontend_api\Rest\SearchApi\Autocomplete\SuggestionListInterface
   *   The suggestion list.
   */
  public function buildSuggestionList(
    SearchInterface $search,
    Request $request
  ): SuggestionListInterface;

}
