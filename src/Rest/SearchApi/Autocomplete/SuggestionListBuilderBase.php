<?php

namespace Drupal\frontend_api\Rest\SearchApi\Autocomplete;

use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\SearchApiException;
use Drupal\search_api_autocomplete\SearchApiAutocompleteException;
use Drupal\search_api_autocomplete\SearchInterface;
use Drupal\search_api_autocomplete\Utility\AutocompleteHelperInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Base class for autocomplete suggestion list builders of the Search API.
 *
 * It's a refactored version of the autocomplete controller from the Search API
 * Autocomplete module.
 *
 * @see \Drupal\frontend_api\Rest\SearchApi\Autocomplete\SuggestionListBuilderInterface
 * @see \Drupal\search_api_autocomplete\Controller\AutocompleteController::autocomplete()
 */
abstract class SuggestionListBuilderBase implements SuggestionListBuilderInterface {

  /**
   * The query variable name of the search keywords submitted.
   */
  public const KEYWORDS_QUERY_VAR = 'q';

  /**
   * The suggestion list class.
   *
   * It must be overridden with the actual class name.
   */
  protected const SUGGESTION_LIST_CLASS = NULL;

  /**
   * The autocomplete helper service.
   *
   * @var \Drupal\search_api_autocomplete\Utility\AutocompleteHelperInterface
   */
  protected $autocompleteHelper;

  /**
   * The transliterator.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliterator;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * A constructor.
   *
   * @param \Drupal\search_api_autocomplete\Utility\AutocompleteHelperInterface $autocomplete_helper
   *   The autocomplete helper service.
   * @param \Drupal\Component\Transliteration\TransliterationInterface $transliterator
   *   The transliterator.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(
    AutocompleteHelperInterface $autocomplete_helper,
    TransliterationInterface $transliterator,
    ModuleHandlerInterface $module_handler,
    LanguageManagerInterface $language_manager
  ) {
    $this->autocompleteHelper = $autocomplete_helper;
    $this->transliterator = $transliterator;
    $this->moduleHandler = $module_handler;
    $this->languageManager = $language_manager;
  }

  /**
   * Builds and returns the list of suggesters sorted by weight.
   *
   * @param \Drupal\search_api_autocomplete\SearchInterface $search
   *   The search entity.
   *
   * @return \Drupal\search_api_autocomplete\Suggester\SuggesterInterface[]
   *   The suggesters keyed by ID and sorted by weight.
   */
  protected function buildSuggestersList(SearchInterface $search) {
    // Get all enabled suggesters, ordered by weight.
    $suggesters = $search->getSuggesters();
    $suggester_weights = $search->getSuggesterWeights();
    $suggester_weights = array_intersect_key($suggester_weights, $suggesters);
    $suggester_weights += array_fill_keys(array_keys($suggesters), 0);
    asort($suggester_weights);

    $result = [];
    foreach (array_keys($suggester_weights) as $suggester_id) {
      $result[$suggester_id] = $suggesters[$suggester_id];
    }
    return $result;
  }

  /**
   * Parses search keywords.
   *
   * @param \Drupal\search_api_autocomplete\SearchInterface $search
   *   The search entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request to extract the query variables from.
   *
   * @return string[]
   *   The numerically indexed array with the following items:
   *   - complete keywords,
   *   - incomplete keywords,
   *   - full source keywords.
   */
  protected function splitKeywords(SearchInterface $search, Request $request) {
    $keys = $request->query->get(static::KEYWORDS_QUERY_VAR);

    // If the "Transliteration" processor is enabled for the search index, we
    // also need to transliterate the user input for autocompletion.
    if ($search->getIndex()->isValidProcessor('transliteration')) {
      $langcode = $this->languageManager
        ->getCurrentLanguage()
        ->getId();
      $keys = $this->transliterator->transliterate($keys, $langcode);
    }

    $result = $this->autocompleteHelper->splitKeys($keys);
    $result[] = $keys;
    return $result;
  }

  /**
   * Builds and returns the base Search API query.
   *
   * @param \Drupal\search_api_autocomplete\SearchInterface $search
   *   The search entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request to extract the query variables from.
   * @param string $complete
   *   The complete keywords.
   *
   * @return \Drupal\search_api\Query\QueryInterface|null
   *   The query or NULL if it can't be created.
   */
  protected function buildBaseQuery(
    SearchInterface $search,
    Request $request,
    string $complete
  ): ?QueryInterface {
    $data = $request->query->all();
    unset($data[static::KEYWORDS_QUERY_VAR]);

    $query = $search->createQuery($complete, $data);
    if (!$query) {
      return NULL;
    }

    // Prepare the query.
    $query->setSearchId('search_api_autocomplete:' . $search->id());
    $query->addTag('search_api_autocomplete');
    $query->preExecute();

    return $query;
  }

  /**
   * Validates length of the search keywords.
   *
   * @param \Drupal\search_api_autocomplete\SearchInterface $search
   *   The search entity.
   * @param string $keys
   *   The keywords to validate.
   *
   * @return bool
   *   TRUE if keywords has length valid for the search.
   */
  protected function isKeysLengthValid(
    SearchInterface $search,
    string $keys
  ): bool {
    $min_length = $search->getOption('min_length');
    $keys_length = mb_strlen($keys);
    if ($keys_length < $min_length) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Builds and returns a suggestion array.
   *
   * @param \Drupal\search_api_autocomplete\SearchInterface $search
   *   The search entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request to extract the query variables from.
   *
   * @return \Drupal\search_api_autocomplete\Suggestion\Suggestion[]
   *   The suggestions.
   */
  protected function buildSuggestions(
    SearchInterface $search,
    Request $request
  ): array {
    if (!$search->status() || !$search->hasValidIndex()) {
      return [];
    }

    list($complete, $incomplete, $keys) = $this
      ->splitKeywords($search, $request);

    // Validate keys length.
    if (!$this->isKeysLengthValid($search, $keys)) {
      return [];
    }

    $query = $this->buildBaseQuery($search, $request, $complete);
    if (!$query) {
      return [];
    }

    // Get total limit and per-suggester limits.
    $limit = $search->getOption('limit');
    $suggester_limits = $search->getSuggesterLimits();
    $suggesters = $this->buildSuggestersList($search);

    /** @var \Drupal\search_api_autocomplete\Suggestion\SuggestionInterface[] $suggestions */
    $suggestions = [];
    // Go through all enabled suggesters in order of increasing weight and
    // add their suggestions (until the limit is reached).
    foreach ($suggesters as $suggester_id => $suggester) {
      // Clone the query in case the suggester makes any modifications.
      $tmp_query = clone $query;

      // Compute the applicable limit based on (remaining) total limit and
      // the suggester-specific limit (if set).
      if (isset($suggester_limits[$suggester_id])) {
        $suggester_limit = min($limit, $suggester_limits[$suggester_id]);
      }
      else {
        $suggester_limit = $limit;
      }
      $tmp_query->range(0, $suggester_limit);

      $new_suggestions = $suggester
        ->getAutocompleteSuggestions($tmp_query, $incomplete, $keys);
      foreach ($new_suggestions as $suggestion) {
        $suggestions[] = $suggestion;

        if (--$limit == 0) {
          // If we've reached the limit, stop adding suggestions.
          break 2;
        }
      }
    }

    // Allow other modules to alter the created suggestions.
    $alter_params = [
      'query' => $query,
      'search' => $search,
      'incomplete_key' => $incomplete,
      'user_input' => $keys,
    ];
    $this->moduleHandler->alter(
      'search_api_autocomplete_suggestions',
      $suggestions,
      $alter_params
    );

    return $suggestions;
  }

  /**
   * {@inheritdoc}
   */
  public function buildSuggestionList(
    SearchInterface $search,
    Request $request
  ): SuggestionListInterface {
    $suggestions = [];

    try {
      $suggestions = $this->buildSuggestions($search, $request);
    }
    catch (SearchApiAutocompleteException | SearchApiException $e) {
      watchdog_exception(
        'frontend_api',
        $e,
        '%type while retrieving autocomplete suggestions: @message in %function (line %line of %file).'
      );
    }

    $class_name = static::SUGGESTION_LIST_CLASS;
    return new $class_name($search, $suggestions);
  }

}
