<?php

namespace Drupal\frontend_api\Rest\SearchApi\Autocomplete;

/**
 * Represents basic entity suggestion list got from Search API autocomplete.
 *
 * It gets normalized info a format expected by basic entity autocomplete
 * widget.
 *
 * @see \Drupal\frontend_api\Normalizer\SearchApi\Autocomplete\BasicEntitySuggestionListNormalizer
 */
class BasicEntitySuggestionList extends SuggestionListBase {

}
