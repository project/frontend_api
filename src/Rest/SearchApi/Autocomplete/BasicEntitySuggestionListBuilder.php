<?php

namespace Drupal\frontend_api\Rest\SearchApi\Autocomplete;

/**
 * Builds basic entity suggestion list.
 */
class BasicEntitySuggestionListBuilder extends SuggestionListBuilderBase {

  /**
   * {@inheritdoc}
   */
  public const SUGGESTION_LIST_CLASS = BasicEntitySuggestionList::class;

}
