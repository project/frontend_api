<?php

namespace Drupal\frontend_api\Rest\SearchApi\Autocomplete;

/**
 * Represents an autocomplete suggestions list of the Search API.
 *
 * It gets normalized into extended list with entities and groups.
 *
 * @see \Drupal\frontend_api\Normalizer\SearchApi\Autocomplete\DefaultSuggestionListNormalizer
 */
class DefaultSuggestionList extends SuggestionListBase {

}
