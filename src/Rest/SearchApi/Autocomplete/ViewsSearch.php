<?php

namespace Drupal\frontend_api\Rest\SearchApi\Autocomplete;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\search_api\Plugin\views\query\SearchApiQuery;
use Drupal\search_api_autocomplete\Plugin\search_api_autocomplete\search\Views;
use Drupal\search_api_autocomplete\SearchApiAutocompleteException;
use Drupal\views\ViewEntityInterface;

/**
 * Provides extended Search API autocomplete support for Views search.
 *
 * The changes applied to the base class:
 * - support for submitting exposed input in order to narrow the list of
 *   suggestions with already selected filters,
 * - added options parameter for relevance sorting instead of view sort,
 * - basic refactoring.
 */
class ViewsSearch extends Views {

  use StringTranslationTrait;

  /**
   * Sort parameter in query configuration.
   */
  public const RELEVANCE_SORT = 'relevance_sort';

  /**
   * Sort order parameter in query configuration.
   */
  public const RELEVANCE_SORT_DIRECTION = 'relevance_sort_direction';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();

    // @TODO We need to find proper way to update config schema for
    // plugin.plugin_configuration.search_api_autocomplete_search.views:*
    // Ideal solution to create own plugin type and make extended version of
    // schema.
    $config['query'] = [
      static::RELEVANCE_SORT => FALSE,
      static::RELEVANCE_SORT_DIRECTION => 'ASC',
    ];

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $relevance_field = static::RELEVANCE_SORT;
    $direction_field = static::RELEVANCE_SORT_DIRECTION;
    $query_configuration = $this->configuration['query'];

    $form['query'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Query options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    $form['query'][$relevance_field] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Sort results by relevance'),
      '#description' => $this->t('Removes view default sort and add sort by relevance'),
      '#default_value' => $query_configuration[$relevance_field],
    ];

    $states_name = "search_settings[query][$relevance_field]";

    $form['query'][$direction_field] = [
      '#type' => 'radios',
      '#title' => $this->t('Order'),
      '#description' => $this->t('The ordering to use for the sort criteria selected above.'),
      '#options' => [
        'ASC' => $this->t('Ascending'),
        'DESC' => $this->t('Descending'),
      ],
      '#default_value' => $query_configuration[$direction_field],
      '#states' => [
        'visible' => [
          ':input[name="' . $states_name . '"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function createQuery($keys, array $data = []) {
    $view_id = $this->getDerivativeId();
    $view = $this->getEntityTypeManager()
      ->getStorage('view')
      ->load($view_id);
    if ($view instanceof ViewEntityInterface) {
      $view = $this->getViewsExecutableFactory()
        ->get($view);
    }
    if (!$view) {
      throw new SearchApiAutocompleteException(sprintf(
        "Could not load the view '%s'.",
          $view_id
      ));
    }

    $query_configuration = $this->configuration['query'];

    $data += [
      'display' => NULL,
      'arguments' => [],
      'exposed' => [],
    ];

    $view->setDisplay($data['display']);
    $view->setArguments($data['arguments']);

    $exposed_input = $data['exposed'];

    // Set the keys via the exposed input, to get the correct handling for the
    // filter in question.
    $single_field_filter = !empty($data['field']);
    if (!empty($data['filter'])) {
      $input = $keys;
      // The Views filter for individual fulltext fields uses a nested "value"
      // field for the real input, due to Views internals.
      if ($single_field_filter) {
        $input = ['value' => $keys];
      }
      $exposed_input[$data['filter']] = $input;
    }

    if (!empty($exposed_input)) {
      $view->setExposedInput($exposed_input);
    }

    $view->preExecute();

    // Since we only have a single value in the exposed input, any exposed
    // filters set to "Required" might cause problems – especially "Search:
    // Fulltext search", which aborts the query when validation fails (instead
    // of relying on the Form API "#required" validation). The normal filters
    // which use the Form API actually don't seem to cause problems, but it's
    // still better to be on the safe side here and just disabled "Required" for
    // all filters. (It also makes the code simpler.)
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    foreach ($view->display_handler->getHandlers('filter') as $filter) {
      $filter->options['expose']['required'] = FALSE;
    }

    $view->build();

    $query_plugin = $view->getQuery();
    if (!($query_plugin instanceof SearchApiQuery)) {
      $view_label = $view->storage->label() ?: $view_id;
      throw new SearchApiAutocompleteException(sprintf(
        "Could not create search query for view '%s': view is not based on Search API.",
          $view_label
      ));
    }
    $query = $query_plugin->getSearchApiQuery();
    if (!$query) {
      $view_label = $view->storage->label() ?: $view_id;
      throw new SearchApiAutocompleteException(sprintf(
        "Could not create search query for view '%s'.",
          $view_label
      ));
    }

    if ($single_field_filter) {
      $query->setFulltextFields([$data['field']]);
    }

    // Adjust query according our selected options.
    if ($query_configuration[static::RELEVANCE_SORT]) {
      $sorts =& $query->getSorts();
      $sorts = [];

      $query->sort(
        'search_api_relevance',
        $query_configuration[static::RELEVANCE_SORT_DIRECTION]
      );
    }

    return $query;
  }

}
