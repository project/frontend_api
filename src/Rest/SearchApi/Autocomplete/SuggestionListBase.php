<?php

namespace Drupal\frontend_api\Rest\SearchApi\Autocomplete;

use Drupal\search_api_autocomplete\SearchInterface;

/**
 * Provides base class for Search API autocomplete suggestion lists.
 */
abstract class SuggestionListBase implements SuggestionListInterface {

  /**
   * The Search API Autocomplete search entity.
   *
   * @var \Drupal\search_api_autocomplete\SearchInterface
   */
  protected $search;

  /**
   * The suggestion list.
   *
   * @var \Drupal\search_api_autocomplete\Suggestion\Suggestion[]
   */
  protected $suggestions;

  /**
   * A constructor.
   *
   * @param \Drupal\search_api_autocomplete\SearchInterface $search
   *   The Search API Autocomplete search entity.
   * @param \Drupal\search_api_autocomplete\Suggestion\Suggestion[] $suggestions
   *   The suggestion list.
   */
  public function __construct(
    SearchInterface $search,
    array $suggestions
  ) {
    $this->search = $search;
    $this->suggestions = $suggestions;
  }

  /**
   * {@inheritdoc}
   */
  public function getSuggestions(): array {
    return $this->suggestions;
  }

  /**
   * {@inheritdoc}
   */
  public function getSearch(): SearchInterface {
    return $this->search;
  }

}
