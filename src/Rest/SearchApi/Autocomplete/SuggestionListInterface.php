<?php

namespace Drupal\frontend_api\Rest\SearchApi\Autocomplete;

use Drupal\search_api_autocomplete\SearchInterface;

/**
 * Provides an interface for the Search API autocomplete suggestion list.
 */
interface SuggestionListInterface {

  /**
   * Returns suggestion array.
   *
   * @return \Drupal\search_api_autocomplete\Suggestion\Suggestion[]
   *   The suggestions.
   */
  public function getSuggestions(): array;

  /**
   * Returns the search entity the list was built for.
   *
   * @return \Drupal\search_api_autocomplete\SearchInterface
   *   The Search API Autocomplete search entity.
   */
  public function getSearch(): SearchInterface;

}
