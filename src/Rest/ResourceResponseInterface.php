<?php

namespace Drupal\frontend_api\Rest;

/**
 * Defines a common interface for resource responses.
 */
interface ResourceResponseInterface {

  /**
   * Returns response data that should be serialized.
   *
   * @return mixed
   *   Response data that should be serialized.
   */
  public function getResponseData();

  /**
   * Add additional data to existing response data.
   *
   * @param array $data
   *   Data which need to add to existing response data.
   */
  public function addResponseData(array $data);

  /**
   * Set context array (view_mode etc..) to ResourceResponse.
   */
  public function setContext(array $context);

  /**
   * Get context array ResourceResponse.
   */
  public function getContext();

}
