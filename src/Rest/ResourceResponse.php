<?php

namespace Drupal\frontend_api\Rest;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyTrait;

/**
 * Contains data for serialization with context argument.
 *
 * @see \Drupal\rest\ModifiedResourceResponse
 */
class ResourceResponse extends ResourceResponseBase implements CacheableResponseInterface, RefinableCacheableDependencyInterface {

  use RefinableCacheableDependencyTrait;

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return (new CacheableMetadata())
      ->addCacheContexts($this->cacheContexts)
      ->addCacheTags($this->cacheTags)
      ->setCacheMaxAge($this->cacheMaxAge);
  }

}
