<?php

namespace Drupal\frontend_api\Rest\Media;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\file\FileInterface;
use Drupal\image\ImageStyleInterface;
use Drupal\frontend_api\Dictionary\EntityTypes;

/**
 * Provides an image stylist that builds styled images.
 *
 * @see \Drupal\frontend_api\Rest\Media\ImageStylistInterface
 * @see template_preprocess_image_formatter()
 * @see template_preprocess_image_style()
 */
class ImageStylist implements ImageStylistInterface {

  /**
   * The entity type ID of an image style.
   */
  protected const STYLE_ENTITY_TYPE_ID = EntityTypes::IMAGE_STYLE;

  /**
   * The list of field item properties that are exposed as attributes.
   */
  public const ATTRIBUTE_PROPERTIES = [
    'alt',
    'title',
  ];

  /**
   * Query key of the changed timestamp in the image URL.
   */
  public const CHANGED_QUERY_KEY = 'changed';

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    LoggerChannelInterface $logger,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->logger = $logger;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Returns URI of the original image from an image field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The image field item.
   *
   * @return string
   *   The URI.
   *
   * @see template_preprocess_image_formatter()
   */
  protected function getOriginalUri(
    FieldItemInterface $item
  ): string {
    if (!empty($item->uri)) {
      return $item->uri;
    }

    $file = $this->getFile($item);
    return $file->getFileUri();
  }

  /**
   * Returns an image URL with the specified style applied, if possible.
   *
   * @param string $original_uri
   *   The original image URI.
   * @param \Drupal\image\ImageStyleInterface|null $style
   *   The image style or NULL to use the original image URL.
   *
   * @return string
   *   The absolute image URL with style applied.
   *
   * @see template_preprocess_image_style()
   */
  protected function getStyledUrl(
    string $original_uri,
    ImageStyleInterface $style = NULL
  ): string {
    if (!isset($style)) {
      return file_create_url($original_uri);
    }

    // If the current image toolkit supports this file type, prepare the URI for
    // the derivative image. If not, just use the original image resized to the
    // dimensions specified by the style.
    if ($style->supportsUri($original_uri)) {
      return $style->buildUrl($original_uri);
    }
    else {
      $this->logger->warning(
        'Could not apply @style image style to @uri because the style does not support it.',
        [
          '@style' => $style->label(),
          '@uri' => $original_uri,
        ]
      );

      return file_create_url($original_uri);
    }
  }

  /**
   * Applies options to the styled image URL.
   *
   * Adds different query parameters to the URL according to options.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The field item.
   * @param string $styled_url
   *   The styled image URL.
   * @param array $options
   *   The options to apply.
   *
   * @return string
   *   The updated URL of the styled image.
   */
  protected function applyOptionsToStyledUrl(
    FieldItemInterface $item,
    string $styled_url,
    array $options
  ): string {
    $query_addon = $options[static::QUERY_OPTION] ?? [];

    $options += [
      static::CHANGED_OPTION => TRUE,
    ];
    if (!empty($options[static::CHANGED_OPTION])) {
      $file = $this->getFile($item);
      $query_addon[static::CHANGED_QUERY_KEY] = $file->getChangedTime();
    }

    return $this->applyQueryAddonToUrl($styled_url, $query_addon);
  }

  /**
   * Adds query parameters to the URL.
   *
   * Parameter is only added if there is no such parameter in the URL already.
   *
   * @param string $url
   *   The URL.
   * @param array $query_addon
   *   Query parameters to add.
   *
   * @return string
   *   The updated URL.
   */
  protected function applyQueryAddonToUrl(
    string $url,
    array $query_addon
  ): string {
    if (empty($query_addon)) {
      return $url;
    }

    $parsed_url = UrlHelper::parse($url);
    $parsed_url['query'] += $query_addon;

    $result_url = $parsed_url['path'];

    $query = http_build_query($parsed_url['query'], '', '&');
    $result_url .= '?' . $query;

    $fragment = $parsed_url['fragment'];
    if ($fragment) {
      $result_url .= '#' . $fragment;
    }

    return $result_url;
  }

  /**
   * Returns image dimensions with an image style applied.
   *
   * @param string $original_uri
   *   The original image URI.
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The image field item.
   * @param \Drupal\image\ImageStyleInterface|null $style
   *   The image style to apply to dimensions.
   *
   * @return array
   *   The image dimensions.
   *
   * @see template_preprocess_image_style()
   */
  protected function getStyledDimensions(
    string $original_uri,
    FieldItemInterface $item,
    ImageStyleInterface $style = NULL
  ): array {
    $dimensions = [
      'width' => $item->width,
      'height' => $item->height,
    ];

    if (isset($style)) {
      $style->transformDimensions($dimensions, $original_uri);
    }

    return $dimensions;
  }

  /**
   * Returns image attributes (without dimensions).
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The image field item.
   *
   * @return array
   *   The attributes.
   */
  protected function getImageAttributes(FieldItemInterface $item): array {
    $attributes = [];

    foreach (static::ATTRIBUTE_PROPERTIES as $property) {
      $value = $item->{$property};
      if (mb_strlen($value) === 0) {
        continue;
      }

      $attributes[$property] = $value;
    }

    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function getImageStyle(?string $style_name): ?ImageStyleInterface {
    if (!isset($style_name)) {
      return NULL;
    }

    $styles = $this->getImageStyles([
      'default' => $style_name,
    ]);
    return $styles['default'];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getImageStyles(array $style_names): array {
    $styles = [];
    $ids = array_values(array_filter($style_names));
    if (!empty($ids)) {
      $styles = $this->entityTypeManager
        ->getStorage(static::STYLE_ENTITY_TYPE_ID)
        ->loadMultiple($ids);
    }

    $result = [];
    foreach ($style_names as $key => $style_name) {
      $style = NULL;

      if ($style_name !== NULL) {
        $style = $styles[$style_name] ?? NULL;
        if ($style === NULL) {
          throw new \LogicException(sprintf(
            'Image style %s not found.',
            $style_name
          ));
        }
      }

      $result[$key] = $style;
    }
    return $result;
  }

  /**
   * Adds cacheable dependencies to the styled image.
   *
   * @param \Drupal\frontend_api\Rest\Media\StyledImageInterface $styled_image
   *   The styled image to apply metadata to.
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The image field item.
   * @param \Drupal\image\ImageStyleInterface|null $style
   *   The image style or NULL.
   */
  protected function addCacheableDependenciesToStyledImage(
    StyledImageInterface $styled_image,
    FieldItemInterface $item,
    ImageStyleInterface $style = NULL
  ): void {
    // Take care about the cacheability metadata.
    if (isset($style)) {
      $styled_image->addCacheableDependency($style);
    }

    $file = $item->entity;
    if ($file instanceof FileInterface) {
      $styled_image->addCacheableDependency($file);
    }
  }

  /**
   * Return file entity from an image field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The field Item.
   *
   * @return \Drupal\file\FileInterface
   *   The file entity.
   */
  protected function getFile(
    FieldItemInterface $item
  ): FileInterface {
    $file = $item->entity;
    if (!$file instanceof FileInterface) {
      throw new \InvalidArgumentException(
        'The referenced entity is not a file.'
      );
    }

    return $file;
  }

  /**
   * {@inheritdoc}
   */
  public function buildStyledImage(
    FieldItemInterface $item,
    ImageStyleInterface $style = NULL,
    array $options = []
  ): StyledImageInterface {
    $original_uri = $this->getOriginalUri($item);
    $styled_url = $this->getStyledUrl($original_uri, $style);
    $styled_url = $this->applyOptionsToStyledUrl($item, $styled_url, $options);

    $dimensions = $this->getStyledDimensions($original_uri, $item, $style);
    $attributes = $dimensions + $this->getImageAttributes($item);

    $file = $this->getFile($item);

    $result = new StyledImage($styled_url, $file->id(), $attributes);
    $this->addCacheableDependenciesToStyledImage($result, $item, $style);
    return $result;
  }

}
