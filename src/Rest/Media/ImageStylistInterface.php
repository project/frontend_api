<?php

namespace Drupal\frontend_api\Rest\Media;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\image\ImageStyleInterface;

/**
 * Provides an interface for an image stylist.
 *
 * It is responsible for producing a styled image from an image field item.
 *
 * @see \Drupal\frontend_api\Rest\Media\StyledImageInterface
 */
interface ImageStylistInterface {

  /**
   * The query option key.
   *
   * Allows to include additional GET query parameters in the styled URL.
   */
  public const QUERY_OPTION = 'query';

  /**
   * Changed option key.
   *
   * It's a boolean option that tells the stylist whether the changed timestamp
   * of the file should be added to the styled image URL or not.
   *
   * Defaults to TRUE.
   */
  public const CHANGED_OPTION = 'changed';

  /**
   * Builds styled image from an image field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The image field item.
   * @param \Drupal\image\ImageStyleInterface|null $style
   *   The image style to apply, or NULL to build styled image from original
   *   image.
   * @param array $options
   *   The options. See constants on the stylist interface for available
   *   options.
   *
   * @return \Drupal\frontend_api\Rest\Media\StyledImageInterface
   *   The styled image.
   */
  public function buildStyledImage(
    FieldItemInterface $item,
    ImageStyleInterface $style = NULL,
    array $options = []
  ): StyledImageInterface;

  /**
   * Returns image style with a specified name.
   *
   * Throws a logic exception if the style is not found.
   *
   * @param string|null $style_name
   *   The style name or NULL to return NULL.
   *
   * @return \Drupal\image\ImageStyleInterface|null
   *   The image style entity or NULL.
   *
   * @throws \LogicException
   */
  public function getImageStyle(?string $style_name): ?ImageStyleInterface;

  /**
   * Returns image styles with specified names.
   *
   * It loads all the image styles except NULL values and returns them under the
   * same key. Missing style triggers a logic exception.
   *
   * @param array $style_names
   *   List of style names to return. Keys of this array are preserved in result
   *   array. NULL values from this array are also returned as NULL.
   *
   * @return \Drupal\image\ImageStyleInterface[]|null[]
   *   The loaded image styles. The NULL style name is returned as NULL here.
   *
   * @throws \LogicException
   */
  public function getImageStyles(array $style_names): array;

}
