<?php

namespace Drupal\frontend_api\Rest\Media;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;

/**
 * Provides an interface for an image with an image style applied.
 *
 * It's an intermediate representation between the image field item and a
 * rendered image with a style applied. It may also represent an original image
 * in case no image style has been selected for rendering.
 */
interface StyledImageInterface extends RefinableCacheableDependencyInterface {

  /**
   * Returns image attributes.
   *
   * @return array
   *   The attributes.
   */
  public function getAttributes(): array;

  /**
   * Returns absolute image URL.
   *
   * @return string
   *   The absolute URL.
   */
  public function getUrl(): string;

  /**
   * Return file id.
   *
   * @return string
   *   The file id.
   */
  public function getFileId(): string;

}
