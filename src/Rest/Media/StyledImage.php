<?php

namespace Drupal\frontend_api\Rest\Media;

use Drupal\Core\Cache\RefinableCacheableDependencyTrait;

/**
 * Provides an interface for an image with an image style applied.
 *
 * @see \Drupal\frontend_api\Rest\Media\StyledImageInterface
 */
class StyledImage implements StyledImageInterface {

  use RefinableCacheableDependencyTrait;

  /**
   * The absolute image URL.
   *
   * @var string
   */
  protected $url;

  /**
   * The image attributes.
   *
   * @var array
   */
  protected $attributes = [];

  /**
   * The file id.
   *
   * @var string
   */
  protected $fileId;

  /**
   * A constructor.
   *
   * @param string $url
   *   The absolute image URL.
   * @param string $file_id
   *   The file id.
   * @param array $attributes
   *   The image attributes.
   */
  public function __construct(string $url, $file_id, array $attributes = []) {
    $this->url = $url;
    $this->attributes = $attributes + ['alt' => ''];
    $this->fileId = $file_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl(): string {
    return $this->url;
  }

  /**
   * {@inheritdoc}
   */
  public function getAttributes(): array {
    return $this->attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileId(): string {
    return $this->fileId;
  }

}
