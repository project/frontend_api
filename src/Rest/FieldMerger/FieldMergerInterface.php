<?php

namespace Drupal\frontend_api\Rest\FieldMerger;

/**
 * Provides an interface for a field merger.
 *
 * Its purpose is to collect the entity IDs and then merge their fields into
 * the passed array of parent fields.
 */
interface FieldMergerInterface {

  /**
   * Adds an entity to the merger.
   *
   * Fields of the entity will be displayed in a passed view mode and added to
   * the parent entity fields with a specified prefix.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param int $entity_id
   *   The entity ID.
   * @param string $view_mode
   *   The view mode to use for fields formatting.
   * @param string $prefix
   *   The field name prefix to add when merging.
   *
   * @return static
   *   Self.
   */
  public function addEntity(
    string $entity_type_id,
    int $entity_id,
    string $view_mode,
    string $prefix
  ): FieldMergerInterface;

  /**
   * Merges fields of collected entities into the passed fields list.
   *
   * @param array $fields
   *   The parent fields to merge into.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   *
   * @return array
   *   The list of normalized fields after the merge.
   */
  public function mergeInto(
    array $fields,
    string $format = NULL,
    array $context = []
  ): array;

}
