<?php

namespace Drupal\frontend_api\Rest\FieldMerger;

use Drupal\frontend_api\ArgumentsPassingFactoryBase;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Factory of the field mergers.
 */
class FieldMergerFactory extends ArgumentsPassingFactoryBase implements FieldMergerFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function get(
    NormalizerInterface $formatted_fields_normalizer
  ): FieldMergerInterface {
    return new FieldMerger($formatted_fields_normalizer, ...$this->arguments);
  }

}
