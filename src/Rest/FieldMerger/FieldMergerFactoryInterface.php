<?php

namespace Drupal\frontend_api\Rest\FieldMerger;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Provides an interface for the field merger factory.
 */
interface FieldMergerFactoryInterface {

  /**
   * Returns the new field merger.
   *
   * @param \Symfony\Component\Serializer\Normalizer\NormalizerInterface $formatted_fields_normalizer
   *   The normalizer to use for merged fields.
   *
   * @return \Drupal\frontend_api\Rest\FieldMerger\FieldMergerInterface
   *   The field merger.
   */
  public function get(
    NormalizerInterface $formatted_fields_normalizer
  ): FieldMergerInterface;

}
