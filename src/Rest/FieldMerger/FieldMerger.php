<?php

namespace Drupal\frontend_api\Rest\FieldMerger;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\frontend_api\Rest\NormalizerContextKeys;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Provides the field merger.
 *
 * @see \Drupal\frontend_api\Rest\FieldMerger\FieldMergerInterface
 */
class FieldMerger implements FieldMergerInterface {

  /**
   * Separator between the field name prefix and its actual name.
   */
  public const PREFIX_SEPARATOR = '__';

  /**
   * The context key of the view mode name.
   */
  protected const VIEW_MODE_CONTEXT = NormalizerContextKeys::VIEW_MODE_CONTEXT;

  /**
   * Multi-level array of collected source data.
   *
   * Levels are:
   * - 1st level key is the entity type ID,
   * - 2nd level key is the entity ID,
   * - 2nd level values are options for the merged entity.
   *
   * The following entity options are handled:
   * - prefix: the field name prefix,
   * - view_mode: the view mode name to use for the field normalization.
   *
   * @var array
   */
  protected $sourceMap = [];

  /**
   * The list of prefixes in a collected source data.
   *
   * @var array
   */
  protected $prefixesMap = [];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The normalizer of the formatted entity fields.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected $formattedFieldsNormalizer;

  /**
   * A constructor.
   *
   * @param \Symfony\Component\Serializer\Normalizer\NormalizerInterface $formatted_fields_normalizer
   *   The normalizer of the formatted entity fields.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    NormalizerInterface $formatted_fields_normalizer,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->formattedFieldsNormalizer = $formatted_fields_normalizer;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function addEntity(string $entity_type_id, int $entity_id, string $view_mode, string $prefix): FieldMergerInterface {
    if (isset($this->prefixesMap[$prefix])) {
      throw new \InvalidArgumentException(sprintf(
        'Prefix %s is already defined.',
        $prefix
      ));
    }
    $this->prefixesMap[$prefix] = TRUE;

    $this->sourceMap[$entity_type_id][$entity_id] = [
      'view_mode' => $view_mode,
      'prefix' => $prefix,
    ];

    return $this;
  }

  /**
   * Normalizes and merges entity fields into the passed fields list.
   *
   * @param array $fields
   *   The parent fields to merge into.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to merge the fields of.
   * @param array $entity_options
   *   The options defining how to format and merge the entity.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   *
   * @return array
   *   The merged list of normalized fields.
   */
  protected function mergeEntityInto(
    array $fields,
    ContentEntityInterface $entity,
    array $entity_options,
    string $format = NULL,
    array $context = []
  ): array {
    $prefix = $entity_options['prefix'];
    $view_mode = $entity_options['view_mode'];

    $entity_context = [
      static::VIEW_MODE_CONTEXT => $view_mode,
    ];
    $entity_context += $context;

    $entity_fields = $this->formattedFieldsNormalizer
      ->normalize($entity, $format, $entity_context);
    foreach ($entity_fields as $field_name => $field_value) {
      $field_name = $prefix . static::PREFIX_SEPARATOR . $field_name;
      if (isset($fields[$field_name])) {
        throw new \LogicException(sprintf(
          'Field %s already exist on the source fields list.',
          $field_name
        ));
      }

      $fields[$field_name] = $field_value;
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function mergeInto(array $fields, string $format = NULL, array $context = []): array {
    // We should erase the source map before normalizing any entities and
    // iterate until it is empty, as more source entities could be added by
    // normalized ones in case of multi-level merging.
    while (!empty($this->sourceMap)) {
      $source_map = $this->sourceMap;
      $this->sourceMap = [];

      foreach ($source_map as $entity_type_id => $entity_ids_map) {
        $entities = $this->entityTypeManager
          ->getStorage($entity_type_id)
          ->loadMultiple(array_keys($entity_ids_map));

        foreach ($entity_ids_map as $entity_id => $entity_options) {
          $entity = $entities[$entity_id] ?? NULL;
          if (!$entity) {
            continue;
          }

          $fields = $this->mergeEntityInto(
            $fields,
            $entity,
            $entity_options,
            $format,
            $context
          );
        }
      }
    }

    return $fields;
  }

}
