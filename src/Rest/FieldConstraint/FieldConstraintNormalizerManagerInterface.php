<?php

namespace Drupal\frontend_api\Rest\FieldConstraint;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface;

/**
 * Provides plugin manager for the field constraint normalizers.
 */
interface FieldConstraintNormalizerManagerInterface extends PluginManagerInterface {

  /**
   * Creates normalizers for the field constraints.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface $field
   *   The field info.
   * @param \Drupal\field_constraints\FieldConstraintInterface[] $constraints
   *   The list of constraint plugins keyed by constraint plugin ID.
   *
   * @return \Drupal\frontend_api\Rest\FieldConstraint\FieldConstraintNormalizerInterface[]
   *   The list of suitable normalizers keyed by the constraint plugin ID. In
   *   case there is no normalizer for the constraint, it won't appear in this
   *   list.
   */
  public function createNormalizersForFieldConstraints(
    FormFieldInterface $field,
    array $constraints
  ): array;

}
