<?php

namespace Drupal\frontend_api\Rest\FieldConstraint;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface;

/**
 * Represents the list of field constraints in the normalization process.
 *
 * This class is necessary to trigger specific normalizer in the Symfony/Drupal
 * normalizer.
 */
interface FieldConstraintListInterface {

  /**
   * Returns the field the constraint list belongs to.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface
   *   The field.
   */
  public function getField(): FormFieldInterface;

  /**
   * Returns the list of constraints.
   *
   * @return \Drupal\field_constraints\FieldConstraintInterface[]
   *   The list of constraints keyed by their plugin IDs.
   */
  public function getConstraints(): array;

}
