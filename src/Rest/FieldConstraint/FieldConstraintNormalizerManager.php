<?php

namespace Drupal\frontend_api\Rest\FieldConstraint;

use Drupal\frontend_api\Annotation\FieldConstraintNormalizer;
use Drupal\frontend_api\PluginManagerBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface;

/**
 * Provides plugin manager for the field constraint normalizers.
 */
class FieldConstraintNormalizerManager extends PluginManagerBase implements FieldConstraintNormalizerManagerInterface {

  /**
   * {@inheritdoc}
   */
  public const SUBDIR = 'Plugin/frontend_api/EntityDisplayInfo/FieldConstraintNormalizer';

  /**
   * {@inheritdoc}
   */
  public const PLUGIN_ANNOTATION_NAME = FieldConstraintNormalizer::class;

  /**
   * {@inheritdoc}
   */
  protected const CACHE_KEY = 'frontend_api_field_constraints';

  /**
   * {@inheritdoc}
   */
  protected const PLUGIN_INTERFACE = FieldConstraintNormalizerInterface::class;

  /**
   * The field types key in the plugin definition.
   */
  public const FIELD_TYPES_DEFINITION_KEY = 'field_types';

  /**
   * The constraints key in the plugin definition.
   */
  public const CONSTRAINTS_DEFINITION_KEY = 'field_constraints';

  /**
   * The priority key in the plugin definition.
   */
  public const PRIORITY_DEFINITION_KEY = 'priority';

  /**
   * Returns plugin definitions for a specified field type and constraint.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface $field
   *   The field to match the plugins against.
   * @param string $constraint_id
   *   The field constraint plugin ID.
   *
   * @return array
   *   The list of plugin definitions keyed by the plugin ID.
   */
  protected function getDefinitionsForFieldConstraint(
    FormFieldInterface $field,
    string $constraint_id
  ): array {
    $field_definition = $field->getFieldDefinition();

    $field_types = [
      $field_definition->getType(),
    ];
    $constraint_ids = [
      $constraint_id,
    ];

    $filter = function (
      array $definition
    ) use (
      $field_types,
      $constraint_ids
    ): bool {
      // Match by field constraint.
      $matches = array_intersect(
        $constraint_ids,
        $definition[static::CONSTRAINTS_DEFINITION_KEY]
      );
      if (empty($matches)) {
        return FALSE;
      }

      // Match by field type.
      $matches = array_intersect(
        $field_types,
        $definition[static::FIELD_TYPES_DEFINITION_KEY]
      );
      if (empty($matches)) {
        return FALSE;
      }

      return TRUE;
    };
    $sort = function (array $a, array $b): int {
      return $b[static::PRIORITY_DEFINITION_KEY] - $a[static::PRIORITY_DEFINITION_KEY];
    };

    // @todo Definitions could be cached per field type + constraint.
    $matched_definitions = array_filter($this->getDefinitions(), $filter);
    uasort($matched_definitions, $sort);

    return $matched_definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function createNormalizersForFieldConstraints(
    FormFieldInterface $field,
    array $constraints
  ): array {
    $result = [];

    foreach (array_keys($constraints) as $constraint_id) {
      $plugin_definitions = $this
        ->getDefinitionsForFieldConstraint($field, $constraint_id);
      foreach (array_keys($plugin_definitions) as $plugin_id) {
        $result[$constraint_id] = $this->createInstance($plugin_id);

        // We take the first matching normalizer. In case there are no
        // normalizers, the field constraint is simply not exposed to the front.
        break;
      }
    }

    return $result;
  }

}
