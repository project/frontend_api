<?php

namespace Drupal\frontend_api\Rest\FieldConstraint;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface;

/**
 * Represents the list of field constraints in the normalization process.
 *
 * @see \Drupal\frontend_api\Rest\FieldConstraint\FieldConstraintListInterface
 */
class FieldConstraintList implements FieldConstraintListInterface {

  /**
   * The field this list belongs to.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface
   */
  protected $field;

  /**
   * The list of field constraints keyed by the plugin ID.
   *
   * @var \Drupal\field_constraints\FieldConstraintInterface[]
   */
  protected $constraints;

  /**
   * A constructor.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface $field
   *   The field this list belongs to.
   * @param array $constraints
   *   The list of field constraints keyed by the plugin ID.
   */
  public function __construct(
    FormFieldInterface $field,
    array $constraints
  ) {
    $this->field = $field;
    $this->constraints = $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public function getField(): FormFieldInterface {
    return $this->field;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints(): array {
    return $this->constraints;
  }

}
