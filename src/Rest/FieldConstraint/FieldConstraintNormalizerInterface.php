<?php

namespace Drupal\frontend_api\Rest\FieldConstraint;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\field_constraints\FieldConstraintInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface;

/**
 * Interface of a field constraint normalizer.
 */
interface FieldConstraintNormalizerInterface extends PluginInspectionInterface {

  /**
   * Normalizes the field constraint to be exposed to the front API.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface $field
   *   The field.
   * @param \Drupal\field_constraints\FieldConstraintInterface $constraint
   *   The constraint instance.
   *
   * @return array|null
   *   The normalized field constraint or NULL to hide it from the front API.
   */
  public function normalizeConstraint(
    FormFieldInterface $field,
    FieldConstraintInterface $constraint
  ): ?array;

}
