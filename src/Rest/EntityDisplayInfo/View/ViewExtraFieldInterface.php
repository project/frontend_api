<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\View;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface;

/**
 * Provides an interface for a extra field of the entity view info.
 *
 * Empty class is required in order to distinguish between view and form fields.
 */
interface ViewExtraFieldInterface extends DisplayExtraFieldInterface {

}
