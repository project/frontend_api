<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\View;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerInterface;

/**
 * Provides an interface for a normalizer of the entity view info field.
 */
interface ViewFieldNormalizerInterface extends DisplayFieldNormalizerInterface {

  /**
   * Prepares the field value.
   *
   * It may be used to populate some kind of collector with values in order to
   * do a storage query and then use it in the normalizeFieldValue().
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface $field
   *   The field of the entity view info.
   * @param \Drupal\Core\Field\FieldItemListInterface $item_list
   *   The item list to prepare the values of.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   */
  public function prepareFieldValue(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  ): void;

  /**
   * Normalizes field values for the passed field of an entity view info.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\View\ViewFieldInterface $field
   *   The field of the entity view info.
   * @param \Drupal\Core\Field\FieldItemListInterface $item_list
   *   The item list to normalize the values of.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   *
   * @return mixed
   *   The normalization result.
   */
  public function normalizeFieldValue(
    ViewFieldInterface $field,
    FieldItemListInterface $item_list,
    string $format = NULL,
    array $context = []
  );

}
