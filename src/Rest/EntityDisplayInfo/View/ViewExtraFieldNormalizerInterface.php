<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\View;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldNormalizerInterface;

/**
 * Provides an interface for a normalizer of the extra field view info.
 */
interface ViewExtraFieldNormalizerInterface extends DisplayExtraFieldNormalizerInterface {

  /**
   * Normalizes a display info for extra field into a set of arrays/scalars.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface $field
   *   The entity display info field to normalize.
   * @param string $format
   *   Format the normalization result will be encoded as.
   * @param array $context
   *   Context options for the normalizer.
   *
   * @return array
   *   The list of normalized fields keyed by field name. Results are merged
   *   with the ones from other extra fields and entity fields.
   */
  public function normalizeField(
    ContentEntityInterface $entity,
    DisplayExtraFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): array;

}
