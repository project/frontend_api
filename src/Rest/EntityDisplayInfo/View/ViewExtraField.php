<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\View;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldBase;

/**
 * Represents the extra field of the entity view info.
 *
 * Empty class is required in order to distinguish between view and form fields.
 */
class ViewExtraField extends DisplayExtraFieldBase implements ViewExtraFieldInterface {

}
