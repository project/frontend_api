<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\View;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoBuilderBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Dictionary\EntityTypes;

/**
 * Provides the entity view information builder.
 */
class ViewInfoBuilder extends DisplayInfoBuilderBase {

  /**
   * The field operation to check access for.
   */
  public const FIELD_OPERATION = 'view';

  /**
   * The entity type of the view display.
   */
  protected const DISPLAY_ENTITY_TYPE_ID = EntityTypes::ENTITY_VIEW_DISPLAY;

  /**
   * {@inheritdoc}
   */
  protected function createDisplayField(
    FieldDefinitionInterface $field_definition,
    array $component_data
  ): DisplayFieldInterface {
    return new ViewField($field_definition, $component_data);
  }

  /**
   * {@inheritdoc}
   */
  protected function createExtraFields(
    array $visible_components,
    array $extra_fields
  ): array {
    $extra_fields = $extra_fields['display'];
    $extra_fields = array_intersect_key(
      $extra_fields,
      $visible_components
    );

    $fields = [];
    foreach ($extra_fields as $field_name => $field) {
      $component = $visible_components[$field_name];
      $fields[$field_name] = $this
        ->createDisplayExtraField($field_name, $component);
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  protected function createDisplayExtraField(
    string $field_name,
    array $component_data
  ): DisplayExtraFieldInterface {
    return new ViewExtraField($field_name, $component_data);
  }

}
