<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\View;

use Drupal\frontend_api\Annotation\EntityViewInfoExtraFieldNormalizer;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldNormalizerManagerBase;

/**
 * Provides plugin manager for the extra field normalizers of the view info.
 */
class ViewExtraFieldNormalizerManager extends DisplayExtraFieldNormalizerManagerBase {

  /**
   * The plugin sub-folder in the module.
   */
  public const SUBDIR = 'Plugin/frontend_api/EntityDisplayInfo/ViewExtraFieldNormalizer';

  /**
   * The plugin annotation class name.
   */
  public const PLUGIN_ANNOTATION_NAME = EntityViewInfoExtraFieldNormalizer::class;

  /**
   * The cache key prefix.
   */
  public const CACHE_KEY_PREFIX = 'frontend_api_extra_view_field_normalizer';

}
