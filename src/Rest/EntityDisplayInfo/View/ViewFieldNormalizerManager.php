<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\View;

use Drupal\frontend_api\Annotation\EntityViewInfoFieldNormalizer;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerManagerBase;

/**
 * Provides plugin manager for the field normalizers of the view info.
 */
class ViewFieldNormalizerManager extends DisplayFieldNormalizerManagerBase {

  /**
   * The plugin sub-folder in the module.
   */
  public const SUBDIR = 'Plugin/frontend_api/EntityDisplayInfo/ViewFieldNormalizer';

  /**
   * The plugin annotation class name.
   */
  public const PLUGIN_ANNOTATION_NAME = EntityViewInfoFieldNormalizer::class;

  /**
   * The annotation key that contains the component types supported.
   */
  public const COMPONENT_TYPES_ANNOTATION_KEY = 'formatter_types';

  /**
   * The cache key prefix.
   */
  public const CACHE_KEY_PREFIX = 'frontend_api_view_field_normalizer';

}
