<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides base plugin manager of the display info for extra field normalizers.
 */
abstract class DisplayExtraFieldNormalizerManagerBase extends DefaultPluginManager implements DisplayExtraFieldNormalizerManagerInterface {

  /**
   * The plugin sub-folder in the module.
   *
   * Should be provided by the child class.
   */
  protected const SUBDIR = NULL;

  /**
   * The plugin annotation class name.
   *
   * Should be provided by the child class.
   */
  protected const PLUGIN_ANNOTATION_NAME = NULL;

  /**
   * The annotation key that contains the component types supported.
   */
  public const EXTRA_FIELD_ANNOTATION_KEY = 'extra_field_ids';

  /**
   * The cache key prefix.
   *
   * Should be provided by the child class.
   */
  protected const CACHE_KEY_PREFIX = NULL;

  /**
   * The plugin interface.
   */
  public const PLUGIN_INTERFACE = DisplayExtraFieldNormalizerInterface::class;

  /**
   * A constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      static::SUBDIR,
      $namespaces,
      $module_handler,
      static::PLUGIN_INTERFACE,
      static::PLUGIN_ANNOTATION_NAME
    );

    $this->setCacheBackend($cache_backend, static::CACHE_KEY_PREFIX);
  }

  /**
   * Returns plugin definitions for a specified field.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface $field
   *   The field to match the plugins against.
   *
   * @return array
   *   The list of plugin definitions keyed by the plugin ID.
   */
  protected function getPluginDefinitionsForField(
    DisplayExtraFieldInterface $field
  ): array {
    $extra_field_ids = [
      $field->getFieldName(),
    ];

    $filter = function (
      array $definition
    ) use (
      $extra_field_ids
    ): bool {
      // Make sure the widget/formatter types match.
      $extra_field_key = static::EXTRA_FIELD_ANNOTATION_KEY;
      $matches = array_intersect(
        $extra_field_ids,
        $definition[$extra_field_key]
      );
      if (empty($matches)) {
        return FALSE;
      }

      return TRUE;
    };

    // @TODO Definitions could be cached per field type for performance.
    $matched_definitions = array_filter($this->getDefinitions(), $filter);

    return $matched_definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function createNormalizerForField(
    DisplayExtraFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): DisplayExtraFieldNormalizerInterface {
    $matched_definitions = $this->getPluginDefinitionsForField($field);
    foreach (array_keys($matched_definitions) as $plugin_id) {
      /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerInterface $plugin */
      $plugin = $this->createInstance($plugin_id);
      if ($plugin->supportsNormalization($field, $format, $context)) {
        return $plugin;
      }
    }

    throw new PluginException(
      sprintf(
        'Unable to find normalizer plugin for %s field',
        $field->getFieldName()
      )
    );
  }

}
