<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\serialization\Normalizer\CacheableNormalizerInterface;

/**
 * Provides base class for a normalizer of the entity view/form info field.
 */
abstract class DisplayFieldNormalizerBase extends PluginBase implements
    DisplayFieldNormalizerInterface {

  /**
   * The context key that holds the cacheable metadata.
   */
  protected const CACHEABILITY_CONTEXT_KEY = CacheableNormalizerInterface::SERIALIZATION_CONTEXT_CACHEABILITY;

  /**
   * The widget type key.
   */
  public const WIDGET_TYPE_KEY = 'widget_type';

  /**
   * Widget type returned.
   *
   * For simple mapping it's enough to override this constant in a child class.
   *
   * @see getWidgetType()
   */
  protected const WIDGET_TYPE = NULL;

  /**
   * Returns the mapped widget type.
   *
   * Should be overridden by child classes to implement custom logic.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   * @param array $context
   *   The context.
   *
   * @return string|null
   *   The widget type or NULL in case it can't be determined.
   */
  protected function getWidgetType(
    DisplayFieldInterface $field,
    array $context = []
  ): ?string {
    return static::WIDGET_TYPE;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): bool {
    return TRUE;
  }

  /**
   * Adds cacheable dependency if applicable.
   *
   * @param array $context
   *   Context options for the normalizer.
   * @param mixed $data
   *   The data that might have cacheability information.
   */
  protected function addCacheableDependency(array $context, $data): void {
    if (!$data instanceof CacheableDependencyInterface) {
      return;
    }

    $cacheability = $context[static::CACHEABILITY_CONTEXT_KEY] ?? NULL;
    if (!$cacheability instanceof RefinableCacheableDependencyInterface) {
      return;
    }

    $cacheability->addCacheableDependency($data);
  }

  /**
   * Adds cache metadata to the context.
   *
   * @param array $context
   *   The normalization context.
   * @param string[] $tags
   *   The list of cache tags to add.
   */
  protected function addCacheTags(array $context, array $tags): void {
    if (empty($tags)) {
      return;
    }

    $metadata = new CacheableMetadata();
    $metadata->addCacheTags($tags);
    $this->addCacheableDependency($context, $metadata);
  }

  /**
   * Flattens the value of every field item, if possible.
   *
   * This includes extracting the value of a main property.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field the value is flatten for.
   * @param array $items_list
   *   Values of the items list.
   *
   * @return mixed
   *   The flattened value.
   */
  protected function flattenValueLiteralItems(
    DisplayFieldInterface $field,
    array $items_list
  ) {
    $main_property = $this->getFieldMainPropertyName($field);
    if (!isset($main_property)) {
      return $items_list;
    }

    $result = [];
    foreach ($items_list as $item) {
      if (!isset($item[$main_property])) {
        continue;
      }

      $result[] = $item[$main_property];
    }
    return $result;
  }

  /**
   * Flattens the field item list, if applicable.
   *
   * This includes flattening on a single-value fields.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   * @param array $items_list
   *   Values of the items list.
   *
   * @return mixed
   *   The flattened value.
   */
  protected function flattenValueLiteralItemList(
    DisplayFieldInterface $field,
    array $items_list
  ) {
    $cardinality = $this->getFieldCardinality($field);
    if ($cardinality !== 1) {
      return $items_list;
    }

    if (empty($items_list)) {
      return NULL;
    }

    return reset($items_list);
  }

  /**
   * Normalizes literal values of the field item list.
   *
   * The value could be extracted from a field item list of an entity or be a
   * default value.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field the value is normalized for.
   * @param array $value
   *   The values.
   *
   * @return mixed
   *   The normalized value.
   */
  protected function normalizeValueLiteral(
    DisplayFieldInterface $field,
    array $value
  ) {
    $value = $this->flattenValueLiteralItems($field, $value);
    $value = $this->flattenValueLiteralItemList($field, $value);
    return $value;
  }

  /**
   * Returns field cardinality.
   *
   * Could be overridden by child classes to force some specific cardinality
   * that is different from the field one.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   *
   * @return int
   *   The cardinality.
   */
  protected function getFieldCardinality(DisplayFieldInterface $field): int {
    return $field->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getCardinality();
  }

  /**
   * Returns main property name of the field, if any.
   *
   * Could be overridden by child classes to specify the main property of a
   * multi-property field if the normalizer is only interested in one property.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field.
   *
   * @return string|null
   *   Main property name or NULL if there is no main property on the field.
   */
  protected function getFieldMainPropertyName(
    DisplayFieldInterface $field
  ): ?string {
    return $field->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getMainPropertyName();
  }

}
