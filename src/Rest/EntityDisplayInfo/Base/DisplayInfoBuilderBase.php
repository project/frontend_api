<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Provides base class for a form/view info builder.
 */
abstract class DisplayInfoBuilderBase implements DisplayInfoBuilderInterface {

  /**
   * The field operation to check access for.
   *
   * Should be overwritten by child classes.
   */
  protected const FIELD_OPERATION = NULL;

  /**
   * The entity type of the form/view display.
   *
   * Should be overwritten by child classes.
   */
  protected const DISPLAY_ENTITY_TYPE_ID = NULL;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * A constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager
   *   The field manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $field_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldManager = $field_manager;
  }

  /**
   * Creates entity form/view field metadata.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $component
   *   The widget/formatter data provided by the entity form/view display.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface
   *   The just created field metadata.
   */
  abstract protected function createDisplayField(
    FieldDefinitionInterface $field_definition,
    array $component
  ): DisplayFieldInterface;

  /**
   * Creates entity form/view extra field metadata.
   *
   * @param string $field_name
   *   Machine name of a field.
   * @param array $component
   *   The widget/formatter data provided by the entity form/view display.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface
   *   The just created field metadata.
   */
  abstract protected function createDisplayExtraField(
    string $field_name,
    array $component
  ): DisplayExtraFieldInterface;

  /**
   * Creates new instance of the entity display info.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string|null $bundle_id
   *   The bundle ID.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface
   *   The just created entity form/view info.
   */
  protected function createDisplayInfo(
    string $entity_type_id,
    string $bundle_id = NULL
  ): DisplayInfoInterface {
    return new DisplayInfo($entity_type_id, $bundle_id);
  }

  /**
   * Adds field definitions to the entity display info.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $display_info
   *   The display info.
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string|null $bundle_id
   *   The entity bundle ID.
   */
  protected function addFieldsDefinition(
    DisplayInfoInterface $display_info,
    string $entity_type_id,
    string $bundle_id = NULL
  ): void {
    $bundle_id = $bundle_id ?? $entity_type_id;

    $field_definitions = $this->fieldManager
      ->getFieldDefinitions($entity_type_id, $bundle_id);

    $visible_components = $this->getVisibleComponents($display_info);
    $fields = $this->createEnabledFields(
      $visible_components,
      $field_definitions
    );
    $fields = $this->filterAccessibleFields(
      $display_info,
      $fields
    );
    $display_info->setFields($fields);

    $translatable_fields = [];
    foreach ($fields as $field_name => $field) {
      $is_translatable = $field->getFieldDefinition()
        ->isTranslatable();
      if ($is_translatable) {
        $translatable_fields[$field_name] = clone $field;
      }
    }
    $display_info->setTranslatableFields($translatable_fields);
  }

  /**
   * Adds extra fields to the entity display info.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $display_info
   *   The display info.
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string|null $bundle_id
   *   The entity bundle ID.
   */
  protected function addExtraFields(
    DisplayInfoInterface $display_info,
    string $entity_type_id,
    string $bundle_id = NULL
  ): void {
    $bundle_id = $bundle_id ?? $entity_type_id;

    $extra_fields = $this->fieldManager
      ->getExtraFields($entity_type_id, $bundle_id);
    $visible_components = $this->getVisibleComponents($display_info);
    $extra_fields = $this->createExtraFields(
      $visible_components,
      $extra_fields
    );
    $display_info->setExtraFields($extra_fields);
  }

  /**
   * Filters fields accessible for the operation specified in a constant.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $display_info
   *   The entity form/view info.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface[] $fields
   *   The fields to filter.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface[]
   *   The filtered fields.
   */
  protected function filterAccessibleFields(
    DisplayInfoInterface $display_info,
    array $fields
  ): array {
    $access_handler = $this->entityTypeManager
      ->getAccessControlHandler($display_info->getEntityTypeId());

    $accessible_fields = [];
    foreach ($fields as $field_name => $field) {
      $field_access = $access_handler->fieldAccess(
        static::FIELD_OPERATION,
        $field->getFieldDefinition(),
        NULL,
        NULL,
        TRUE
      );
      $display_info->addCacheableDependency($field_access);

      if (!$field_access->isForbidden()) {
        $accessible_fields[$field_name] = $field;
      }
    }
    return $accessible_fields;
  }

  /**
   * Creates wrappers on extra fields enabled in the display mode.
   *
   * @param array $visible_components
   *   Array with components, enabled on display mode.
   * @param array $extra_fields
   *   The array with extra field.
   *
   * @return array
   *   The filtered extra fields.
   */
  abstract protected function createExtraFields(
    array $visible_components,
    array $extra_fields
  ): array;

  /**
   * Gets visible components of display mode.
   *
   * @param DisplayInfoInterface $display_info
   *   Info about display mode.
   *
   * @return array
   *   Array with components available on display mode.
   */
  protected function getVisibleComponents(DisplayInfoInterface $display_info): array {
    /** @var \Drupal\Core\Entity\Display\EntityDisplayInterface $entity_display */
    $entity_display = $this->entityTypeManager
      ->getStorage(static::DISPLAY_ENTITY_TYPE_ID)
      ->load($display_info->getDisplayId());
    if (!$entity_display) {
      throw new \InvalidArgumentException(sprintf(
        'Unable to load %s display.',
        $display_info->getDisplayId()
      ));
    }

    $display_info->addCacheableDependency($entity_display);
    $visible_components = $entity_display->getComponents();

    return $visible_components;
  }

  /**
   * Creates field wrappers on fields enabled in the display mode.
   *
   * @param array $visible_components
   *   Array with components, enabled on display mode.
   * @param \Drupal\Core\Field\FieldDefinitionInterface[] $field_definitions
   *   The field definitions.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   The filtered field definitions.
   */
  protected function createEnabledFields(
    array $visible_components,
    array $field_definitions
  ): array {
    $field_definitions = array_intersect_key(
      $field_definitions,
      $visible_components
    );

    $fields = [];
    foreach ($field_definitions as $field_name => $field_definition) {
      $component = $visible_components[$field_name];
      $fields[$field_name] = $this
        ->createDisplayField($field_definition, $component);
    }
    return $fields;
  }

  /**
   * Sets the form display ID to the entity form info.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $display_info
   *   The entity form/view info.
   * @param string|null $form_mode
   *   The form mode name to build the form display ID from.
   */
  protected function setFormDisplayId(
    DisplayInfoInterface $display_info,
    string $form_mode = NULL
  ): void {
    if (!isset($form_mode)) {
      return;
    }

    $entity_type_id = $display_info->getEntityTypeId();
    $bundle = $display_info->getEntityBundleId() ?? $entity_type_id;
    $form_display_id = "{$entity_type_id}.{$bundle}.{$form_mode}";

    $display_info->setDisplayId($form_display_id);
  }

  /**
   * {@inheritdoc}
   */
  public function build(
    string $entity_type_id,
    string $bundle_id = NULL,
    string $display_mode = NULL
  ): DisplayInfoInterface {
    $form_info = $this->createDisplayInfo($entity_type_id, $bundle_id);

    $this->setFormDisplayId($form_info, $display_mode);
    $this->addFieldsDefinition($form_info, $entity_type_id, $bundle_id);
    $this->addExtraFields($form_info, $entity_type_id, $bundle_id);

    return $form_info;
  }

}
