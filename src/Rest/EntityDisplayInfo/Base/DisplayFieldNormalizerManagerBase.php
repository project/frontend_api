<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides base plugin manager of the display info field normalizers.
 */
abstract class DisplayFieldNormalizerManagerBase extends DefaultPluginManager implements DisplayFieldNormalizerManagerInterface {

  /**
   * The plugin sub-folder in the module.
   *
   * Should be provided by the child class.
   */
  protected const SUBDIR = NULL;

  /**
   * The plugin annotation class name.
   *
   * Should be provided by the child class.
   */
  protected const PLUGIN_ANNOTATION_NAME = NULL;

  /**
   * The annotation key that contains the component types supported.
   *
   * Should be provided by the child class.
   */
  protected const COMPONENT_TYPES_ANNOTATION_KEY = NULL;

  /**
   * The cache key prefix.
   *
   * Should be provided by the child class.
   */
  protected const CACHE_KEY_PREFIX = NULL;

  /**
   * The plugin interface.
   */
  public const PLUGIN_INTERFACE = DisplayFieldNormalizerInterface::class;

  /**
   * The field type wildcard.
   *
   * It means all the field types are supported by the plugin.
   */
  public const FIELD_TYPES_WILDCARD = '*';

  /**
   * The component (widget/formatter) type wildcard.
   *
   * It means all the component types are supported by the plugin.
   */
  public const COMPONENT_TYPES_WILDCARD = '*';

  /**
   * A constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      static::SUBDIR,
      $namespaces,
      $module_handler,
      static::PLUGIN_INTERFACE,
      static::PLUGIN_ANNOTATION_NAME
    );

    $this->setCacheBackend($cache_backend, static::CACHE_KEY_PREFIX);
  }

  /**
   * Returns plugin definitions for a specified field.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field to match the plugins against.
   *
   * @return array
   *   The list of plugin definitions keyed by the plugin ID.
   */
  protected function getPluginDefinitionsForField(
    DisplayFieldInterface $field
  ): array {
    $component_data = $field->getComponentData();
    $field_definition = $field->getFieldDefinition();

    $field_types = [
      $field_definition->getType(),
      static::FIELD_TYPES_WILDCARD,
    ];
    $component_types = [
      $component_data['type'],
      static::COMPONENT_TYPES_WILDCARD,
    ];

    $filter = function (
      array $definition
    ) use (
      $field_types,
      $component_types
    ): bool {
      $matches = array_intersect($field_types, $definition['field_types']);
      if (empty($matches)) {
        return FALSE;
      }

      // Make sure the widget/formatter types match.
      $component_types_key = static::COMPONENT_TYPES_ANNOTATION_KEY;
      $matches = array_intersect(
        $component_types,
        $definition[$component_types_key]
      );
      if (empty($matches)) {
        return FALSE;
      }

      return TRUE;
    };
    $sort = function (array $a, array $b): int {
      return $b['priority'] - $a['priority'];
    };

    // @TODO Definitions could be cached per field type for performance.
    $matched_definitions = array_filter($this->getDefinitions(), $filter);
    uasort($matched_definitions, $sort);

    return $matched_definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function createNormalizerForField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): DisplayFieldNormalizerInterface {
    $matched_definitions = $this->getPluginDefinitionsForField($field);
    foreach (array_keys($matched_definitions) as $plugin_id) {
      /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerInterface $plugin */
      $plugin = $this->createInstance($plugin_id);
      if ($plugin->supportsNormalization($field, $format, $context)) {
        return $plugin;
      }
    }

    // This should be a rare case, since we have a default plugin.
    $field_definition = $field->getFieldDefinition();
    throw new PluginException(
      sprintf(
        'Unable to find normalizer plugin for %s field of %s type on %s.',
        $field_definition->getName(),
        $field_definition->getType(),
        $field_definition->getTargetEntityTypeId()
      )
    );
  }

}
