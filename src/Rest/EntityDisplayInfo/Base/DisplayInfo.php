<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base;

use Drupal\Core\Cache\RefinableCacheableDependencyTrait;

/**
 * Represents the entity form/view info.
 */
class DisplayInfo implements DisplayInfoInterface {

  use RefinableCacheableDependencyTrait;

  /**
   * Default display mode name.
   */
  public const DEFAULT_DISPLAY_MODE = 'default';

  /**
   * The entity type ID.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * The entity bundle ID.
   *
   * @var string|null
   */
  protected $entityBundleId;

  /**
   * The form/view mode ID.
   *
   * @var string
   */
  protected $displayId;

  /**
   * The list of the fields.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface[]
   */
  protected $fields = [];

  /**
   * The list of extra fields.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface[]
   */
  protected $extraFields = [];

  /**
   * The list of translatable fields.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface[]
   */
  protected $translatableFields = [];

  /**
   * List of attached third party settings.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings\DisplaySettingsItemInterface[]
   */
  protected $thirdPartySettings = [];

  /**
   * A constructor.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string|null $bundle_id
   *   The bundle ID.
   */
  public function __construct(
    string $entity_type_id,
    string $bundle_id = NULL
  ) {
    $this->entityTypeId = $entity_type_id;
    $this->entityBundleId = $bundle_id;

    $this->setDefaultDisplayId();
  }

  /**
   * Sets default form/view display ID.
   */
  protected function setDefaultDisplayId(): void {
    $entity_type_id = $this->entityTypeId;
    $bundle_id = $this->entityBundleId ?? $entity_type_id;
    $default_view_mode = static::DEFAULT_DISPLAY_MODE;
    $this->displayId = "{$entity_type_id}.{$bundle_id}.{$default_view_mode}";
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId(): string {
    return $this->entityTypeId;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityBundleId(): ?string {
    return $this->entityBundleId;
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayId(): string {
    return $this->displayId;
  }

  /**
   * {@inheritdoc}
   */
  public function setDisplayId(string $form_display_id): DisplayInfoInterface {
    $this->displayId = $form_display_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFields(): array {
    return $this->fields;
  }

  /**
   * {@inheritdoc}
   */
  public function setFields(array $fields): DisplayInfoInterface {
    // Remove display info references from existing fields.
    foreach ($this->fields as $field) {
      $field->unsetDisplayInfo();
    }

    // Assign display info to the new fields.
    foreach ($fields as $field) {
      $field->setDisplayInfo($this);
    }

    $this->fields = $fields;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getExtraFields(): array {
    return $this->extraFields;
  }

  /**
   * {@inheritdoc}
   */
  public function setExtraFields(array $extra_fields): DisplayInfoInterface {
    // Remove display info references from existing fields.
    foreach ($this->extraFields as $extra_field) {
      $extra_field->unsetDisplayInfo();
    }

    // Assign display info to the new fields.
    foreach ($extra_fields as $extra_field) {
      $extra_field->setDisplayInfo($this);
    }

    $this->extraFields = $extra_fields;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNames(): array {
    return array_keys($this->fields);
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslatableFields(): array {
    return $this->translatableFields;
  }

  /**
   * {@inheritdoc}
   */
  public function setTranslatableFields(array $fields): DisplayInfoInterface {
    // Remove display info references from existing fields.
    foreach ($this->translatableFields as $field) {
      $field->unsetDisplayInfo();
    }

    // Assign display info to the new fields.
    foreach ($fields as $field) {
      $field->setDisplayInfo($this);
    }
    $this->translatableFields = $fields;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getThirdPartySettings(): array {
    return $this->thirdPartySettings;
  }

  /**
   * {@inheritdoc}
   */
  public function setThirdPartySettings(array $data): DisplayInfoInterface {
    $this->thirdPartySettings = $data;
    return $this;
  }

}
