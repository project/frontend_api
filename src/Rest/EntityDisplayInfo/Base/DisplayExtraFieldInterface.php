<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base;

/**
 * Provides an interface for the extra field metadata on the form/view info.
 */
interface DisplayExtraFieldInterface {

  /**
   * Returns extra field machine name.
   *
   * @return string
   *   Field machine name.
   */
  public function getFieldName(): string;

  /**
   * Returns component data as provided by the form/view display.
   *
   * @return array
   *   The widget/formatter data.
   */
  public function getComponentData(): array;

  /**
   * Returns the display info this extra field belongs to.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface
   *   The display info.
   */
  public function getDisplayInfo(): DisplayInfoInterface;

  /**
   * Sets the display info reference.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $display_info
   *   The display info.
   *
   * @return static
   *   Self.
   */
  public function setDisplayInfo(
    DisplayInfoInterface $display_info
  ): DisplayExtraFieldInterface;

  /**
   * Erases the display info reference.
   *
   * @return static
   *   Self.
   */
  public function unsetDisplayInfo(): DisplayExtraFieldInterface;

}
