<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Provides interface for a manager of normalizers for form/view info fields.
 */
interface DisplayFieldNormalizerManagerInterface extends PluginManagerInterface {

  /**
   * Creates plugin using passed field, format and context.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The field of a view/form entity info.
   * @param string|null $format
   *   The format.
   * @param array $context
   *   The serialization context.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerInterface
   *   The field normalizer plugin.
   */
  public function createNormalizerForField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): DisplayFieldNormalizerInterface;

}
