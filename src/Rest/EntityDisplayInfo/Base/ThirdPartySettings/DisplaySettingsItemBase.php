<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface;

/**
 * Represents the third-party settings item on the entity form/view info.
 */
abstract class DisplaySettingsItemBase implements DisplaySettingsItemInterface {

  /**
   * The item's provider.
   *
   * @var string
   */
  protected $module;

  /**
   * The module settings.
   *
   * @var array
   */
  protected $settings;

  /**
   * The display info this settings item belongs to.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface
   */
  protected $displayInfo;

  /**
   * A constructor.
   *
   * @param string $module
   *   Item's provider name.
   * @param array $settings
   *   Settings provided by the module.
   */
  public function __construct(
    string $module,
    array $settings
  ) {
    $this->module = $module;
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getModule(): string {
    return $this->module;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings() {
    return $this->settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayInfo(): DisplayInfoInterface {
    return $this->displayInfo;
  }

  /**
   * {@inheritdoc}
   */
  public function setDisplayInfo(
    DisplayInfoInterface $display_info
  ): DisplaySettingsItemInterface {
    $this->displayInfo = $display_info;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unsetDisplayInfo(): DisplaySettingsItemInterface {
    $this->displayInfo = NULL;
    return $this;
  }

  /**
   * Implements the magic __clone function.
   */
  public function __clone() {
    $this->unsetDisplayInfo();
  }

}
