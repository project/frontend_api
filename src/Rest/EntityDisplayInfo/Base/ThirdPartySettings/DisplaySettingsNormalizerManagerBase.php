<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Base plugin manager class for third-party settings normalizers.
 */
abstract class DisplaySettingsNormalizerManagerBase extends DefaultPluginManager implements DisplaySettingsNormalizerManagerInterface {

  /**
   * The plugin sub-folder in the module.
   *
   * Should be provided by the child class.
   */
  protected const SUBDIR = NULL;

  /**
   * The plugin annotation class name.
   *
   * Should be provided by the child class.
   */
  protected const PLUGIN_ANNOTATION_NAME = NULL;

  /**
   * The cache key prefix.
   *
   * Should be provided by the child class.
   */
  protected const CACHE_KEY_PREFIX = NULL;

  /**
   * The plugin interface.
   */
  protected const PLUGIN_INTERFACE = DisplaySettingsNormalizerInterface::class;

  /**
   * The annotation key that contains settings provider.
   */
  public const SETTINGS_MODULE_ANNOTATION_KEY = 'setting_module';

  /**
   * A constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      static::SUBDIR,
      $namespaces,
      $module_handler,
      static::PLUGIN_INTERFACE,
      static::PLUGIN_ANNOTATION_NAME
    );

    $this->setCacheBackend($cache_backend, static::CACHE_KEY_PREFIX);
  }

  /**
   * Returns plugin definitions for a specified field.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings\DisplaySettingsItemInterface $item
   *   The field to match the plugins against.
   *
   * @return array
   *   The list of plugin definitions keyed by the plugin ID.
   */
  protected function getPluginDefinitionsForItem(
    DisplaySettingsItemInterface $item
  ): array {
    $item_module = $item->getModule();
    $filter = function (
      array $definition
    ) use (
      $item_module
    ): bool {
      $module = $definition[static::SETTINGS_MODULE_ANNOTATION_KEY] ?? NULL;
      return $item_module === $module;
    };

    return array_filter($this->getDefinitions(), $filter);
  }

  /**
   * {@inheritdoc}
   */
  public function createNormalizersForItem(
    DisplaySettingsItemInterface $item,
    string $format = NULL,
    array $context = []
  ): array {
    $normalizers = [];
    $matched_definitions = $this->getPluginDefinitionsForItem($item);
    foreach (array_keys($matched_definitions) as $plugin_id) {
      /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings\DisplaySettingsNormalizerInterface $plugin */
      $plugin = $this->createInstance($plugin_id);
      if ($plugin->supportsNormalization($item, $format, $context)) {
        $normalizers[] = $plugin;
      }
    }

    return $normalizers;
  }

  /**
   * {@inheritdoc}
   */
  public function getModules(): array {
    $definitions = $this->getDefinitions();
    $modules = array_column($definitions, static::SETTINGS_MODULE_ANNOTATION_KEY);
    return array_unique($modules);
  }

}
