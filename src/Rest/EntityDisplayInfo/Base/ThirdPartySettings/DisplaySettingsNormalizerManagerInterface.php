<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Provides interface for plugin manager for third-party settings normalizers.
 */
interface DisplaySettingsNormalizerManagerInterface extends PluginManagerInterface {

  /**
   * Creates plugin using passed settings item, format and context.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings\DisplaySettingsItemInterface $item
   *   The settings item of a view/form entity info.
   * @param string|null $format
   *   The format.
   * @param array $context
   *   The serialization context.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings\DisplaySettingsNormalizerInterface[]
   *   The list of settings normalizer plugins.
   */
  public function createNormalizersForItem(
    DisplaySettingsItemInterface $item,
    string $format = NULL,
    array $context = []
  ): array;

  /**
   * Retrieve list of setting modules.
   *
   * @return array
   *   List of module IDs.
   */
  public function getModules(): array;

}
