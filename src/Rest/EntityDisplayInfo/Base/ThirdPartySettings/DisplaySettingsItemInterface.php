<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface;

/**
 * Provides an interface for the third-party module settings on form/view info.
 */
interface DisplaySettingsItemInterface {

  /**
   * Returns provider's name.
   *
   * @return string
   *   The provider's name.
   */
  public function getModule(): string;

  /**
   * Returns settings provided by module.
   *
   * @return mixed
   *   Settings provided by the module.
   */
  public function getSettings();

  /**
   * Returns the display info this item belongs to.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface
   *   The display info.
   */
  public function getDisplayInfo(): DisplayInfoInterface;

  /**
   * Sets the display info reference.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $display_info
   *   The display info.
   *
   * @return static
   */
  public function setDisplayInfo(
    DisplayInfoInterface $display_info
  ): DisplaySettingsItemInterface;

  /**
   * Erases the display info reference.
   *
   * @return static
   */
  public function unsetDisplayInfo(): DisplaySettingsItemInterface;

}
