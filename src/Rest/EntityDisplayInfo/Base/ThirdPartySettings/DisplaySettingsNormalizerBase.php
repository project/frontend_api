<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\serialization\Normalizer\CacheableNormalizerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for a third-party settings normalizer.
 */
abstract class DisplaySettingsNormalizerBase extends PluginBase implements DisplaySettingsNormalizerInterface, ContainerFactoryPluginInterface {

  /**
   * The result key.
   *
   * Should be overridden in child class.
   */
  protected const RESULT_KEY = NULL;

  /**
   * The context key that holds the cacheable metadata.
   */
  protected const CACHEABILITY_CONTEXT_KEY = CacheableNormalizerInterface::SERIALIZATION_CONTEXT_CACHEABILITY;

  /**
   * Adds cacheable dependency if applicable.
   *
   * @param array $context
   *   Context options for the normalizer.
   * @param mixed $data
   *   The data that might have cacheability information.
   */
  protected function addCacheableDependency(array $context, $data): void {
    if (!$data instanceof CacheableDependencyInterface) {
      return;
    }

    $cacheability = $context[static::CACHEABILITY_CONTEXT_KEY] ?? NULL;
    if (!$cacheability instanceof RefinableCacheableDependencyInterface) {
      return;
    }

    $cacheability->addCacheableDependency($data);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Returns list of applicable settings.
   *
   * @param mixed $data
   *   The third-party module settings data.
   *
   * @return array
   *   List of settings keyed by setting ID.
   */
  abstract protected function getApplicableSettings($data): array;

  /**
   * Checks if setting applicable to normalizer.
   *
   * @param array $config
   *   Configs list.
   *
   * @return bool
   *   Is setting applicable for this normalizer.
   */
  abstract protected function isApplicableSetting(array $config): bool;

}
