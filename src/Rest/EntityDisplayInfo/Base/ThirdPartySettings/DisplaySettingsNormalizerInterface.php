<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for a third-party module settings normalizer.
 */
interface DisplaySettingsNormalizerInterface extends PluginInspectionInterface {

  /**
   * Checks whether the given item is supported by this normalizer.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings\DisplaySettingsItemInterface $item
   *   Display settings item instance.
   * @param string $format
   *   Format the normalization result will be encoded as.
   * @param array $context
   *   Context options for the normalizer.
   *
   * @return bool
   *   Whether the plugin supports normalization or not.
   */
  public function supportsNormalization(
    DisplaySettingsItemInterface $item,
    string $format = NULL,
    array $context = []
  ): bool;

}
