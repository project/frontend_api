<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base;

/**
 * Represents the extra field on the entity form/view info.
 */
abstract class DisplayExtraFieldBase implements DisplayExtraFieldInterface {

  /**
   * Machine name of a field.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * The widget/formatter data.
   *
   * @var array
   */
  protected $componentData;

  /**
   * The display info this extra field belongs to.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface
   */
  protected $displayInfo;

  /**
   * A constructor.
   *
   * @param string $field_name
   *   Field machine name.
   * @param array $component_data
   *   The widget/formatter data.
   */
  public function __construct(string $field_name, array $component_data) {
    $this->fieldName = $field_name;
    $this->componentData = $component_data;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldName(): string {
    return $this->fieldName;
  }

  /**
   * {@inheritdoc}
   */
  public function getComponentData(): array {
    return $this->componentData;
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayInfo(): DisplayInfoInterface {
    return $this->displayInfo;
  }

  /**
   * {@inheritdoc}
   */
  public function setDisplayInfo(
    DisplayInfoInterface $display_info
  ): DisplayExtraFieldInterface {
    $this->displayInfo = $display_info;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unsetDisplayInfo(): DisplayExtraFieldInterface {
    $this->displayInfo = NULL;
    return $this;
  }

  /**
   * Implements the magic __clone function.
   */
  public function __clone() {
    $this->unsetDisplayInfo();
  }

}
