<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\serialization\Normalizer\CacheableNormalizerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for a normalizer of the entity view/form info extra field.
 */
abstract class DisplayExtraFieldNormalizerBase extends PluginBase implements DisplayExtraFieldNormalizerInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The widget type key.
   */
  protected const WIDGET_TYPE_KEY = DisplayFieldNormalizerBase::WIDGET_TYPE_KEY;

  /**
   * The context key that holds the cacheable metadata.
   */
  protected const CACHEABILITY_CONTEXT_KEY = CacheableNormalizerInterface::SERIALIZATION_CONTEXT_CACHEABILITY;

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization(
    DisplayExtraFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): bool {
    return TRUE;
  }

  /**
   * Adds cacheable dependency if applicable.
   *
   * @param array $context
   *   Context options for the normalizer.
   * @param mixed $data
   *   The data that might have cacheability information.
   */
  protected function addCacheableDependency(array $context, $data): void {
    if (!$data instanceof CacheableDependencyInterface) {
      return;
    }

    $cacheability = $context[static::CACHEABILITY_CONTEXT_KEY] ?? NULL;
    if (!$cacheability instanceof RefinableCacheableDependencyInterface) {
      return;
    }

    $cacheability->addCacheableDependency($data);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

}
