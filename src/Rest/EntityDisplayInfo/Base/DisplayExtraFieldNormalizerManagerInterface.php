<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Provides interface for a manager of form/view info extra fields normalizers.
 */
interface DisplayExtraFieldNormalizerManagerInterface extends PluginManagerInterface {

  /**
   * Creates plugin using passed field, format and context.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface $field
   *   The field of a view/form entity info.
   * @param string|null $format
   *   The format.
   * @param array $context
   *   The serialization context.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldNormalizerInterface
   *   The field normalizer plugin.
   */
  public function createNormalizerForField(
    DisplayExtraFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): DisplayExtraFieldNormalizerInterface;

}
