<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\frontend_api\Dictionary\Field\Widget\WidgetSettings;

/**
 * Represents the field on the entity form/view info.
 *
 * It contains both field definition and the widget/formatter data provided by
 * the entity form/view display.
 */
abstract class DisplayFieldBase implements DisplayFieldInterface {

  /**
   * The third-party settings key in the widget data stored in the form display.
   */
  protected const WIDGET_THIRD_PARTY_KEY = WidgetSettings::THIRD_PARTY;

  /**
   * The field definition.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected $fieldDefinition;

  /**
   * The widget/formatter data.
   *
   * @var array
   */
  protected $componentData;

  /**
   * The display info this field belongs to.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface
   */
  protected $displayInfo;

  /**
   * A constructor.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $component_data
   *   The widget/formatter data.
   */
  public function __construct(
    FieldDefinitionInterface $field_definition,
    array $component_data
  ) {
    $this->fieldDefinition = $field_definition;
    $this->componentData = $component_data;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldDefinition(): FieldDefinitionInterface {
    return $this->fieldDefinition;
  }

  /**
   * {@inheritdoc}
   */
  public function getComponentData(): array {
    return $this->componentData;
  }

  /**
   * {@inheritdoc}
   */
  public function getComponentThirdPartySettings(): array {
    return $this->componentData[static::WIDGET_THIRD_PARTY_KEY] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayInfo(): DisplayInfoInterface {
    return $this->displayInfo;
  }

  /**
   * {@inheritdoc}
   */
  public function setDisplayInfo(
    DisplayInfoInterface $display_info
  ): DisplayFieldInterface {
    $this->displayInfo = $display_info;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unsetDisplayInfo(): DisplayFieldInterface {
    $this->displayInfo = NULL;
    return $this;
  }

  /**
   * Implements the magic __clone function.
   */
  public function __clone() {
    $this->unsetDisplayInfo();
  }

}
