<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Provides an interface for a normalizer of the display info extra field.
 */
interface DisplayExtraFieldNormalizerInterface extends PluginInspectionInterface {

  /**
   * Checks whether the given field is supported by this normalizer.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface $field
   *   Field to check support of.
   * @param string $format
   *   Format the normalization result will be encoded as.
   * @param array $context
   *   Context options for the normalizer.
   *
   * @return bool
   *   Whether the plugin supports normalization or not.
   */
  public function supportsNormalization(
    DisplayExtraFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): bool;

}
