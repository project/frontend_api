<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;

/**
 * Provides an interface for the entity form/view information.
 *
 * It represents the information necessary to build the front-end form in a
 * specific form mode or render an entity in a specific view mode.
 */
interface DisplayInfoInterface extends RefinableCacheableDependencyInterface {

  /**
   * Returns the entity bundle ID.
   *
   * @return string|null
   *   The bundle ID.
   */
  public function getEntityBundleId(): ?string;

  /**
   * Returns the entity type.
   *
   * @return string
   *   The entity type ID.
   */
  public function getEntityTypeId(): string;

  /**
   * Returns form display ID.
   *
   * @return string
   *   The form display ID.
   */
  public function getDisplayId(): string;

  /**
   * Sets form display ID.
   *
   * @param string $formModeId
   *   The form display ID.
   *
   * @return static
   */
  public function setDisplayId(string $formModeId): DisplayInfoInterface;

  /**
   * Returns field names.
   *
   * @return string[]
   *   The array of field names.
   */
  public function getFieldNames(): array;

  /**
   * Sets the available fields.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface[] $fields
   *   The fields metadata.
   *
   * @return static
   */
  public function setFields(array $fields): DisplayInfoInterface;

  /**
   * Returns the fields metadata.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface[]
   *   The metadata.
   */
  public function getFields(): array;

  /**
   * Returns the extra fields metadata.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface[]
   *   The metadata.
   */
  public function getExtraFields(): array;

  /**
   * Sets the available extra fields.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface[] $extra_fields
   *   The fields metadata.
   *
   * @return static
   */
  public function setExtraFields(array $extra_fields): DisplayInfoInterface;

  /**
   * Get third party settings.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings\DisplaySettingsItemInterface[]
   *   The metadata.
   */
  public function getThirdPartySettings(): array;

  /**
   * Sets third party settings.
   *
   * @param array $data
   *   The third party metadata.
   *
   * @return static
   */
  public function setThirdPartySettings(array $data): DisplayInfoInterface;

  /**
   * Returns translatable fields found in the display info.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface[]
   *   Translatable fields keyed by field name.
   */
  public function getTranslatableFields(): array;

  /**
   * Sets the translatable fields.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface[] $fields
   *   The fields metadata.
   *
   * @return static
   */
  public function setTranslatableFields(array $fields): DisplayInfoInterface;

}
