<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base;

/**
 * Provides an interface for the entity form/view info builder.
 */
interface DisplayInfoBuilderInterface {

  /**
   * Builds the entity form/view information.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string|null $bundle_id
   *   The bundle ID.
   * @param string|null $display_mode
   *   The form/view mode name.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface
   *   The built entity form/view information.
   */
  public function build(
    string $entity_type_id,
    string $bundle_id = NULL,
    string $display_mode = NULL
  ): DisplayInfoInterface;

}
