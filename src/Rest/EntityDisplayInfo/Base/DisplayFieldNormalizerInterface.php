<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Provides an interface for a normalizer of the display info field.
 */
interface DisplayFieldNormalizerInterface extends PluginInspectionInterface {

  /**
   * Normalizes a display info field into a set of arrays/scalars.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   The entity display info field to normalize.
   * @param string $format
   *   Format the normalization result will be encoded as.
   * @param array $context
   *   Context options for the normalizer.
   *
   * @return array|string|int|float|bool
   *   The normalized value.
   */
  public function normalizeField(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  );

  /**
   * Checks whether the given field is supported by this normalizer.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface $field
   *   Field to check support of.
   * @param string $format
   *   Format the normalization result will be encoded as.
   * @param array $context
   *   Context options for the normalizer.
   *
   * @return bool
   *   Whether the plugin supports normalization or not.
   */
  public function supportsNormalization(
    DisplayFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): bool;

}
