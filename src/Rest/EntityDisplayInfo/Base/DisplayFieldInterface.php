<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Base;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Provides an interface for the field metadata on the entity form/view info.
 */
interface DisplayFieldInterface {

  /**
   * Returns component data as provided by the form/view display.
   *
   * @return array
   *   The widget/formatter data.
   */
  public function getComponentData(): array;

  /**
   * Returns third-party settings of the component.
   *
   * @return array
   *   The widget/formatter third-party settings.
   */
  public function getComponentThirdPartySettings(): array;

  /**
   * Returns field definition.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   *   The field definition.
   */
  public function getFieldDefinition(): FieldDefinitionInterface;

  /**
   * Returns the display info this field belongs to.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface
   *   The display info.
   */
  public function getDisplayInfo(): DisplayInfoInterface;

  /**
   * Sets the display info reference.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $display_info
   *   The display info.
   *
   * @return static
   *   Self.
   */
  public function setDisplayInfo(
    DisplayInfoInterface $display_info
  ): DisplayFieldInterface;

  /**
   * Erases the display info reference.
   *
   * @return static
   *   Self.
   */
  public function unsetDisplayInfo(): DisplayFieldInterface;

}
