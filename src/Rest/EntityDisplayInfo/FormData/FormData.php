<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\FormData;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfo;

/**
 * Represents the entity form data.
 *
 * @see \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataInterface
 */
class FormData extends DisplayInfo implements FormDataInterface {

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entity;

  /**
   * A constructor.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   */
  public function __construct(
    ContentEntityInterface $entity
  ) {
    parent::__construct($entity->getEntityTypeId(), $entity->bundle());

    $this->entity = $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity(): ContentEntityInterface {
    return $this->entity;
  }

}
