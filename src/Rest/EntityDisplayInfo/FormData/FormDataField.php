<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\FormData;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormField;

/**
 * Represents the field of the entity form data.
 *
 * @see \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface
 */
class FormDataField extends FormField implements FormDataFieldInterface {

  /**
   * The field items list.
   *
   * @var \Drupal\Core\Field\FieldItemListInterface
   */
  protected $itemList;

  /**
   * A constructor.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $component_data
   *   The widget data.
   * @param \Drupal\Core\Field\FieldItemListInterface $item_list
   *   The field items list.
   */
  public function __construct(
    FieldDefinitionInterface $field_definition,
    array $component_data,
    FieldItemListInterface $item_list
  ) {
    parent::__construct($field_definition, $component_data, []);

    $this->itemList = $item_list;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemList(): FieldItemListInterface {
    return $this->itemList;
  }

  /**
   * {@inheritdoc}
   */
  public function cloneForItemList(
    FieldItemListInterface $item_list
  ): FormDataFieldInterface {
    $clone = clone $this;
    $clone->itemList = $item_list;
    return $clone;
  }

}
