<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\FormData;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraFieldInterface;

/**
 * Represents the extra field of the entity form data.
 *
 * Empty class is required in order to distinguish between form info and form
 * data fields.
 */
interface FormDataExtraFieldInterface extends FormExtraFieldInterface {

}
