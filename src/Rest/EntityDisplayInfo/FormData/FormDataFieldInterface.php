<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\FormData;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface;

/**
 * Provides an interface for a field of the entity form data.
 *
 * It contains field definition, the widget data provided by the entity form
 * display and the field item list extracted from the entity.
 */
interface FormDataFieldInterface extends FormFieldInterface {

  /**
   * Returns the field item list extracted from the entity.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   The field item list.
   */
  public function getItemList(): FieldItemListInterface;

  /**
   * Creates a copy of the form data field with a replaced field item list.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $item_list
   *   The field item list to use for a copy.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface
   *   The copy with a specified field item list.
   */
  public function cloneForItemList(
    FieldItemListInterface $item_list
  ): FormDataFieldInterface;

}
