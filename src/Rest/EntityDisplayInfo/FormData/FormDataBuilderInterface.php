<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\FormData;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for an entity form data builder.
 */
interface FormDataBuilderInterface {

  /**
   * Builds the entity form data.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to build the form data for.
   * @param string|null $form_mode
   *   The form mode name.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataInterface
   *   The built entity form data.
   */
  public function build(
    ContentEntityInterface $entity,
    string $form_mode = NULL
  ): FormDataInterface;

}
