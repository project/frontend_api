<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\FormData;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface;

/**
 * It provides an interface for the entity form data.
 *
 * It includes both the form meta-data and the entity field values. The
 * information of this class should be enough to populate values of the entity
 * edit form on the front.
 */
interface FormDataInterface extends DisplayInfoInterface {

  /**
   * Returns the entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity.
   */
  public function getEntity(): ContentEntityInterface;

}
