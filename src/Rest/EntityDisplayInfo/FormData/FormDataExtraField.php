<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\FormData;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraField;

/**
 * Represents the extra field of the entity form data.
 *
 * Empty class is required in order to distinguish between form info and form
 * data fields.
 */
class FormDataExtraField extends FormExtraField implements FormDataExtraFieldInterface {

}
