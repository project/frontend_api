<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\FormData;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoBuilderInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface;

/**
 * Provides an entity form data builder.
 */
class FormDataBuilder implements FormDataBuilderInterface {

  /**
   * The form info builder.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormInfoBuilderInterface
   */
  protected $formInfoBuilder;

  /**
   * A constructor.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoBuilderInterface $form_info_builder
   *   The form info builder.
   */
  public function __construct(
    DisplayInfoBuilderInterface $form_info_builder
  ) {
    $this->formInfoBuilder = $form_info_builder;
  }

  /**
   * Creates new instance of the entity form data.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataInterface
   *   The just created form data.
   */
  protected function createFormData(
    ContentEntityInterface $entity
  ): FormDataInterface {
    return new FormData($entity);
  }

  /**
   * Creates a field for the entity form data.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldInterface $info_field
   *   The form info field to use as a source.
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field item list with the values.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface
   *   The just created field.
   *
   * @todo Merge form info and form data builder to drop this workaround with
   *   copying info field into data field.
   */
  protected function createDataField(
    FormFieldInterface $info_field,
    FieldItemListInterface $items
  ): FormDataFieldInterface {
    $result = new FormDataField(
      $info_field->getFieldDefinition(),
      $info_field->getComponentData(),
      $items
    );

    if ($info_field->isForcedReadOnly()) {
      $result->forceReadOnly(TRUE);
    }
    if ($info_field->isForcedRequired()) {
      $result->forceRequired(TRUE);
    }

    return $result;
  }

  /**
   * Converts entity form info fields into entity form data fields.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to extract the field item lists from.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface[] $info_fields
   *   The fields of the entity form info to covert.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataField[]
   *   The converted fields.
   */
  protected function convertInfoFieldsToDataFields(
    ContentEntityInterface $entity,
    array $info_fields
  ): array {
    $data_fields = [];

    foreach ($info_fields as $field_name => $info_field) {
      $data_fields[$field_name] = $this->createDataField(
        $info_field,
        $entity->get($field_name)
      );
    }

    return $data_fields;
  }

  /**
   * Converts form info extra fields into form data extra fields.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraFieldInterface[] $extra_fields
   *   The form info extra fields.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataExtraFieldInterface[]
   *   The form data extra fields.
   *
   * @todo Merge form info and form data builder to drop this workaround with
   *   copying info field into data field.
   */
  protected function convertInfoExtraFieldsToDataExtraFields(
    array $extra_fields
  ): array {
    $result = [];
    foreach ($extra_fields as $key => $extra_field) {
      $result[$key] = $this->createDataExtraField($extra_field);
    }
    return $result;
  }

  /**
   * Creates the form data extra field.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraFieldInterface $extra_field
   *   The source form info field.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataExtraFieldInterface
   *   The created form data extra field.
   */
  protected function createDataExtraField(
    FormExtraFieldInterface $extra_field
  ): FormDataExtraFieldInterface {
    return new FormDataExtraField(
      $extra_field->getFieldName(),
      $extra_field->getComponentData()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(
    ContentEntityInterface $entity,
    string $form_mode = NULL
  ): FormDataInterface {
    // Re-use the form info builder in order to filter the list of fields,
    // perform the access check, etc.
    $form_info = $this->formInfoBuilder->build(
      $entity->getEntityTypeId(),
      $entity->bundle(),
      $form_mode,
      $entity
    );
    $fields = $this->convertInfoFieldsToDataFields(
      $entity,
      $form_info->getFields()
    );
    $translatable_fields = $this->convertInfoFieldsToDataFields(
      $entity,
      $form_info->getTranslatableFields()
    );
    $extra_fields = $this->convertInfoExtraFieldsToDataExtraFields(
      $form_info->getExtraFields()
    );

    $form_data = $this->createFormData($entity);
    $form_data->setDisplayId($form_info->getDisplayId());
    $form_data->setFields($fields);
    $form_data->setExtraFields($extra_fields);
    $form_data->setTranslatableFields($translatable_fields);
    $form_data->addCacheableDependency($form_info);
    $form_data->addCacheableDependency($entity);
    return $form_data;
  }

}
