<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;

/**
 * Provides an interface for a field of the entity form info.
 */
interface FormFieldInterface extends DisplayFieldInterface {

  /**
   * Checks if the field was forced to be read-only.
   *
   * @return bool
   *   TRUE if the field is read-only.
   */
  public function isForcedReadOnly(): bool;

  /**
   * Forces the field to be read-only.
   *
   * @param bool $value
   *   TRUE to mark the field read-only.
   *
   * @return static
   *   Self.
   */
  public function forceReadOnly(bool $value): FormFieldInterface;

  /**
   * Checks if the field was forced to be required.
   *
   * @return bool
   *   TRUE if the field is required.
   */
  public function isForcedRequired(): bool;

  /**
   * Forces the field to be required.
   *
   * @param bool $value
   *   TRUE to mark the field required.
   *
   * @return static
   *   Self.
   */
  public function forceRequired(bool $value): FormFieldInterface;

  /**
   * Returns the list of attached field constraints.
   *
   * @return \Drupal\field_constraints\FieldConstraintInterface[]
   *   The list of constraints keyed by ID.
   */
  public function getConstraints(): array;

}
