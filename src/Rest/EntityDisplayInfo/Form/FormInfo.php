<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfo;

/**
 * Represents entity form info.
 *
 * Used to differentiate form info data from form data and view info.
 */
class FormInfo extends DisplayInfo implements FormInfoInterface {

  /**
   * Empty entity needed for getting some field info.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $sampleEntity;

  /**
   * Optional entity, used for special purposes.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * A constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    string $entityTypeId,
    $bundleId = NULL
  ) {
    parent::__construct($entityTypeId, $bundleId);

    $this->setSampleEntity($entityTypeManager, $entityTypeId, $bundleId);
  }

  /**
   * {@inheritdoc}
   */
  public function getSampleEntity(): EntityInterface {
    return $this->sampleEntity;
  }

  /**
   * Creates an empty entity and sets it to property.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param string $entityTypeId
   *   Entity type id.
   * @param mixed $bundleId
   *   Bundle.
   *
   * @todo Use sample entity provider and set it from the form info builder.
   *
   * @see \Drupal\frontend_api\Entity\SampleEntityProvider
   */
  protected function setSampleEntity(
    EntityTypeManagerInterface $entityTypeManager,
    string $entityTypeId,
    $bundleId
  ): void {
    $options = [];
    $entityType = $entityTypeManager->getDefinition($entityTypeId);
    if (isset($bundleId) && $bundleKey = $entityType->getKey('bundle')) {
      $options[$bundleKey] = $bundleId;
    }
    $entity = $entityTypeManager->getStorage($entityTypeId)
      ->create($options);
    $this->sampleEntity = $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity) {
    $this->entity = $entity;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasEntity(): bool {
    if ($this->entity instanceof EntityInterface) {
      return TRUE;
    }

    return FALSE;
  }

}
