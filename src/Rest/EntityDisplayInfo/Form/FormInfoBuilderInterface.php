<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoBuilderInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface;

/**
 * Provides an interface for the entity form info builder.
 */
interface FormInfoBuilderInterface extends DisplayInfoBuilderInterface {

  /**
   * Builds the entity form information.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string|null $bundle_id
   *   The bundle ID.
   * @param string|null $display_mode
   *   The form/view mode name.
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   The entity.
   * @param string|null $operation
   *   The entity operation.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface
   *   The built entity form/view information.
   */
  public function build(
    string $entity_type_id,
    string $bundle_id = NULL,
    string $display_mode = NULL,
    ?EntityInterface $entity = NULL,
    string $operation = NULL
  ): DisplayInfoInterface;

}
