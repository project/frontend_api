<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldNormalizerManagerInterface;

/**
 * Provides interface for a manager of form info extra field normalizers.
 */
interface FormExtraFieldNormalizerManagerInterface extends DisplayExtraFieldNormalizerManagerInterface {

  /**
   * Creates de-normalizer for a passed form extra field.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraFieldInterface $field
   *   The extra field.
   * @param string|null $format
   *   The format.
   * @param array $context
   *   The context.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldNormalizerInterface
   *   The de-normalizer plugin instance.
   */
  public function createDenormalizerByField(
    FormExtraFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): FormExtraFieldNormalizerInterface;

}
