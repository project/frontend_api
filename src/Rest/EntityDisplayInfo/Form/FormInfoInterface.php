<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface;

/**
 * Interface for form info.
 *
 * Provides the info about fields of the entity and other data
 * necessary for creating a form.
 *
 * @method FormFieldInterface[] getFields()
 */
interface FormInfoInterface extends DisplayInfoInterface {

  /**
   * Creates sample entity for getting additional field info.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Empty entity.
   */
  public function getSampleEntity(): EntityInterface;

  /**
   * Optional entity that was set during form info building.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Entity.
   */
  public function getEntity(): EntityInterface;

  /**
   * Sets entity to form info.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return static
   */
  public function setEntity(EntityInterface $entity);

  /**
   * Checks whether form info has entity or not.
   *
   * @return bool
   *   Boolean result of the check.
   */
  public function hasEntity(): bool;

}
