<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldBase;

/**
 * Represents the extra field of the entity form info.
 *
 * Empty class is required in order to distinguish between view and form fields.
 */
class FormExtraField extends DisplayExtraFieldBase implements FormExtraFieldInterface {

}
