<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\frontend_api\Annotation\EntityFormInfoExtraFieldNormalizer;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldNormalizerManagerBase;

/**
 * Provides plugin manager for the extra field normalizers of the form info.
 */
class FormExtraFieldNormalizerManager extends DisplayExtraFieldNormalizerManagerBase implements FormExtraFieldNormalizerManagerInterface {

  /**
   * The plugin sub-folder in the module.
   */
  public const SUBDIR = 'Plugin/frontend_api/EntityDisplayInfo/FormExtraFieldNormalizer';

  /**
   * The plugin annotation class name.
   */
  public const PLUGIN_ANNOTATION_NAME = EntityFormInfoExtraFieldNormalizer::class;

  /**
   * The cache key prefix.
   */
  public const CACHE_KEY_PREFIX = 'frontend_api_extra_form_field_normalizer';

  /**
   * {@inheritdoc}
   */
  public function createDenormalizerByField(
    FormExtraFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): FormExtraFieldNormalizerInterface {
    $matched_definitions = $this->getPluginDefinitionsForField($field);
    foreach (array_keys($matched_definitions) as $plugin_id) {
      /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraFieldNormalizerInterface $plugin */
      $plugin = $this->createInstance($plugin_id);
      if ($plugin->supportsNormalization($field, $format, $context)) {
        return $plugin;
      }
    }

    throw new PluginException(
      sprintf(
        'Unable to find normalizer plugin for %s field',
        $field->getFieldName()
      )
    );
  }

}
