<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldBase;

/**
 * Represents the field of the entity form info.
 *
 * Empty class is required in order to distinguish between view and form fields.
 */
class FormField extends DisplayFieldBase implements FormFieldInterface {

  /**
   * Whether the field was forced to be read-only or not.
   *
   * @var bool
   */
  protected $forceReadOnly;

  /**
   * Whether the field was forced to be required or not.
   *
   * @var bool
   */
  protected $forceRequired;

  /**
   * The list of field constraints that applies to the field.
   *
   * @var \Drupal\field_constraints\FieldConstraintInterface[]
   */
  protected $constraints;

  /**
   * A constructor.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $component_data
   *   The widget/formatter data.
   * @param \Drupal\field_constraints\FieldConstraintInterface[] $constraints
   *   The list of field constraints that applies to the field.
   */
  public function __construct(
    FieldDefinitionInterface $field_definition,
    array $component_data,
    array $constraints
  ) {
    parent::__construct($field_definition, $component_data);

    $this->constraints = $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public function isForcedReadOnly(): bool {
    return $this->forceReadOnly ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function forceReadOnly(bool $value): FormFieldInterface {
    $this->forceReadOnly = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isForcedRequired(): bool {
    return $this->forceRequired ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function forceRequired(bool $value): FormFieldInterface {
    $this->forceRequired = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints(): array {
    return $this->constraints;
  }

}
