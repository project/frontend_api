<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;

/**
 * Provides an interface for a normalizer of the form info/data field.
 */
interface FormFieldNormalizerInterface extends DisplayFieldNormalizerInterface {

  /**
   * Normalizes field values of the entity form data into arrays/scalars.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The entity form data field to normalize.
   * @param string $format
   *   Format the normalization result will be encoded as.
   * @param array $context
   *   Context options for the normalizer.
   *
   * @return array|string|int|float|bool
   *   The normalized value.
   */
  public function normalizeFieldValue(
    FormDataFieldInterface $field,
    string $format = NULL,
    array $context = []
  );

  /**
   * Checks whether the field value could be de-normalized by this normalizer.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   Field to check support of.
   * @param string $format
   *   The format.
   * @param array $context
   *   The context.
   *
   * @return bool
   *   Whether the plugin supports de-normalization or not.
   */
  public function supportsDenormalization(
    FormDataFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): bool;

  /**
   * De-normalizes field values of the entity form data into the items lists.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The entity form data field to de-normalize.
   * @param array $data
   *   The un-serialized data to de-normalize.
   * @param string $format
   *   The format.
   * @param array $context
   *   The context.
   *
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   *   In case of data format issues or any other data validation failures an
   *   exception is thrown with the field name in the error path.
   */
  public function denormalizeFieldValue(
    FormDataFieldInterface $field,
    array $data,
    string $format = NULL,
    array $context = []
  ): void;

}
