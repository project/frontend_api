<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\frontend_api\Annotation\EntityFormInfoFieldNormalizer;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerManagerBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;

/**
 * Provides plugin manager for the field normalizers of the form info.
 */
class FormFieldNormalizerManager extends DisplayFieldNormalizerManagerBase implements FormFieldNormalizerManagerInterface {

  /**
   * The plugin sub-folder in the module.
   */
  public const SUBDIR = 'Plugin/frontend_api/EntityDisplayInfo/FormFieldNormalizer';

  /**
   * The plugin annotation class name.
   */
  public const PLUGIN_ANNOTATION_NAME = EntityFormInfoFieldNormalizer::class;

  /**
   * The annotation key that contains the component types supported.
   */
  public const COMPONENT_TYPES_ANNOTATION_KEY = 'widget_types';

  /**
   * The cache key prefix.
   */
  public const CACHE_KEY_PREFIX = 'frontend_api_form_field_normalizer';

  /**
   * {@inheritdoc}
   */
  public function createDenormalizerByField(
    FormDataFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): FormFieldNormalizerInterface {
    $matched_definitions = $this->getPluginDefinitionsForField($field);
    foreach (array_keys($matched_definitions) as $plugin_id) {
      /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldNormalizerInterface $plugin */
      $plugin = $this->createInstance($plugin_id);
      if ($plugin->supportsDenormalization($field, $format, $context)) {
        return $plugin;
      }
    }

    // This should be a rare case, since we have a default plugin.
    $field_definition = $field->getFieldDefinition();
    throw new PluginException(
      sprintf(
        'Unable to find denormalizer plugin for %s field of %s type on %s.',
        $field_definition->getName(),
        $field_definition->getType(),
        $field_definition->getTargetEntityTypeId()
      )
    );
  }

}
