<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerManagerInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface;

/**
 * Provides interface for a manager of normalizers for form info fields.
 */
interface FormFieldNormalizerManagerInterface extends DisplayFieldNormalizerManagerInterface {

  /**
   * Creates normalizer plugin for de-normalization of the passed field.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface $field
   *   The field of a form entity info.
   * @param string|null $format
   *   The format.
   * @param array $context
   *   The serialization context.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldNormalizerInterface
   *   The field normalizer plugin.
   */
  public function createDenormalizerByField(
    FormDataFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): FormFieldNormalizerInterface;

}
