<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldNormalizerBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataExtraFieldInterface;

/**
 * Provides base class for a normalizer of entity form extra fields.
 */
abstract class FormExtraFieldNormalizerBase extends DisplayExtraFieldNormalizerBase implements FormExtraFieldNormalizerInterface {

  /**
   * {@inheritdoc}
   */
  public function getExtraFieldNames() {
    $definition = $this->getPluginDefinition();
    $extra_field_names = $definition['field_names'];

    return $extra_field_names;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeFieldValue(
    ContentEntityInterface $entity,
    FormDataExtraFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): array {
    return [];
  }

}
