<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldNormalizerInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataExtraFieldInterface;

/**
 * Provides an interface for a normalizer of the extra field form info.
 */
interface FormExtraFieldNormalizerInterface extends DisplayExtraFieldNormalizerInterface {

  /**
   * Normalizes a display info for extra field into a set of arrays/scalars.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface $field
   *   The entity display info field to normalize.
   * @param string|null $format
   *   Format the normalization result will be encoded as.
   * @param array $context
   *   Context options for the normalizer.
   *
   * @return array
   *   The list of normalized fields keyed by field name. Results are merged
   *   with the ones from other extra fields and entity fields.
   */
  public function normalizeField(
    DisplayExtraFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): array;

  /**
   * Normalizes the extra field value for the entity form data.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataExtraFieldInterface $field
   *   The entity form data field to normalize.
   * @param string|null $format
   *   Format the normalization result will be encoded as.
   * @param array $context
   *   Context options for the normalizer.
   *
   * @return array
   *   The list of normalized field data keyed by field name. Results are merged
   *   with the ones from other extra fields and entity fields.
   */
  public function normalizeFieldValue(
    ContentEntityInterface $entity,
    FormDataExtraFieldInterface $field,
    string $format = NULL,
    array $context = []
  ): array;

  /**
   * Performs extra field denormalization.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface $field
   *   Field for which denormalization is performed.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   * @param array $data
   *   Request data.
   * @param string $format
   *   The format.
   * @param array $context
   *   Serialization context.
   */
  public function denormalizeExtraField(
    DisplayExtraFieldInterface $field,
    ContentEntityInterface $entity,
    array $data,
    string $format,
    array $context
  ): void;

  /**
   * Gets the list of extra field names from plugin annotation.
   *
   * @return string[]
   *   List of extra field names.
   */
  public function getExtraFieldNames();

}
