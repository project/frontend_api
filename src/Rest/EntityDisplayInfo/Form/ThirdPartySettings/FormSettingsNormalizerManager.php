<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form\ThirdPartySettings;

use Drupal\frontend_api\Annotation\EntityFormInfoThirdPartySettingsNormalizer;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings\DisplaySettingsNormalizerManagerBase;

/**
 * Plugin manager for third-party settings normalizers of the form info.
 */
class FormSettingsNormalizerManager extends DisplaySettingsNormalizerManagerBase {

  /**
   * The plugin sub-folder in the module.
   */
  public const SUBDIR = 'Plugin/frontend_api/EntityDisplayInfo/FormThirdPartySettingsNormalizer';

  /**
   * The plugin annotation class name.
   */
  protected const PLUGIN_ANNOTATION_NAME = EntityFormInfoThirdPartySettingsNormalizer::class;

  /**
   * The cache key prefix.
   */
  public const CACHE_KEY_PREFIX = 'frontend_api_form_third_party_settings_normalizer';

}
