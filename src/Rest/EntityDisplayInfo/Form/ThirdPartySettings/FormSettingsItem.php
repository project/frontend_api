<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form\ThirdPartySettings;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings\DisplaySettingsItemBase;

/**
 * Represents the third-party settings item of the entity form info.
 *
 * Empty class is required in order to distinguish between view and form fields.
 */
class FormSettingsItem extends DisplaySettingsItemBase implements FormSettingsItemInterface {

}
