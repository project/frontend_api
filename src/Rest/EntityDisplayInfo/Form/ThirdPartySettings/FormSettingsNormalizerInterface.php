<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form\ThirdPartySettings;

use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings\DisplaySettingsNormalizerInterface;

/**
 * Providers interface for form info third-party settings normalizer.
 */
interface FormSettingsNormalizerInterface extends DisplaySettingsNormalizerInterface {

  /**
   * Normalizes a display info for third-party module settings.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\ThirdPartySettings\FormSettingsItemInterface $field
   *   The entity display info item to normalize.
   * @param array $result
   *   The normalization result. Provides flexible way to fill result from
   *   plugin itself.
   * @param string|null $format
   *   Format the normalization result will be encoded as.
   * @param array $context
   *   Context options for the normalizer.
   */
  public function normalizeItem(
    FormSettingsItemInterface $field,
    array &$result,
    string $format = NULL,
    array $context = []
  ): void;

}
