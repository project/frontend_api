<?php

namespace Drupal\frontend_api\Rest\EntityDisplayInfo\Form;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\field_constraints\FieldConstraintsFactoryInterface;
use Drupal\frontend_api\Dictionary\EntityTypes;
use Drupal\frontend_api\Dictionary\Event\DisplayInfoEvents;
use Drupal\frontend_api\Event\FormInfoBuildEvent;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoBuilderBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayFieldInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings\DisplaySettingsNormalizerManagerInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\ThirdPartySettings\FormSettingsItem;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provides the entity form information builder.
 */
class FormInfoBuilder extends DisplayInfoBuilderBase implements FormInfoBuilderInterface {

  /**
   * The edit field operation.
   */
  public const FIELD_OPERATION = 'edit';

  /**
   * The entity type of the form display.
   */
  protected const DISPLAY_ENTITY_TYPE_ID = EntityTypes::ENTITY_FORM_DISPLAY;

  /**
   * Default entity revision metadata keys.
   *
   * @see \Drupal\Core\Entity\RevisionLogEntityTrait::getRevisionMetadataKey()
   */
  public const DEFAULT_REVISION_METADATA_KEYS = [
    'revision_created' => 'revision_created',
    'revision_user' => 'revision_user',
    'revision_log_message' => 'revision_log_message',
  ];

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The plugin manager.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\ThirdPartySettings\DisplaySettingsNormalizerManagerInterface
   */
  protected $thirdPartySettingPluginManager;

  /**
   * The field constraints factory.
   *
   * @var \Drupal\field_constraints\FieldConstraintsFactoryInterface
   */
  protected $fieldConstraintsFactory;

  /**
   * A constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $field_manager,
    EventDispatcherInterface $event_dispatcher,
    DisplaySettingsNormalizerManagerInterface $plugin_manager
  ) {
    parent::__construct($entity_type_manager, $field_manager);

    $this->eventDispatcher = $event_dispatcher;
    $this->thirdPartySettingPluginManager = $plugin_manager;
  }

  /**
   * Sets the field constraint factory.
   *
   * This is called from the service container. It was done to make this
   * dependency optional and avoid tracking all the services that depend on this
   * one for the case of the Field Constraints module not yet enabled.
   *
   * @param \Drupal\field_constraints\FieldConstraintsFactoryInterface $field_constraints_factory
   *   The field constraints factory.
   *
   * @todo Move it to the constructor once we deploy the field_constraints
   *   module.
   */
  public function setFieldConstraintsFactory(
    FieldConstraintsFactoryInterface $field_constraints_factory
  ): void {
    $this->fieldConstraintsFactory = $field_constraints_factory;
  }

  /**
   * {@inheritdoc}
   */
  protected function createDisplayInfo(
    string $entity_type_id,
    string $bundle_id = NULL
  ): DisplayInfoInterface {
    return new FormInfo(
      $this->entityTypeManager,
      $entity_type_id,
      $bundle_id
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function createDisplayField(
    FieldDefinitionInterface $field_definition,
    array $component_data
  ): DisplayFieldInterface {
    $constraints = $this->createFieldConstraints($field_definition);
    return new FormField($field_definition, $component_data, $constraints);
  }

  /**
   * Creates field constraints attached to the field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return \Drupal\field_constraints\FieldConstraintInterface[]
   *   The list of field constraints configured for the field, keyed by
   *   the field constraint plugin ID.
   */
  protected function createFieldConstraints(
    FieldDefinitionInterface $field_definition
  ): array {
    return $this->fieldConstraintsFactory
      ->createFieldConstraints($field_definition);
  }

  /**
   * {@inheritdoc}
   */
  protected function createExtraFields(
    array $visible_components,
    array $extra_fields
  ): array {
    $extra_fields = $extra_fields['form'];

    $extra_fields = array_intersect_key(
      $extra_fields,
      $visible_components
    );

    $fields = [];
    foreach ($extra_fields as $field_name => $field) {
      $component = $visible_components[$field_name];
      $fields[$field_name] = $this
        ->createDisplayExtraField($field_name, $component);
    }

    return $fields;
  }

  /**
   * Creates entity form/view extra field metadata.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldInterface
   *   The just created field metadata.
   */
  protected function createDisplayExtraField(string $field_name, array $component_data): DisplayExtraFieldInterface {
    return new FormExtraField($field_name, $component_data);
  }

  /**
   * Creates entity form settings item.
   *
   * @param string $module
   *   Module ID.
   * @param mixed $data
   *   Item's data.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\ThirdPartySettings\FormSettingsItem
   *   The form setting metadata.
   */
  protected function createFormSettingsItem($module, $data) {
    return new FormSettingsItem($module, $data);
  }

  /**
   * Adds extra fields to the entity display info.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface $display_info
   *   The display info.
   */
  protected function addThirdPartySettings(
    DisplayInfoInterface $display_info
  ): void {
    /** @var \Drupal\Core\Entity\Display\EntityDisplayInterface $entity_display */
    $entity_display = $this->entityTypeManager
      ->getStorage(static::DISPLAY_ENTITY_TYPE_ID)
      ->load($display_info->getDisplayId());
    if (!$entity_display) {
      throw new \InvalidArgumentException(sprintf(
        'Unable to load %s display.',
        $display_info->getDisplayId()
      ));
    }
    $display_info->addCacheableDependency($entity_display);
    $modules = $this->thirdPartySettingPluginManager->getModules();

    if (empty($modules)) {
      return;
    }

    $third_party_settings = [];
    foreach ($modules as $module_id) {
      $settings = $entity_display->getThirdPartySettings($module_id);
      $settings_item = $this->createFormSettingsItem($module_id, $settings);
      $settings_item->setDisplayInfo($display_info);
      $third_party_settings[] = $settings_item;
    }

    $display_info->setThirdPartySettings($third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public function build(
    string $entity_type_id,
    string $bundle_id = NULL,
    string $display_mode = NULL,
    ?EntityInterface $entity = NULL,
    string $operation = NULL
  ): DisplayInfoInterface {
    /** @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormInfoInterface $form_info */
    $form_info = parent::build($entity_type_id, $bundle_id, $display_mode);
    if (isset($entity)) {
      $form_info->setEntity($entity);
    }

    // @TODO Move to parent class if view info support will be added.
    $this->addThirdPartySettings($form_info);

    $event = new FormInfoBuildEvent($form_info, $operation);
    $this->eventDispatcher->dispatch(
      DisplayInfoEvents::FORM_INFO_BUILD,
      $event
    );

    return $form_info;
  }

  /**
   * {@inheritdoc}
   */
  protected function getVisibleComponents(
    DisplayInfoInterface $display_info
  ): array {
    $components = parent::getVisibleComponents(
      $display_info
    );

    // Hide the revision information from the form info/data.
    // @todo Should we just make it configurable and set it up for all the form
    //   modes.
    $entity_type = $this->entityTypeManager
      ->getDefinition($display_info->getEntityTypeId());
    if (
      $entity_type->isRevisionable()
      && $entity_type instanceof ContentEntityTypeInterface
    ) {
      $revision_metadata_keys = $entity_type->getRevisionMetadataKeys(FALSE);
      $revision_metadata_keys += static::DEFAULT_REVISION_METADATA_KEYS;
      foreach ($revision_metadata_keys as $field_name) {
        unset($components[$field_name]);
      }
    }

    return $components;
  }

}
