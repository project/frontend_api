<?php

namespace Drupal\frontend_api\Rest\File;

use Drupal\file\FileInterface;

/**
 * Interface of a file entity normalizer for the REST app.
 *
 * It provides a reusable way to expose the file entity to the front app either
 * as a value in the form data or after uploading the file to the upload REST
 * resource.
 */
interface FileNormalizerInterface {

  /**
   * Normalizes the file for the front app.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file to normalize.
   *
   * @return array
   *   The normalized representation of the file.
   */
  public function normalizeFile(FileInterface $file): array;

}
