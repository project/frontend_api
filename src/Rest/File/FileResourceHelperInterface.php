<?php

namespace Drupal\frontend_api\Rest\File;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\file\FileInterface;

/**
 * Provides an interface of a file resource helper.
 */
interface FileResourceHelperInterface {

  /**
   * Load field definition instance.
   *
   * @param string $entity_type_id
   *   Entity type ID.
   * @param string $bundle
   *   Entity bundle.
   * @param string $field_name
   *   Field name.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   *   Field definition.
   */
  public function getFieldDefinition(
    $entity_type_id,
    $bundle,
    $field_name
  );

  /**
   * Validates the file.
   *
   * @param \Drupal\file\FileInterface $file
   *   File entity.
   * @param array $validators
   *   The file validators array.
   *
   * @return array
   *   Array with errors if exists.
   */
  public function validate(
    FileInterface $file,
    array $validators
  );

  /**
   * Returns file validators.
   *
   * @param array $settings
   *   The file settings array.
   *
   * @return array
   *   The file validators array.
   */
  public function getFileValidators(array $settings): array;

  /**
   * Returns file validators from field definition.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The file field definition.
   *
   * @return array
   *   The file validators array.
   */
  public function getFileValidatorsFromField(
    FieldDefinitionInterface $field_definition
  ): array;

  /**
   * Determines the URI for a file field.
   *
   * @param array $settings
   *   Field settings.
   *
   * @return string
   *   File directory URI.
   */
  public function getUploadLocation(array $settings);

  /**
   * Generates a lock ID based on the file URI.
   *
   * @param string $file_uri
   *   File URI.
   *
   * @return string
   *   Generated lock ID.
   */
  public function generateLockIdFromFileUri($file_uri);

  /**
   * Returns max upload file size.
   *
   * @param array $settings
   *   Field settings.
   *
   * @return int
   *   Max file size in bytes.
   */
  public function getMaxFileSize(array $settings): int;

}
