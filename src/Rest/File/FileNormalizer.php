<?php

namespace Drupal\frontend_api\Rest\File;

use Drupal\file\FileInterface;

/**
 * File entity normalizer for the REST app.
 *
 * @see \Drupal\frontend_api\Rest\File\FileNormalizerInterface
 */
class FileNormalizer implements FileNormalizerInterface {

  /**
   * The file ID key in the normalized array.
   */
  public const ID_KEY = 'entity_id';

  /**
   * The filename key in the normalized array.
   */
  public const FILENAME_KEY = 'filename';

  /**
   * The file URL key  in the normalized array.
   */
  public const URL_KEY = 'url';

  /**
   * {@inheritdoc}
   */
  public function normalizeFile(FileInterface $file): array {
    $url = file_create_url($file->getFileUri());

    return [
      static::ID_KEY => $file->id(),
      static::FILENAME_KEY => $file->getFilename(),
      static::URL_KEY => $url,
    ];
  }

}
