<?php

namespace Drupal\frontend_api\Rest\File;

use Drupal\Component\Utility\Bytes;
use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\Environment;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\FileInterface;
use Drupal\Component\Render\PlainTextOutput;
use Drupal\frontend_api\Rest\ResourceValidator\ResourceValidatorInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class provides helper methods for file resource.
 */
class FileResourceHelper implements FileResourceHelperInterface {

  /**
   * The max file size field setting.
   */
  public const MAX_FILE_SIZE_SETTING = 'max_filesize';

  /**
   * The allowed file extensions field setting.
   */
  public const FILE_EXTENSIONS_SETTING = 'file_extensions';

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The token replacement instance.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * ResourceValidator service.
   *
   * @var \Drupal\frontend_api\Rest\ResourceValidator\ResourceValidatorInterface
   */
  protected $resourceValidator;

  /**
   * A Constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Utility\Token $token
   *   The token replacement instance.
   * @param \Drupal\frontend_api\Rest\ResourceValidator\ResourceValidatorInterface $resource_validator
   *   The resource validator.
   */
  public function __construct(
    FileSystemInterface $file_system,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    Token $token,
    ResourceValidatorInterface $resource_validator
  ) {
    $this->fileSystem = $file_system;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->token = $token;
    $this->resourceValidator = $resource_validator;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldDefinition(
    $entity_type_id,
    $bundle,
    $field_name
  ) {
    $field_definitions = $this->entityFieldManager
      ->getFieldDefinitions($entity_type_id, $bundle);
    if (!isset($field_definitions[$field_name])) {
      throw new NotFoundHttpException(
        sprintf('Field "%s" does not exist', $field_name)
      );
    }

    /** @var \Drupal\Core\Field\FieldDefinitionInterface $field_definition */
    $field_definition = $field_definitions[$field_name];
    if ($field_definition->getSetting('target_type') !== 'file') {
      throw new AccessDeniedHttpException(
        sprintf('"%s" is not a file field', $field_name)
      );
    }

    $entity_access_control_handler = $this->entityTypeManager
      ->getAccessControlHandler($entity_type_id);
    $bundle = $this->entityTypeManager->getDefinition($entity_type_id)
      ->hasKey('bundle') ? $bundle : NULL;
    $access_result = $entity_access_control_handler->createAccess(
      $bundle, NULL, [], TRUE)->andIf(
      $entity_access_control_handler->fieldAccess(
        'edit', $field_definition, NULL, NULL, TRUE
      )
    );
    if (!$access_result->isAllowed()) {
      throw new AccessDeniedHttpException($access_result->getReason());
    }

    return $field_definition;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(
    FileInterface $file,
    array $validators
  ) {
    // Validate the file based on the field definition configuration.
    $file_validate = file_validate($file, $validators);

    $errors = [];
    if (!empty($file_validate)) {
      $message = implode("\n", array_map(function ($error) {
        return PlainTextOutput::renderFromHtml($error);
      }, $file_validate));

      $errors += ['file' => $message];
    }

    $errors += $this->resourceValidator->validate($file, []);

    return $errors;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileValidators(array $settings): array {
    $validators = [];
    $file_extensions_setting = static::FILE_EXTENSIONS_SETTING;
    // Add file size check.
    $validators['file_validate_size'] = [$this->getMaxFileSize($settings)];

    // Add the extension check.
    if (!empty($settings[$file_extensions_setting])) {
      $validators['file_validate_extensions'] = [$settings[$file_extensions_setting]];
    }

    return $validators;
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxFileSize(array $settings): int {
    $php_max_file_size = Environment::getUploadMaxSize();
    $max_filesize_setting = static::MAX_FILE_SIZE_SETTING;

    $field_max_file_size = $settings[$max_filesize_setting] ?? NULL;

    if ($field_max_file_size === NULL || $field_max_file_size === '') {
      return $php_max_file_size;
    }

    $field_max_file_size = Bytes::toInt($field_max_file_size);
    if ($field_max_file_size > $php_max_file_size) {
      return $php_max_file_size;
    }

    return $field_max_file_size;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileValidatorsFromField(
    FieldDefinitionInterface $field_definition
  ): array {
    $settings = $field_definition->getSettings();
    return $this->getFileValidators($settings);
  }

  /**
   * {@inheritdoc}
   */
  public function getUploadLocation(array $settings) {
    $destination = trim($settings['file_directory'], '/');

    // Replace tokens.
    $destination = PlainTextOutput::renderFromHtml(
      $this->token->replace($destination, [])
    );
    return $settings['uri_scheme'] . '://' . $destination;
  }

  /**
   * {@inheritdoc}
   */
  public function generateLockIdFromFileUri($file_uri) {
    return 'file:rest:' . Crypt::hashBase64($file_uri);
  }

}
