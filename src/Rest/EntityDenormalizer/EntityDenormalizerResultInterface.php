<?php

namespace Drupal\frontend_api\Rest\EntityDenormalizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\frontend_api\Rest\Denormalizer\DenormalizerResultInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataInterface;

/**
 * Interface of a result of an entity de-normalization.
 *
 * @see \Drupal\frontend_api\Rest\EntityDenormalizer\EntityDenormalizerInterface
 */
interface EntityDenormalizerResultInterface extends DenormalizerResultInterface {

  /**
   * Returns the entity form data used for de-normalization.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataInterface
   *   The entity form data.
   */
  public function getFormData(): FormDataInterface;

  /**
   * Returns the de-normalized entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity.
   */
  public function getEntity(): ContentEntityInterface;

  /**
   * Returns the list of submitted field names found on the request data.
   *
   * @return string[]
   *   The list of submitted field names.
   */
  public function getSubmittedFieldNames(): array;

  /**
   * Sets the list of submitted field names.
   *
   * @param string[] $field_names
   *   The field names.
   *
   * @return static
   */
  public function setSubmittedFieldNames(
    array $field_names
  ): EntityDenormalizerResultInterface;

  /**
   * Gets the list of extra field names.
   *
   * @return string[]
   *   Array with extra field names.
   */
  public function getExtraFieldNames(): array;

  /**
   * Collects the list of extra field names.
   *
   * @param array $extra_field_names
   *   Array extra field names.
   *
   * @return static
   */
  public function collectExtraFieldNames(
    array $extra_field_names
  ): EntityDenormalizerResultInterface;

  /**
   * Returns field names that exist on the entity form.
   *
   * Merges submitted field names with extra field ones.
   *
   * @return array
   *   Array with merged field names.
   */
  public function getFormFieldNames(): array;

}
