<?php

namespace Drupal\frontend_api\Rest\EntityDenormalizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\frontend_api\Rest\Denormalizer\DenormalizerResultBase;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataInterface;

/**
 * Represents result of an entity de-normalization.
 */
class EntityDenormalizerResult extends DenormalizerResultBase implements EntityDenormalizerResultInterface {

  /**
   * The de-normalized entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entity;

  /**
   * The entity form data used for de-normalization.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataInterface
   */
  protected $formData;

  /**
   * The list of submitted field names.
   *
   * @var string[]
   */
  protected $submittedFieldNames = [];

  /**
   * The list of extra field names.
   *
   * @var string[]
   */
  protected $extraFieldNames = [];

  /**
   * A constructor.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The de-normalized entity.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataInterface $form_data
   *   The entity form data used for de-normalization.
   */
  public function __construct(
    ContentEntityInterface $entity,
    FormDataInterface $form_data
  ) {
    $this->entity = $entity;
    $this->formData = $form_data;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity(): ContentEntityInterface {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormData(): FormDataInterface {
    return $this->formData;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmittedFieldNames(): array {
    return $this->submittedFieldNames;
  }

  /**
   * {@inheritdoc}
   */
  public function setSubmittedFieldNames(
    array $field_names
  ): EntityDenormalizerResultInterface {
    $this->submittedFieldNames = $field_names;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getExtraFieldNames(): array {
    return $this->extraFieldNames;
  }

  /**
   * {@inheritdoc}
   */
  public function collectExtraFieldNames(
    array $extra_field_names
  ): EntityDenormalizerResultInterface {
    foreach ($extra_field_names as $extra_field_name) {
      $this->extraFieldNames[] = $extra_field_name;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormFieldNames(): array {
    return array_merge(
      $this->getFormData()->getFieldNames(),
      $this->getExtraFieldNames()
    );
  }

}
