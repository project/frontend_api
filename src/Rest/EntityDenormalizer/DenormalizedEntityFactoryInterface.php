<?php

namespace Drupal\frontend_api\Rest\EntityDenormalizer;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Interface for a factory of the entity created during de-normalization.
 */
interface DenormalizedEntityFactoryInterface {

  /**
   * Creates an entity.
   *
   * @param string $entity_type_id
   *   The entity type ID of the created entity.
   * @param array $data
   *   The un-serialized entity data to parse the bundle from.
   * @param string|null $bundle_id
   *   The bundle ID to use. If specified, overrides the bundle ID found in the
   *   entity data.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The created entity.
   */
  public function create(
    string $entity_type_id,
    array &$data,
    string $bundle_id = NULL
  ): ContentEntityInterface;

}
