<?php

namespace Drupal\frontend_api\Rest\EntityDenormalizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\frontend_api\Exception\DenormalizationException;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldNormalizerManagerInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataBuilderInterface;
use Drupal\frontend_api\Rest\NormalizerContextKeys;
use Drupal\frontend_api\Rest\ResourceValidator\ResourceValidatorInterface;
use Drupal\frontend_api\Rest\UnserializedDataInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Helper class that de-normalizes the entity.
 *
 * De-normalizes the un-serialized data using normalizer plugins and optionally
 * validates the entity.
 *
 * @see \Drupal\frontend_api\Rest\EntityDenormalizer\EntityDenormalizerInterface
 */
class EntityDenormalizer implements EntityDenormalizerInterface {

  /**
   * The context key of the entity validation boolean flag.
   */
  protected const ENTITY_VALIDATION_CONTEXT_KEY = NormalizerContextKeys::ENTITY_VALIDATION;

  /**
   * The entity form data builder.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataBuilderInterface
   */
  protected $formDataBuilder;

  /**
   * The fields de-normalizer.
   *
   * @var \Drupal\frontend_api\Rest\EntityDenormalizer\EntityFieldsDenormalizerInterface
   */
  protected $fieldsDenormalizer;

  /**
   * Plugin manager for extra fields.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormExtraFieldNormalizerManagerInterface
   */
  protected $extraFieldDenormalizerManager;

  /**
   * The de-normalized entity factory.
   *
   * @var \Drupal\frontend_api\Rest\EntityDenormalizer\DenormalizedEntityFactoryInterface
   */
  protected $entityFactory;

  /**
   * The resource validator.
   *
   * @var \Drupal\frontend_api\Rest\ResourceValidator\ResourceValidatorInterface
   */
  protected $resourceValidator;

  /**
   * The entity type ID to de-normalize.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * The entity to de-normalize the data into.
   *
   * NULL in case an entity should be created.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface|null
   */
  protected $entity;

  /**
   * The bundle ID to create the entity with.
   *
   * @var string|null
   */
  protected $bundleId;

  /**
   * The form mode name.
   *
   * @var string|null
   */
  protected $formModeName;

  /**
   * The entity form data.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataInterface|null
   */
  protected $formData;

  /**
   * The un-serialized request data to de-normalize.
   *
   * @var array|null
   */
  protected $requestData;

  /**
   * The un-serialization format.
   *
   * @var string|null
   */
  protected $format = NULL;

  /**
   * The un-serialization context.
   *
   * @var array
   */
  protected $context = [];

  /**
   * Validation flag.
   *
   * @var bool
   */
  protected $validationEnabled = FALSE;

  /**
   * A constructor.
   *
   * @param string $entity_type_id
   *   The entity type ID to de-normalize.
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $entity
   *   The entity to de-normalize the data into.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataBuilderInterface $form_data_builder
   *   The entity form data builder.
   * @param \Drupal\frontend_api\Rest\EntityDenormalizer\EntityFieldsDenormalizerInterface $fields_denormalizer
   *   The entity fields de-normalizer.
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayExtraFieldNormalizerManagerInterface $extra_field_denormalizer_manager
   *   Plugin manager responsible for extra fields.
   * @param \Drupal\frontend_api\Rest\EntityDenormalizer\DenormalizedEntityFactoryInterface $entity_factory
   *   The de-normalized entity factory.
   * @param \Drupal\frontend_api\Rest\ResourceValidator\ResourceValidatorInterface $resource_validator
   *   The resource validator.
   */
  public function __construct(
    string $entity_type_id,
    ?ContentEntityInterface $entity,
    FormDataBuilderInterface $form_data_builder,
    EntityFieldsDenormalizerInterface $fields_denormalizer,
    DisplayExtraFieldNormalizerManagerInterface $extra_field_denormalizer_manager,
    DenormalizedEntityFactoryInterface $entity_factory,
    ResourceValidatorInterface $resource_validator
  ) {
    $this->entityTypeId = $entity_type_id;
    $this->entity = $entity;

    $this->formDataBuilder = $form_data_builder;
    $this->fieldsDenormalizer = $fields_denormalizer;
    $this->extraFieldDenormalizerManager = $extra_field_denormalizer_manager;
    $this->entityFactory = $entity_factory;
    $this->resourceValidator = $resource_validator;
  }

  /**
   * Returns the entity type ID of the de-normalized entity.
   *
   * @return string
   *   The entity type ID.
   */
  protected function getEntityTypeId(): string {
    return $this->entityTypeId;
  }

  /**
   * {@inheritdoc}
   */
  public function setFormModeName(string $form_mode_name) {
    $this->formModeName = $form_mode_name;
    return $this;
  }

  /**
   * Returns the form mode name.
   *
   * @return string
   *   The form mode name.
   */
  protected function getFormModeName(): string {
    if (!isset($this->formModeName)) {
      throw new \LogicException(
        'Either form data or the form mode name must be set.'
      );
    }

    return $this->formModeName;
  }

  /**
   * Builds the entity form data using the form mode name.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface
   *   The built entity form data.
   */
  protected function buildFormData(): DisplayInfoInterface {
    $entity = $this->getEntity();
    $form_mode_name = $this->formModeName;

    return $this->formDataBuilder->build(
      $entity,
      $form_mode_name
    );
  }

  /**
   * Returns the entity form data.
   *
   * @return \Drupal\frontend_api\Rest\EntityDisplayInfo\Base\DisplayInfoInterface
   *   The entity form data.
   */
  protected function getFormData(): DisplayInfoInterface {
    if (!isset($this->formData)) {
      $this->formData = $this->buildFormData();
    }

    return $this->formData;
  }

  /**
   * The de-serialization format.
   *
   * @return string|null
   *   The format.
   */
  protected function getFormat() {
    return $this->format;
  }

  /**
   * {@inheritdoc}
   */
  public function setFormat(?string $format) {
    $this->format = $format;
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\rest\RequestHandler::deserialize()
   */
  public function setFormatFromRequest(Request $request) {
    $format = $request->getContentType();
    $this->setFormat($format);

    return $this;
  }

  /**
   * Returns the un-serialization context.
   *
   * @return array
   *   The context.
   */
  protected function getContext(): array {
    $context = $this->context;
    $context += [
      static::ENTITY_VALIDATION_CONTEXT_KEY => $this->isValidationEnabled(),
    ];
    return $context;
  }

  /**
   * {@inheritdoc}
   */
  public function setContext(array $context): EntityDenormalizerInterface {
    $this->context = $context;
    return $this;
  }

  /**
   * Returns the entity for de-normalization.
   *
   * If there is no entity, a new one will be created.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity.
   */
  protected function getEntity(): ContentEntityInterface {
    if (!isset($this->entity)) {
      $data =& $this->getRequestData();
      $this->entity = $this->entityFactory
        ->create($this->entityTypeId, $data, $this->bundleId);
      unset($data);
    }

    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setBundleId(string $bundle_id): EntityDenormalizerInterface {
    $this->bundleId = $bundle_id;
    return $this;
  }

  /**
   * Returns the request data to de-normalize.
   *
   * @return array
   *   The request, returned by reference.
   */
  protected function &getRequestData() {
    if (!isset($this->requestData)) {
      throw new \LogicException('Request data must be set for denormalization.');
    }

    return $this->requestData;
  }

  /**
   * {@inheritdoc}
   */
  public function setRequestData(array $request_data) {
    $this->requestData = $request_data;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setRequestDataFromUnserialized(
    UnserializedDataInterface $unserialized
  ): EntityDenormalizerInterface {
    $this->setRequestData($unserialized->getData());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function enableValidation(): EntityDenormalizerInterface {
    $this->validationEnabled = TRUE;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function disableValidation(): EntityDenormalizerInterface {
    $this->validationEnabled = FALSE;
    return $this;
  }

  /**
   * Returns TRUE if the entity validation is enabled.
   *
   * @return bool
   *   TRUE for enabled validation.
   */
  protected function isValidationEnabled(): bool {
    return $this->validationEnabled;
  }

  /**
   * Creates the entity de-normalization result.
   *
   * @return \Drupal\frontend_api\Rest\EntityDenormalizer\EntityDenormalizerResultInterface
   *   The entity de-normalization result.
   */
  protected function createDenormalizerResult(): EntityDenormalizerResultInterface {
    $entity = $this->getEntity();
    $form_data = $this->getFormData();
    return new EntityDenormalizerResult($entity, $form_data);
  }

  /**
   * De-normalizes entity fields.
   *
   * @param \Drupal\frontend_api\Rest\EntityDenormalizer\EntityDenormalizerResultInterface $result
   *   The entity de-normalization result.
   */
  protected function denormalizeFields(
    EntityDenormalizerResultInterface $result
  ): void {
    $fields = $result->getFormData()
      ->getFields();
    $request_data = $this->getRequestData();
    $format = $this->getFormat();
    $context = $this->getContext();

    $fields_result = $this->fieldsDenormalizer->denormalizeFields(
      $fields,
      $request_data,
      $format,
      $context
    );
    $result->addErrors($fields_result->getErrors());

    // Do pretty much the same as core entity normalizer does to set the
    // _restSubmittedFields property.
    // @see \Drupal\serialization\Normalizer\EntityNormalizer::denormalize()
    $submitted_field_names = array_keys($request_data);
    $result->setSubmittedFieldNames($submitted_field_names);
  }

  /**
   * De-normalizes entity fields.
   *
   * @param \Drupal\frontend_api\Rest\EntityDenormalizer\EntityDenormalizerResultInterface $result
   *   The entity de-normalization result.
   *
   * @todo Move to a separate class same way as for fields, if necessary.
   */
  protected function denormalizeExtraFields(
    EntityDenormalizerResultInterface $result
  ): void {
    $form_data = $result->getFormData();

    $resuest_data = $this->getRequestData();
    $format = $this->getFormat();
    $context = $this->getContext();
    $entity = $this->getEntity();
    foreach ($form_data->getExtraFields() as $field) {
      $denormalizer = $this->extraFieldDenormalizerManager
        ->createDenormalizerByField($field, $format, $context);
      $extra_field_names = $denormalizer->getExtraFieldNames();
      $result->collectExtraFieldNames($extra_field_names);

      try {
        $denormalizer
          ->denormalizeExtraField($field, $entity, $resuest_data, $format, $context);
      }
      catch (DenormalizationException $e) {
        $errors = $e->getErrors();
        if (empty($errors)) {
          throw new \LogicException(sprintf(
            'Entity denormalizer %s specified no errors for the %s extra field.',
            $denormalizer->getPluginId(),
            $field->getFieldName()
          ));
        }

        $result->addErrors($errors);
      }
    }
  }

  /**
   * Validates the entity, if validation is enabled.
   *
   * Validation errors are added to the passed result.
   *
   * @param \Drupal\frontend_api\Rest\EntityDenormalizer\EntityDenormalizerResultInterface $result
   *   The entity de-normalization result.
   */
  protected function validateEntity(
    EntityDenormalizerResultInterface $result
  ): void {
    // Skip entity validation if it wasn't requested. Also skip it in case there
    // are issues with the data format, as otherwise we would try to validate
    // invalid data, errors could appear on wrong field deltas, etc.
    if (!$this->isValidationEnabled() || $result->hasErrors()) {
      return;
    }

    $entity = $result->getEntity();
    $submitted_fields = $result->getSubmittedFieldNames();
    $form_fields = $result->getFormFieldNames();
    $errors = $this->resourceValidator
      ->validate($entity, $submitted_fields, $form_fields);
    if (!empty($errors)) {
      $result->addErrors($errors);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize(): EntityDenormalizerResultInterface {
    $result = $this->createDenormalizerResult();
    $this->denormalizeFields($result);
    $this->denormalizeExtraFields($result);
    $this->validateEntity($result);

    return $result;
  }

}
