<?php

namespace Drupal\frontend_api\Rest\EntityDenormalizer;

use Drupal\frontend_api\Rest\UnserializedDataInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * An interface for the entity de-normalizer.
 *
 * It exists in order to take more control over the entity de-normalization, as
 * the core REST module doesn't provide the necessary flexibility. The issues
 * it solves:
 * - There is no way to pass any dynamic information to the core de-normalizer.
 *   The only way is to define a separate class per use case, specify it as a
 *   `serialization_class` on the REST plugin definition and write a
 *   de-normalizer service for it.
 * - There is no way to apply any changes to the submitted data before it gets
 *   to the entity.
 * - There is no other way to get the list of submitted fields except for using
 *   the workaround `_restSubmittedFields` that will be deleted any time soon.
 */
interface EntityDenormalizerInterface {

  /**
   * Sets the request data to de-normalize.
   *
   * @param array $request_data
   *   The request data after un-serialization.
   *
   * @return static
   */
  public function setRequestData(array $request_data);

  /**
   * Sets the entity form mode name.
   *
   * @param string $form_mode_name
   *   The form mode name.
   *
   * @return static
   */
  public function setFormModeName(string $form_mode_name);

  /**
   * Sets the de-serialization format.
   *
   * @param string|null $format
   *   The format.
   *
   * @return static
   */
  public function setFormat(string $format);

  /**
   * Sets the un-serialization format using the provided request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request to set the format from.
   *
   * @return static
   */
  public function setFormatFromRequest(Request $request);

  /**
   * Sets the un-serialization context.
   *
   * It is useful if you need to pass something from the resource to the
   * de-normalization plugin.
   *
   * @param array $context
   *   The context.
   *
   * @return static
   */
  public function setContext(array $context): EntityDenormalizerInterface;

  /**
   * Sets the request data to de-normalize from the un-serialized wrapper.
   *
   * @param \Drupal\frontend_api\Rest\UnserializedDataInterface $unserialized
   *   The un-serialized data wrapper.
   *
   * @return static
   */
  public function setRequestDataFromUnserialized(
    UnserializedDataInterface $unserialized
  ): EntityDenormalizerInterface;

  /**
   * Enables the entity validation.
   *
   * @return static
   */
  public function enableValidation(): EntityDenormalizerInterface;

  /**
   * Disables the entity validation.
   *
   * @return static
   */
  public function disableValidation(): EntityDenormalizerInterface;

  /**
   * Runs the entity de-normalization (and optional validation).
   *
   * @return \Drupal\frontend_api\Rest\EntityDenormalizer\EntityDenormalizerResultInterface
   *   The de-normalization and validation result.
   */
  public function denormalize(): EntityDenormalizerResultInterface;

  /**
   * Sets the entity bundle ID to use for entity creation.
   *
   * The bundle ID found in the un-serialized data is ignored if this method
   * is called.
   *
   * @param string $bundle_id
   *   The bundle ID.
   *
   * @return static
   *   Self.
   */
  public function setBundleId(string $bundle_id): EntityDenormalizerInterface;

}
