<?php

namespace Drupal\frontend_api\Rest\EntityDenormalizer;

use Drupal\frontend_api\Rest\Denormalizer\DenormalizerResultInterface;

/**
 * Provides de-normalizer for the entity fields.
 */
interface EntityFieldsDenormalizerInterface {

  /**
   * De-normalizes the entity fields from the raw submitted data.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\FormData\FormDataFieldInterface[] $fields
   *   The form data fields to denormalize the data to.
   * @param array $request_data
   *   The raw request data.
   * @param string|null $format
   *   The serialization format.
   * @param array $context
   *   The serialization context.
   *
   * @return \Drupal\frontend_api\Rest\Denormalizer\DenormalizerResultInterface
   *   The de-normalization result with errors, if any.
   */
  public function denormalizeFields(
    array $fields,
    array $request_data,
    string $format = NULL,
    array $context = []
  ): DenormalizerResultInterface;

}
