<?php

namespace Drupal\frontend_api\Rest\EntityDenormalizer;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Interface for a factory of entity de-normalizer instances.
 *
 * @see \Drupal\frontend_api\Rest\EntityDenormalizer\EntityDenormalizerInterface
 */
interface EntityDenormalizerFactoryInterface {

  /**
   * Creates an entity de-normalizer.
   *
   * @param string $entity_type_id
   *   The entity type ID to de-normalize.
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $entity
   *   The entity to de-normalize the data into.
   *
   * @return \Drupal\frontend_api\Rest\EntityDenormalizer\EntityDenormalizerInterface
   *   The de-normalizer.
   */
  public function create(
    string $entity_type_id,
    ContentEntityInterface $entity = NULL
  ): EntityDenormalizerInterface;

}
