<?php

namespace Drupal\frontend_api\Rest\EntityDenormalizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\frontend_api\ArgumentsPassingFactoryBase;

/**
 * Creates entity de-normalizer instances.
 */
class EntityDenormalizerFactory extends ArgumentsPassingFactoryBase implements EntityDenormalizerFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function create(
    string $entity_type_id,
    ContentEntityInterface $entity = NULL
  ): EntityDenormalizerInterface {
    return new EntityDenormalizer(
      $entity_type_id,
      $entity,
      ...$this->arguments
    );
  }

}
