<?php

namespace Drupal\frontend_api\Rest\EntityDenormalizer;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\frontend_api\Exception\DenormalizationException;

/**
 * Provides a factory for the entity created during de-normalization.
 */
class DenormalizedEntityFactory implements DenormalizedEntityFactoryInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The bundle info provider.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfoProvider;

  /**
   * A constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info_provider
   *   The bundle info provider.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeBundleInfoInterface $bundle_info_provider
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->bundleInfoProvider = $bundle_info_provider;
  }

  /**
   * {@inheritdoc}
   */
  public function create(
    string $entity_type_id,
    array &$data,
    string $bundle_id = NULL
  ): ContentEntityInterface {
    $entity_values = $this
      ->extractEntityData($entity_type_id, $data, $bundle_id);
    $entity = $this->entityTypeManager
      ->getStorage($entity_type_id)
      ->create($entity_values);
    if (!$entity instanceof ContentEntityInterface) {
      throw new \LogicException(
        'Only content entities are supported by entity denormalizer.'
      );
    }

    return $entity;
  }

  /**
   * Extracts the entity values required to create it.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param array $data
   *   The un-serialized entity data to parse the bundle from.
   * @param string|null $bundle_id
   *   The bundle ID to use. If specified, overrides the bundle ID found in the
   *   entity data.
   *
   * @return array
   *   Empty entity values with just a bundle set, if necessary.
   *
   * @see \Drupal\serialization\Normalizer\FieldableEntityNormalizerTrait::extractBundleData()
   *
   * @todo Refactor this method into multiple smaller ones.
   */
  protected function extractEntityData(
    string $entity_type_id,
    array &$data,
    string $bundle_id = NULL
  ): array {
    $entity_type = $this->entityTypeManager
      ->getDefinition($entity_type_id);

    // The getKey() method should return FALSE on non-existing key, but it
    // returns empty string for the "bundle" key (e.g. on Offer entity), so we
    // check for emptiness instead of equality to FALSE.
    $bundle_key = $entity_type->getKey('bundle');
    if (empty($bundle_key)) {
      return [];
    }

    // Extract the bundle ID if it has not been forced.
    if (!isset($bundle_id)) {
      $bundle_id = $this->extractBundleId($entity_type, $bundle_key, $data);
    }

    // Unset the bundle from the data, as it is already set on new entity.
    unset($data[$bundle_key]);

    return [$bundle_key => $bundle_id];
  }

  /**
   * Extracts the bundle ID from the un-serialized entity data.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param string $bundle_key
   *   The bundle key in the entity.
   * @param array $data
   *   The un-serialized entity data to parse the bundle from.
   *
   * @return string
   *   The bundle ID.
   *
   * @throws \Drupal\frontend_api\Exception\DenormalizationException
   */
  protected function extractBundleId(
    EntityTypeInterface $entity_type,
    string $bundle_key,
    array $data
  ): string {
    $entity_type_id = $entity_type->id();

    // Get the ID key from the base field definition for the bundle key or
    // default to 'value'.
    $base_field_definitions = $this->entityFieldManager
      ->getBaseFieldDefinitions($entity_type_id);
    $bundle_field_definitions = $base_field_definitions[$bundle_key] ?? NULL;
    if (isset($bundle_field_definitions)) {
      $property_name = $bundle_field_definitions->getFieldStorageDefinition()
        ->getMainPropertyName();
    }
    else {
      $property_name = 'value';
    }

    // Get the bundle value from the request data. Try different paths from
    // full `field_name.0.column_name` to just `field_name`.
    $candidate_paths = [
      [$bundle_key, 0, $property_name],
      [$bundle_key, $property_name],
      [$bundle_key, 0],
      [$bundle_key],
    ];
    $exists = NULL;
    foreach ($candidate_paths as $path) {
      $bundle_id = NestedArray::getValue($data, $path, $exists);
      if ($exists) {
        break;
      }
    }
    if (!isset($bundle_id)) {
      throw new DenormalizationException([
        $bundle_key => $this->t('The entity bundle is not specified.'),
      ]);
    }

    // Make sure the submitted bundle is a valid bundle for the entity type.
    $bundles_info = $this->bundleInfoProvider->getBundleInfo($entity_type_id);
    if (!isset($bundles_info[$bundle_id])) {
      throw new DenormalizationException([
        $bundle_key => $this->t(
          "The entity bundle @bundle doesn't exist.",
          [
            '@bundle' => $bundle_id,
          ]
        ),
      ]);
    }

    return $bundle_id;
  }

}
