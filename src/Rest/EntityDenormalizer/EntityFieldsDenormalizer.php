<?php

namespace Drupal\frontend_api\Rest\EntityDenormalizer;

use Drupal\frontend_api\Exception\DenormalizationException;
use Drupal\frontend_api\Rest\Denormalizer\DenormalizerResult;
use Drupal\frontend_api\Rest\Denormalizer\DenormalizerResultInterface;
use Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldNormalizerManagerInterface;

/**
 * Provides de-normalizer for the entity fields.
 */
class EntityFieldsDenormalizer implements EntityFieldsDenormalizerInterface {

  /**
   * The plugin manager of the form data field (de-)normalizers.
   *
   * @var \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldNormalizerManagerInterface
   */
  protected $denormalizerManager;

  /**
   * A constructor.
   *
   * @param \Drupal\frontend_api\Rest\EntityDisplayInfo\Form\FormFieldNormalizerManagerInterface $denormalizer_manager
   *   The plugin manager of the form data field (de-)normalizers.
   */
  public function __construct(
    FormFieldNormalizerManagerInterface $denormalizer_manager
  ) {
    $this->denormalizerManager = $denormalizer_manager;
  }

  /**
   * Creates an instance of the de-normalizer result.
   *
   * @return \Drupal\frontend_api\Rest\Denormalizer\DenormalizerResultInterface
   *   The result instance created.
   */
  protected function createDenormalizerResult(): DenormalizerResultInterface {
    return new DenormalizerResult();
  }

  /**
   * {@inheritdoc}
   */
  public function denormalizeFields(
    array $fields,
    array $request_data,
    string $format = NULL,
    array $context = []
  ): DenormalizerResultInterface {
    $result = $this->createDenormalizerResult();

    foreach ($fields as $field) {
      $denormalizer = $this->denormalizerManager
        ->createDenormalizerByField($field, $format, $context);

      try {
        $denormalizer
          ->denormalizeFieldValue($field, $request_data, $format, $context);
      }
      catch (DenormalizationException $e) {
        $errors = $e->getErrors();
        if (empty($errors)) {
          $component_data = $field->getComponentData();
          $field_definition = $field->getFieldDefinition();
          throw new \LogicException(sprintf(
            'Entity denormalizer %s specified no errors for the %s field with %s widget.',
            $denormalizer->getPluginId(),
            $field_definition->getName(),
            $component_data['type']
          ));
        }

        $result->addErrors($errors);
      }
    }

    return $result;
  }

}
