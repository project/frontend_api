<?php

namespace Drupal\frontend_api\Rest;

/**
 * A response that does not contain cacheability metadata.
 *
 * @see \Drupal\rest\ResourceResponse
 */
class ModifiedResourceResponse extends ResourceResponseBase {

}
