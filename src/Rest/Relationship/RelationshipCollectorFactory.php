<?php

namespace Drupal\frontend_api\Rest\Relationship;

use Drupal\frontend_api\ArgumentsPassingFactoryBase;

/**
 * Provides a factory of the relationship collectors.
 */
class RelationshipCollectorFactory extends ArgumentsPassingFactoryBase implements RelationshipCollectorFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function get(): RelationshipCollectorInterface {
    return new RelationshipCollector(...$this->arguments);
  }

}
