<?php

namespace Drupal\frontend_api\Rest\Relationship;

/**
 * Provides an interface for a factory of a relationship collectors.
 */
interface RelationshipCollectorFactoryInterface {

  /**
   * Returns the new relationship collector.
   *
   * @return \Drupal\frontend_api\Rest\Relationship\RelationshipCollectorInterface
   *   The relationship collector.
   */
  public function get(): RelationshipCollectorInterface;

}
