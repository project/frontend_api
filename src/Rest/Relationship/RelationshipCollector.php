<?php

namespace Drupal\frontend_api\Rest\Relationship;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides a relationship collector.
 *
 * @see \Drupal\frontend_api\Rest\Relationship\RelationshipCollectorInterface
 */
class RelationshipCollector implements RelationshipCollectorInterface {

  /**
   * The list of pending entity IDs grouped by type.
   *
   * @var array
   */
  protected $pendingEntityIds = [];

  /**
   * The list of entity ID that were attempted to be loaded, grouped by type.
   *
   * We need a separate list from the entities in order to avoid trying the same
   * non-loadable entity ID over and over again.
   *
   * @var array
   */
  protected $loadedEntityIds = [];

  /**
   * The list of loaded entities grouped by type.
   *
   * @var array
   */
  protected $entities = [];

  /**
   * The view modes per entity type ID.
   *
   * @var string[]
   */
  protected $viewModes = [];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function setViewMode(
    string $entity_type_id,
    string $view_mode
  ): RelationshipCollectorInterface {
    $existing_view_mode = $this->viewModes[$entity_type_id] ?? NULL;
    if (isset($existing_view_mode) && $existing_view_mode !== $view_mode) {
      throw new \InvalidArgumentException(sprintf(
        'The %s view mode is already set for the %s and it can not be changed to %s.',
        $existing_view_mode,
        $entity_type_id,
        $view_mode
      ));
    }

    $this->viewModes[$entity_type_id] = $view_mode;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addEntityIds(
    string $entity_type_id,
    array $ids
  ): RelationshipCollectorInterface {
    // Convert the list of IDs to a map with TRUE values, filter out the IDs
    // we already tried to load.
    $ids_map = array_fill_keys($ids, TRUE);
    $ids_map = array_diff_key(
      $ids_map,
      $this->loadedEntityIds[$entity_type_id] ?? []
    );
    if (empty($ids_map)) {
      return $this;
    }

    $this->pendingEntityIds += [
      $entity_type_id => [],
    ];
    $this->pendingEntityIds[$entity_type_id] += $ids_map;

    // Also init the list of loaded entities and IDs, if not already.
    $this->entities += [
      $entity_type_id => [],
    ];
    $this->loadedEntityIds += [
      $entity_type_id => [],
    ];

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function loadEntities(): array {
    $loaded = [];
    foreach ($this->pendingEntityIds as $entity_type_id => $entity_ids_map) {
      $loaded[$entity_type_id] = $this->entityTypeManager
        ->getStorage($entity_type_id)
        ->loadMultiple(array_keys($entity_ids_map));

      $this->entities[$entity_type_id] += $loaded[$entity_type_id];
      $this->loadedEntityIds[$entity_type_id] += $entity_ids_map;
    }
    $this->pendingEntityIds = [];

    return $loaded;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntities(): array {
    return $this->entities;
  }

  /**
   * {@inheritdoc}
   */
  public function getViewModes(): array {
    return $this->viewModes;
  }

}
