<?php

namespace Drupal\frontend_api\Rest\Relationship;

/**
 * Provides an interface for a relationship collector.
 *
 * Its purpose is to accumulate the list of entity IDs and view mode names in
 * order to load a list of entities at once and to retrieve an entities list
 * grouped by entity type from nested structure of entities.
 */
interface RelationshipCollectorInterface {

  /**
   * Sets view mode for the passed entity type.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $view_mode
   *   The view mode.
   *
   * @return static
   *   Self.
   */
  public function setViewMode(
    string $entity_type_id,
    string $view_mode
  ): RelationshipCollectorInterface;

  /**
   * Register entity IDs of a specified type.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param array $ids
   *   The list of IDs.
   *
   * @return static
   *   Self.
   */
  public function addEntityIds(
    string $entity_type_id,
    array $ids
  ): RelationshipCollectorInterface;

  /**
   * Loads pending entities.
   *
   * It only loads new entities added after the previous call.
   *
   * @return array
   *   The list of new entities loaded, grouped by entity type. Empty in case
   *   there were no pending entities. In case the entities couldn't be loaded,
   *   their entity type is still presented in the results.
   */
  public function loadEntities(): array;

  /**
   * Returns the accumulated list of entities grouped by entity type.
   *
   * @return array
   *   The entities list grouped by the entity type ID.
   */
  public function getEntities(): array;

  /**
   * Returns view modes per entity type ID.
   *
   * @return array
   *   Key is the entity type ID, value is the view mode name.
   */
  public function getViewModes(): array;

}
