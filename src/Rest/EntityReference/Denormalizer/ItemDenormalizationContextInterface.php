<?php

namespace Drupal\frontend_api\Rest\EntityReference\Denormalizer;

/**
 * Provides context for the entity reference item de-normalization.
 *
 * @see \Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer\EntityReferenceAutocompleteNormalizer::denormalizeEntityReferenceItemLiteral()
 */
interface ItemDenormalizationContextInterface {

  /**
   * Checks if referenced entity auto-creation is enabled.
   *
   * @return bool
   *   TRUE if entity may be created automatically.
   */
  public function isAutocreateEnabled(): bool;

  /**
   * Returns the bundle ID of the automatically created entity to reference.
   *
   * @return string
   *   The bundle ID.
   */
  public function getAutocreateBundleId(): string;

  /**
   * Returns the user ID to assign as an author for the auto-created entity.
   *
   * @return int
   *   The author ID.
   */
  public function getAutocreateAuthorId(): int;

  /**
   * Sets the bundle ID of the automatically created entity to reference.
   *
   * @param string $autocreateBundle
   *   The bundle ID.
   *
   * @return static
   *   Self.
   */
  public function setAutocreateBundleId(
    string $autocreateBundle
  ): ItemDenormalizationContextInterface;

  /**
   * Sets the user ID to assign as an author for the auto-created entity.
   *
   * @param int $autocreateAuthorId
   *   The author ID.
   *
   * @return static
   *   Self.
   */
  public function setAutocreateAuthorId(
    int $autocreateAuthorId
  ): ItemDenormalizationContextInterface;

  /**
   * Returns the entity type ID of the auto-created entity to reference.
   *
   * @return string
   *   The entity type ID.
   */
  public function getAutocreateEntityTypeId(): string;

  /**
   * Sets the entity type ID of the auto-created entity to reference.
   *
   * @param string $autocreateEntityTypeId
   *   The entity type ID.
   *
   * @return static
   *   Self.
   */
  public function setAutocreateEntityTypeId(
    string $autocreateEntityTypeId
  ): ItemDenormalizationContextInterface;

}
