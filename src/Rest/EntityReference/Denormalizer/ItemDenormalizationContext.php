<?php

namespace Drupal\frontend_api\Rest\EntityReference\Denormalizer;

/**
 * Provides context for the entity reference item de-normalization.
 */
class ItemDenormalizationContext implements ItemDenormalizationContextInterface {

  /**
   * TRUE if the entity auto-creation is enabled on the field.
   *
   * @var bool
   */
  protected $isAutocreateEnabled;

  /**
   * The bundle ID of the automatically created entity to reference.
   *
   * @var string
   */
  protected $autocreateBundleId;

  /**
   * The user ID to assign as an author for the auto-created entity.
   *
   * @var int
   */
  protected $autocreateAuthorId;

  /**
   * The callback to call once in order to populate auto-creation properties.
   *
   * @var callable
   */
  protected $autocreateInitCallback;

  /**
   * The entity type ID of the auto-created entity to reference.
   *
   * @var string
   */
  protected $autocreateEntityTypeId;

  /**
   * Whether the auto-create options were initialized by the callback or not.
   *
   * @var bool
   */
  protected $autocreateInitialized = FALSE;

  /**
   * A constructor.
   *
   * @param bool $isAutocreateEnabled
   *   TRUE if the entity auto-creation is enabled.
   * @param callable $autocreateInitCallback
   *   The callback to call once in order to populate auto-creation properties.
   */
  public function __construct(
    bool $isAutocreateEnabled,
    callable $autocreateInitCallback
  ) {
    $this->isAutocreateEnabled = $isAutocreateEnabled;
    $this->autocreateInitCallback = $autocreateInitCallback;
  }

  /**
   * Initializes the auto-creation properties.
   */
  protected function initAutocreate(): void {
    if ($this->autocreateInitialized) {
      return;
    }

    if (!$this->isAutocreateEnabled) {
      throw new \LogicException('Auto-creation is disabled.');
    }

    ($this->autocreateInitCallback)($this);
    $this->autocreateInitialized = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isAutocreateEnabled(): bool {
    return $this->isAutocreateEnabled;
  }

  /**
   * {@inheritdoc}
   */
  public function getAutocreateBundleId(): string {
    $this->initAutocreate();
    return $this->autocreateBundleId;
  }

  /**
   * {@inheritdoc}
   */
  public function getAutocreateAuthorId(): int {
    $this->initAutocreate();
    return $this->autocreateAuthorId;
  }

  /**
   * {@inheritdoc}
   */
  public function getAutocreateEntityTypeId(): string {
    $this->initAutocreate();
    return $this->autocreateEntityTypeId;
  }

  /**
   * {@inheritdoc}
   */
  public function setAutocreateBundleId(string $autocreateBundleId): ItemDenormalizationContextInterface {
    $this->autocreateBundleId = $autocreateBundleId;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setAutocreateAuthorId(int $autocreateAuthorId): ItemDenormalizationContextInterface {
    $this->autocreateAuthorId = $autocreateAuthorId;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setAutocreateEntityTypeId(string $autocreateEntityTypeId): ItemDenormalizationContextInterface {
    $this->autocreateEntityTypeId = $autocreateEntityTypeId;
    return $this;
  }

}
