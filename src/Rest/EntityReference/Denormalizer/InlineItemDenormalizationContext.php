<?php

namespace Drupal\frontend_api\Rest\EntityReference\Denormalizer;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides context for the inline entity form item de-normalization.
 *
 * @see \Drupal\frontend_api\Rest\EntityReference\Denormalizer\InlineItemDenormalizationContextInterface
 */
class InlineItemDenormalizationContext implements InlineItemDenormalizationContextInterface {

  /**
   * The entity type ID of the referenced entity.
   *
   * @var string
   */
  protected $targetTypeId;

  /**
   * The bundle ID of the referenced entity.
   *
   * @var string|null
   */
  protected $targetBundleId = NULL;

  /**
   * The list of allowed bundle IDs of the target entity.
   *
   * Keys are bundle IDs, values are TRUE.
   *
   * @var array|null
   */
  protected $allowedTargetBundleIdMap;

  /**
   * The form mode name of the inline entity form.
   *
   * @var string
   */
  protected $formModeName = 'default';

  /**
   * The list of entities that are referenced on the field at the moment.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface[]
   */
  protected $referencedEntities = [];

  /**
   * The map of entity IDs that were met during de-normalization.
   *
   * Keys are entity IDs, values are TRUE.
   *
   * @var array
   */
  protected $metReferencedEntities = [];

  /**
   * TRUE if the entity validation is enabled.
   *
   * @var bool
   */
  protected $validation = FALSE;

  /**
   * The serialization format.
   *
   * @var string|null
   */
  protected $format = NULL;

  /**
   * The serialization context.
   *
   * @var array
   */
  protected $context = [];

  /**
   * A constructor.
   *
   * @param string $target_type_id
   *   The bundle ID of the referenced entity.
   */
  public function __construct(string $target_type_id) {
    $this->targetTypeId = $target_type_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetTypeId(): string {
    return $this->targetTypeId;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetBundleId(): string {
    return $this->targetBundleId;
  }

  /**
   * {@inheritdoc}
   */
  public function hasTargetBundleId(): bool {
    return isset($this->targetBundleId);
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetBundleId(
    string $target_bundle_id = NULL
  ): InlineItemDenormalizationContextInterface {
    $this->targetBundleId = $target_bundle_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetBundleIds(
    array $bundle_ids = NULL
  ): InlineItemDenormalizationContextInterface {
    if ($bundle_ids === NULL) {
      $this->allowedTargetBundleIdMap = NULL;
    }
    else {
      foreach ($bundle_ids as $bundle_id) {
        $this->allowedTargetBundleIdMap[$bundle_id] = TRUE;
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isTargetBundleIdAllowed(string $bundle_id): bool {
    if ($this->allowedTargetBundleIdMap === NULL) {
      // NULL means all the bundles are allowed.
      return TRUE;
    }

    return isset($this->allowedTargetBundleIdMap[$bundle_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormModeName(): string {
    return $this->formModeName;
  }

  /**
   * {@inheritdoc}
   */
  public function setFormModeName(
    string $form_mode_name
  ): InlineItemDenormalizationContextInterface {
    $this->formModeName = $form_mode_name;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setReferencedEntities(
    array $entities
  ): InlineItemDenormalizationContextInterface {
    $this->metReferencedEntities = [];

    $this->referencedEntities = [];
    foreach ($entities as $entity) {
      $this->referencedEntities[$entity->id()] = $entity;
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasReferencedEntity(int $entity_id): bool {
    return isset($this->referencedEntities[$entity_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getReferencedEntity(int $entity_id): ContentEntityInterface {
    return $this->referencedEntities[$entity_id];
  }

  /**
   * {@inheritdoc}
   */
  public function meetReferencedEntity(int $entity_id): void {
    $this->metReferencedEntities[$entity_id] = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getReferencedEntities(bool $unmet_only = FALSE): array {
    $entities = $this->referencedEntities;

    if ($unmet_only) {
      $entities = array_diff_key($entities, $this->metReferencedEntities);
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormat(): ?string {
    return $this->format;
  }

  /**
   * {@inheritdoc}
   */
  public function setFormat(
    string $format = NULL
  ): InlineItemDenormalizationContextInterface {
    $this->format = $format;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getContext(): array {
    return $this->context;
  }

  /**
   * {@inheritdoc}
   */
  public function setContext(
    array $context
  ): InlineItemDenormalizationContextInterface {
    $this->context = $context;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setValidation(
    bool $value
  ): InlineItemDenormalizationContextInterface {
    $this->validation = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isValidationEnabled(): bool {
    return $this->validation;
  }

}
