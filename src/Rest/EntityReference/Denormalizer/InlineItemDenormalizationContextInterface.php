<?php

namespace Drupal\frontend_api\Rest\EntityReference\Denormalizer;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides context for the inline entity form item de-normalization.
 *
 * @see \Drupal\frontend_api\Plugin\frontend_api\EntityDisplayInfo\FormFieldNormalizer\FrontInlineSimpleEntityNormalizerBase::denormalizeInlineEntity()
 */
interface InlineItemDenormalizationContextInterface {

  /**
   * Returns entity type ID of the referenced entity.
   *
   * @return string
   *   The target entity type ID.
   */
  public function getTargetTypeId(): string;

  /**
   * Returns bundle ID of the referenced entity to use on creation.
   *
   * @return string|null
   *   The bundle ID or NULL if entity doesn't have bundle or it's impossible
   *   to determine the bundle.
   */
  public function getTargetBundleId(): ?string;

  /**
   * Checks if the target bundle ID is set on the item context.
   *
   * @return bool
   *   TRUE if the target bundle ID is set.
   */
  public function hasTargetBundleId(): bool;

  /**
   * Sets bundle ID of the referenced entity to use on creation.
   *
   * @param string|null $target_bundle_id
   *   The bundle ID or NULL if entity doesn't have bundle or it's impossible
   *   to determine the bundle.
   *
   * @return static
   *   Self.
   */
  public function setTargetBundleId(
    string $target_bundle_id = NULL
  ): InlineItemDenormalizationContextInterface;

  /**
   * Enables/disables the entity validation.
   *
   * @param bool $value
   *   TRUE to enable referenced entity validation.
   *
   * @return static
   *   Self.
   */
  public function setValidation(
    bool $value
  ): InlineItemDenormalizationContextInterface;

  /**
   * Checks if entity validation is enabled.
   *
   * @return bool
   *   TRUE if validation of the referenced entity is enabled.
   */
  public function isValidationEnabled(): bool;

  /**
   * Sets the serialization format.
   *
   * @param string|null $format
   *   The format.
   *
   * @return static
   *   Self.
   */
  public function setFormat(
    string $format = NULL
  ): InlineItemDenormalizationContextInterface;

  /**
   * Returns the serialization format.
   *
   * @return string|null
   *   The format.
   */
  public function getFormat(): ?string;

  /**
   * Returns the form mode used for the inline entity form.
   *
   * @return string
   *   The form mode name.
   */
  public function getFormModeName(): string;

  /**
   * Sets the form mode used for the inline entity form.
   *
   * @param string $form_mode_name
   *   The form mode name.
   *
   * @return static
   *   Self.
   */
  public function setFormModeName(
    string $form_mode_name
  ): InlineItemDenormalizationContextInterface;

  /**
   * Returns the normalization context array.
   *
   * @return array
   *   The context array.
   */
  public function getContext(): array;

  /**
   * Sets the normalization context array.
   *
   * @param array $context
   *   The context array.
   *
   * @return static
   *   Self.
   */
  public function setContext(
    array $context
  ): InlineItemDenormalizationContextInterface;

  /**
   * Checks if the entity exists among the already referenced ones.
   *
   * @param int $entity_id
   *   The entity ID to check.
   *
   * @return bool
   *   TRUE if the entity with this ID exists on the field item list.
   */
  public function hasReferencedEntity(int $entity_id): bool;

  /**
   * Returns already referenced entity.
   *
   * @param int $entity_id
   *   The entity ID.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity.
   */
  public function getReferencedEntity(int $entity_id): ContentEntityInterface;

  /**
   * Sets the list of entities found on the existing field item list.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $entities
   *   The list of entities.
   *
   * @return static
   *   Self.
   */
  public function setReferencedEntities(
    array $entities
  ): InlineItemDenormalizationContextInterface;

  /**
   * Marks the referenced entity as the one met in the de-normalized data.
   *
   * @param int $entity_id
   *   The entity ID to mark as met.
   */
  public function meetReferencedEntity(int $entity_id): void;

  /**
   * Returns the list of entities that exist on the field item list.
   *
   * @param bool $unmet_only
   *   TRUE to only return entities that were not met during de-normalization.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   The list of entities keyed by their IDs.
   */
  public function getReferencedEntities(bool $unmet_only = FALSE): array;

  /**
   * Sets the target bundle IDs allowed for de-normalization.
   *
   * @param string[]|null $bundle_ids
   *   The list of bundle IDs to allow. NULL in case any bundle is allowed.
   *
   * @return static
   *   Self.
   */
  public function setTargetBundleIds(
    array $bundle_ids = NULL
  ): InlineItemDenormalizationContextInterface;

  /**
   * Checks if the bundle ID of the target entity is allowed.
   *
   * @param string $bundle_id
   *   The target bundle ID.
   *
   * @return bool
   *   TRUE if the bundle is allowed for de-normalization.
   */
  public function isTargetBundleIdAllowed(string $bundle_id): bool;

}
