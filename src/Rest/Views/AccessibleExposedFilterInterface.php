<?php

namespace Drupal\frontend_api\Rest\Views;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access interface for views exposed filter.
 *
 * Some user should have access to exposed filter according to some permissions.
 */
interface AccessibleExposedFilterInterface {

  /**
   * Checks filter for accessibility for current user.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   Current user.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $user): AccessResultInterface;

}
