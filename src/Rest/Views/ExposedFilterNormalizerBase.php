<?php

namespace Drupal\frontend_api\Rest\Views;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Plugin\views\ViewsHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base class for the normalizer of the Views exposed filter.
 *
 * @see \Drupal\frontend_api\Rest\Views\ExposedHandlerNormalizerInterface
 */
abstract class ExposedFilterNormalizerBase extends PluginBase implements ExposedHandlerNormalizerInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeHandler(
    ViewsHandlerInterface $handler,
    array $form,
    string $format = NULL,
    array $context = []
  ): array {
    if (!$handler instanceof FilterPluginBase) {
      return [];
    }

    $result = [];
    $exposed_info = $handler->exposedInfo();

    // @todo Support for exposed operator.
    $result += $this->normalizeValueWidget($handler, $form, $exposed_info);
    return $result;
  }

  /**
   * Normalizes the main filter value widget.
   *
   * @param \Drupal\views\Plugin\views\ViewsHandlerInterface $handler
   *   The Views handler.
   * @param array $form
   *   The form render array of the exposed form.
   * @param array $exposed_info
   *   The exposed info returned by the Views handler.
   *
   * @return array
   *   The normalization result merged into the main result.
   */
  protected function normalizeValueWidget(
    ViewsHandlerInterface $handler,
    array $form,
    array $exposed_info
  ): array {
    $identifier = $exposed_info['value'] ?? NULL;
    if (!isset($identifier)) {
      return [];
    }

    $element = $form[$identifier] ?? NULL;
    if (!isset($element)) {
      return [];
    }

    return [
      $identifier => $this->normalizeValueFormElement(
        $handler,
        $element,
        $exposed_info
      ),
    ];
  }

  /**
   * Normalizes the form element of the main filter value widget.
   *
   * Gets called only in case the form actually contains the element.
   *
   * @param \Drupal\views\Plugin\views\ViewsHandlerInterface $handler
   *   The Views handler.
   * @param array $element
   *   The form element to normalize.
   * @param array $exposed_info
   *   The exposed info returned by the Views handler.
   *
   * @return array
   *   The normalization result for this specific widget.
   */
  protected function normalizeValueFormElement(
    ViewsHandlerInterface $handler,
    array $element,
    array $exposed_info
  ): array {
    // Child classes should override and implement custom logic here.
    return [];
  }

}
