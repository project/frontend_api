<?php

namespace Drupal\frontend_api\Rest\Views;

use Drupal\views\Plugin\views\ViewsHandlerInterface;

/**
 * Interface of a normalizer for a Views exposed handler.
 *
 * The normalizer exposes the necessary information for the front app to build
 * the widget of a filter/pager/etc.
 */
interface ExposedHandlerNormalizerInterface {

  /**
   * Normalizes the passed handler.
   *
   * @param \Drupal\views\Plugin\views\ViewsHandlerInterface $handler
   *   The Views handler to normalize.
   * @param array $form
   *   The exposed form rendered by the view executable.
   * @param string|null $format
   *   The format.
   * @param array $context
   *   The serialization context.
   *
   * @return array
   *   Normalization result. Format:
   *   - keys are query variable names handled by this plugin,
   *   - values are widget information for every query variable.
   */
  public function normalizeHandler(
    ViewsHandlerInterface $handler,
    array $form,
    string $format = NULL,
    array $context = []
  ): array;

}
