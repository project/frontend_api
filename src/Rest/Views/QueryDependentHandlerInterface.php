<?php

namespace Drupal\frontend_api\Rest\Views;

/**
 * Defines an interface for a Views handler that depends on query execution.
 *
 * The handler could depend on query in order to build its exposed form (e.g.
 * to add result counts into the option labels or filter the list for items that
 * provide any content only) or for any other reason.
 */
interface QueryDependentHandlerInterface {

  /**
   * Checks if the handler need the view query to be executed.
   *
   * @return bool
   *   TRUE if the handler requires the query, FALSE otherwise.
   */
  public function needsQuery(): bool;

}
