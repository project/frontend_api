<?php

namespace Drupal\frontend_api\Rest\Views\Parameters;

use Drupal\views\ViewExecutable;

/**
 * Factory interface of a REST parameters for the front serializer style plugin.
 *
 * @see \Drupal\frontend_api\Rest\Views\Parameters\FrontSerializerParametersInterface
 */
interface FrontSerializerParametersFactoryInterface {

  /**
   * Creates REST parameters wrapper.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view.
   * @param array $options
   *   The options to control the parameters, e.g. whether the parameter is
   *   enabled or not.
   *
   * @return \Drupal\frontend_api\Rest\Views\Parameters\FrontSerializerParametersInterface
   *   The created parameters instance.
   */
  public function create(
    ViewExecutable $view,
    array $options
  ): FrontSerializerParametersInterface;

}
