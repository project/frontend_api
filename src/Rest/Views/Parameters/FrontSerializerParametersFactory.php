<?php

namespace Drupal\frontend_api\Rest\Views\Parameters;

use Drupal\views\ViewExecutable;

/**
 * Factory of a REST parameters for the front serializer style plugin.
 *
 * @see \Drupal\frontend_api\Rest\Views\Parameters\FrontSerializerParametersInterface
 */
class FrontSerializerParametersFactory implements FrontSerializerParametersFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function create(
    ViewExecutable $view,
    array $options
  ): FrontSerializerParametersInterface {
    return new FrontSerializerParameters($view, $options);
  }

}
