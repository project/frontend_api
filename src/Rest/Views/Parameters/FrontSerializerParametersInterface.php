<?php

namespace Drupal\frontend_api\Rest\Views\Parameters;

/**
 * Interface for a REST query parameters of the front serializer plugin.
 *
 * REST views may accept some GET query parameters that could change the view
 * behavior or returned data. The interface wraps the logic of these parameters.
 *
 * @see \Drupal\frontend_api\Plugin\views\style\FrontSerializer
 */
interface FrontSerializerParametersInterface {

  /**
   * Checks if the view query should be executed.
   *
   * For example, the query execution may be not necessary in case the client
   * asks for exposed form only.
   *
   * @return bool
   *   TRUE if the query should be executed.
   */
  public function isQueryEnabled(): bool;

  /**
   * Checks if the exposed form should be returned.
   *
   * @return bool
   *   TRUE if the exposed form should be returned.
   */
  public function isExposedFormReturned(): bool;

  /**
   * Checks if the view result and related elements should be returned.
   *
   * @return bool
   *   TRUE if the view result should be returned.
   */
  public function isResultReturned(): bool;

  /**
   * Returns keys of GET query parameters this wrapper handles.
   *
   * This should only include enabled parameters that are actually handled.
   *
   * @return string[]
   *   The list of query parameter names.
   */
  public function getHandledQueryParameters(): array;

}
