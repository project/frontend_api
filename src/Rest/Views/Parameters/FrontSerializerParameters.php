<?php

namespace Drupal\frontend_api\Rest\Views\Parameters;

use Drupal\views\ViewExecutable;

/**
 * Provides wrapper around the REST view parameters.
 *
 * @see \Drupal\frontend_api\Rest\Views\Parameters\FrontSerializerParametersInterface
 */
class FrontSerializerParameters implements FrontSerializerParametersInterface {

  /**
   * The query key of the returned elements parameter.
   */
  public const RETURN_KEY = 'return';

  /**
   * The exposed form key in the returned elements parameter.
   */
  public const EXPOSED_KEY = 'exposed_form';

  /**
   * The view result key in the returned elements parameter.
   */
  public const RESULT_KEY = 'result';

  /**
   * The view executable.
   *
   * @var \Drupal\views\ViewExecutable
   */
  protected $view;

  /**
   * The options that control parameters.
   *
   * For example, the Views handler could provide options to enable/disable some
   * of the parameters.
   *
   * @var array
   */
  protected $options;

  /**
   * A constructor.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view executable.
   * @param array $options
   *   The options.
   */
  public function __construct(ViewExecutable $view, array $options) {
    $this->view = $view;
    $this->options = $options;
  }

  /**
   * {@inheritdoc}
   */
  public function isQueryEnabled(): bool {
    return $this->isResultReturned();
  }

  /**
   * Returns TRUE if all the components should be returned.
   */
  protected function isEverythingReturned(): bool {
    if (empty($this->options[static::RETURN_KEY])) {
      return TRUE;
    }

    $input = $this->view->getExposedInput();
    return empty($input[static::RETURN_KEY]);
  }

  /**
   * {@inheritdoc}
   */
  public function isExposedFormReturned(): bool {
    if ($this->isEverythingReturned()) {
      return TRUE;
    }

    /** @var array $input */
    $input = $this->view->getExposedInput();
    return !empty($input[static::RETURN_KEY][static::EXPOSED_KEY]);
  }

  /**
   * {@inheritdoc}
   */
  public function isResultReturned(): bool {
    if ($this->isEverythingReturned()) {
      return TRUE;
    }

    /** @var array $input */
    $input = $this->view->getExposedInput();
    return !empty($input[static::RETURN_KEY][static::RESULT_KEY]);
  }

  /**
   * {@inheritdoc}
   */
  public function getHandledQueryParameters(): array {
    $keys = [
      static::RETURN_KEY,
    ];

    $result = [];
    foreach ($keys as $key) {
      if (!empty($this->options[$key])) {
        $result[] = $key;
      }
    }
    return $result;
  }

}
