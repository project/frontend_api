<?php

namespace Drupal\frontend_api\Rest\Views;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\views\Plugin\views\exposed_form\ExposedFormPluginInterface;
use Drupal\views\Plugin\views\ViewsHandlerInterface;

/**
 * Manager interface of the normalizer plugins for the Views exposed handlers.
 */
interface ExposedHandlerNormalizerManagerInterface extends PluginManagerInterface {

  /**
   * Creates a plugin instance for a specified exposed handler.
   *
   * @param \Drupal\views\Plugin\views\exposed_form\ExposedFormPluginInterface $exposed_form
   *   The exposed form plugin.
   * @param string $handler_type
   *   The handler type to create the plugin for.
   * @param \Drupal\views\Plugin\views\ViewsHandlerInterface $handler
   *   The handler plugin to create the plugin for.
   *
   * @return \Drupal\frontend_api\Rest\Views\ExposedHandlerNormalizerInterface
   *   The normalizer instance.
   */
  public function createInstanceByHandler(
    ExposedFormPluginInterface $exposed_form,
    string $handler_type,
    ViewsHandlerInterface $handler
  ): ExposedHandlerNormalizerInterface;

}
