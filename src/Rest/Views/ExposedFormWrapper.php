<?php

namespace Drupal\frontend_api\Rest\Views;

use Drupal\views\Plugin\views\exposed_form\ExposedFormPluginInterface;

/**
 * Wrapper for normalizer around views exposed form data.
 */
class ExposedFormWrapper {

  /**
   * Rendered form exposed form.
   *
   * @var array
   */
  public $exposedForm;

  /**
   * Exposed form views plugin.
   *
   * @var \Drupal\views\Plugin\views\exposed_form\ExposedFormPluginInterface
   */
  public $exposedFormPlugin;

  /**
   * ExposedFormWrapper constructor.
   *
   * @param array $exposedForm
   *   Exposed rendered form.
   * @param \Drupal\views\Plugin\views\exposed_form\ExposedFormPluginInterface $exposedFormPlugin
   *   Views exposed plugin.
   */
  public function __construct(array $exposedForm, ExposedFormPluginInterface $exposedFormPlugin) {
    $this->exposedForm = $exposedForm;
    $this->exposedFormPlugin = $exposedFormPlugin;
  }

}
