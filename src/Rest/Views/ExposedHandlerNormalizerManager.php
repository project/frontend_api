<?php

namespace Drupal\frontend_api\Rest\Views;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\frontend_api\Annotation\ViewsExposedHandlerNormalizer;
use Drupal\views\Plugin\views\exposed_form\ExposedFormPluginInterface;
use Drupal\views\Plugin\views\ViewsHandlerInterface;

/**
 * Provides a manager of the normalizer plugins for the Views exposed handlers.
 */
class ExposedHandlerNormalizerManager extends DefaultPluginManager implements ExposedHandlerNormalizerManagerInterface {

  /**
   * The plugin sub-folder in the module.
   */
  public const SUBDIR = 'Plugin/frontend_api/Views/ExposedHandlerNormalizer';

  /**
   * The plugin annotation class name.
   */
  public const PLUGIN_ANNOTATION_NAME = ViewsExposedHandlerNormalizer::class;

  /**
   * The plugin interface.
   */
  public const PLUGIN_INTERFACE = ExposedHandlerNormalizerInterface::class;

  /**
   * The cache key prefix.
   */
  public const CACHE_KEY_PREFIX = 'frontend_api_views_exp_handler_nzr';

  /**
   * The wildcard of the exposed form plugin ID.
   *
   * It means all the exposed forms are supported by the plugin.
   */
  public const EXPOSED_FORM_WILDCARD = '*';

  /**
   * A constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      static::SUBDIR,
      $namespaces,
      $module_handler,
      static::PLUGIN_INTERFACE,
      static::PLUGIN_ANNOTATION_NAME
    );

    $this->setCacheBackend($cache_backend, static::CACHE_KEY_PREFIX);
  }

  /**
   * {@inheritdoc}
   */
  public function createInstanceByHandler(
    ExposedFormPluginInterface $exposed_form,
    string $handler_type,
    ViewsHandlerInterface $handler
  ): ExposedHandlerNormalizerInterface {
    $exposed_form_ids = [
      $exposed_form->getPluginId(),
      static::EXPOSED_FORM_WILDCARD,
    ];
    $handler_ids = [
      $handler->getPluginId(),
    ];

    $filter = function (
      array $definition
    ) use (
      $exposed_form_ids,
      $handler_type,
      $handler_ids
    ): bool {
      if ($definition['handler_type'] !== $handler_type) {
        return FALSE;
      }

      // Make sure it matches by exposed form ID.
      $matches = array_intersect($exposed_form_ids, $definition['exposed_forms']);
      if (empty($matches)) {
        return FALSE;
      }

      // Make sure it matches by handler plugin ID.
      $matches = array_intersect($handler_ids, $definition['handlers']);
      if (empty($matches)) {
        return FALSE;
      }

      return TRUE;
    };
    $sort = function (array $a, array $b): int {
      return $b['priority'] - $a['priority'];
    };

    // @TODO Definitions could be indexed and cached for better performance.
    $matched_definitions = array_filter($this->getDefinitions(), $filter);
    uasort($matched_definitions, $sort);

    if (empty($matched_definitions)) {
      // This should be a rare case, since we have a default plugin.
      throw new PluginException(
        sprintf(
          'Unable to find plugin for %s %s handler of %s exposed form.',
          $handler->getPluginId(),
          $handler_type,
          $exposed_form->getPluginId()
        )
      );
    }

    /** @var \Drupal\frontend_api\Rest\Views\ExposedHandlerNormalizerInterface $plugin */
    $plugin_id = array_keys($matched_definitions)[0];
    $plugin = $this->createInstance($plugin_id);
    return $plugin;
  }

}
