<?php

namespace Drupal\frontend_api\Rest;

/**
 * Interface for a wrapper class for an un-serialized data.
 *
 * This class exists, because Drupal argument resolver isn't able to resolve
 * an array argument provided by the REST request handler. It has to fallback to
 * a legacy argument resolving with a deprecation warning. The argument has to
 * be an object to be properly resolved.
 */
interface UnserializedDataInterface {

  /**
   * Returns the un-serialized data.
   *
   * @return mixed
   *   The un-serialized data.
   */
  public function getData();

}
